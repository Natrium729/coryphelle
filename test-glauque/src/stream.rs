use glauque::{FileUsageMode, OpaqueId};

use crate::{OpaqueCollection, WindowType};

pub(crate) enum Stream {
    Window(Window),
    Memory(Memory),
    File(File),
}

impl Stream {
    pub(crate) fn as_window(&self) -> Option<&Window> {
        if let Self::Window(win_stream) = self {
            Some(win_stream)
        } else {
            None
        }
    }

    pub(crate) fn as_window_mut(&mut self) -> Option<&mut Window> {
        if let Self::Window(win_stream) = self {
            Some(win_stream)
        } else {
            None
        }
    }
}

pub(crate) struct Window {
    pub(crate) write_count: u32,
    pub(crate) read_count: u32,
    pub(crate) owner: OpaqueId,
    // TODO: Since the contents are only useful for buffer windows,
    // we could make a separate variant, `Type::BufferWindow`,
    //containing a string, and no string here.
    pub(crate) contents: String,
    pub(crate) pending: String,
}

impl Window {
    pub(crate) fn put_slice(
        &mut self,
        win_collection: &OpaqueCollection<crate::Window>,
        bytes: &[u8],
    ) {
        // Window streams always have an owner, so we can unwrap.
        let window = win_collection.get(&self.owner).unwrap();
        match window.ty {
            WindowType::AllTypes => unreachable!("a window cannot have a type of `AllTypes`"),
            WindowType::Pair => todo!(),
            WindowType::Blank => (),
            WindowType::TextBuffer => {
                self.pending.extend(bytes.iter().map(|&byte| byte as char));
            }
            WindowType::TextGrid => (), // TODO
            WindowType::Graphics => todo!(),
        }
        self.write_count += u32::try_from(bytes.len()).unwrap();
    }

    pub(crate) fn put_slice_uni(
        &mut self,
        win_collection: &OpaqueCollection<crate::Window>,
        chars: &[u32],
    ) {
        // Window streams always have an owner, so we can unwrap.
        let window = win_collection.get(&self.owner).unwrap();
        match window.ty {
            WindowType::AllTypes => unreachable!("a window cannot have a type of `AllTypes`"),
            WindowType::Pair => todo!(),
            WindowType::Blank => (),
            WindowType::TextBuffer => {
                self.pending.extend(
                    chars
                        .iter()
                        .map(|&ch| char::from_u32(ch).unwrap_or(char::REPLACEMENT_CHARACTER)),
                );
            }
            WindowType::TextGrid => todo!(),
            WindowType::Graphics => todo!(),
        }
        self.write_count += u32::try_from(chars.len()).unwrap();
    }

    pub(crate) fn put_chars(
        &mut self,
        win_collection: &OpaqueCollection<crate::Window>,
        bytes: impl IntoIterator<Item = u8>,
    ) {
        // Window streams always have an owner, so we can unwrap.
        let window = win_collection.get(&self.owner).unwrap();
        match window.ty {
            WindowType::AllTypes => unreachable!("a window cannot have a type of `AllTypes`"),
            WindowType::Pair => todo!(),
            WindowType::Blank => (),
            WindowType::TextBuffer => {
                for ch in bytes.into_iter().map(|byte| byte as char) {
                    self.pending.push(ch);
                    self.write_count += 1;
                }
            }
            WindowType::TextGrid => (), // TODO
            WindowType::Graphics => todo!(),
        }
    }

    pub(crate) fn put_chars_uni(
        &mut self,
        win_collection: &OpaqueCollection<crate::Window>,
        chars: impl IntoIterator<Item = u32>,
    ) {
        // Window streams always have an owner, so we can unwrap.
        let window = win_collection.get(&self.owner).unwrap();
        match window.ty {
            WindowType::AllTypes => unreachable!("a window cannot have a type of `AllTypes`"),
            WindowType::Pair => todo!(),
            WindowType::Blank => (),
            WindowType::TextBuffer => {
                for ch in chars
                    .into_iter()
                    .map(|ch| char::from_u32(ch).unwrap_or(char::REPLACEMENT_CHARACTER))
                {
                    self.pending.push(ch);
                    self.write_count += 1;
                }
            }
            WindowType::TextGrid => (), // TODO
            WindowType::Graphics => todo!(),
        }
    }
}

impl From<Window> for Stream {
    fn from(value: Window) -> Self {
        Self::Window(value)
    }
}

pub(crate) struct Memory {
    pub(crate) write_count: u32,
    pub(crate) read_count: u32,
    pub(crate) start: u32,
    pub(crate) len: u32,
    pub(crate) encoding: MemoryStreamType,
}

impl Memory {
    pub(crate) fn put_slice(&mut self, mem: &mut impl glauque::Memory, bytes: &[u8]) {
        let write_count: usize = self.write_count.try_into().unwrap();
        let start: usize = self.start.try_into().unwrap();
        let len: usize = self.len.try_into().unwrap();
        let first = start + write_count;
        let left = len.saturating_sub(write_count);
        match &mut self.encoding {
            MemoryStreamType::Latin1 => {
                mem.write_u8s(first.try_into().unwrap(), bytes.iter().copied().take(left));
            }
            MemoryStreamType::Unicode(buffer) => {
                buffer.extend(bytes.iter().take(left).map(|&byte| byte as u32));
            }
        }
        self.write_count += u32::try_from(bytes.len()).unwrap();
    }

    pub(crate) fn put_slice_uni(&mut self, mem: &mut impl glauque::Memory, chars: &[u32]) {
        let write_count: usize = self.write_count.try_into().unwrap();
        let start: usize = self.start.try_into().unwrap();
        let len: usize = self.len.try_into().unwrap();
        let first = start + write_count;
        let left = len.saturating_sub(write_count);
        if left > 0 {
            match &mut self.encoding {
                MemoryStreamType::Latin1 => {
                    let chars = chars
                        .iter()
                        .take(left)
                        .map(|&ch| u8::try_from(ch).unwrap_or(b'?'));
                    mem.write_u8s(first.try_into().unwrap(), chars);
                }
                MemoryStreamType::Unicode(buffer) => {
                    buffer.extend(chars.iter().take(left));
                }
            }
        }
        self.write_count += u32::try_from(chars.len()).unwrap();
    }

    pub(crate) fn put_chars(
        &mut self,
        mem: &mut impl crate::Memory,
        bytes: impl IntoIterator<Item = u8>,
    ) {
        let write_count: usize = self.write_count.try_into().unwrap();
        let start: usize = self.start.try_into().unwrap();
        let len: usize = self.len.try_into().unwrap();
        let first = start + write_count;
        let left = len.saturating_sub(write_count);
        let mut bytes = bytes.into_iter();
        match &mut self.encoding {
            MemoryStreamType::Latin1 => {
                for i in first..first + left {
                    let Some(byte) = bytes.next() else {
                        break;
                    };
                    mem.write_u8(i.try_into().unwrap(), byte);
                    self.write_count += 1;
                }
            }
            MemoryStreamType::Unicode(buffer) => {
                for _ in 0..left {
                    let Some(byte) = bytes.next() else {
                        break;
                    };
                    buffer.push(byte as u32);
                    self.write_count += 1;
                }
            }
        }
        // Count the rest, which did not fit in the stream.
        let rest: u32 = bytes.count().try_into().unwrap();
        self.write_count += rest;
    }

    pub(crate) fn put_chars_uni(
        &mut self,
        mem: &mut impl crate::Memory,
        chars: impl IntoIterator<Item = u32>,
    ) {
        let write_count: usize = self.write_count.try_into().unwrap();
        let start: usize = self.start.try_into().unwrap();
        let len: usize = self.len.try_into().unwrap();
        let first = start + write_count;
        let left = len.saturating_sub(write_count);
        let mut chars = chars.into_iter();
        match &mut self.encoding {
            MemoryStreamType::Latin1 => {
                for i in first..first + left {
                    let Some(ch) = chars.next() else {
                        break;
                    };
                    mem.write_u8(i.try_into().unwrap(), ch.try_into().unwrap_or(b'?'));
                    self.write_count += 1;
                }
            }
            MemoryStreamType::Unicode(buffer) => {
                for _ in 0..left {
                    let Some(ch) = chars.next() else {
                        break;
                    };
                    buffer.push(ch);
                    self.write_count += 1;
                }
            }
        }
        // Count the rest, which did not fit in the stream.
        let rest: u32 = chars.count().try_into().unwrap();
        self.write_count += rest;
    }

    pub(crate) fn get_to_end(&mut self, mem: &impl crate::Memory, buffer: &mut impl Extend<u8>) {
        buffer.extend(mem.read_u8s(self.read_count, self.len).into_iter().copied());
        self.read_count += self.len - self.read_count;
    }
}

impl From<Memory> for Stream {
    fn from(value: Memory) -> Self {
        Self::Memory(value)
    }
}

pub(crate) enum MemoryStreamType {
    Latin1,
    Unicode(Vec<u32>),
}

pub(crate) struct File {
    pub(crate) write_count: u32,
    pub(crate) read_count: u32,
    pub(crate) fileref: OpaqueId,
    pub(crate) is_uni: bool,
}

impl File {
    pub(crate) fn put_slice(
        &mut self,
        fileref_collection: &OpaqueCollection<crate::Fileref>,
        glk_owner: &str,
        bytes: &[u8],
    ) {
        let fileref = fileref_collection
            .get(&self.fileref)
            .expect("TODO: check if None can happen and handle it if so");
        let mut files = crate::get_files().lock().unwrap();
        let file = files
            .get_mut(glk_owner, fileref)
            .expect("TODO: check if None can happen and handle it if so");
        match fileref.mode {
            FileUsageMode::Text => {
                for &byte in bytes {
                    // Two bytes is enough to encode
                    // a Latin-1 character into UTF-8.
                    let mut buffer = [0; 2];
                    let encoded = (byte as char).encode_utf8(&mut buffer);
                    file.0.extend_from_slice(encoded.as_bytes())
                }
            }
            FileUsageMode::Binary => {
                if self.is_uni {
                    file.0.extend(
                        bytes
                            .into_iter()
                            .flat_map(|&byte| (byte as u32).to_be_bytes()),
                    );
                } else {
                    file.0.extend_from_slice(bytes);
                }
            }
        }
        self.write_count += u32::try_from(bytes.len()).unwrap();
    }

    pub(crate) fn put_slice_uni(
        &mut self,
        fileref_collection: &OpaqueCollection<crate::Fileref>,
        glk_owner: &str,
        chars: &[u32],
    ) {
        let fileref = fileref_collection
            .get(&self.fileref)
            .expect("TODO: check if None can happen and handle it if so");
        let mut files = crate::get_files().lock().unwrap();
        let file = files
            .get_mut(glk_owner, fileref)
            .expect("TODO: check if None can happen and handle it if so");
        match fileref.mode {
            FileUsageMode::Text => {
                for &ch in chars {
                    let mut buffer = [0; 4];
                    let encoded = char::from_u32(ch)
                        .unwrap_or(char::REPLACEMENT_CHARACTER)
                        .encode_utf8(&mut buffer);
                    file.0.extend_from_slice(encoded.as_bytes())
                }
            }
            FileUsageMode::Binary => {
                if self.is_uni {
                    file.0
                        .extend(chars.into_iter().flat_map(|&ch| ch.to_be_bytes()));
                } else {
                    file.0
                        .extend(chars.into_iter().map(|&ch| ch.try_into().unwrap_or(b'?')));
                }
            }
        }
        self.write_count += u32::try_from(chars.len()).unwrap();
    }

    pub(crate) fn put_chars(
        &mut self,
        fileref_collection: &OpaqueCollection<crate::Fileref>,
        glk_owner: &str,
        bytes: impl IntoIterator<Item = u8>,
    ) {
        let fileref = fileref_collection
            .get(&self.fileref)
            .expect("TODO: check if None can happen and handle it if so");
        let mut files = crate::get_files().lock().unwrap();
        let file = files
            .get_mut(glk_owner, fileref)
            .expect("TODO: check if None can happen and handle it if so");
        match fileref.mode {
            FileUsageMode::Text => {
                for byte in bytes.into_iter() {
                    // Two bytes is enough to encode
                    // a Latin-1 character into UTF-8.
                    let mut buffer = [0; 2];
                    let encoded = (byte as char).encode_utf8(&mut buffer);
                    file.0.extend_from_slice(encoded.as_bytes());
                    self.write_count += 1;
                }
            }
            FileUsageMode::Binary => {
                if self.is_uni {
                    for byte in bytes.into_iter() {
                        file.0.extend((byte as u32).to_be_bytes());
                        self.write_count += 1;
                    }
                } else {
                    for byte in bytes.into_iter() {
                        file.0.push(byte);
                        self.write_count += 1;
                    }
                }
            }
        }
    }

    pub(crate) fn put_chars_uni(
        &mut self,
        fileref_collection: &OpaqueCollection<crate::Fileref>,
        glk_owner: &str,
        chars: impl IntoIterator<Item = u32>,
    ) {
        let fileref = fileref_collection
            .get(&self.fileref)
            .expect("TODO: check if None can happen and handle it if so");
        let mut files = crate::get_files().lock().unwrap();
        let file = files
            .get_mut(glk_owner, fileref)
            .expect("TODO: check if None can happen and handle it if so");
        match fileref.mode {
            FileUsageMode::Text => {
                for ch in chars.into_iter() {
                    let mut buffer = [0; 4];
                    let encoded = char::from_u32(ch)
                        .unwrap_or(char::REPLACEMENT_CHARACTER)
                        .encode_utf8(&mut buffer);
                    file.0.extend_from_slice(encoded.as_bytes());
                    self.write_count += 1;
                }
            }
            FileUsageMode::Binary => {
                if self.is_uni {
                    for ch in chars.into_iter() {
                        file.0.extend(ch.to_be_bytes());
                        self.write_count += 1;
                    }
                } else {
                    for ch in chars.into_iter() {
                        file.0.push(ch.try_into().unwrap_or(b'?'));
                        self.write_count += 1;
                    }
                }
            }
        }
    }

    pub(crate) fn get_to_end(
        &mut self,
        fileref_collection: &OpaqueCollection<crate::Fileref>,
        glk_owner: &str,
        buffer: &mut impl Extend<u8>,
    ) {
        let fileref = fileref_collection
            .get(&self.fileref)
            .expect("TODO: check if None can happen and handle it if so");
        let mut files = crate::get_files().lock().unwrap();
        let file = files
            .get_mut(glk_owner, fileref)
            .expect("TODO: check if None can happen and handle it if so");

        match fileref.mode {
            FileUsageMode::Text => {
                String::from_utf8_lossy(&file.0)
                    .chars()
                    .skip(self.read_count.try_into().unwrap())
                    .map(|ch| if ch as u32 > 255 { b'?' } else { ch as u8 })
                    .for_each(|byte| {
                        buffer.extend(std::iter::once(byte));
                        self.read_count += 1;
                    });
            }
            FileUsageMode::Binary => {
                if self.is_uni {
                    file.0
                        .chunks_exact(4)
                        .skip(self.read_count.try_into().unwrap())
                        .map(|slice| {
                            u32::from_be_bytes(slice.try_into().unwrap())
                                .try_into()
                                .unwrap_or(b'?')
                        })
                        .for_each(|byte| {
                            buffer.extend(std::iter::once(byte));
                            self.read_count += 1;
                        })
                } else {
                    let read_count: usize = self.read_count.try_into().unwrap();
                    buffer.extend(file.0[read_count..].iter().copied());
                    self.read_count += u32::try_from(file.0.len()).unwrap() - self.read_count;
                }
            }
        }
    }
}

impl From<File> for Stream {
    fn from(value: File) -> Self {
        Self::File(value)
    }
}
