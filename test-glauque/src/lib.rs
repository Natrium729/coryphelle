use std::{
    collections::HashMap,
    path::PathBuf,
    sync::{Mutex, OnceLock},
};

use glauque::{
    Color, Date, Event, EventType, FileMode, FileUsage, FileUsageMode, FileUsageType,
    GestaltCharOutput, Glauque, Key, Memory, OpaqueId, OpaqueIdFactory, SeekMode, StreamResult,
    Style, Stylehint, Timeval, WindowMethod, WindowType,
};

mod stream;

use stream::{MemoryStreamType, Stream};

struct OpaqueCollectionEntry<T> {
    content: T,
    rock: u32,
    next: Option<OpaqueId>,
    prev: Option<OpaqueId>,
}

struct OpaqueCollection<T> {
    map: HashMap<OpaqueId, OpaqueCollectionEntry<T>>,
    first_id: Option<OpaqueId>,
}

impl<T> OpaqueCollection<T> {
    fn new() -> Self {
        Self {
            map: HashMap::new(),
            first_id: None,
        }
    }

    fn contains_key(&self, id: &OpaqueId) -> bool {
        self.map.contains_key(id)
    }

    fn get(&self, id: &OpaqueId) -> Option<&T> {
        self.map.get(id).map(|entry| &entry.content)
    }

    fn get_mut(&mut self, id: &OpaqueId) -> Option<&mut T> {
        self.map.get_mut(id).map(|entry| &mut entry.content)
    }

    fn get_rock(&self, id: &OpaqueId) -> Option<u32> {
        self.map.get(&id).map(|entry| entry.rock)
    }

    fn insert(&mut self, id: OpaqueId, obj: T, rock: u32) {
        if let Some(first_id) = self.first_id {
            // The key `first_id` will be present in the collection
            // if we maintained it correctly, so we can unwrap.
            let first = self.map.get_mut(&first_id).unwrap();
            first.prev = Some(id);
        }

        let entry = OpaqueCollectionEntry {
            content: obj,
            rock,
            next: self.first_id,
            prev: None,
        };

        self.first_id = Some(id);
        self.map.insert(id, entry);
    }

    fn remove(&mut self, removed_id: &OpaqueId) {
        let removed = self
            .map
            .remove(&removed_id)
            .expect("TODO: handle removing non-existent opaque object");
        let next_id = removed.next;
        let prev_id = removed.prev;
        // If there's a next/previous ID,
        // then the next/previous object must be in the collection,
        // since we normally maintain the linked list correctly.
        let next = next_id.map(|id| self.map.get_mut(&id).unwrap());
        if let Some(next) = next {
            next.prev = prev_id;
        }

        let prev = prev_id.map(|id| self.map.get_mut(&id).unwrap());
        if let Some(prev) = prev {
            prev.next = next_id;
        } else {
            self.first_id = next_id;
        }
    }

    // TODO: rename because it could be confused with `Iterator`'s `next`?
    fn next(&self, id: Option<OpaqueId>) -> Option<(OpaqueId, u32)> {
        let next_id = match id {
            None => self.first_id,
            Some(id) => self.map.get(&id).expect("TODO: handle None").next,
        }?;
        // If we maintained the collection correctly,
        // `next_id` will be present so we can unwrap.
        let next_rock = self.map.get(&next_id).unwrap().rock;
        Some((next_id, next_rock))
    }

    // TODO: Instead of relying on the iterator of the inner hashmap's,
    // we could iterate by going through the linked list using the `next` method.
    // It could be faster because we wouldn't visit the hashmap's empty buckets,
    // But it could be slower because it's less cache friendly.
    fn values_mut(&mut self) -> impl Iterator<Item = &mut OpaqueCollectionEntry<T>> {
        self.map.values_mut()
    }
}

struct Window {
    ty: WindowType,
    parent: Option<OpaqueId>,
    stream: OpaqueId,
    echo_stream: Option<OpaqueId>,
}

struct Fileref {
    ty: FileUsageType,
    mode: FileUsageMode,
    path: PathBuf,
}

impl Fileref {
    fn is_global(&self) -> bool {
        matches!(self.ty, FileUsageType::Data)
    }
}

fn file_type_extension(ty: FileUsageType) -> &'static str {
    match ty {
        FileUsageType::Data => "glkdata",
        FileUsageType::SavedGame => "glksave",
        FileUsageType::Transcript => "txt",
        FileUsageType::InputRecord => "txt",
    }
}

// With this Glauque implementation, files will live in memory, not on disk.
// (and so will be lost when the process exits, but it'll do.)
//
// This struct represents a file.
struct File(Vec<u8>);

impl Default for File {
    fn default() -> Self {
        Self(Vec::new())
    }
}

struct FileCollection {
    /// The files that can be accessed by all stories (i.e. the data files).
    common: HashMap<PathBuf, File>,

    /// The files that are specific to each story (e.g. save files).
    ///
    /// It maps story names to maps of paths to files.
    individual: HashMap<String, HashMap<PathBuf, File>>,
}

impl FileCollection {
    fn new() -> Self {
        Self {
            common: HashMap::new(),
            individual: HashMap::new(),
        }
    }

    fn get_dir(&mut self, owner: &str, fileref: &Fileref) -> &HashMap<PathBuf, File> {
        if fileref.is_global() {
            &self.common
        } else {
            self.individual.entry(owner.into()).or_default()
        }
    }

    fn get_dir_mut(&mut self, owner: &str, fileref: &Fileref) -> &mut HashMap<PathBuf, File> {
        if fileref.is_global() {
            &mut self.common
        } else {
            self.individual.entry(owner.into()).or_default()
        }
    }

    fn does_file_exist(&mut self, owner: &str, fileref: &Fileref) -> bool {
        self.get_dir(owner, fileref).contains_key(&fileref.path)
    }

    /// Adds an empty file to the collection if it doesn't exist.
    ///
    /// Does nothing if it already exists.
    ///
    /// A mutable reference to the newly created file
    /// (or the one that already existed)
    /// is returned.
    fn create(&mut self, owner: &str, fileref: &Fileref) -> &mut File {
        let dir = self.get_dir_mut(owner, &fileref);
        dir.entry(fileref.path.clone()).or_default()
    }

    fn get_mut(&mut self, owner: &str, fileref: &Fileref) -> Option<&mut File> {
        let dir = self.get_dir_mut(owner, fileref);
        dir.get_mut(&fileref.path)
    }

    fn delete(&mut self, owner: &str, fileref: &Fileref) {
        let dir = self.get_dir_mut(owner, fileref);
        dir.remove(&fileref.path)
            .expect("TODO: handle deleting non existent file");
    }
}

// TODO: Use `LazyLock` once it's stabilised.
static FILES: OnceLock<Mutex<FileCollection>> = OnceLock::new();

fn get_files() -> &'static Mutex<FileCollection> {
    FILES.get_or_init(|| Mutex::new(FileCollection::new()))
}

struct Schannel {}

#[derive(Debug)]
enum PendingInput {
    Char,
    Line {
        is_unicode: bool,
        addr: u32,
        max_len: u32,
    },
}

pub struct TestGlauque {
    owner: String,

    opaque_id_factory: OpaqueIdFactory,

    pending_inputs: HashMap<OpaqueId, PendingInput>,

    streams: OpaqueCollection<Stream>,
    current_stream: Option<OpaqueId>,

    windows: OpaqueCollection<Window>,
    root_window: Option<OpaqueId>,
    main_window: Option<OpaqueId>,

    filerefs: OpaqueCollection<Fileref>,
    next_fileref_id: u32,

    schannels: OpaqueCollection<Schannel>,

    output: String,
}

impl TestGlauque {
    // TODO: Re-enable warning (or implement `Default`)
    // when the API will be more stable.
    #[allow(clippy::new_without_default)]
    pub fn new(owner: String) -> Self {
        Self {
            owner,

            opaque_id_factory: OpaqueIdFactory::new(),
            pending_inputs: HashMap::new(),

            streams: OpaqueCollection::new(),
            current_stream: None,

            windows: OpaqueCollection::new(),
            root_window: None,
            main_window: None,

            filerefs: OpaqueCollection::new(),
            next_fileref_id: 0,

            schannels: OpaqueCollection::new(),

            output: String::new(),
        }
    }

    fn flush_output(&mut self) {
        self.output = match self.main_window {
            None => "".into(),
            Some(win_id) => {
                let win = self.windows.get(&win_id).unwrap();
                // There's always a stream associated with the window.
                let stream = self.streams.get(&win.stream).unwrap().as_window().unwrap();
                stream.pending.clone()
            }
        }
    }

    // A helper to generate a line or char event for the main window from a string.
    pub fn event_from_str(&self, input: &str) -> Option<Event> {
        let main_window = self.main_window?;
        let ty = match *self.pending_inputs.get(&main_window)? {
            PendingInput::Char => {
                let mut ch = input.chars().next().unwrap_or(' ') as u32;
                if ch > 255 {
                    ch = '?' as u32
                }
                let ch = ch;
                EventType::CharInput(Key::Code(ch))
            }
            PendingInput::Line { max_len, .. } => {
                // Ideally, we would check if the conversion of `max_len` into an `usize`
                // doesn't return an error, but:
                //
                // - The input would have to be unusually long,
                // even for architectures smaller that 32 bits.
                // - Glauque will likely be run only on 32-bit architecture.
                //
                // So everything will be fine. (Famous last words, I know.)
                EventType::LineInput {
                    command: input.chars().take(max_len.try_into().unwrap()).collect(),
                    terminator: None,
                }
            }
        };

        Some(Event {
            ty,
            win: Some(main_window),
        })
    }
}

impl Glauque for TestGlauque {
    // TODO: Right now we only give the contents of the main window.
    // We will give the contents of every window in the future.
    type Output = String;

    fn output(&mut self) -> Self::Output {
        let cap = self.output.capacity();
        std::mem::replace(&mut self.output, String::with_capacity(cap))
    }

    fn warn(&mut self, msg: String) {
        eprint!("[Glk warning: {msg}]");
    }

    fn error(&mut self, msg: String) {
        eprint!("[Glk error: {msg}]");
        // TODO: quit.
    }

    // 0x0001
    fn exit(&mut self) {
        self.flush_output();
    }

    fn gestalt_char_input(&self, _ch: Key) -> bool {
        // TODO
        true
    }

    fn gestalt_char_output(&self, ch: u32) -> (GestaltCharOutput, u32) {
        match ch {
            // Unprintable 8-bit character.
            0..=9 | 11..=31 | 127..=159 => (GestaltCharOutput::CannotPrint, 0),
            // Invalid Unicode codepoint are replaced by placeholder.
            n if char::from_u32(n).is_none() => (GestaltCharOutput::CannotPrint, 1),
            _ => (GestaltCharOutput::ExactPrint, 1),
        }
    }

    fn gestalt_mouse_input(&self, _window_type: WindowType) -> bool {
        false
    }

    fn gestalt_timer(&self) -> bool {
        false
    }

    fn gestalt_graphics(&self) -> bool {
        // TODO: add an option to "support" graphics for testing purpose.
        false
    }

    fn gestalt_draw_image(&self, _window_type: WindowType) -> bool {
        false
    }

    fn gestalt_sound(&self) -> bool {
        // TODO: add an option to "support" sounds for testing purpose.
        false
    }

    fn gestalt_sound_volume(&self) -> bool {
        false
    }

    fn gestalt_sound_notify(&self) -> bool {
        false
    }

    fn gestalt_hyperlinks(&self) -> bool {
        // TODO: add an option to "support" hyperlinks for testing purpose.
        false
    }

    fn gestalt_hyperlink_input(&self, _window_type: WindowType) -> bool {
        false
    }

    fn gestalt_sound_music(&self) -> bool {
        false
    }

    fn gestalt_graphics_transparency(&self) -> bool {
        false
    }

    fn gestalt_line_input_echo(&self) -> bool {
        // TODO: add an option to "support" line input echoing for testing purpose.
        false
    }

    fn gestalt_line_terminators(&self) -> bool {
        false
    }

    fn gestalt_line_terminator_key(&self, _ch: Key) -> bool {
        false
    }

    fn gestalt_sound2(&self) -> bool {
        false
    }

    fn gestalt_resource_stream(&self) -> bool {
        false
    }

    fn gestalt_graphics_char_input(&self) -> bool {
        false
    }

    // 0x0020
    fn window_iterate(&self, id: Option<OpaqueId>) -> Option<(OpaqueId, u32)> {
        self.windows.next(id)
    }

    // 0x0021
    fn window_get_rock(&self, win: OpaqueId) -> u32 {
        self.windows.get_rock(&win).expect("TODO handle invalid ID")
    }

    // 0x0022
    fn window_get_root(&self) -> Option<OpaqueId> {
        self.root_window
    }

    // 0x0023
    fn window_open(
        &mut self,
        split: Option<OpaqueId>,
        _method: WindowMethod,
        _size: u32,
        ty: WindowType,
        rock: u32,
    ) -> Option<OpaqueId> {
        let window_id = self.opaque_id_factory.get().unwrap();

        let parent = match split {
            None => {
                if self.root_window.is_some() {
                    self.warn("can't open a new window without splitting an existing one".into());
                    return None;
                }
                self.root_window = Some(window_id);
                None
            }
            Some(sibling_id) => {
                // That's for the new parent, a pair window.
                let parent_id = self.opaque_id_factory.get().unwrap();
                let parent_stream_id = self.opaque_id_factory.get().unwrap();

                // The parent  of the split becomes
                // the grandparent of itself and the new window
                // (because of the new pair window).
                let sibling = self
                    .windows
                    .get_mut(&sibling_id)
                    .expect("TODO: handle non-existent split");
                let grandparent_id = sibling.parent;
                sibling.parent = Some(parent_id);

                // And now we create the parent.
                let parent_stream = stream::Window {
                    write_count: 0,
                    read_count: 0,
                    owner: parent_id,
                    contents: String::new(),
                    pending: String::new(),
                };
                self.streams
                    .insert(parent_stream_id, parent_stream.into(), 0);
                let parent = Window {
                    ty: WindowType::Pair,
                    parent: grandparent_id,
                    stream: parent_stream_id,
                    echo_stream: None,
                };
                self.windows.insert(parent_id, parent, 0);

                Some(parent_id)
            }
        };

        let stream_id = self.opaque_id_factory.get().unwrap();

        let stream = stream::Window {
            write_count: 0,
            read_count: 0,
            owner: window_id,
            contents: String::new(),
            pending: String::new(),
        };
        self.streams.insert(stream_id, stream.into(), rock);

        let window = Window {
            ty,
            parent,
            stream: stream_id,
            echo_stream: None,
        };

        if self.main_window.is_none() && ty == WindowType::TextBuffer {
            self.main_window = Some(window_id);
        }
        self.windows.insert(window_id, window, rock);

        Some(window_id)
    }

    // 0x0024
    fn window_close(&mut self, _win: OpaqueId) -> StreamResult {
        todo!()
    }

    // 0x0025
    fn window_get_size(&self, _win: OpaqueId) -> (u32, u32) {
        // TODO: We simply return 0 for now.
        (0, 0)
    }

    // 0x0026
    fn window_set_arrangement(
        &mut self,
        _win: OpaqueId,
        _method: WindowMethod,
        _size: u32,
        _key: Option<OpaqueId>,
    ) {
        // TODO: at least check if the OpaqueId correspond to existing windows.
        // And also track the positions and so on to check if the call is legal,
        // even if there's nothing to do UI-wise (since there are no real UI).
    }

    // 0x0027
    fn window_get_arrangement(&self, _win: OpaqueId) -> (WindowMethod, u32, Option<OpaqueId>) {
        todo!()
    }

    // 0x0028
    fn window_get_type(&self, win: OpaqueId) -> WindowType {
        self.windows
            .get(&win)
            .expect("TODO handle invalid window")
            .ty
    }

    // 0x0029
    fn window_get_parent(&self, win: OpaqueId) -> Option<OpaqueId> {
        self.windows
            .get(&win)
            .expect("TODO handle invalid window")
            .parent
    }

    // 0x002A
    fn window_clear(&mut self, win: OpaqueId) {
        let win = self
            .windows
            .get(&win)
            .expect("TODO: handle inexistant window.");
        match win.ty {
            WindowType::TextBuffer => todo!("clearing buffer window"),
            WindowType::TextGrid => (), // TODO,
            WindowType::Graphics => todo!("clearing graphics window"),
            _ => (), // Do nothing.
        }
    }

    // 0x002B
    fn window_move_cursor(&mut self, win: OpaqueId, _x: u32, _y: u32) {
        let Some(win) = self.windows.get(&win) else {
            // TODO handle non existent window.
            return;
        };
        if win.ty != WindowType::TextGrid {
            self.warn("cannot move cursor of a non-grid window".into());
            // TODO: it's not a needless return,
            // it's just that we didn't implement what come after the condition.
            // Remove the `allow` when we'll do it.
            #[allow(clippy::needless_return)]
            return;
        }

        // TODO: Implement it for the sake of the example.
    }

    // 0x002C
    fn window_get_stream(&self, win: OpaqueId) -> OpaqueId {
        let win = self
            .windows
            .get(&win)
            .expect("TODO: handle non-existent window");
        win.stream
    }

    // 0x002D
    fn window_set_echo_stream(&mut self, win: OpaqueId, stream: Option<OpaqueId>) {
        let win = self
            .windows
            .get_mut(&win)
            .expect("TODO handle invalid window");
        win.echo_stream = stream;
    }

    // 0x002E
    fn window_get_echo_stream(&self, win: OpaqueId) -> Option<OpaqueId> {
        self.windows
            .get(&win)
            .expect("TODO handle invalid window")
            .echo_stream
    }

    // 0x0030

    fn window_get_sibling(&self, win: OpaqueId) -> Option<OpaqueId> {
        if Some(win) == self.root_window {
            return None;
        }

        todo!()
    }

    // 0x0040
    fn stream_iterate(&self, id: Option<OpaqueId>) -> Option<(OpaqueId, u32)> {
        self.streams.next(id)
    }

    // 0x0041
    fn stream_get_rock(&self, stream: OpaqueId) -> u32 {
        self.streams
            .get_rock(&stream)
            .expect("TODO handle invalid stream")
    }

    // 0x0042
    fn stream_open_file(
        &mut self,
        fileref: OpaqueId,
        mode: FileMode,
        rock: u32,
    ) -> Option<OpaqueId> {
        let fileref_id = fileref;
        let fileref = self
            .filerefs
            .get(&fileref)
            .expect("TODO: handle non-existant fileref");
        {
            let mut files = get_files().lock().unwrap();
            match mode {
                FileMode::Write => {
                    files.create(&self.owner, fileref).0.clear();
                }
                FileMode::Read => {
                    if !files.does_file_exist(&self.owner, fileref) {
                        return None;
                    }
                }
                FileMode::ReadWrite => {
                    files.create(&self.owner, fileref);
                }
                FileMode::WriteAppend => {
                    files.create(&self.owner, fileref);
                    todo!("set mark to end of file");
                }
            }
        }

        let stream = stream::File {
            write_count: 0,
            read_count: 0,
            fileref: fileref_id,
            is_uni: false,
        };
        let id = self.opaque_id_factory.get().unwrap();
        self.streams.insert(id, stream.into(), rock);
        Some(id)
    }

    // 0x0043
    fn stream_open_memory(
        &mut self,
        _mem: &impl Memory,
        start: u32,
        len: u32,
        mode: FileMode,
        rock: u32,
    ) -> OpaqueId {
        if !matches!(mode, FileMode::Read | FileMode::Write | FileMode::ReadWrite) {
            todo!("handle invalid `FileMode` in `stream_open_memory_uni`")
        }
        let id = self.opaque_id_factory.get().unwrap();
        let stream = stream::Memory {
            write_count: 0,
            read_count: 0,
            start,
            len,
            encoding: MemoryStreamType::Latin1,
        };
        self.streams.insert(id, stream.into(), rock);
        id
    }

    // 0x0044
    fn stream_close(&mut self, mem: &mut impl Memory, stream: OpaqueId) -> StreamResult {
        // TODO: error if it's a window stream.
        //
        // TODO: set current stream to NULL if it's the closed stream.
        //
        // TODO: Remove stream from collection.
        let stream = self.streams.get_mut(&stream).expect("TODO: handle None");
        let (read_count, write_count) = match stream {
            Stream::Window(window_stream) => (window_stream.read_count, window_stream.write_count),
            Stream::Memory(stream::Memory {
                start,
                ref mut encoding,
                read_count,
                write_count,
                ..
            }) => {
                match encoding {
                    MemoryStreamType::Unicode(buffer) => {
                        // TODO: Put back the Vec in the pool
                        // if we end up adding a pool.
                        mem.write_u32s(*start, buffer.drain(..))
                    }
                    _ => (), // Nothing to do.
                }
                (*read_count, *write_count)
            }
            Stream::File(stream::File {
                write_count,
                read_count,
                ..
            }) => (*read_count, *write_count),
        };
        StreamResult {
            read_count,
            write_count,
        }
    }

    // 0x0045
    fn stream_set_position(&mut self, _stream: OpaqueId, _pos: i32, _seek_mode: SeekMode) {
        todo!()
    }

    // 0x0046
    fn stream_get_position(&self, _stream: OpaqueId) -> u32 {
        todo!()
    }

    // 0x0047
    fn stream_set_current(&mut self, stream: Option<OpaqueId>) {
        if let Some(id) = stream {
            if !self.streams.contains_key(&id) {
                todo!("can't set current stream to inexistent stream");
            }
        }
        self.current_stream = stream;
    }

    // 0x0048
    fn stream_get_current(&self) -> Option<crate::OpaqueId> {
        self.current_stream
    }

    // 0x0049
    fn stream_open_resource(&mut self, _file_num: u32, _rock: u32) -> Option<OpaqueId> {
        // TODO: We don't support resource streams yet
        None
    }

    // 0x0060
    fn fileref_create_temp(&mut self, usage: glauque::FileUsage, rock: u32) -> Option<OpaqueId> {
        let id = self.opaque_id_factory.get().unwrap();
        let ty = usage.ty;
        let fileref = Fileref {
            ty: usage.ty,
            mode: usage.mode,
            path: format!(
                "temp/{}_{}.{}",
                &self.owner,
                self.next_fileref_id,
                file_type_extension(ty)
            )
            .into(),
        };
        self.next_fileref_id += 1;
        self.filerefs.insert(id, fileref, rock);

        Some(id)
    }

    // 0x0061
    fn fileref_create_by_name(
        &mut self,
        _usage: FileUsage,
        _name: &[u8],
        _rock: u32,
    ) -> Option<OpaqueId> {
        todo!()
    }

    // 0x0062
    fn fileref_create_by_prompt(
        &mut self,
        _usage: FileUsage,
        _mode: FileMode,
        _rock: u32,
    ) -> Option<OpaqueId> {
        todo!()
    }

    // 0x0063
    fn fileref_destroy(&mut self, fileref: OpaqueId) {
        self.filerefs.remove(&fileref)
    }

    // 0x0064
    fn fileref_iterate(&self, id: Option<OpaqueId>) -> Option<(OpaqueId, u32)> {
        self.filerefs.next(id)
    }

    // 0x0065
    fn fileref_get_rock(&self, fileref: OpaqueId) -> u32 {
        self.filerefs
            .get_rock(&fileref)
            .expect("TODO handle invalid fileref")
    }

    // 0x0066
    fn fileref_delete_file(&self, firelef: OpaqueId) {
        let fileref = self
            .filerefs
            .get(&firelef)
            .expect("TODO: handle inexistant fileref");
        get_files().lock().unwrap().delete(&self.owner, fileref);
    }

    // 0x0067
    fn fileref_does_file_exist(&self, fileref: OpaqueId) -> bool {
        let fileref = self
            .filerefs
            .get(&fileref)
            .expect("TODO: handle inexistant fileref");
        get_files()
            .lock()
            .unwrap()
            .does_file_exist(&self.owner, fileref)
    }

    // 0x0068
    fn fileref_create_from_fileref(
        &mut self,
        _usage: FileUsage,
        _fileref: OpaqueId,
        _rock: u32,
    ) -> OpaqueId {
        todo!()
    }

    // 0x0084, 0x0085
    fn put_slice_stream(&mut self, mem: &mut impl Memory, stream: OpaqueId, bytes: &[u8]) {
        // TODO: Error on input-only stream.
        let stream = self.streams.get_mut(&stream).expect("TODO: handle None");
        match stream {
            Stream::Window(ref mut win_stream) => win_stream.put_slice(&self.windows, bytes),
            Stream::Memory(mem_stream) => mem_stream.put_slice(mem, bytes),
            Stream::File(file_stream) => {
                file_stream.put_slice(&self.filerefs, &self.owner, bytes);
            }
        }
    }

    // 0x0084, 0x0085
    fn put_chars_stream(
        &mut self,
        mem: &mut impl Memory,
        stream: OpaqueId,
        bytes: impl IntoIterator<Item = u8>,
    ) {
        // TODO: Error on input-only stream.
        let stream = self.streams.get_mut(&stream).expect("TODO: handle None");
        match stream {
            Stream::Window(win_stream) => win_stream.put_chars(&self.windows, bytes),
            Stream::Memory(mem_stream) => mem_stream.put_chars(mem, bytes),
            Stream::File(file_stream) => file_stream.put_chars(&self.filerefs, &self.owner, bytes),
        }
    }

    // 0x0087
    fn set_style_stream(&mut self, _stream: OpaqueId, _style: Style) {
        // TODO: Maybe do something.
    }

    // 0x0090
    fn get_char_stream(&mut self, _mem: &impl Memory, _stream: OpaqueId) -> u8 {
        todo!()
    }

    // 0x0091
    // TODO:
    fn get_line_stream(&mut self, _mem: &impl Memory, _stream: OpaqueId, _slice: &mut [u8]) -> u32 {
        todo!()
    }

    // 0x0092
    fn get_slice_stream(
        &mut self,
        _mem: &impl Memory,
        _stream: OpaqueId,
        _slice: &mut [u8],
    ) -> u32 {
        todo!()
    }

    // 0x0092
    fn get_to_end_stream(
        &mut self,
        mem: &impl Memory,
        stream: OpaqueId,
        buffer: &mut impl Extend<u8>,
    ) {
        let stream = self.streams.get_mut(&stream).expect("TODO: handle None");
        match stream {
            Stream::Window(_) => todo!("handle illegal reading from window stream"),
            Stream::Memory(mem_stream) => mem_stream.get_to_end(mem, buffer),
            Stream::File(file_stream) => {
                file_stream.get_to_end(&self.filerefs, &self.owner, buffer)
            }
        }
    }

    // 0x00B0
    fn stylehint_set(&mut self, _ty: WindowType, _style: Style, _hint: Stylehint) {
        // TODO: Maybe do something.
    }

    // 0x00B1
    fn stylehint_clear(&mut self, _win_type: WindowType, _style: Style, _hint: Stylehint) {
        // TODO: Maybe do something.
    }

    // 0x00B2
    fn style_distinguish(&self, _win: OpaqueId, _style1: Style, _style2: Style) -> bool {
        // Styles are not supported, so they all look the same.
        false
    }

    // 0x00B3
    fn style_measure(&self, _win: OpaqueId, _style: Style, _hint: Stylehint) -> Option<u32> {
        todo!()
    }

    // 0x00C0
    fn select(&mut self) {
        self.flush_output()
    }

    // 0x00C1
    fn select_poll(&mut self) {
        self.flush_output();
    }

    fn accept(&mut self, mem: &mut impl Memory, event: Event) -> Event {
        // The pending output of the window streams have been read,
        // So we put it in the contents.
        //
        // We could have iterated on the windows and get their streams and echo streams,
        // but it would make a bit of indirection.
        // It's simple to iterate through the streams directly.
        for stream in self
            .streams
            .values_mut()
            .filter_map(|entry| entry.content.as_window_mut())
        {
            stream.contents.push_str(&stream.pending);
            stream.pending.clear();
        }
        self.output.clear();

        match &event.ty {
            EventType::None => (),
            EventType::CharInput(_) => {
                match self
                    .pending_inputs
                    .remove(&event.win.expect("TODO: handle no window in char event"))
                {
                    Some(PendingInput::Char) => {
                        // No need to do anything besides removing the event.
                    }
                    _ => {
                        self.warn(
                            "cannot accept char event in window not awaiting for one.".into(),
                        );
                    }
                }
            }
            EventType::LineInput { command, .. } => {
                match self
                    .pending_inputs
                    .remove(&event.win.expect("TODO: handle no window in line event"))
                {
                    Some(PendingInput::Line {
                        is_unicode,
                        addr,
                        max_len,
                    }) => {
                        if is_unicode {
                            todo!("accepting Unicode line input")
                        } else {
                            let chars =
                                command.iter().take(max_len.try_into().unwrap()).map(|&ch| {
                                    if ch as u32 > 255 {
                                        b'?'
                                    } else {
                                        ch as u8
                                    }
                                });
                            mem.write_u8s(addr, chars);
                        }
                    }
                    _ => {
                        self.warn(
                            "cannot accept line event in window not awaiting for one.".into(),
                        );
                    }
                }
            }
            other => todo!("{other:?}"),
        }

        event
    }

    // 0x00D0
    fn request_line_event(
        &mut self,
        _mem: &impl Memory,
        win: OpaqueId,
        addr: u32,
        max_len: u32,
        _init_len: u32,
    ) {
        match self.pending_inputs.get(&win) {
            Some(_) => todo!("handling requesting line input in window with input already pending"),
            None => self.pending_inputs.insert(
                win,
                PendingInput::Line {
                    is_unicode: false,
                    addr,
                    max_len,
                },
            ),
        };
    }

    // 0x00D1
    fn cancel_line_event(&mut self, _win: OpaqueId) -> Event {
        todo!()
    }

    // 0x00D2
    fn request_char_event(&mut self, win: OpaqueId) {
        match self.pending_inputs.get(&win) {
            Some(_) => todo!("handling requesting char input in window with input already pending"),
            None => self.pending_inputs.insert(win, PendingInput::Char),
        };
    }

    // 0x00D3
    fn cancel_char_event(&mut self, _win: OpaqueId) {
        todo!()
    }

    // 0x00D4
    fn request_mouse_event(&mut self, _win: OpaqueId) {
        // Not supported.
    }

    // 0x00D5
    fn cancel_mouse_event(&mut self, _win: OpaqueId) {
        // Not supported.
    }

    // 0x00D6
    fn request_timer_events(&mut self, _millis: u32) {
        // Not supported.
    }

    // 0x00E0
    fn image_get_info(&self, _image: u32) -> Option<(u32, u32)> {
        // Graphics are not supported.
        None
    }

    // 0x00E1
    fn image_draw(&mut self, _image: u32, _val1: i32, _val2: i32) -> bool {
        // Graphics are not supported.
        false
    }

    // 0x00E2
    fn image_draw_scaled(
        &mut self,
        _image: u32,
        _val1: i32,
        _val2: i32,
        _witdth: u32,
        _height: u32,
    ) -> bool {
        // Graphics are not supported.
        false
    }

    // 0x00E8
    fn window_flow_break(&mut self, _win: OpaqueId) {
        // Graphics are not supported.
    }

    // 0x00E9
    fn window_erase_rect(&mut self, _left: i32, _top: i32, _width: u32, _height: u32) {
        // Graphics are not supported.
    }

    // 0x00EA
    fn window_fill_rect(
        &mut self,
        _win: OpaqueId,
        _color: Color,
        _left: i32,
        _top: i32,
        _width: u32,
        _height: u32,
    ) {
        // Graphics are not supported.
    }

    // 0x00EB
    fn window_set_background_color(&mut self, _win: OpaqueId, _color: Color) {
        // Graphics are not supported.
    }

    // 0x00F0
    fn schannel_iterate(&self, id: Option<OpaqueId>) -> Option<(OpaqueId, u32)> {
        self.schannels.next(id)
    }

    // 0x00F1
    /// Returns the rock value of the channel with the given ID.
    fn schannel_get_rock(&self, chan: OpaqueId) -> u32 {
        self.schannels
            .get_rock(&chan)
            .expect("TODO handle invalid channel")
    }

    // 0x00F4
    fn schannel_create_ext(&mut self, _rock: u32, _volume: u32) -> Option<OpaqueId> {
        // let channel = Schannel {};
        // let id = self.new_opaque_id();
        // self.schannels.insert(id, channel, rock);
        // Some(id)

        // Sounds are not supported.
        None
    }

    // 0x00F3
    /// Destroys the channel with the given ID.
    ///
    /// If the channel is playing a sound,
    /// the sound stops immediately (with no notification event).
    fn schannel_destroy(&mut self, _chan: OpaqueId) {
        // Sounds are not supported.
    }

    // 0x00F7
    fn schannel_play_multi(&mut self, _chans: &[OpaqueId], _sounds: &[u32], _notify: u32) -> u32 {
        // Sounds are not supported.
        0
    }

    // 0x00F9
    fn schannel_play_ext(
        &mut self,
        _chan: OpaqueId,
        _sound: u32,
        _repeats: u32,
        _notify: u32,
    ) -> bool {
        // Sounds are not supported.
        false
    }

    // 0x00FA
    fn schannel_stop(&mut self, _chan: OpaqueId) {
        // Sounds are not supported.
    }

    // 0x00FC
    fn sound_load_hint(&self, _sound: u32, _flag: bool) {
        // Sounds are not supported.
    }

    // 0x00FD
    fn schannel_set_volume_ext(
        &mut self,
        _chan: OpaqueId,
        _volume: u32,
        _duration: u32,
        _notify: u32,
    ) {
        // Sounds are not supported.
    }

    // 0x00FE
    fn schannel_pause(&mut self, _chan: OpaqueId) {
        // Sounds are not supported.
    }

    // 0x00FF
    fn schannel_unpause(&mut self, _chan: OpaqueId) {
        // Sounds are not supported.
    }

    // 0x0101
    fn set_hyperlink_stream(&mut self, _stream: OpaqueId, _link_val: u32) {
        // Hyperlinks are not supported.
    }

    // 0x0102
    fn request_hyperlink_event(&mut self, _win: OpaqueId) {
        // Hyperlinks are not supported.
    }

    // 0x0103
    fn cancel_hyperlink_event(&mut self, _win: OpaqueId) {
        // Hyperlinks are not supported.
    }

    // 0x0122
    /// Returns the string corresponding to the titlecase of the given slice taken as Unicode code points.
    fn slice_to_title_case_uni(_slice: &[u32]) -> String {
        todo!()
    }

    // 0x0123
    fn slice_canon_decompose_uni(_slice: &[u32]) -> String {
        todo!()
    }

    // 0x0124
    fn slice_canon_normalize_uni(_slice: &[u32]) -> String {
        todo!()
    }

    // 0x012C, 0x012D
    fn put_slice_stream_uni(&mut self, mem: &mut impl Memory, stream: OpaqueId, chars: &[u32]) {
        // TODO: It's mostly a duplicate of `put_slice_stream`
        // so it would be easier to maintain
        // if we could factorise the identical code.
        //
        // TODO: Error on input-only stream.
        let stream = self.streams.get_mut(&stream).expect("TODO: handle None");
        match stream {
            Stream::Window(win_stream) => win_stream.put_slice_uni(&self.windows, chars),
            Stream::Memory(mem_stream) => mem_stream.put_slice_uni(mem, chars),
            Stream::File(file_stream) => {
                file_stream.put_slice_uni(&self.filerefs, &self.owner, chars)
            }
        }
    }

    // 0x012C, 0x012D
    fn put_chars_stream_uni(
        &mut self,
        mem: &mut impl Memory,
        stream: OpaqueId,
        chars: impl IntoIterator<Item = u32>,
    ) {
        // TODO: It's mostly a duplicate of `put_chars_stream`
        // so it would be easier to maintain
        // if we could factorise the identical code.
        //
        // TODO: Error on input-only stream.
        let stream = self.streams.get_mut(&stream).expect("TODO: handle None");
        match stream {
            Stream::Window(win_stream) => win_stream.put_chars_uni(&self.windows, chars),
            Stream::Memory(mem_stream) => mem_stream.put_chars_uni(mem, chars),
            Stream::File(file_stream) => {
                file_stream.put_chars_uni(&self.filerefs, &self.owner, chars)
            }
        }
    }

    // 0x0130
    fn get_char_stream_uni(&mut self, _mem: &impl Memory, _stream: OpaqueId) -> u32 {
        todo!()
    }

    // 0x0131
    fn get_slice_stream_uni(
        &mut self,
        _mem: &impl Memory,
        _stream: OpaqueId,
        _slice: &mut [u32],
    ) -> u32 {
        todo!()
    }

    // 0x0132
    fn get_line_stream_uni(
        &mut self,
        _mem: &impl Memory,
        _stream: OpaqueId,
        _slice: &mut [u32],
    ) -> u32 {
        todo!()
    }

    // 0x0138
    fn stream_open_file_uni(
        &mut self,
        _fileref: OpaqueId,
        _mode: FileMode,
        _rock: u32,
    ) -> Option<OpaqueId> {
        todo!()
    }

    // 0x0139
    fn stream_open_memory_uni(
        &mut self,
        _mem: &impl Memory,
        start: u32,
        len: u32,
        mode: FileMode,
        rock: u32,
    ) -> OpaqueId {
        if !matches!(mode, FileMode::Read | FileMode::Write | FileMode::ReadWrite) {
            todo!("handle invalid `FileMode` in `stream_open_memory_uni`")
        }
        let id = self.opaque_id_factory.get().unwrap();
        let stream = stream::Memory {
            write_count: 0,
            read_count: 0,
            start,
            len,
            // TODO: Instead of allocating a new Vec every time,
            // have a pool we can draw from?
            encoding: MemoryStreamType::Unicode(Vec::new()),
        };
        self.streams.insert(id, stream.into(), rock);
        id
    }

    // 0x013A
    fn stream_open_resource_uni(&mut self, _file_num: u32, _rock: u32) -> Option<OpaqueId> {
        // Not supported.
        None
    }

    // 0x0140
    /// Requests input of a Latin-1 character or special key in the window with the given ID.
    fn request_char_event_uni(&mut self, _win: OpaqueId) {
        todo!()
    }

    // 0x0141
    fn request_line_event_uni(
        &mut self,
        _mem: &impl Memory,
        _win: OpaqueId,
        _addr: u32,
        _max_len: u32,
        _init_len: u32,
    ) {
        todo!()
    }

    // 0x0150
    fn set_echo_line_event(&mut self, _win: OpaqueId, _val: bool) {
        // Not supported.
    }

    // 0x0151
    fn set_terminators_line_event(&mut self, _win: OpaqueId, _keycodes: &[Key]) {
        // Not supported
    }

    fn current_time() -> Timeval {
        // Not supported
        Timeval {
            high_sec: -1,
            low_sec: u32::MAX,
            microsec: 0,
        }
    }

    // 0x0161
    fn current_simple_time(_factor: u32) -> i32 {
        // Not supported
        0
    }

    // 0x0168
    fn time_to_date_utc(_time: Timeval) -> Date {
        // Not supported
        Date {
            year: 0,
            month: 0,
            day: 0,
            weekday: 0,
            hour: 0,
            minute: 0,
            second: -1,
            microsec: 0,
        }
    }

    // 0x0169
    fn time_to_date_local(_time: Timeval) -> Date {
        // Not supported
        Date {
            year: 0,
            month: 0,
            day: 0,
            weekday: 0,
            hour: 0,
            minute: 0,
            second: -1,
            microsec: 0,
        }
    }

    // 0x016A
    fn simple_time_to_date_utc(_time: i32, _factor: u32) -> Date {
        // Not supported
        Date {
            year: 0,
            month: 0,
            day: 0,
            weekday: 0,
            hour: 0,
            minute: 0,
            second: -1,
            microsec: 0,
        }
    }

    // 0x016B
    fn simple_time_to_date_local(_time: i32, _factor: u32) -> Date {
        // Not supported
        Date {
            year: 0,
            month: 0,
            day: 0,
            weekday: 0,
            hour: 0,
            minute: 0,
            second: -1,
            microsec: 0,
        }
    }

    // 0x016C
    fn date_to_time_utc(_date: Date, _factor: u32) -> Option<Timeval> {
        // Not supported
        None
    }

    // 0x016D
    fn date_to_time_local(_date: Date, _factor: u32) -> Option<Timeval> {
        // Not supported
        None
    }

    // 0x016E
    fn date_to_simple_time_utc(_date: Date, _factor: u32) -> Option<i32> {
        None
    }

    // 0x016F
    fn date_to_simple_time_local(_date: Date, _factor: u32) -> Option<i32> {
        // Not supported
        None
    }
}
