use std::{path::Path, sync::OnceLock};

use regex::Regex;

use crate::common::{get_story, TestStory};

// Some helper functions.

fn new_story() -> TestStory {
    static CELL: OnceLock<Vec<u8>> = OnceLock::new();
    let path = Path::new(file!()).with_file_name("accelfunctest.inf");
    get_story(&CELL, path, "", &[])
}

// Same as `new_story` but with a non-default `NUM_ATTR_BYTES`.
fn new_story_alt() -> TestStory {
    static CELL: OnceLock<Vec<u8>> = OnceLock::new();
    let path = Path::new(file!()).with_file_name("accelfunctest.inf");
    let dest = Path::new(file!()).with_file_name("accelfunctest_alt.ulx");
    get_story(&CELL, path, dest, &["--opt", "NUM_ATTR_BYTES=11"])
}

/// Run the given Glulxercise test.
fn run_command(cmd: &str) -> String {
    // Get a brand new story.
    let mut story = new_story();
    // We assume accelfunctest won't have runtime errors.
    story.send_command(cmd).unwrap();

    story.output
}

// Same as `run_command` but with a non-default `NUM_ATTR_BYTES`.
fn run_command_alt(cmd: &str) -> String {
    // Get a brand new story.
    let mut story = new_story_alt();
    // We assume accelfunctest won't have runtime errors.
    story.send_command(cmd).unwrap();

    story.output
}

// And the test functions.

#[test]
fn z_region() {
    // Accelerated function 1.

    let slow = run_command("slow zregion");
    let fast = run_command("fast zregion");

    assert!(slow.contains("Kitchen: 1"));
    assert!(fast.contains("Kitchen: 1"));

    assert!(slow.contains("test_zregion(): 2"));
    assert!(fast.contains("test_zregion(): 2"));

    assert!(slow.contains("\"foo\": 3"));
    assert!(fast.contains("\"foo\": 3"));

    assert!(slow.contains("Various zeroes: 0 0 0 0 0 0"));
    assert!(fast.contains("Various zeroes: 0 0 0 0 0 0"));

    assert!(slow.contains("Z__Region itself: 2"));
    assert!(fast.contains("Z__Region itself: 2"));
}

// The assertions for functions 2 and 8.
fn assert_cptab(slow: &str, fast: &str) {
    let re =
        Regex::new("Kitchen.description:\nProperty 35, flags 0, 1 words:\n  0: ([0-9]+) <func>")
            .unwrap();
    let addr_slow = re.captures(slow).unwrap().get(1).unwrap().as_str();
    let addr_fast = re.captures(fast).unwrap().get(1).unwrap().as_str();
    assert_eq!(addr_slow, addr_fast);

    assert!(slow.contains("Kitchen.testfunc:\nNo such property."));
    assert!(fast.contains("Kitchen.testfunc:\nNo such property."));

    let re =
        Regex::new("TestCPTab.testfunc:\nProperty 311, flags 0, 1 words:\n  0: ([0-9]+) <func>")
            .unwrap();
    let addr_slow = re.captures(slow).unwrap().get(1).unwrap().as_str();
    let addr_fast = re.captures(fast).unwrap().get(1).unwrap().as_str();
    assert_eq!(addr_slow, addr_fast);

    let re = Regex::new("TestCPTab.name:\nProperty 1, flags 0, 5 words:\n  0: ([0-9]+) 'cptab'\n  1: ([0-9]+) 'cp'\n  2: ([0-9]+) 'tab'\n  3: ([0-9]+) 'new'\n  4: ([0-9]+) 'test'").unwrap();
    let addr_slow = re.captures(slow).unwrap().get(1).unwrap().as_str();
    let addr_fast = re.captures(fast).unwrap().get(1).unwrap().as_str();
    assert_eq!(addr_slow, addr_fast);
    let addr_slow = re.captures(slow).unwrap().get(2).unwrap().as_str();
    let addr_fast = re.captures(fast).unwrap().get(2).unwrap().as_str();
    assert_eq!(addr_slow, addr_fast);
    let addr_slow = re.captures(slow).unwrap().get(3).unwrap().as_str();
    let addr_fast = re.captures(fast).unwrap().get(3).unwrap().as_str();
    assert_eq!(addr_slow, addr_fast);
    let addr_slow = re.captures(slow).unwrap().get(4).unwrap().as_str();
    let addr_fast = re.captures(fast).unwrap().get(4).unwrap().as_str();
    assert_eq!(addr_slow, addr_fast);
    let addr_slow = re.captures(slow).unwrap().get(5).unwrap().as_str();
    let addr_fast = re.captures(fast).unwrap().get(5).unwrap().as_str();
    assert_eq!(addr_slow, addr_fast);

    assert!(slow.contains("Kitchen.321:\nNo such property."));
    assert!(fast.contains("Kitchen.321:\nNo such property."));

    assert!(slow.contains("bareobject.name:\nNo such property."));
    assert!(fast.contains("bareobject.name:\nNo such property."));

    assert!(slow.contains("Kitchen.(0):\nNo such property."));
    assert!(fast.contains("Kitchen.(0):\nNo such property."));

    // The error messages are more precise when not accelerated.
    // We thus only check that the message appears enough times when accelerated.
    assert!(slow.contains("[** Programming error: tried to find the \".\" of nothing **]"));
    assert!(slow.contains(
        "[** Programming error: tried to find the \".\" of <illegal object number 1> **]"
    ));
    assert!(
        Regex::new("[** Programming error: tried to find the \".\" of <routine \\d+> **]")
            .unwrap()
            .is_match(slow)
    );
    assert_eq!(
        fast.matches("[** Programming error: tried to find the \".\" of (something) **]")
            .count(),
        3
    );
}

#[test]
fn cptab_old() {
    // Accelerated function 2.

    let slow = run_command("slow cptab old");
    let fast = run_command("fast cptab old");
    assert_cptab(&slow, &fast);
}

#[test]
fn cptab_new() {
    // Accelerated function 8.

    let slow = run_command("slow cptab new");
    let fast = run_command("fast cptab new");
    assert_cptab(&slow, &fast);
}

#[test]
fn cptab_new_alt() {
    // Accelerated function 8 alt.

    let slow = run_command_alt("slow cptab new");
    let fast = run_command_alt("fast cptab new");
    assert_cptab(&slow, &fast);
}

fn assert_rapr(slow: &str, fast: &str) {
    assert!(slow.contains("bareobject.name: No such property."));
    assert!(fast.contains("bareobject.name: No such property."));

    let re = Regex::new("CoreRegion.name: ([0-9]+) 'core'").unwrap();
    let addr_slow = re.captures(slow).unwrap().get(1).unwrap().as_str();
    let addr_fast = re.captures(fast).unwrap().get(1).unwrap().as_str();
    assert_eq!(addr_slow, addr_fast);

    assert!(slow.contains("CoreRegion.indexnum: 1"));
    assert!(fast.contains("CoreRegion.indexnum: 1"));

    assert!(slow.contains("bareobject.TestObj::indexnum: No such property."));
    assert!(fast.contains("bareobject.TestObj::indexnum: No such property."));

    assert!(slow.contains("CoreRegion.TestObj::indexnum: 0"));
    assert!(fast.contains("CoreRegion.TestObj::indexnum: 0"));

    assert!(slow.contains("botspecial.TestObj::indexnum: No such property."));
    assert!(fast.contains("botspecial.TestObj::indexnum: No such property."));

    assert!(slow.contains("topobj.gloop: 11"));
    assert!(fast.contains("topobj.gloop: 11"));

    assert!(slow.contains("midobj.gloop: 22"));
    assert!(fast.contains("midobj.gloop: 22"));

    assert!(slow.contains("midobj.TopClass::gloop: 11"));
    assert!(fast.contains("midobj.TopClass::gloop: 11"));

    assert!(slow.contains("midobj.BotClass::gloop: No such property."));
    assert!(fast.contains("midobj.BotClass::gloop: No such property."));

    let re = Regex::new("botspecial.glark: ([0-9]+) 'special'").unwrap();
    let addr_slow = re.captures(slow).unwrap().get(1).unwrap().as_str();
    let addr_fast = re.captures(fast).unwrap().get(1).unwrap().as_str();
    assert_eq!(addr_slow, addr_fast);

    let re = Regex::new("botspecial.BotClass::glark: ([0-9]+) 'goodbye'").unwrap();
    let addr_slow = re.captures(slow).unwrap().get(1).unwrap().as_str();
    let addr_fast = re.captures(fast).unwrap().get(1).unwrap().as_str();
    assert_eq!(addr_slow, addr_fast);

    let re = Regex::new("botspecial.MidClass::glark: ([0-9]+) 'hello'").unwrap();
    let addr_slow = re.captures(slow).unwrap().get(1).unwrap().as_str();
    let addr_fast = re.captures(fast).unwrap().get(1).unwrap().as_str();
    assert_eq!(addr_slow, addr_fast);

    let re = Regex::new("midobj.password: ([0-9]+) 'pass'").unwrap();
    let addr_slow = re.captures(slow).unwrap().get(1).unwrap().as_str();
    let addr_fast = re.captures(fast).unwrap().get(1).unwrap().as_str();
    assert_eq!(addr_slow, addr_fast);

    assert!(slow.contains("botspecial.password (foreign): No such property."));
    assert!(fast.contains("botspecial.password (foreign): No such property."));

    assert!(slow.contains("midobj.password (foreign): No such property."));
    assert!(fast.contains("midobj.password (foreign): No such property."));

    let re = Regex::new("botspecial.password: ([0-9]+) 'swordfish'").unwrap();
    let addr_slow = re.captures(slow).unwrap().get(1).unwrap().as_str();
    let addr_fast = re.captures(fast).unwrap().get(1).unwrap().as_str();
    assert_eq!(addr_slow, addr_fast);

    assert!(slow.contains("TopClass.gloop: No such property."));
    assert!(fast.contains("TopClass.gloop: No such property."));

    assert!(slow.contains("TopClass.create: No such property."));
    assert!(fast.contains("TopClass.create: No such property."));
}

#[test]
fn rapr_old() {
    // Accelerated function 3.

    let slow = run_command("slow rapr old");
    let fast = run_command("fast rapr old");
    assert_rapr(&slow, &fast)
}

#[test]
fn rapr_new() {
    // Accelerated function 9.

    let slow = run_command("slow rapr new");
    let fast = run_command("fast rapr new");
    assert_rapr(&slow, &fast)
}

#[test]
fn rapr_new_alt() {
    // Accelerated function 9 alt.

    let slow = run_command_alt("slow rapr new");
    let fast = run_command_alt("fast rapr new");
    assert_rapr(&slow, &fast)
}

fn assert_rlpr(slow: &str, fast: &str) {
    assert!(slow.contains("bareobject.name: 0 bytes"));
    assert!(fast.contains("bareobject.name: 0 bytes"));

    assert!(slow.contains("CoreRegion.name: 12 bytes"));
    assert!(fast.contains("CoreRegion.name: 12 bytes"));

    assert!(slow.contains("CoreRegion.indexnum: 4 bytes"));
    assert!(fast.contains("CoreRegion.indexnum: 4 bytes"));

    assert!(slow.contains("bareobject.TestObj::indexnum: 0 bytes"));
    assert!(fast.contains("bareobject.TestObj::indexnum: 0 bytes"));

    assert!(slow.contains("CoreRegion.TestObj::indexnum: 4 bytes"));
    assert!(fast.contains("CoreRegion.TestObj::indexnum: 4 bytes"));

    assert!(slow.contains("botspecial.TestObj::indexnum: 0 bytes"));
    assert!(fast.contains("botspecial.TestObj::indexnum: 0 bytes"));

    assert!(slow.contains("topobj.gloop: 4 bytes"));
    assert!(fast.contains("topobj.gloop: 4 bytes"));

    assert!(slow.contains("midobj.gloop: 8 bytes"));
    assert!(fast.contains("midobj.gloop: 8 bytes"));

    assert!(slow.contains("midobj.TopClass::gloop: 4 bytes"));
    assert!(fast.contains("midobj.TopClass::gloop: 4 bytes"));

    assert!(slow.contains("midobj.BotClass::gloop: 0 bytes"));
    assert!(fast.contains("midobj.BotClass::gloop: 0 bytes"));

    assert!(slow.contains("botspecial.glark: 12 bytes"));
    assert!(fast.contains("botspecial.glark: 12 bytes"));

    assert!(slow.contains("botspecial.BotClass::glark: 4 bytes"));
    assert!(fast.contains("botspecial.BotClass::glark: 4 bytes"));

    assert!(slow.contains("botspecial.MidClass::glark: 8 bytes"));
    assert!(fast.contains("botspecial.MidClass::glark: 8 bytes"));

    assert!(slow.contains("midobj.password: 4 bytes"));
    assert!(fast.contains("midobj.password: 4 bytes"));

    assert!(slow.contains("botspecial.password (foreign): 0 bytes"));
    assert!(fast.contains("botspecial.password (foreign): 0 bytes"));

    assert!(slow.contains("midobj.password (foreign): 0 bytes"));
    assert!(fast.contains("midobj.password (foreign): 0 bytes"));

    assert!(slow.contains("botspecial.password: 4 bytes"));
    assert!(fast.contains("botspecial.password: 4 bytes"));

    assert!(slow.contains("TopClass.gloop: 0 bytes"));
    assert!(fast.contains("TopClass.gloop: 0 bytes"));

    assert!(slow.contains("TopClass.create: 0 bytes"));
    assert!(fast.contains("TopClass.create: 0 bytes"));
}

#[test]
fn rlpr_old() {
    // Accelerated function 4.

    let slow = run_command("slow rlpr old");
    let fast = run_command("fast rlpr old");
    assert_rlpr(&slow, &fast);
}

#[test]
fn rlpr_new() {
    // Accelerated function 10.

    let slow = run_command("slow rlpr new");
    let fast = run_command("fast rlpr new");
    assert_rlpr(&slow, &fast);
}

#[test]
fn rlpr_new_alt() {
    // Accelerated function 10 alt.

    let slow = run_command_alt("slow rlpr new");
    let fast = run_command_alt("fast rlpr new");
    assert_rlpr(&slow, &fast);
}

fn assert_occl(slow: &str, fast: &str) {
    assert!(slow.contains("\"str\" ofclass String: yes"));
    assert!(fast.contains("\"str\" ofclass String: yes"));

    assert!(slow.contains("\"str\" ofclass Routine: no"));
    assert!(fast.contains("\"str\" ofclass Routine: no"));

    assert!(slow.contains("\"str\" ofclass Object: no"));
    assert!(fast.contains("\"str\" ofclass Object: no"));

    assert!(slow.contains("\"str\" ofclass Class: no"));
    assert!(fast.contains("\"str\" ofclass Class: no"));

    assert!(slow.contains("\"str\" ofclass TopClass: no"));
    assert!(fast.contains("\"str\" ofclass TopClass: no"));

    assert!(slow.contains("printbool() ofclass String: no"));
    assert!(fast.contains("printbool() ofclass String: no"));

    assert!(slow.contains("printbool() ofclass Routine: yes"));
    assert!(fast.contains("printbool() ofclass Routine: yes"));

    assert!(slow.contains("printbool() ofclass Object: no"));
    assert!(fast.contains("printbool() ofclass Object: no"));

    assert!(slow.contains("printbool() ofclass Class: no"));
    assert!(fast.contains("printbool() ofclass Class: no"));

    assert!(slow.contains("printbool() ofclass TopClass: no"));
    assert!(fast.contains("printbool() ofclass TopClass: no"));

    assert!(slow.contains("'word' ofclass String: no"));
    assert!(fast.contains("'word' ofclass String: no"));

    assert!(slow.contains("'word' ofclass Routine: no"));
    assert!(fast.contains("'word' ofclass Routine: no"));

    assert!(slow.contains("'word' ofclass Object: no"));
    assert!(fast.contains("'word' ofclass Object: no"));

    assert!(slow.contains("'word' ofclass Class: no"));
    assert!(fast.contains("'word' ofclass Class: no"));

    assert!(slow.contains("'word' ofclass TopClass: no"));
    assert!(fast.contains("'word' ofclass TopClass: no"));

    assert!(slow.contains("String ofclass Class: yes"));
    assert!(fast.contains("String ofclass Class: yes"));

    assert!(slow.contains("Routine ofclass Class: yes"));
    assert!(fast.contains("Routine ofclass Class: yes"));

    assert!(slow.contains("Object ofclass Class: yes"));
    assert!(fast.contains("Object ofclass Class: yes"));

    assert!(slow.contains("Class ofclass Class: yes"));
    assert!(fast.contains("Class ofclass Class: yes"));

    assert!(slow.contains("TopClass ofclass Class: yes"));
    assert!(fast.contains("TopClass ofclass Class: yes"));

    assert!(slow.contains("bareobject ofclass Class: no"));
    assert!(fast.contains("bareobject ofclass Class: no"));

    assert!(slow.contains("String ofclass Object: no"));
    assert!(fast.contains("String ofclass Object: no"));

    assert!(slow.contains("Routine ofclass Object: no"));
    assert!(fast.contains("Routine ofclass Object: no"));

    assert!(slow.contains("Object ofclass Object: no"));
    assert!(fast.contains("Object ofclass Object: no"));

    assert!(slow.contains("Class ofclass Object: no"));
    assert!(fast.contains("Class ofclass Object: no"));

    assert!(slow.contains("TopClass ofclass Object: no"));
    assert!(fast.contains("TopClass ofclass Object: no"));

    assert!(slow.contains("bareobject ofclass Object: yes"));
    assert!(fast.contains("bareobject ofclass Object: yes"));

    assert!(slow.contains("TopClass ofclass String: no"));
    assert!(fast.contains("TopClass ofclass String: no"));

    assert!(slow.contains("bareobject ofclass String: no"));
    assert!(fast.contains("bareobject ofclass String: no"));

    assert!(slow.contains("TopClass ofclass Routine: no"));
    assert!(fast.contains("TopClass ofclass Routine: no"));

    assert!(slow.contains("bareobject ofclass Routine: no"));
    assert!(fast.contains("bareobject ofclass Routine: no"));

    assert!(slow.contains("bareobject ofclass TopClass: no"));
    assert!(fast.contains("bareobject ofclass TopClass: no"));

    assert!(slow.contains("bareobject ofclass BotClass: no"));
    assert!(fast.contains("bareobject ofclass BotClass: no"));

    assert!(slow.contains("topobj ofclass TopClass: yes"));
    assert!(fast.contains("topobj ofclass TopClass: yes"));

    assert!(slow.contains("topobj ofclass MidClass: no"));
    assert!(fast.contains("topobj ofclass MidClass: no"));

    assert!(slow.contains("topobj ofclass BotClass: no"));
    assert!(fast.contains("topobj ofclass BotClass: no"));

    assert!(slow.contains("midobj ofclass TopClass: yes"));
    assert!(fast.contains("midobj ofclass TopClass: yes"));

    assert!(slow.contains("midobj ofclass MidClass: yes"));
    assert!(fast.contains("midobj ofclass MidClass: yes"));

    assert!(slow.contains("midobj ofclass BotClass: no"));
    assert!(fast.contains("midobj ofclass BotClass: no"));

    assert!(slow.contains("botobj ofclass TopClass: yes"));
    assert!(fast.contains("botobj ofclass TopClass: yes"));

    assert!(slow.contains("botobj ofclass MidClass: yes"));
    assert!(fast.contains("botobj ofclass MidClass: yes"));

    assert!(slow.contains("botobj ofclass BotClass: yes"));
    assert!(fast.contains("botobj ofclass BotClass: yes"));

    assert!(Regex::new("[** Programming error: (topobj) (object number [0-9]+) is not of class <illegal object number 1> to apply 'ofclass' for **]").unwrap().is_match(slow));

    assert!(slow.contains("bareobject ofclass topobj: no"));
    assert!(fast.contains("bareobject ofclass topobj: no"));

    assert!(Regex::new("[** Programming error: (object number [0-9]+) is not of class <illegal object number 1> to apply 'ofclass' for **]").unwrap().is_match(slow));

    assert!(slow.contains("topobj ofclass \"str\": no"));
    assert!(fast.contains("topobj ofclass \"str\": no"));

    assert!(Regex::new("[** Programming error: (object number [0-9]+) is not of class <illegal object number 1> to apply 'ofclass' for **]").unwrap().is_match(slow));

    assert!(slow.contains("TopClass ofclass printbool(): no"));
    assert!(fast.contains("TopClass ofclass printbool(): no"));

    // The error messages are more precise when not accelerated.
    // We thus only check that the message appears enough times when accelerated.
    assert_eq!(
        fast.matches("[** Programming error: tried to apply 'ofclass' with non-class **]")
            .count(),
        3
    );
}

#[test]
fn occl_old() {
    // Accelerated function 5.

    let slow = run_command("slow occl old");
    let fast = run_command("fast occl old");
    assert_occl(&slow, &fast);
}

#[test]
fn occl_new() {
    // Accelerated function 11.

    let slow = run_command("slow occl new");
    let fast = run_command("fast occl new");
    assert_occl(&slow, &fast);
}

#[test]
fn occl_new_alt() {
    // Accelerated function 11 alt.

    let slow = run_command_alt("slow occl new");
    let fast = run_command_alt("fast occl new");
    assert_occl(&slow, &fast);
}

fn assert_rvpr(slow: &str, fast: &str) {
    assert!(slow.contains("bareobject.name: 0"));
    assert!(fast.contains("bareobject.name: 0"));

    assert!(slow.contains("botobj.gloop: 22"));
    assert!(fast.contains("botobj.gloop: 22"));

    assert!(slow.contains("midobj.gloop: 22"));
    assert!(fast.contains("midobj.gloop: 22"));

    assert!(slow.contains("topobj.gloop: 11"));
    assert!(fast.contains("topobj.gloop: 11"));

    assert!(slow.contains("botobj.comprop: 123"));
    assert!(fast.contains("botobj.comprop: 123"));

    assert!(slow.contains("midobj.comprop: 123"));
    assert!(fast.contains("midobj.comprop: 123"));

    assert!(slow.contains("topobj.comprop: 99"));
    assert!(fast.contains("topobj.comprop: 99"));

    assert!(slow.contains("TopClass.comprop: 99"));
    assert!(fast.contains("TopClass.comprop: 99"));

    assert!(Regex::new(
        "[** Programming error: (topobj) (object number 139757)  has no property glark to read **]"
    )
    .unwrap()
    .is_match(slow));

    assert!(slow.contains("topobj.glark: 0"));
    assert!(fast.contains("topobj.glark: 0"));

    assert!(Regex::new("[** Programming error: class TopClass (object number 139661)  has no property gloop to read **]").unwrap().is_match(slow));

    assert!(slow.contains("TopClass.gloop: 0"));
    assert!(fast.contains("TopClass.gloop: 0"));

    // The error messages are more precise when not accelerated.
    // We thus only check that the message appears enough times when accelerated.
    assert_eq!(
        fast.matches("[** Programming error: tried to read (something) **]")
            .count(),
        2
    );
}

#[test]
fn rvpr_old() {
    // Accelerated function 6.

    let slow = run_command("slow rvpr old");
    let fast = run_command("fast rvpr old");
    assert_rvpr(&slow, &fast);
}

#[test]
fn rvpr_new() {
    // Accelerated function 12.

    let slow = run_command("slow rvpr new");
    let fast = run_command("fast rvpr new");
    assert_rvpr(&slow, &fast);
}

#[test]
fn rvpr_new_alt() {
    // Accelerated function 12 alt.

    let slow = run_command_alt("slow rvpr new");
    let fast = run_command_alt("fast rvpr new");
    assert_rvpr(&slow, &fast);
}

fn assert_oppr(slow: &str, fast: &str) {
    assert!(slow.contains("\"str\" provides name: no"));
    assert!(fast.contains("\"str\" provides name: no"));

    assert!(slow.contains("\"str\" provides gloop: no"));
    assert!(fast.contains("\"str\" provides gloop: no"));

    assert!(slow.contains("\"str\" provides print: yes"));
    assert!(fast.contains("\"str\" provides print: yes"));

    assert!(slow.contains("\"str\" provides print_to_array: yes"));
    assert!(fast.contains("\"str\" provides print_to_array: yes"));

    assert!(slow.contains("\"str\" provides create: no"));
    assert!(fast.contains("\"str\" provides create: no"));

    assert!(slow.contains("\"str\" provides call: no"));
    assert!(fast.contains("\"str\" provides call: no"));

    assert!(slow.contains("printbool() provides name: no"));
    assert!(fast.contains("printbool() provides name: no"));

    assert!(slow.contains("printbool() provides gloop: no"));
    assert!(fast.contains("printbool() provides gloop: no"));

    assert!(slow.contains("printbool() provides print: no"));
    assert!(fast.contains("printbool() provides print: no"));

    assert!(slow.contains("printbool() provides print_to_array: no"));
    assert!(fast.contains("printbool() provides print_to_array: no"));

    assert!(slow.contains("printbool() provides create: no"));
    assert!(fast.contains("printbool() provides create: no"));

    assert!(slow.contains("printbool() provides call: yes"));
    assert!(fast.contains("printbool() provides call: yes"));

    assert!(slow.contains("'word' provides name: no"));
    assert!(fast.contains("'word' provides name: no"));

    assert!(slow.contains("'word' provides gloop: no"));
    assert!(fast.contains("'word' provides gloop: no"));

    assert!(slow.contains("'word' provides print: no"));
    assert!(fast.contains("'word' provides print: no"));

    assert!(slow.contains("'word' provides print_to_array: no"));
    assert!(fast.contains("'word' provides print_to_array: no"));

    assert!(slow.contains("'word' provides create: no"));
    assert!(fast.contains("'word' provides create: no"));

    assert!(slow.contains("'word' provides call: no"));
    assert!(fast.contains("'word' provides call: no"));

    assert!(slow.contains("TopClass provides name: no"));
    assert!(fast.contains("TopClass provides name: no"));

    assert!(slow.contains("TopClass provides gloop: no"));
    assert!(fast.contains("TopClass provides gloop: no"));

    assert!(slow.contains("TopClass provides glark: no"));
    assert!(fast.contains("TopClass provides glark: no"));

    assert!(slow.contains("TopClass provides print: yes"));
    assert!(fast.contains("TopClass provides print: yes"));

    assert!(slow.contains("TopClass provides print_to_array: yes"));
    assert!(fast.contains("TopClass provides print_to_array: yes"));

    assert!(slow.contains("TopClass provides create: yes"));
    assert!(fast.contains("TopClass provides create: yes"));

    assert!(slow.contains("TopClass provides call: yes"));
    assert!(fast.contains("TopClass provides call: yes"));

    assert!(slow.contains("topobj provides gloop: yes"));
    assert!(fast.contains("topobj provides gloop: yes"));

    assert!(slow.contains("midobj provides gloop: yes"));
    assert!(fast.contains("midobj provides gloop: yes"));

    assert!(slow.contains("midobj provides TopClass::gloop: yes"));
    assert!(fast.contains("midobj provides TopClass::gloop: yes"));

    assert!(slow.contains("midobj provides BotClass::gloop: no"));
    assert!(fast.contains("midobj provides BotClass::gloop: no"));

    assert!(slow.contains("botspecial provides glark: yes"));
    assert!(fast.contains("botspecial provides glark: yes"));

    assert!(slow.contains("botspecial provides BotClass::glark: yes"));
    assert!(fast.contains("botspecial provides BotClass::glark: yes"));

    assert!(slow.contains("botspecial provides MidClass::glark: yes"));
    assert!(fast.contains("botspecial provides MidClass::glark: yes"));

    assert!(slow.contains("botspecial provides TopClass::glark: no"));
    assert!(fast.contains("botspecial provides TopClass::glark: no"));
}

#[test]
fn oppr_old() {
    // Accelerated function 7.

    let slow = run_command("slow oppr old");
    let fast = run_command("fast oppr old");

    assert_oppr(&slow, &fast);
}

#[test]
fn oppr_new() {
    // Accelerated function 13.

    let slow = run_command("slow oppr new");
    let fast = run_command("fast oppr new");

    assert_oppr(&slow, &fast);
}

#[test]
fn oppr_new_alt() {
    // Accelerated function 13 alt.

    let slow = run_command_alt("slow oppr new");
    let fast = run_command_alt("fast oppr new");

    assert_oppr(&slow, &fast);
}
