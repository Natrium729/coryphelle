use std::{
    fs,
    path::{Path, PathBuf},
    process::Command,
    sync::OnceLock,
};

use coryphelle::{Coryphelle, RuntimeError, Status};
use glauque::{Event, EventType};
use test_glauque::TestGlauque;

/// Gets the Glulx image compiled from the Inform 6 source at `path`.
///
/// If `dest` is empty, the path will be used, with the extension ".ulx".
///
/// It will always be compiled as Glulx with the I6 standard library in the path,
/// but additional arguments can be passed with `args`.
///
/// The image is cached in `vec_cell`,
/// so calling it several time with the same lock is fast.
fn get_image(cell: &OnceLock<Vec<u8>>, path: &str, dest: &str, args: &[&str]) -> Vec<u8> {
    cell.get_or_init(|| {
        let path = Path::new(path);

        let dest = if dest.is_empty() {
            path.with_extension("ulx")
        } else {
            PathBuf::from(dest)
        };

        let stdlib_path = Path::new(file!())
            .parent()
            .unwrap()
            .parent()
            .unwrap()
            .join("inform6lib");

        // TODO: Add a way to change the Inform 6 compiler path,
        // maybe with an environment variable?
        let mut command = Command::new("inform");

        command
            .args([
                "-G",
                &format!("++include_path={}", stdlib_path.to_str().unwrap()),
            ])
            .args(args)
            .arg(path)
            .arg(&dest);

        let result = command.output().unwrap();
        if !result.status.success() {
            panic!(
                "Inform 6 stdout:\n{}\nInform 6 stderr:\n{}",
                String::from_utf8_lossy(&result.stdout),
                String::from_utf8_lossy(&result.stderr)
            );
        }

        fs::read(dest).unwrap()
    })
    .to_owned()
}

/// Returns a new story compiled from the given path.
///
/// The cell will receive the story
/// so that it is not compiled several times.
pub(crate) fn get_story(
    cell: &OnceLock<Vec<u8>>,
    path: impl AsRef<Path>,
    dest: impl AsRef<Path>,
    args: &[&str],
) -> TestStory {
    let path = path.as_ref().to_str().unwrap();
    let dest = dest.as_ref().to_str().unwrap();
    let image = get_image(&cell, path, dest, args);
    TestStory::new(image)
}

// A simple Coryphelle wrapper with useful methods.
pub(crate) struct TestStory {
    cory: Coryphelle<TestGlauque>,
    pub(crate) output: String,
}

impl TestStory {
    pub(crate) fn new(image: Vec<u8>) -> Self {
        // First get a unique string identifying this story file
        // with a crude "hash":
        // we take the first 128 bytes, interpret them as characters
        // and filter out those who would be illegal in paths.
        // It will keep unprintable characters,
        // but it's OK for our purpose.
        let story_id = image[..128]
            .into_iter()
            .map(|&byte| byte as char)
            .filter(|&ch| !"/\\<>:\"|?*".contains(ch))
            .collect();
        let glauque = TestGlauque::new(story_id);
        let (cory, result) = Coryphelle::builder(glauque, image)
            .validate()
            .unwrap()
            .start();
        let initial_output = result.unwrap();
        let mut story = Self {
            cory,
            output: initial_output,
        };

        // We assume that there won't be a runtime error on startup.
        story.advance_to_next_input().unwrap();

        story
    }

    fn advance_to_next_input(&mut self) -> Result<(), RuntimeError> {
        // Loop to accumulate the output after each `select_poll` until the story accepts a real input.
        loop {
            match self.cory.status() {
                Status::Ended => return Ok(()),
                Status::Polling => {
                    // We don't handle internally-spawned events.
                    let output = self.cory.accept(Event {
                        ty: EventType::None,
                        win: None,
                    })?;
                    self.output.push_str(&output);
                }
                coryphelle::Status::Awaiting => return Ok(()),
            }
        }
    }

    // Send the given command to this story and return its output.
    pub(crate) fn send_command<'a>(&'a mut self, cmd: &str) -> Result<(), RuntimeError> {
        let event = self.cory.glk().event_from_str(cmd).unwrap();
        let output = self.cory.accept(event)?;
        self.output.push_str(&output);
        // We don't update the output here
        // because `advance_to_next_input` will do it.
        self.advance_to_next_input()?;

        Ok(())
    }

    // Returns true if the story has ended.
    pub(crate) fn ended(&self) -> bool {
        self.cory.status() == Status::Ended
    }
}
