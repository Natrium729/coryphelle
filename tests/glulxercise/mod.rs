use std::{path::Path, sync::OnceLock};

use crate::common::{get_story, TestStory};

// Some helper functions.

fn new_story() -> TestStory {
    static CELL: OnceLock<Vec<u8>> = OnceLock::new();
    let path = Path::new(file!()).with_file_name("glulxercise.inf");
    get_story(&CELL, path, "", &[])
}

/// Run the given Glulxercise test.
///
/// The `Err` contains the failure message.
fn run_test(cmd: &str) -> Result<(), String> {
    // Get a brand new story.
    let mut story = new_story();
    // Gluxercise should never have runtime errors.
    story.send_command(cmd).unwrap();

    // I guess if not very efficient to call `contains` several times.
    // But there seemed to be no improvements when I tried with a single regex.
    if story.output.contains("Skipping test.") {
        Err(format!(
            "Coryphelle doesn't support Glulxercise test \"{cmd}\""
        ))
    } else if story.output.contains("failed.") {
        Err("Glulxercise test failed".into())
    } else if story.output.contains("Passed.") {
        Ok(())
    } else {
        Err(format!(
            "Glulxercise test \"{cmd}\" didn't explicitly fail nor pass"
        ))
    }
}

/// Same as `run_test` except it panics instead of returning an error.
fn assert_test(cmd: &str) {
    run_test(cmd).unwrap_or_else(|msg| panic!("{msg}"));
}

// And a test function for each Glulxercise test.
//
// TODO: maybe grouping some tests in a single function
// would make the tests faster?

#[test]
fn operand() {
    assert_test("operand");
}

#[test]
fn arith() {
    assert_test("arith");
}

#[test]
fn bigmul() {
    assert_test("bigmul");
}

#[test]
fn comvar() {
    assert_test("comvar");
}

#[test]
fn comarith() {
    assert_test("comarith");
}

#[test]
fn bitwise() {
    assert_test("bitwise");
}

#[test]
fn shift() {
    assert_test("shift");
}

#[test]
fn trunc() {
    assert_test("trunc");
}

#[test]
fn extend() {
    assert_test("extend");
}

#[test]
fn aload() {
    assert_test("aload");
}

#[test]
fn astore() {
    assert_test("astore");
}

#[test]
fn arraybit() {
    assert_test("arraybit");
}

#[test]
fn call() {
    assert_test("call");
}

#[test]
fn callstack() {
    assert_test("callstack");
}

#[test]
fn jump() {
    assert_test("jump");
}

#[test]
fn jumpform() {
    assert_test("jumpform");
}

#[test]
fn compare() {
    assert_test("compare");
}

#[test]
fn stack() {
    assert_test("stack");
}

#[test]
fn gestalt() {
    assert_test("gestalt");
}

#[test]
fn throw() {
    assert_test("throw");
}

#[test]
fn streamnum() {
    assert_test("streamnum");
}

#[test]
fn strings() {
    assert_test("strings");
}

#[test]
fn ramstring() {
    assert_test("ramstring");
}

#[test]
fn iosys() {
    assert_test("iosys");
}

#[test]
fn iosys2() {
    assert_test("iosys2");
}

#[test]
fn iosys3() {
    assert_test("iosys3");
}

#[test]
fn filter() {
    assert_test("filter");
}

#[test]
fn nullio() {
    assert_test("nullio");
}

#[test]
fn glk() {
    assert_test("glk");
}

#[test]
fn gidispa() {
    assert_test("gidispa");
}

#[test]
fn random() {
    // The Glulxercise test for the random number generator can fail if we're unlucky,
    // so we run it three times to make sure one will succeed.
    assert!((0..3).any(|_| run_test("random").is_ok()))
}

#[test]
fn nonrandom() {
    assert_test("nonrandom");
}

#[test]
fn search() {
    assert_test("search");
}

#[test]
fn mzero() {
    assert_test("mzero");
}

#[test]
fn mcopy() {
    assert_test("mcopy");
}

#[test]
fn undo() {
    assert_test("undo");
}

#[test]
fn multiundo() {
    assert_test("multiundo");
}

#[test]
fn extundo() {
    assert_test("extundo");
}

#[test]
fn restore() {
    assert_test("restore");
}

#[test]
fn verify() {
    assert_test("verify");
}

#[test]
fn protect() {
    assert_test("protect");
}

#[test]
fn memsize() {
    assert_test("memsize");
}

#[test]
fn undomemsize() {
    assert_test("undomemsize");
}

#[test]
fn undorestart() {
    assert_test("undorestart");
}

#[test]
fn heap() {
    assert_test("heap");
}

#[test]
fn undoheap() {
    assert_test("undoheap");
}

#[test]
fn acceleration() {
    assert_test("acceleration");
}

#[test]
fn floatconv() {
    assert_test("floatconv");
}

#[test]
fn floatarith() {
    assert_test("floatarith");
}

#[test]
fn floatmod() {
    assert_test("floatmod");
}

#[test]
fn floatround() {
    assert_test("floatround");
}

#[test]
fn floatexp() {
    assert_test("floatexp");
}

#[test]
fn floattrig() {
    assert_test("floattrig");
}

#[test]
fn floatatan2() {
    assert_test("floatatan2");
}

#[test]
fn fjumpform() {
    assert_test("fjumpform");
}

#[test]
fn fjump() {
    assert_test("fjump");
}

#[test]
fn fcompare() {
    assert_test("fcompare");
}

#[test]
fn fprint() {
    assert_test("fprint");
}

#[test]
fn doubleconv() {
    assert_test("doubleconv");
}

#[test]
fn doublearith() {
    assert_test("doublearith");
}

#[test]
fn doubleround() {
    assert_test("doubleround");
}

#[test]
fn doubleexp() {
    assert_test("doubleexp");
}

#[test]
fn doublemod() {
    assert_test("doublemod");
}

#[test]
fn doubletrig() {
    assert_test("doubletrig");
}

#[test]
fn doubleatan2() {
    assert_test("doubleatan2");
}

#[test]
fn doublejump() {
    assert_test("doublejump");
}

#[test]
fn doublecomp() {
    assert_test("doublecomp");
}

#[test]
fn doublehypot() {
    assert_test("doublehypot");
}

#[test]
fn doubleprint() {
    assert_test("doubleprint");
}

#[test]
fn safari5() {
    assert_test("safari5");
}

#[test]
fn quit() {
    let mut story = new_story();
    story.send_command("quit").unwrap();

    assert!(story.output.contains("Goodbye."));
    assert!(story.ended());
}

#[test]
fn opquit() {
    let mut story = new_story();
    story.send_command("opquit").unwrap();

    assert!(story.output.contains("Goodbye."));
    assert!(story.ended());
}

#[test]
fn glkquit() {
    let mut story = new_story();
    story.send_command("glkquit").unwrap();

    assert!(story.output.contains("Goodbye."));
    assert!(story.ended());
}

// Not strictly necessary since every test has been run above,
// but might be useful if some test "corrupts" the result of another.
#[test]
fn all() {
    // It'll only succeed of no test is skipped or fails
    // and at least one test passes.
    // (So it'll be erroneous if one test doesn't say if it passes or fails.)
    assert_test("all")
}
