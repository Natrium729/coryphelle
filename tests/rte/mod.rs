//! This module tests runtime errors.

use std::{error::Error, fmt::Write, path::Path, sync::OnceLock};

use coryphelle::RuntimeError;

use crate::common::{get_story, TestStory};

// Some helper functions.

fn new_story() -> TestStory {
    static CELL: OnceLock<Vec<u8>> = OnceLock::new();
    let path = Path::new(file!()).with_file_name("rte.inf");
    get_story(&CELL, path, "", &[])
}

/// Runs the given test from the story file.
///
/// The `Err` contains the failure message.
fn run_test(cmd: &str) -> Result<(), RuntimeError> {
    // Get a brand new story.
    let mut story = new_story();
    story.send_command(cmd)
}

/// A helper function to convert an error to a message chain by walking its sources.
fn error_to_message_chain<E: Error>(err: &E) -> String {
    let mut msgs = err.to_string();
    let mut source = err.source();
    while let Some(err) = source {
        msgs.push('\n');
        write!(msgs, "{err}").unwrap();
        source = err.source()
    }
    msgs
}

// And a test function for each Opcode-errors test.

#[test]
fn out_bounds_memory_read() {
    let err = run_test("oobread").unwrap_err();
    assert!(error_to_message_chain(&err)
        .contains("tried to read 4 byte(s) from address 0xFFFFFFFF, which is out of bounds"));
}

#[test]
fn out_bounds_memory_read_short() {
    let err = run_test("oobreads").unwrap_err();
    assert!(error_to_message_chain(&err)
        .contains("tried to read 2 byte(s) from address 0xFFFFFFFF, which is out of bounds"));
}

#[test]
fn out_bounds_memory_read_byte() {
    let err = run_test("oobreadb").unwrap_err();
    assert!(error_to_message_chain(&err)
        .contains("tried to read 1 byte(s) from address 0xFFFFFFFF, which is out of bounds"));
}

#[test]
fn opcode_out_range() {
    let err = run_test("ooropcode").unwrap_err();
    assert!(error_to_message_chain(&err).contains("opcode with first byte 0xD0 is out of range"));
}

#[test]
fn unknown_opcode() {
    let err = run_test("unknownopcode").unwrap_err();
    assert!(error_to_message_chain(&err).contains("opcode number 0xFFFFFFF is unknown"));
}

#[test]
fn unused_addressing_mode_4() {
    let err = run_test("addrmode4").unwrap_err();
    assert!(error_to_message_chain(&err).contains("unused operand addressing mode 0x4"));
}

#[test]
fn unused_addressing_mode_c() {
    let err = run_test("addrmodec").unwrap_err();
    assert!(error_to_message_chain(&err).contains("unused operand addressing mode 0xC"));
}

#[test]
fn stkpeek() {
    let err = run_test("stkpeek").unwrap_err();
    assert!(error_to_message_chain(&err)
        .contains("the stack count is 0, but a stack count of at least 1 was needed"));
}

#[test]
fn stkswap() {
    let err = run_test("stkswap").unwrap_err();
    assert!(error_to_message_chain(&err)
        .contains("the stack count is 1, but a stack count of at least 2 was needed"));
}

#[test]
fn stkroll() {
    let err = run_test("stkroll").unwrap_err();
    assert!(error_to_message_chain(&err)
        .contains("the stack count is 1, but a stack count of at least 2 was needed"));
}

#[test]
fn stkcopy() {
    let err = run_test("stkcopy").unwrap_err();
    assert!(error_to_message_chain(&err)
        .contains("the stack count is 0, but a stack count of at least 1 was needed"));
}

#[test]
fn mfree() {
    let err = run_test("mfree").unwrap_err();
    assert!(error_to_message_chain(&err).contains("0x3801 is not the address of an extant block"));
}

#[test]
fn save_null() {
    let err = run_test("savenull").unwrap_err();
    assert!(error_to_message_chain(&err).contains("the current I/O system is \"null\""));
}

#[test]
fn save_filter() {
    let err = run_test("savefilt").unwrap_err();
    assert!(error_to_message_chain(&err).contains("the current I/O system is \"filter\""));
}

#[test]
fn save_glk_null() {
    let err = run_test("savegnull").unwrap_err();
    assert!(error_to_message_chain(&err).contains("the Glk stream 0 is invalid"));
}

#[test]
fn restore_null() {
    let err = run_test("restnull").unwrap_err();
    assert!(error_to_message_chain(&err).contains("the current I/O system is \"null\""));
}

#[test]
fn restore_filter() {
    let err = run_test("restfilt").unwrap_err();
    assert!(error_to_message_chain(&err).contains("the current I/O system is \"filter\""));
}

#[test]
fn restore_glk_null() {
    let err = run_test("restgnull").unwrap_err();
    assert!(error_to_message_chain(&err).contains("the Glk stream 0 is invalid"));
}

#[test]
fn linearsearch_invalid_key_size() {
    let err = run_test("linsrchks").unwrap_err();
    assert!(error_to_message_chain(&err).contains("invalid key size 3"));
}

#[test]
fn binarysearch_invalid_key_size() {
    let err = run_test("binsrchks").unwrap_err();
    assert!(error_to_message_chain(&err).contains("invalid key size 3"));
}

#[test]
fn linkedsearch_invalid_key_size() {
    let err = run_test("lkdsrchks").unwrap_err();
    assert!(error_to_message_chain(&err).contains("invalid key size 3"));
}

#[test]
fn debugtrap() {
    let err = run_test("debugtrap").unwrap_err();
    assert!(error_to_message_chain(&err).contains("debugtrap was executed with value 729"));
}

#[test]
fn glk_put_string_e2() {
    let err = run_test("gputstre2").unwrap_err();
    assert!(error_to_message_chain(&err)
        .contains("tried to dispatch `glk_put_string` with a Unicode string"));
}

#[test]
fn glk_put_string_e1() {
    let err = run_test("gputstre1").unwrap_err();
    assert!(error_to_message_chain(&err)
        .contains("tried to dispatch `glk_put_string` with a compressed string"));
}

#[test]
fn glk_put_string_non_string() {
    let err = run_test("gputstrxx").unwrap_err();
    assert!(error_to_message_chain(&err)
        .contains("tried to dispatch `glk_put_string` with a non-string"));
}

#[test]
fn glk_put_string_uni_e0() {
    let err = run_test("gputunie0").unwrap_err();
    assert!(error_to_message_chain(&err)
        .contains("tried to dispatch `glk_put_string_uni` with a Latin-1 string"));
}

#[test]
fn glk_put_string_uni_e1() {
    let err = run_test("gputunie1").unwrap_err();
    assert!(error_to_message_chain(&err)
        .contains("tried to dispatch `glk_put_string_uni` with a compressed string"));
}

#[test]
fn glk_put_string_uni_non_string() {
    let err = run_test("gputunixx").unwrap_err();
    assert!(error_to_message_chain(&err)
        .contains("tried to dispatch `glk_put_string_uni` with a non-string"));
}
