use std::error::Error;
use std::fmt;

use glauque::{Event, Glauque, Memory};

mod accel;
mod cpu;
mod decoding;
mod memory;
mod op;
mod quetzal;
mod rng;
mod runtime;

use accel::Acceleration;
use cpu::{store_at_dest, Cpu, Operand};
use memory::{MainMemory, MemoryReadError, NewMainMemoryError, Stack};
use op::OpcodeError;
use quetzal::{Quetzal, QuetzalParams};
use rng::Rng;
pub use runtime::RuntimeError;
use runtime::{call_function_without_stub, RuntimeErrorKind};

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct GlulxVersion {
    /// The major number of this version (e.g. the 3 in 3.1.2).
    pub major: u16,

    /// The minor number of this version (e.g. the 1 in 3.1.2).
    pub minor: u8,

    /// The subminor number of the version (e.g. the 2 in 3.1.2).
    pub subminor: u8, // "Subminor" is the term used in the spec.
}

impl GlulxVersion {
    pub fn new(major: u16, minor: u8, subminor: u8) -> Self {
        GlulxVersion {
            major,
            minor,
            subminor,
        }
    }

    pub fn as_u32(&self) -> u32 {
        (u32::from(self.major) << 16) + (u32::from(self.minor) << 8) + u32::from(self.subminor)
    }
}

impl fmt::Display for GlulxVersion {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}.{}.{}", self.major, self.minor, self.subminor)
    }
}

/// The Glulx version implemented by Coryphelle.
///
/// The current values mean we support Glulx version 2.0.0 and 3.1.z (for any z).
const GLULX_VERSION: GlulxVersion = GlulxVersion {
    major: 3,
    minor: 1,
    subminor: 3,
};

/// Determines if the version `major`.`minor`.`subminor` of Glulx is supported by Coryphelle.
fn is_glulx_version_supported(version: GlulxVersion) -> bool {
    // As per the spec, we should support version 2 if we implement version 3.
    if GLULX_VERSION.major == 3 && version.major == 2 {
        return true;
    } else if version.major != GLULX_VERSION.major || version.minor > GLULX_VERSION.minor {
        return false;
    }
    true
}

/// Describes the current status of a Coryphelle story.
#[derive(Debug, PartialEq, Eq)]
pub enum Status {
    /// The story is polling events.
    ///
    /// It will continue when an event is sent with [`Coryphelle::accept`].
    Polling,

    /// The story is awaiting a user input before continuing.
    ///
    /// It will continue when an event is sent with [`Coryphelle::accept`].
    Awaiting,

    /// The story has ended.
    Ended,
}

/// Describes the current status of a Coryphelle story.
///
/// It carries more information than [`Status`] that is necessary to make the internals work.
#[derive(Debug)]
enum StatusInternal {
    /// The story is running. Calling [`Coryphelle::step`] will continue its execution.
    Running,

    /// The story is awaiting an input before continuing. Calling
    /// [`Coryphelle::step`] will do nothing.
    AwaitingEvent {
        /// The address in main memory where the event will be stored.
        event_addr: u32,

        /// The store operand where the result of the `glk` opcode will be stored.
        operand: Operand,

        /// If true, the story is awaiting for an event via `select_poll` instead of `select`.
        polling: bool,
    },

    /// The story has ended. Calling [`Coryphelle::step`] will do nothing.
    Ended,
}

/// The type for errors returned when creating a `Coryphelle` instance fails.
#[derive(Debug, PartialEq, Eq)]
pub enum ValidateCoryphelleError {
    /// The story has an invalid magic number and thus is not a Glulx story.
    ///
    /// The member contains a `[u8; 4]` containing the magic number of the story.
    InvalidMagicNumber([u8; 4]),

    /// The story's image is too small.
    ///
    /// The member contains the size of the image given.
    TooSmall(usize),

    /// The story uses a version of Glulx not supported.
    ///
    /// The member contains the version that the story uses.
    UnsupportedGlulxVersion(GlulxVersion),
}

impl Error for ValidateCoryphelleError {}

impl From<NewMainMemoryError> for ValidateCoryphelleError {
    fn from(value: NewMainMemoryError) -> Self {
        use NewMainMemoryError::*;
        match value {
            InvalidMagicNumber(n) => ValidateCoryphelleError::InvalidMagicNumber(n),
            TooSmall(len) => ValidateCoryphelleError::TooSmall(len),
        }
    }
}

impl fmt::Display for ValidateCoryphelleError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            ValidateCoryphelleError::UnsupportedGlulxVersion(version) => write!(
                f, "unsupported Glulx version: {version}"
            ),
            // TODO: Remove String allocation?
            ValidateCoryphelleError::InvalidMagicNumber(magic_number) => write!(
                f, "invalid magic number: {:?} (ASCII for \"{}\") instead of [71, 108, 117, 108] (ASCII for \"Glul\")",
                magic_number, String::from_utf8_lossy(magic_number)
            ),
            ValidateCoryphelleError::TooSmall(len) => write!(
                f, "image too small: {len} while minimum is 256"
            )
        }
    }
}

// TODO: When specialisation is stabilised,
// make it possible to have a working `Coryphelle` without Glk functionality.
// Or maybe abstract the IO (Glk or custom) into a trait,
// so that we don't care if it's Glk or not.

/// A Glulx story.
///
/// This struct allows to play and interact with a story.
#[derive(Debug)]
pub struct Coryphelle<G> {
    status: StatusInternal,
    cpu: Cpu,
    main_memory: MainMemory,
    stack: Stack,
    glk: G,

    initial_snapshot: Option<Quetzal>,
    undo_snapshots: Vec<Quetzal>,

    /// The random number generator used by this instance of Coryphelle.
    rng: Rng,

    /// Struct handling accelerated functions.
    acceleration: Acceleration<G>,
}

/// A builder to create, validate and start a Coryphelle story.
pub struct Builder<Io: Glauque, const VALIDATED: bool> {
    /// The bytes of the Glulx file.
    ///
    /// Is `None` after validation.
    image: Option<Vec<u8>>,

    /// The Glauque implementation the story will use.
    ///
    /// Is `None` after validation.
    io: Option<Io>,

    /// The built story.
    ///
    /// Is `Some` after validation.
    cory: Option<Coryphelle<Io>>,
}

impl<Io: Glauque> Builder<Io, false> {
    /// Creates the builder with the given Glauque implementation and Glulx image.
    pub fn new(io: Io, image: Vec<u8>) -> Self {
        Builder {
            image: Some(image),
            io: Some(io),
            cory: None,
        }
    }

    /// Checks the Glulx image and returns an error if it's not valid.
    ///
    /// This function has to be called before the story can be started.
    pub fn validate(mut self) -> Result<Builder<Io, true>, ValidateCoryphelleError> {
        let main_memory = MainMemory::new(self.image.take().unwrap())?;

        // Check the Glulx version.
        let version = main_memory.glulx_version();
        if !is_glulx_version_supported(version) {
            return Err(ValidateCoryphelleError::UnsupportedGlulxVersion(version));
        }

        // Reserve the needed space for the stack.
        let stack = Stack::with_capacity(main_memory.stack_size().try_into().unwrap());

        let mut cory = Coryphelle {
            status: StatusInternal::Running,
            cpu: Cpu::new(),
            main_memory,
            stack,
            glk: self.io.take().unwrap(),
            initial_snapshot: None,
            undo_snapshots: Vec::new(),
            rng: Rng::new(),
            acceleration: Acceleration::new(),
        };

        // Save the initial VM state.
        cory.initial_snapshot = Some(cory.quetzal_snapshot(Operand::ConstantZero));

        Ok(Builder::<Io, true> {
            image: None,
            io: None,
            cory: Some(cory),
        })
    }
}

impl<Io: Glauque> Builder<Io, true> {
    /// Returns the built `Coryphelle` story after running it until it is awaiting input.
    ///
    /// The second element of the returned tuple indicates if a runtime error was encountered
    pub fn start(mut self) -> (Coryphelle<Io>, Result<Io::Output, RuntimeError>) {
        let mut story = self.cory.take().unwrap();
        // Get the story running to the next input (or the end).
        // No need for a call stub for the start function.
        let start_function = story.main_memory.start_function();
        call_function_without_stub(&mut story, start_function, &[]).expect("TODO handle error");
        let result = story.advance();
        (story, result)
    }
}

impl<G: Glauque> Coryphelle<G> {
    /// Gets a [`Builder`] with the given IO and story image.
    pub fn builder(io: G, image: Vec<u8>) -> Builder<G, false> {
        Builder::new(io, image)
    }

    pub fn glk(&self) -> &G {
        &self.glk
    }

    pub fn glk_mut(&mut self) -> &mut G {
        &mut self.glk
    }

    /// Gets the currend status of the story, whether it is awaiting input or has ended.
    pub fn status(&self) -> Status {
        match self.status {
            StatusInternal::AwaitingEvent { polling: true, .. } => Status::Polling,
            StatusInternal::AwaitingEvent { polling: false, .. } => Status::Awaiting,
            StatusInternal::Ended => Status::Ended,
            StatusInternal::Running => {
                unreachable!("it should be impossible to get the status while the story is running")
            }
        }
    }

    /// Saves the current VM state into a quetzal snapshot.
    ///
    /// The destination will be used to generate the call stub that will be
    /// pushed on the saved stack and popped when restoring the snapshot.
    fn quetzal_snapshot(&self, destination: Operand) -> Quetzal {
        let (dest_type, dest_addr) = destination.to_dest_type_addr(&self.main_memory);
        let snapshot = Quetzal::new(QuetzalParams {
            memory: &self.main_memory,
            stack: &self.stack,
            call_stub: [dest_type, dest_addr, self.cpu.pc, self.stack.frame_ptr],
            compress: false, // TODO: Add the possibility to compress the memory.
        });

        // Some assertions to make sure the story is correctly saved.
        #[cfg(debug_assertions)]
        {
            let mut found_mem = false;
            let mut found_stack = false;
            let mut found_heap = false;
            let mut found_identifier = false;

            for quetzal::Chunk { ty, content } in snapshot.iter() {
                match ty {
                    quetzal::ChunkType::UMem => {
                        if found_mem {
                            continue;
                        }
                        found_mem = true;

                        let mem_size: usize = u32::from_be_bytes(content[..4].try_into().unwrap())
                            .try_into()
                            .unwrap();
                        assert_eq!(mem_size, self.main_memory.as_bytes().len());

                        let ramstart = usize::try_from(self.main_memory.ramstart()).unwrap();
                        assert_eq!(&content[4..], &self.main_memory.as_bytes()[ramstart..])
                    }

                    quetzal::ChunkType::CMem => todo!(),

                    quetzal::ChunkType::Stks => {
                        if found_stack {
                            continue;
                        }
                        found_stack = true;

                        assert!(content.starts_with(self.stack.as_bytes()));
                        // Plus 16 to account for the pushed call stub.
                        assert_eq!(content.len(), self.stack.len() + 16);
                    }

                    quetzal::ChunkType::MAll => {
                        if found_heap {
                            continue;
                        }
                        found_heap = true;

                        let heap_addr = u32::from_be_bytes(content[..4].try_into().unwrap());
                        assert_eq!(heap_addr, self.main_memory.heap_start.unwrap().get());

                        let n_blocks: usize = u32::from_be_bytes(content[4..8].try_into().unwrap())
                            .try_into()
                            .unwrap();
                        assert_eq!(n_blocks, self.main_memory.heap_blocks.len());

                        use std::collections::BTreeMap;
                        let blocks = content[8..]
                            .chunks(8)
                            .map(|chunk| {
                                (
                                    u32::from_be_bytes(chunk[..4].try_into().unwrap()),
                                    u32::from_be_bytes(chunk[4..].try_into().unwrap()),
                                )
                            })
                            .collect::<BTreeMap<_, _>>();
                        assert_eq!(blocks, self.main_memory.heap_blocks);
                    }

                    quetzal::ChunkType::IFhd => {
                        if found_identifier {
                            continue;
                        }
                        found_identifier = true;
                        assert_eq!(content, &self.main_memory.as_bytes()[..128]);
                    }
                }
            }

            assert!(found_mem);
            assert!(found_stack);
            // No need to assert for the heap chunk: it's optional.
            assert!(found_identifier);
        } // end debug_assertions

        snapshot
    }

    /// Steps through the story's instructions until it's ready to accept input or it has ended.
    fn advance(&mut self) -> Result<G::Output, RuntimeError> {
        while matches!(self.status, StatusInternal::Running) {
            let result = self.step();

            // If we encounter an error while stepping,
            // show the error message with Glk and return it in case the user wants to handle it.
            //
            // TODO: Maybe send the message to Glk in `step` instead?
            if let Err(err) = result {
                let msgs = error_to_message_chain(&err);
                let msg_count = msgs.len();
                let mut msgs = msgs.into_iter();
                let mut msg = msgs.next().unwrap();
                if msg_count > 1 {
                    msg.push_str("\nCaused by");
                    for (i, part) in msgs.enumerate() {
                        msg.push_str(&format!("\n  {}. {}", i + 1, part))
                    }
                }
                self.glk.error(msg);
                return Err(err);
            }
        }
        Ok(self.glk.output())
    }

    /// Calls `self.glk.exit` and marks the story as ended.
    pub(crate) fn quit(&mut self) {
        self.glk.exit();
        self.status = StatusInternal::Ended;
    }

    /// Submits and processes a Glauque input event for the given window.
    ///
    /// This will cause to story to advance until it's once more ready to
    /// accept input.
    ///
    /// Does nothing if the story is not awaiting input (e.g. if it has ended).
    pub fn accept(&mut self, event: Event) -> Result<G::Output, RuntimeError> {
        let StatusInternal::AwaitingEvent {
            event_addr,
            operand,
            ..
        } = self.status
        else {
            // TODO: Maybe return something instead of panicking?
            panic!("called `Coryphelle::accept` while the story is not awaiting events");
        };

        // We pass the event to Glk so it can handle it and give it back.
        //
        // TODO: Maybe that means it should only take a (maybe mutable) ref?
        let event = self.glk.accept(&mut self.main_memory, event);
        // Then we write the event at the address
        // that was passed as an argument to Glk's `select`
        // when we dispatched it before awaiting for input.
        if event_addr == 0xFF_FF_FF_FF {
            for word in event.as_raw_ints() {
                self.stack.push_u32(word);
            }
        } else if event_addr != 0 {
            self.main_memory
                .write_u32s(event_addr, event.as_raw_ints().into_iter());
        }
        // Then we store 0 at the store operand from the `glk` opcode
        // (because this is the "end" of the opcode,
        // which was stopped early when dispatching `select`
        // so that the interpreter can be paused.)
        //
        // TODO: We use a Glk operand store error because that's what it is semantically,
        // but it doesn't reflect the inner working of the interpreter,
        // so maybe we should have a specific error.
        operand
            .store(&mut self.main_memory, &mut self.stack, 0)
            .map_err(|err| RuntimeError {
                pc: self.cpu.pc,
                kind: RuntimeErrorKind::Opcode(OpcodeError::Glk(op::GlkError::OperandStore(err))),
            })?;
        // And we go for another round!
        self.status = StatusInternal::Running;
        self.advance()
    }

    // Needs to have Glauque implemented because it calls `op_glk`.
    /// Executes the next instruction of the story.
    ///
    /// # Panics
    ///
    /// This function panics if `self.status` is `Status::Ended` or `Status::AwaitingEvent`.
    fn step(&mut self) -> Result<(), RuntimeError> {
        if !matches!(self.status, StatusInternal::Running) {
            panic!("called `Coryphelle::step` while the story is not running");
        }

        // Get the next opcode's number.
        let num = match self
            .main_memory
            .read_u8(self.cpu.pc)
            .map_err(|err| RuntimeError {
                pc: self.cpu.pc,
                kind: RuntimeErrorKind::Step(err.into()),
            })? {
            n @ 0x00..=0x7F => {
                self.cpu.pc += 1;
                n as u32
            }
            0x80..=0xBF => {
                let op = self
                    .main_memory
                    .read_u16(self.cpu.pc)
                    .map_err(|err| RuntimeError {
                        pc: self.cpu.pc,
                        kind: RuntimeErrorKind::Step(err.into()),
                    })?;
                let op = (op - 0x8000) as u32;
                self.cpu.pc += 2;
                op
            }
            0xC0..=0xCF => {
                let op = self
                    .main_memory
                    .read_u32(self.cpu.pc)
                    .map_err(|err| RuntimeError {
                        pc: self.cpu.pc,
                        kind: RuntimeErrorKind::Step(err.into()),
                    })?;
                let op = op - 0xC0000000;
                self.cpu.pc += 4;
                op
            }
            n => {
                return Err(RuntimeError {
                    pc: self.cpu.pc,
                    kind: RuntimeErrorKind::Step(StepError::OutOfRangeOpcode { first_byte: n }),
                })
            }
        };
        let result: Result<(), OpcodeError> = match num {
            0x0 => op::nop(self).map_err(Into::into),
            0x10 => op::add(self).map_err(Into::into),
            0x11 => op::sub(self).map_err(Into::into),
            0x12 => op::mul(self).map_err(Into::into),
            0x13 => op::div(self).map_err(Into::into),
            0x14 => op::mod_(self).map_err(Into::into),
            0x15 => op::neg(self).map_err(Into::into),
            0x18 => op::bitand(self).map_err(Into::into),
            0x19 => op::bitor(self).map_err(Into::into),
            0x1A => op::bitxor(self).map_err(Into::into),
            0x1B => op::bitnot(self).map_err(Into::into),
            0x1C => op::shiftl(self).map_err(Into::into),
            0x1D => op::sshiftr(self).map_err(Into::into),
            0x1E => op::ushiftr(self).map_err(Into::into),
            0x20 => op::jump(self).map_err(Into::into),
            0x22 => op::jz(self).map_err(Into::into),
            0x23 => op::jnz(self).map_err(Into::into),
            0x24 => op::jeq(self).map_err(Into::into),
            0x25 => op::jne(self).map_err(Into::into),
            0x26 => op::jlt(self).map_err(Into::into),
            0x27 => op::jge(self).map_err(Into::into),
            0x28 => op::jgt(self).map_err(Into::into),
            0x29 => op::jle(self).map_err(Into::into),
            0x2A => op::jltu(self).map_err(Into::into),
            0x2B => op::jgeu(self).map_err(Into::into),
            0x2C => op::jgtu(self).map_err(Into::into),
            0x2D => op::jleu(self).map_err(Into::into),
            0x30 => op::call(self).map_err(Into::into),
            0x31 => op::return_(self).map_err(Into::into),
            0x32 => op::catch(self).map_err(Into::into),
            0x33 => op::throw(self).map_err(Into::into),
            0x34 => op::tailcall(self).map_err(Into::into),
            0x40 => op::copy(self).map_err(Into::into),
            0x41 => op::copys(self).map_err(Into::into),
            0x42 => op::copyb(self).map_err(Into::into),
            0x44 => op::sexs(self).map_err(Into::into),
            0x45 => op::sexb(self).map_err(Into::into),
            0x48 => op::aload(self).map_err(Into::into),
            0x49 => op::aloads(self).map_err(Into::into),
            0x4A => op::aloadb(self).map_err(Into::into),
            0x4B => op::aloadbit(self).map_err(Into::into),
            0x4C => op::astore(self).map_err(Into::into),
            0x4D => op::astores(self).map_err(Into::into),
            0x4E => op::astoreb(self).map_err(Into::into),
            0x4F => op::astorebit(self).map_err(Into::into),
            0x50 => op::stkcount(self).map_err(Into::into),
            0x51 => op::stkpeek(self).map_err(Into::into),
            0x52 => op::stkswap(self).map_err(Into::into),
            0x53 => op::stkroll(self).map_err(Into::into),
            0x54 => op::stkcopy(self).map_err(Into::into),
            0x70 => op::streamchar(self).map_err(Into::into),
            0x71 => op::streamnum(self).map_err(Into::into),
            0x72 => op::streamstr(self).map_err(Into::into),
            0x73 => op::streamunichar(self).map_err(Into::into),
            0x100 => op::gestalt(self).map_err(Into::into),
            0x101 => op::debugtrap(self).map_err(Into::into),
            0x102 => op::getmemsize(self).map_err(Into::into),
            0x103 => op::setmemsize(self).map_err(Into::into),
            0x104 => op::jumpabs(self).map_err(Into::into),
            0x110 => op::random(self).map_err(Into::into),
            0x111 => op::setrandom(self).map_err(Into::into),
            0x120 => op::quit(self).map_err(Into::into),
            0x121 => op::verify(self).map_err(Into::into),
            0x122 => op::restart(self).map_err(Into::into),
            0x123 => op::save(self).map_err(Into::into),
            0x124 => op::restore(self).map_err(Into::into),
            0x125 => op::saveundo(self).map_err(Into::into),
            0x126 => op::restoreundo(self).map_err(Into::into),
            0x127 => op::protect(self).map_err(Into::into),
            0x128 => op::hasundo(self).map_err(Into::into),
            0x129 => op::discardundo(self).map_err(Into::into),
            0x130 => op::glk(self).map_err(Into::into),
            0x140 => op::getstringtbl(self).map_err(Into::into),
            0x141 => op::setstringtbl(self).map_err(Into::into),
            0x148 => op::getiosys(self).map_err(Into::into),
            0x149 => op::setiosys(self).map_err(Into::into),
            0x150 => op::linearsearch(self).map_err(Into::into),
            0x151 => op::binarysearch(self).map_err(Into::into),
            0x152 => op::linkedsearch(self).map_err(Into::into),
            0x160 => op::callf(self).map_err(Into::into),
            0x161 => op::callfi(self).map_err(Into::into),
            0x162 => op::callfii(self).map_err(Into::into),
            0x163 => op::callfiii(self).map_err(Into::into),
            0x170 => op::mzero(self).map_err(Into::into),
            0x171 => op::mcopy(self).map_err(Into::into),
            0x178 => op::malloc(self).map_err(Into::into),
            0x179 => op::mfree(self).map_err(Into::into),
            0x180 => op::accelfunc(self).map_err(Into::into),
            0x181 => op::accelparam(self).map_err(Into::into),
            0x190 => op::numtof(self).map_err(Into::into),
            0x191 => op::ftonumz(self).map_err(Into::into),
            0x192 => op::ftonumn(self).map_err(Into::into),
            0x198 => op::ceil(self).map_err(Into::into),
            0x199 => op::floor(self).map_err(Into::into),
            0x1A0 => op::fadd(self).map_err(Into::into),
            0x1A1 => op::fsub(self).map_err(Into::into),
            0x1A2 => op::fmul(self).map_err(Into::into),
            0x1A3 => op::fdiv(self).map_err(Into::into),
            0x1A4 => op::fmod(self).map_err(Into::into),
            0x1A8 => op::sqrt(self).map_err(Into::into),
            0x1A9 => op::exp(self).map_err(Into::into),
            0x1AA => op::log(self).map_err(Into::into),
            0x1AB => op::pow(self).map_err(Into::into),
            0x1B0 => op::sin(self).map_err(Into::into),
            0x1B1 => op::cos(self).map_err(Into::into),
            0x1B2 => op::tan(self).map_err(Into::into),
            0x1B3 => op::asin(self).map_err(Into::into),
            0x1B4 => op::acos(self).map_err(Into::into),
            0x1B5 => op::atan(self).map_err(Into::into),
            0x1B6 => op::atan2(self).map_err(Into::into),
            0x1C0 => op::jfeq(self).map_err(Into::into),
            0x1C1 => op::jfne(self).map_err(Into::into),
            0x1C2 => op::jflt(self).map_err(Into::into),
            0x1C3 => op::jfle(self).map_err(Into::into),
            0x1C4 => op::jfgt(self).map_err(Into::into),
            0x1C5 => op::jfge(self).map_err(Into::into),
            0x1C8 => op::jisnan(self).map_err(Into::into),
            0x1C9 => op::jisinf(self).map_err(Into::into),
            0x200 => op::numtod(self).map_err(Into::into),
            0x201 => op::dtonumz(self).map_err(Into::into),
            0x202 => op::dtonumn(self).map_err(Into::into),
            0x203 => op::ftod(self).map_err(Into::into),
            0x204 => op::dtof(self).map_err(Into::into),
            0x208 => op::dceil(self).map_err(Into::into),
            0x209 => op::dfloor(self).map_err(Into::into),
            0x210 => op::dadd(self).map_err(Into::into),
            0x211 => op::dsub(self).map_err(Into::into),
            0x212 => op::dmul(self).map_err(Into::into),
            0x213 => op::ddiv(self).map_err(Into::into),
            0x214 => op::dmodr(self).map_err(Into::into),
            0x215 => op::dmodq(self).map_err(Into::into),
            0x218 => op::dsqrt(self).map_err(Into::into),
            0x219 => op::dexp(self).map_err(Into::into),
            0x21A => op::dlog(self).map_err(Into::into),
            0x21B => op::dpow(self).map_err(Into::into),
            0x220 => op::dsin(self).map_err(Into::into),
            0x221 => op::dcos(self).map_err(Into::into),
            0x222 => op::dtan(self).map_err(Into::into),
            0x223 => op::dasin(self).map_err(Into::into),
            0x224 => op::dacos(self).map_err(Into::into),
            0x225 => op::datan(self).map_err(Into::into),
            0x226 => op::datan2(self).map_err(Into::into),
            0x230 => op::jdeq(self).map_err(Into::into),
            0x231 => op::jdne(self).map_err(Into::into),
            0x232 => op::jdlt(self).map_err(Into::into),
            0x233 => op::jdle(self).map_err(Into::into),
            0x234 => op::jdgt(self).map_err(Into::into),
            0x235 => op::jdge(self).map_err(Into::into),
            0x238 => op::jdisnan(self).map_err(Into::into),
            0x239 => op::jdisinf(self).map_err(Into::into),
            n @ 0..=0x0FFFFFFF => {
                return Err(RuntimeError {
                    pc: self.cpu.pc,
                    kind: RuntimeErrorKind::Step(StepError::UnknownOpcode { opcode_number: n }),
                })
            }
            n => unreachable!("opcode {:#X} out of range", n),
        };
        result.map_err(|err| RuntimeError {
            pc: self.cpu.pc,
            kind: err.into(),
        })
    }

    /// Restores the VM state to the one stored in the given Quetzal snapshot.
    ///
    /// If `heap_sorted` is true, the heap blocks from the snapshot won't be
    /// sorted before loading it (so we save time when we know it's already
    /// sorted, for example in an undo snapshot the interpreter generated).
    ///
    /// The function returns true if the load succeeded, and false otherwise.
    fn load_quetzal(&mut self, snapshot: &Quetzal) -> bool {
        let mut found_mem = false;
        let mut found_stack = false;
        let mut found_heap = false;
        let mut found_identifier = false;

        for quetzal::Chunk { ty, content } in snapshot.iter() {
            match ty {
                quetzal::ChunkType::UMem => {
                    if found_mem {
                        continue;
                    }
                    found_mem = true;

                    // Get the protected memory.
                    let memory::Block { start, len } = self.main_memory.protected;
                    let start = usize::try_from(start).unwrap();
                    let len = usize::try_from(len).unwrap();
                    let end = start + len;
                    let mut protected = Vec::with_capacity(len);
                    if (start, len) != (0, 0) && start < self.main_memory.as_bytes().len() {
                        protected.extend_from_slice(
                            &self.main_memory.as_bytes()
                                [start..end.min(self.main_memory.as_bytes().len())],
                        )
                    }
                    // Add zeros if the protected range goes past the memory's end.
                    protected.extend(std::iter::repeat(0).take(len - protected.len()));

                    // Resize the memory.
                    let mem_size = u32::from_be_bytes(content[..4].try_into().unwrap());
                    self.main_memory.resize(mem_size.try_into().unwrap());

                    // Load the memory.
                    self.main_memory.splice(
                        self.main_memory.ramstart().try_into().unwrap(),
                        self.main_memory.len(),
                        &content[4..],
                    );

                    // Restore the protected memory
                    //
                    // TODO: instead of saving the protected memory into a vec,
                    // we could splice the memory twice:
                    // once below the protected memory,
                    // and once above the protected memory.
                    if (start, len) != (0, 0) && start < self.main_memory.as_bytes().len() {
                        self.main_memory.splice(
                            start,
                            end.min(self.main_memory.as_bytes().len()),
                            &protected,
                        );
                    }
                }

                quetzal::ChunkType::CMem => todo!(),

                quetzal::ChunkType::Stks => {
                    if found_stack {
                        continue;
                    }
                    found_stack = true;

                    self.stack.clear();
                    self.stack.push_slice(content);
                    let (dest_type, dest_addr) = self.stack.pop_call_stub(&mut self.cpu);
                    // 0xFF_FF_FF_FF means we are restoring a save file.
                    store_at_dest(self, dest_type, dest_addr, 0xFF_FF_FF_FF)
                        .expect("TODO handle error");
                }

                quetzal::ChunkType::MAll => {
                    if found_heap {
                        continue;
                    }
                    found_heap = true;

                    // Check if the heap blocks chunk has the right size.
                    if content.len() % 8 != 0 {
                        return false;
                    }

                    let heap_addr = u32::from_be_bytes(content[..4].try_into().unwrap());
                    self.main_memory.heap_start = Some(
                        heap_addr
                            .try_into()
                            .expect("TODO handle restoring null heap addr"),
                    );

                    // Check if the heap contains the right number of blocks.
                    // A bit redundant with the check before,
                    // but it does no harm.
                    let n_block = u32::from_be_bytes(content[4..8].try_into().unwrap());
                    if (content.len() - 8) / 8 != usize::try_from(n_block).unwrap() {
                        return false;
                    }

                    self.main_memory.heap_blocks = content[8..]
                        .chunks(8)
                        .map(|chunk| {
                            (
                                u32::from_be_bytes(chunk[0..4].try_into().unwrap()),
                                u32::from_be_bytes(chunk[4..8].try_into().unwrap()),
                            )
                        })
                        .collect();
                }

                quetzal::ChunkType::IFhd => {
                    if found_identifier {
                        continue;
                    }
                    found_identifier = true;

                    // TODO: Maybe add a way to skip this check when we are sure of our snapshot?
                    //
                    // TODO: Display a message so that the user knows exactly what has happened.
                    if &self.main_memory.as_bytes()[..128] != content {
                        return false;
                    }
                }
            }
        }

        // No need to assert that we got all required chunks,
        // because `Quetzal` snapshots guarantee that it's the case.

        true
    }
}

/// An error indicating that something went wrong while stepping through the opcodes.
#[derive(Debug)]
enum StepError {
    /// Reading the main memory failed with the inner error.
    MemoryRead(MemoryReadError),

    /// The opcode was out of range.
    #[non_exhaustive]
    OutOfRangeOpcode {
        /// The first byte of the opcode number.
        first_byte: u8,
    },

    /// The opcode with the given number is not known.
    #[non_exhaustive]
    UnknownOpcode { opcode_number: u32 },
}

impl fmt::Display for StepError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match *self {
            StepError::MemoryRead(_) => write!(f, "failed to read main memory"),
            StepError::OutOfRangeOpcode { first_byte } => {
                write!(f, "opcode with first byte {first_byte:#X} is out of range")
            }
            StepError::UnknownOpcode { opcode_number } => {
                write!(f, "opcode number {opcode_number:#X} is unknown")
            }
        }
    }
}

impl Error for StepError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        match self {
            StepError::MemoryRead(err) => Some(err),
            StepError::OutOfRangeOpcode { .. } => None,
            StepError::UnknownOpcode { .. } => None,
        }
    }
}

impl From<MemoryReadError> for StepError {
    fn from(value: MemoryReadError) -> Self {
        Self::MemoryRead(value)
    }
}

/// A helper function to convert an error to a message chain by walking its sources.
fn error_to_message_chain<E: Error>(err: &E) -> Vec<String> {
    let mut msgs = vec![err.to_string()];
    let mut source = err.source();
    while let Some(err) = source {
        msgs.push(err.to_string());
        source = err.source()
    }
    msgs
}

#[cfg(test)]
mod tests {
    use super::*;

    // TODO
    // use glauque::CheapGlauque;

    // /// A helper function to construct an artificial image with the given values.
    // ///
    // /// Some values are set to arbitrary number (see comments in the function.)
    // fn construct_image(
    //     magic_number: [u8; 4],
    //     version: (u16, u8, u8),
    //     descriptive: [u8; 4]
    // ) -> Vec<u8> {
    //     let mut image = Vec::new();
    //     image.extend_from_slice(&magic_number);
    //     image.extend_from_slice(&[
    //         // Glulx version.
    //         (version.0 >> 4) as u8, // More significant part of major.
    //         (version.0 & 0b11111111) as u8, // Less significant part of major.
    //         version.1,
    //         version.2,
    //         // RAMSTART at 256.
    //         0x00, 0x00, 0x01, 0x00,
    //         // EXTSTART at 512.
    //         0x00, 0x00, 0x02, 0x00,
    //         // ENDMEM at 1024.
    //         0x00, 0x00, 0x04, 0x00,
    //         // Stack size at 729.
    //         0x00, 0x00, 0x02, 0xD9,
    //         // Start function at 40.
    //         0x00, 0x00, 0x00, 0x28,
    //         // Decoding table at 44.
    //         0x00, 0x00, 0x00, 0x2C,
    //         // Checksum (will be computed later).
    //         0, 0, 0, 0,
    //     ]);
    //     image.extend_from_slice(&descriptive);
    //     // Fill the image with 9s up to EXTSTART.
    //     image.resize(512, 9);

    //     // Compute checksum.
    //     let mut checksum: u32 = 0;
    //     for i in 0..image.len() / 4 {
    //         checksum = checksum.wrapping_add(u32::from_be_bytes(
    //             image[i * 4..i * 4 + 4].try_into().unwrap()
    //         ));
    //     }

    //     image[32] = (checksum >> 24) as u8;
    //     image[33] = ((checksum & 0x00_FF_00_FF) >> 16) as u8;
    //     image[34] = ((checksum & 0x00_00_FF_00) >> 8) as u8;
    //     image[35] = (checksum & 0x00_00_00_FF) as u8;

    //     image
    // }

    #[test]
    fn test_is_glulx_version_supported() {
        // Too low.
        assert!(!is_glulx_version_supported(GlulxVersion::new(1, 0, 0)));
        assert!(!is_glulx_version_supported(GlulxVersion::new(1, 2, 3)));

        // Supported.
        // Version 2.y.z should be supported
        assert!(is_glulx_version_supported(GlulxVersion::new(2, 0, 0)));
        assert!(is_glulx_version_supported(GlulxVersion::new(2, 1, 3)));
        // Version 3.[0-1].z should be supported..).
        assert!(is_glulx_version_supported(GlulxVersion::new(3, 0, 0)));
        assert!(is_glulx_version_supported(GlulxVersion::new(3, 0, 2)));
        assert!(is_glulx_version_supported(GlulxVersion::new(3, 1, 2)));
        assert!(is_glulx_version_supported(GlulxVersion::new(3, 1, 3)));
        // ... even if their subminor is higher than the one Coryphelle implements
        assert!(is_glulx_version_supported(GlulxVersion::new(3, 1, 255)));

        // Too high.
        assert!(!is_glulx_version_supported(GlulxVersion::new(3, 2, 0)));
        assert!(!is_glulx_version_supported(GlulxVersion::new(3, 2, 3)));
        assert!(!is_glulx_version_supported(GlulxVersion::new(4, 0, 0)));
        assert!(!is_glulx_version_supported(GlulxVersion::new(4, 3, 2)));
    }

    // #[test]
    // fn test_coryphelle_from_image_magic_number() {
    //     // Invalid magic number.
    //     let image = construct_image(*b"Nope", (3, 1, 3), *b"Info");
    //     assert_eq!(
    //         Coryphelle::from_vec(CheapGlauque::new(), image).unwrap_err(),
    //         NewCoryphelleError::InvalidMagicNumber(*b"Nope")
    //     );

    //     // Valid magic number.
    //     let image = construct_image(*b"Glul", (3, 1, 3), *b"Info");
    //     assert!(Coryphelle::from_vec(CheapGlauque::new(), image).is_ok());
    // }

    // #[test]
    // fn test_coryphelle_from_image_glulx_version() {
    //     // Unsupported Glulx version
    //     let image = construct_image(*b"Glul", (1, 2, 3), *b"Info");
    //     assert_eq!(
    //         Coryphelle::from_vec(CheapGlauque::new(), image).unwrap_err(),
    //         NewCoryphelleError::UnsupportedGlulxVersion(GlulxVersion::new(1, 2, 3))
    //     );

    //     // Supported Glulx version.
    //     let image = construct_image(*b"Glul", (2, 3, 1), *b"Info");
    //     assert!(Coryphelle::from_vec(CheapGlauque::new(), image).is_ok());
    //     let image = construct_image(*b"Glul", (3, 1, 2), *b"Info");
    //     assert!(Coryphelle::from_vec(CheapGlauque::new(), image).is_ok());
    // }

    // #[test]
    // fn test_header() {
    //     let image = construct_image(*b"Glul", (3, 1, 2), *b"Info");
    //     let story = Coryphelle::from_vec(CheapGlauque::new(), image.clone()).unwrap();

    //     // The memory has been copied into the story.
    //     assert!(story.main_memory.as_bytes().starts_with(&image));

    //     // The magic number of the story.
    //     assert_eq!(story.main_memory.magic_number(), b"Glul");

    //     // The Glulx version of the story.
    //     assert_eq!(story.main_memory.glulx_version(), GlulxVersion::new(3, 1, 2));

    //     // The RAMSTART of the story.
    //     assert_eq!(story.main_memory.ramstart(), 256);

    //     // The EXTSTART of the story.
    //     assert_eq!(story.main_memory.extstart(), 512);

    //     // The ENDMEM of the story.
    //     assert_eq!(story.main_memory.endmem(), 1024);

    //     // The stack size of the story.
    //     assert_eq!(story.main_memory.stack_size(), 729);

    //     // The stack size of the story.
    //     assert_eq!(story.main_memory.start_function(), 40);

    //     // The decoding table of the story.
    //     assert_eq!(story.main_memory.decoding_table(), 44);

    //     // The verify function and the checksum (which verify uses).
    //     // assert!(story.verify());

    //     // The memory has a capacity of ENDMEM.
    //     // assert!(story.main_memory.capacity() >= usize::try_from(story.main_memory.endmem()).unwrap());

    //     // The stack has a capacity of stack size.
    //     assert!(story.stack.capacity() >= usize::try_from(story.main_memory.stack_size()).unwrap());
    // }
}
