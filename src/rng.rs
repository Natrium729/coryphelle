use std::cmp::Ordering;

use rand::{Rng as _, SeedableRng};
use rand_pcg::Pcg32;

#[derive(Debug)]
pub(crate) struct Rng(Pcg32);

impl Rng {
    pub(crate) fn new() -> Self {
        Self(Pcg32::from_entropy())
    }

    pub(crate) fn from_seed(seed: u32) -> Self {
        Self(Pcg32::seed_from_u64(seed as u64))
    }

    /// Generates a random value like the random opcode.
    ///
    /// If `x` is 0, returns a number in the full 32-bit range.
    /// If `x` is positive, returns a number between 0 and `x` - 1, inclusive.
    /// If `x` is negative, returns a number between `x` + 1 and 0, inclusive.
    pub(crate) fn gen(&mut self, x: i32) -> i32 {
        match x.cmp(&0) {
            Ordering::Greater => self.0.gen_range(0..x),
            Ordering::Less => self.0.gen_range(x + 1..=0),
            Ordering::Equal => self.0.gen(),
        }
    }
}
