use std::{error::Error, fmt};

use crate::memory::{MainMemory, MemoryReadError};

/// A struct representing a decoding table of a story as a binary tree.
#[derive(Debug)]
pub(crate) struct Tree {
    /// The address of the current decoding table. A value of 0 means there are no decoding table set.
    pub(crate) addr: u32,

    pub(crate) len: u32,

    /// The root of the tree.
    ///
    /// A value of `None` means the tree hasn't been cached.
    pub(crate) root: Option<Node>,
}

/// A node of a decoding tree (used to print compressed strings).
#[derive(Debug)]
pub(crate) enum Node {
    /// A branch leading to its first or second node if a given bit is 0 or 1.
    Branch(Box<Node>, Box<Node>),

    /// Indicates the end of a string.
    Terminator,

    /// Indicates that a byte should be printed (usually as Latin-1).
    Byte(u8),

    /// Indicates that the bytes at the given place should be printed (usually as a Latin-1 string).
    ByteString {
        /// The address of the start of the byte string.
        start: u32,

        /// The address of the end of the byte string.
        end: u32,
    },

    /// Indicates that a `u32` should be printed (usually as Unicode).
    Word(u32),

    /// Indicates that the `u32`s at the given place should be printed (usually as a Unicode string).
    WordString {
        /// The address of the start of the word string.
        start: u32,

        /// The address of the end of the word string.
        end: u32,
    },

    /// Indicates that the string at some address will be printed, or a function called (with
    /// the given arguments). The indirection can be double.
    ///
    /// If it's an indirection without arguments, `args_start` is zero.
    Indirection {
        addr: u32,

        /// If true, means that we should read a `u32` at the given address and that will be
        /// the address of the string to print/function to call.
        double: bool,

        args_start: u32,
        args_end: u32,
    },
}

impl Node {
    /// Creates a `DecodingTree` from the decoding table at the given address in memory.
    pub(crate) fn new(mem: &MainMemory, addr: u32) -> Result<Self, NewNodeError> {
        let content_addr = addr + 1;
        let content_addr_usize = usize::try_from(content_addr).unwrap();

        match mem.read_u8(addr).map_err(|err| NewNodeError {
            addr,
            kind: err.into(),
        })? {
            0x00 => {
                let dir_0 = mem.read_u32(content_addr).map_err(|err| NewNodeError {
                    addr,
                    kind: err.into(),
                })?;
                let dir_1 = mem.read_u32(content_addr + 4).map_err(|err| NewNodeError {
                    addr,
                    kind: err.into(),
                })?;
                let child_0 = Node::new(mem, dir_0).map_err(|err| NewNodeError {
                    addr,
                    kind: err.into(),
                })?;
                let child_1 = Node::new(mem, dir_1).map_err(|err| NewNodeError {
                    addr,
                    kind: err.into(),
                })?;
                Ok(Node::Branch(Box::new(child_0), Box::new(child_1)))
            }
            0x01 => Ok(Node::Terminator),
            0x02 => {
                let byte = mem.read_u8(content_addr).map_err(|err| NewNodeError {
                    addr,
                    kind: err.into(),
                })?;
                Ok(Node::Byte(byte))
            }
            0x03 => {
                let len = mem.as_bytes()[content_addr_usize..]
                    .iter()
                    .take_while(|&&byte| byte != 0)
                    .count();
                Ok(Node::ByteString {
                    start: content_addr,
                    end: (content_addr_usize + len).try_into().unwrap(),
                })
            }
            0x04 => {
                let word = mem.read_u32(content_addr).map_err(|err| NewNodeError {
                    addr,
                    kind: err.into(),
                })?;
                Ok(Node::Word(word))
            }
            0x05 => {
                let len = 4 * mem.as_bytes()[content_addr_usize..]
                    .chunks(4)
                    .take_while(|&bytes| bytes != [0, 0, 0, 0])
                    .count();
                Ok(Node::WordString {
                    start: content_addr,
                    end: (content_addr_usize + len).try_into().unwrap(),
                })
            }
            0x08 => Ok(Node::Indirection {
                addr: mem.read_u32(content_addr).map_err(|err| NewNodeError {
                    addr,
                    kind: err.into(),
                })?,
                double: false,
                args_start: 0,
                args_end: 0,
            }),
            0x09 => Ok(Node::Indirection {
                addr: mem.read_u32(content_addr).map_err(|err| NewNodeError {
                    addr,
                    kind: err.into(),
                })?,
                double: true,
                args_start: 0,
                args_end: 0,
            }),
            0x0A => {
                let indirect_addr = mem.read_u32(content_addr).map_err(|err| NewNodeError {
                    addr,
                    kind: err.into(),
                })?;
                let arg_count = mem.read_u32(content_addr + 4).map_err(|err| NewNodeError {
                    addr,
                    kind: err.into(),
                })?;
                Ok(Node::Indirection {
                    addr: indirect_addr,
                    double: false,
                    args_start: content_addr + 8,
                    args_end: content_addr + 8 + 4 * arg_count,
                })
            }
            0x0B => {
                let indirect_addr = mem.read_u32(content_addr).map_err(|err| NewNodeError {
                    addr,
                    kind: err.into(),
                })?;
                let arg_count = mem.read_u32(content_addr + 4).map_err(|err| NewNodeError {
                    addr,
                    kind: err.into(),
                })?;
                Ok(Node::Indirection {
                    addr: indirect_addr,
                    double: true,
                    args_start: content_addr + 8,
                    args_end: content_addr + 8 + 4 * arg_count,
                })
            }
            n => Err(NewNodeError {
                addr,
                kind: NewNodeErrorKind::InvalidNodeType(InvalidNodeTypeError(n)),
            }),
        }
    }
}

#[derive(Debug)]
#[non_exhaustive]
pub(crate) struct NewNodeError {
    addr: u32,
    kind: NewNodeErrorKind,
}

impl fmt::Display for NewNodeError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "failed to create decoding tree node from address {}",
            self.addr
        )
    }
}

impl Error for NewNodeError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        match &self.kind {
            NewNodeErrorKind::MemoryRead(err) => Some(err),
            NewNodeErrorKind::NewNode(err) => Some(err),
            NewNodeErrorKind::InvalidNodeType(err) => Some(err),
        }
    }
}

#[derive(Debug)]
enum NewNodeErrorKind {
    MemoryRead(MemoryReadError),
    NewNode(Box<NewNodeError>),
    InvalidNodeType(InvalidNodeTypeError),
}

impl From<MemoryReadError> for NewNodeErrorKind {
    fn from(value: MemoryReadError) -> Self {
        Self::MemoryRead(value)
    }
}

impl From<NewNodeError> for NewNodeErrorKind {
    fn from(value: NewNodeError) -> Self {
        Self::NewNode(value.into())
    }
}

#[derive(Debug)]
#[non_exhaustive]
struct InvalidNodeTypeError(u8);

impl fmt::Display for InvalidNodeTypeError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{:#X} is not a valid decoding tree node type", self.0)
    }
}

impl Error for InvalidNodeTypeError {}

#[derive(Debug)]
pub(crate) enum StringPart {
    Bytes(Vec<u8>),
    Words(Vec<u32>),
}

#[derive(Debug)]
pub(crate) struct DecodedString(Vec<StringPart>);

impl DecodedString {
    pub(crate) fn new() -> Self {
        Self(Vec::new())
    }

    pub(crate) fn parts(&self) -> &[StringPart] {
        &self.0
    }

    pub(crate) fn drain(&mut self) -> impl Iterator<Item = StringPart> + '_ {
        self.0.drain(..)
    }

    pub(crate) fn push_byte(&mut self, byte: u8) {
        if let Some(StringPart::Bytes(v)) = self.0.last_mut() {
            v.push(byte);
        } else {
            self.0.push(StringPart::Bytes(vec![byte]));
        }
    }

    pub(crate) fn push_word(&mut self, word: u32) {
        if let Some(StringPart::Words(v)) = self.0.last_mut() {
            v.push(word);
        } else {
            self.0.push(StringPart::Words(vec![word]));
        }
    }
}
