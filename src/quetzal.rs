use crate::memory::{MainMemory, Stack};

/// An iterator over the valid chunks of a Quetzal IFF.
pub(crate) struct QuetzalReader<'a> {
    data: &'a [u8],
    cursor: usize,
}

impl<'a> QuetzalReader<'a> {
    /// Creates a `QuetzalReader` for the given IFF data.
    ///
    /// Returns `None` if the data is not IFF data containing a `FORM` chunk
    /// with ID `IFZS`.
    pub(crate) fn new(data: &[u8]) -> Option<QuetzalReader> {
        if data.len() <= 12 {
            return None;
        }

        if &data[0..4] != b"FORM" || &data[8..12] != b"IFZS" {
            return None;
        }

        Some(QuetzalReader {
            data,
            cursor: 12, // Start after `IFZS`.
        })
    }
}

/// A Quetzal chunk obtained when iterating over a `QuetzalReader`.
#[derive(Debug)]
pub(crate) struct Chunk<'a> {
    pub(crate) ty: ChunkType,
    pub(crate) content: &'a [u8],
}

/// Represents the possible types of a Quetzal chunk.
#[derive(Debug)]
pub(crate) enum ChunkType {
    /// Uncompressed dynamic memory.
    UMem,

    /// Compressed dynamic memory.
    CMem,

    /// Stack.
    Stks,

    /// Heap.
    MAll,

    /// Associated story file.
    IFhd,
}

impl TryFrom<[u8; 4]> for ChunkType {
    type Error = ();

    fn try_from(value: [u8; 4]) -> Result<Self, Self::Error> {
        match &value {
            b"UMem" => Ok(Self::UMem),
            b"CMem" => Ok(Self::CMem),
            b"Stks" => Ok(Self::Stks),
            b"MAll" => Ok(Self::MAll),
            b"IFhd" => Ok(Self::IFhd),
            _ => Err(()),
        }
    }
}

impl<'a> Iterator for QuetzalReader<'a> {
    type Item = Chunk<'a>;

    fn next(&mut self) -> Option<Self::Item> {
        loop {
            // We add 8 because at least need the bytes for ID and the length.
            if self.cursor + 8 > self.data.len() {
                return None;
            }

            // Get the type.
            let ty_arr: [u8; 4] = self.data[self.cursor..self.cursor + 4].try_into().unwrap();
            let ty = ty_arr.try_into();
            self.cursor += 4;

            // Get the length.
            let length: usize =
                u32::from_be_bytes(self.data[self.cursor..self.cursor + 4].try_into().unwrap())
                    .try_into()
                    .unwrap();
            self.cursor += 4;

            // If the type is not a valid Quetzal type, we skip this chunk.
            let Ok(ty) = ty else {
                self.cursor += length;
                self.cursor += self.cursor % 2;
                continue;
            };

            // Get the content.
            let contents_start = self.cursor;
            self.cursor += length;
            // If we go beyond the length, we simply take the rest of the file.
            // If that happens, it means the IFF file is malformed,
            // but we'll be lenient.
            self.cursor = self.cursor.min(self.data.len());
            let content = &self.data[contents_start..self.cursor];
            self.cursor += self.cursor % 2;

            return Some(Chunk { ty, content });
        }
    }
}

/// A snapshot of a VM state (i.e. a save file),
/// whose data is in the Quetzal format.
#[derive(Debug)]
pub(crate) struct Quetzal(pub(crate) Vec<u8>);

pub(crate) struct QuetzalParams<'mem, 'stack> {
    pub(crate) memory: &'mem MainMemory,
    pub(crate) stack: &'stack Stack,
    pub(crate) call_stub: [u32; 4],
    pub(crate) compress: bool,
}

impl Quetzal {
    /// Creates a new Quetzal snapshot with the given memory, stack and heap.
    ///
    /// The call stub will be used when restoring the story from this Quetzal snapshot.
    ///
    /// The heap is a slice of tuple in the form `(block_start, block_len)`.
    ///
    /// If compressed is true, the Quetzal snapshot will be compressed.
    pub(crate) fn new(
        QuetzalParams {
            memory,
            stack,
            call_stub,
            compress,
        }: QuetzalParams,
    ) -> Self {
        let stack = stack.as_bytes();

        let mut data = Vec::with_capacity(
            12 // 4 for "FORM" + 4 for it's length + 4 for "IFZS".
            + 4 * 8 // 8 for the ID and size of each chunk.
            + 8 // 8 for the heap start address and number of blocks.
            + 128 // The story-file identifier.
            + 1 // In case there is padding at the end.
            + memory.len() // Will allocate too much if compressing it, but it'll do.
            + stack.len()
            + memory.heap_blocks.len() * 8, // 8 for the start and length of each heap block.
        );

        // 0. The start of the file.

        data.extend_from_slice(&[
            b'F', b'O', b'R', b'M', 0x00, 0x00, 0x00,
            0x00, // Length of the FORM chunk (will be filled at the end).
            b'I', b'F', b'Z', b'S', // ASCII for "IFZS".
        ]);

        // 1. The story-file identifier.

        let mem_bytes = memory.as_bytes();

        data.extend_from_slice(b"IFhd");
        // The chunk length (128 bytes).
        data.extend_from_slice(&128_u32.to_be_bytes());
        // The first 128 bytes of the story header.
        data.extend_from_slice(&mem_bytes[0..128]);

        // 2. The dynamic memory.

        let mem_size = u32::try_from(memory.len()).unwrap();
        let ramstart = u32::from_be_bytes(mem_bytes[8..12].try_into().unwrap());
        if compress {
            todo!("creating compressed Quetzal")
        } else {
            data.extend_from_slice(b"UMem");
            // The chunk length.
            // 4 for the memory size, minus RAMSTART because we are not storing the ROM.
            data.extend_from_slice(&(4 + mem_size - ramstart).to_be_bytes());
            // The size of the memory (both ROM and RAM, possibly extended).
            data.extend_from_slice(&mem_size.to_be_bytes());
            // The memory.
            let ramstart_usize = usize::try_from(ramstart).unwrap();
            data.extend_from_slice(&mem_bytes[ramstart_usize..]);
        };

        // 3. The stack

        data.extend_from_slice(b"Stks");
        // The chunk length. We add 16 for the call stub that will be pushed below.
        let stack_len = stack.len() + 16;
        data.extend_from_slice(&u32::try_from(stack_len).unwrap().to_be_bytes());
        // The stack.
        data.extend_from_slice(stack);
        // The call stub.
        for value in call_stub {
            data.extend_from_slice(&value.to_be_bytes());
        }

        // 4. The heap

        if let Some(heap_start) = memory.heap_start {
            let heap_start = heap_start.get();
            let heap_block_count = u32::try_from(memory.heap_blocks.len()).unwrap();
            data.extend_from_slice(b"MAll");
            // The chunk length.
            // 8 for the heap start address and the number of block, plus 8 for each block.
            data.extend_from_slice(&(8 + heap_block_count * 8).to_be_bytes());
            // The heap start address and the number of blocks
            data.extend_from_slice(&heap_start.to_be_bytes());
            data.extend_from_slice(&heap_block_count.to_be_bytes());
            // The start and length of each block.
            for (addr, len) in memory.heap_blocks.iter() {
                data.extend_from_slice(&addr.to_be_bytes());
                data.extend_from_slice(&len.to_be_bytes());
            }
        }

        // 5. Setting the length of "FORM".

        // We remove 8 for "FORM" and its length.
        for (i, byte) in (u32::try_from(data.len()).unwrap() - 8)
            .to_be_bytes()
            .iter()
            .enumerate()
        {
            data[4 + i] = *byte;
        }

        // 6. The padding.

        if data.len() % 2 != 0 {
            data.push(0x00);
        }

        Quetzal(data)
    }

    // TODO: Add real error type.
    //
    // TODO: This function iterates over the chunks to check them,
    // but we will often iterate over them just after to load the snapshot.
    // It would maybe be better to check the validity in `QuetzalReader`,
    // So that we could iterate only once.
    //
    /// Creates a Quetzal snapshot from the given IFF data.
    ///
    /// While the resulting snapshot will always be valid,
    /// but it will keep duplicated chunks.
    ///
    /// It is the responsibility of users to check for duplicated chunks
    /// and skip them if necessary.
    pub(crate) fn from_data(data: Vec<u8>) -> Result<Self, ()> {
        let reader = QuetzalReader::new(&data).ok_or_else(|| ())?;

        let mut found_mem = false;
        let mut found_stack = false;
        let mut found_heap = false;
        let mut found_identifier = false;

        for Chunk { ty, content } in reader {
            match ty {
                ChunkType::UMem => found_mem = true,
                ChunkType::CMem => found_mem = true,
                ChunkType::Stks => found_stack = true,
                ChunkType::MAll => {
                    if found_heap {
                        continue;
                    }
                    found_heap = true;

                    if content.len() < 8 {
                        todo!("error when MAll chunk too small");
                    }

                    let n_blocks: usize = u32::from_be_bytes(content[4..8].try_into().unwrap())
                        .try_into()
                        .unwrap();
                    if (content.len() - 8) % n_blocks != 0 {
                        // There should be 8 bytes per block,
                        // plus the 8 bytes for the two values at the start.
                        todo!("error on wrong number of heap blocks");
                    }
                }
                ChunkType::IFhd => {
                    if found_identifier {
                        continue;
                    }
                    found_identifier = true;

                    if content.len() != 128 {
                        todo!("error on story-file identifier with wrong length")
                    }
                }
            }
        }

        if !found_mem {
            return Err(());
        }
        if !found_stack {
            return Err(());
        }
        if !found_identifier {
            return Err(());
        }

        Ok(Quetzal(data))
    }

    /// Returns an iterator over the chunks of the Quetzal data.
    pub(crate) fn iter(&self) -> QuetzalReader {
        QuetzalReader::new(&self.0).unwrap()
    }
}
