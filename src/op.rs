use std::{error::Error, fmt, num::NonZeroU32};

use glauque::{Dispatch, Glauque};

use crate::{
    accel::Acceleration,
    cpu::{
        store_at_dest, IoSystem, Operand, OperandLoadError, OperandStoreError, ReadOperandsError,
        StoreAtDestError,
    },
    memory::{CacheDecodingTreeError, MemoryReadError, MemoryWriteError},
    quetzal::Quetzal,
    rng::Rng,
    runtime::{
        call_function, call_function_without_stub, return_from_function, start_print_string,
        CallFunctionError, ReturnFromFunctionError, StartPrintStringError,
    },
    Coryphelle, StatusInternal, GLULX_VERSION,
};

/// Creates a big enum with all opcodes errors and a `From` implementation for each one.
/// Useful to propagate errors when stepping through opcodes.
macro_rules! all_opcode_errors_enum {
    ($($variant:ident($source:ty)),+ $(,)?) => {
        #[derive(Debug)]
        #[non_exhaustive]
        pub(crate) enum OpcodeError {
            $(
                $variant($source),
            )*
        }

        impl std::fmt::Display for OpcodeError {
            fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
                write!(f, "error executing opcode")
            }
        }

        impl std::error::Error for OpcodeError {
            fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
                match self {
                    $(
                        Self::$variant(err) => Some(err),
                    )+
                }
            }
        }

        $(
            impl From<$source> for OpcodeError {
                fn from(value: $source) -> Self {
                    Self::$variant(value)
                }
            }
        )+
    };
}

// And now let's create that enum!
all_opcode_errors_enum! {
    Nop(NopError),
    Add(AddError),
    Sub(SubError),
    Mul(MulError),
    Div(DivError),
    Mod(ModError),
    Neg(NegError),
    Bitand(BitandError),
    Bitor(BitorError),
    Bitxor(BitxorError),
    Bitnot(BitnotError),
    Shiftl(ShiftlError),
    Sshiftr(SshiftrError),
    Ushiftr(UshiftrError),
    Jump(JumpError),
    Jz(JzError),
    Jnz(JnzError),
    Jeq(JeqError),
    Jne(JneError),
    Jlt(JltError),
    Jge(JgeError),
    Jgt(JgtError),
    Jle(JleError),
    Jltu(JltuError),
    Jgeu(JgeuError),
    Jgtu(JgtuError),
    Jleu(JleuError),
    Call(CallError),
    Return(ReturnError),
    Catch(CatchError),
    Throw(ThrowError),
    Tailcall(TailcallError),
    Copy(CopyError),
    Copys(CopysError),
    Copyb(CopybError),
    Sexs(SexsError),
    Sexb(SexbError),
    Aload(AloadError),
    Aloads(AloadsError),
    Aloadb(AloadbError),
    Aloadbit(AloadbitError),
    Astore(AstoreError),
    Astores(AstoresError),
    Astoreb(AstorebError),
    Astorebit(AstorebitError),
    Stkcount(StkcountError),
    Stkpeek(StkpeekError),
    Stkswap(StkswapError),
    Stkroll(StkrollError),
    Stkcopy(StkcopyError),
    Streamchar(StreamcharError),
    Streamnum(StreamnumError),
    Streamstr(StreamstrError),
    Streamunichar(StreamunicharError),
    Gestalt(GestaltError),
    Debugtrap(DebugtrapError),
    Getmemsize(GetmemsizeError),
    Setmemsize(SetmemsizeError),
    Jumpabs(JumpabsError),
    Random(RandomError),
    Setrandom(SetrandomError),
    Quit(QuitError),
    Verify(VerifyError),
    Restart(RestartError),
    Save(SaveError),
    Restore(RestoreError),
    Saveundo(SaveundoError),
    Restoreundo(RestoreundoError),
    Protect(ProtectError),
    Hasundo(HasundoError),
    Discardundo(DiscardundoError),
    Glk(GlkError),
    Getstringtbl(GetstringtblError),
    Setstringtbl(SetstringtblError),
    Getiosys(GetiosysError),
    Setiosys(SetiosysError),
    Linearsearch(LinearsearchError),
    Binarysearch(BinarysearchError),
    Linkedsearch(LinkedsearchError),
    Callf(CallfError),
    Callfi(CallfiError),
    Calfii(CallfiiError),
    Callfiii(CallfiiiError),
    Mzero(MzeroError),
    Mcopy(McopyError),
    Malloc(MallocError),
    Mfree(MfreeError),
    Accelfunc(AccelfuncError),
    Accelparam(AccelparamError),
    Numtof(NumtofError),
    Ftonumz(FtonumzError),
    Ftonumn(FtonumnError),
    Ceil(CeilError),
    Floor(FloorError),
    Fadd(FaddError),
    Fsub(FsubError),
    Fmul(FmulError),
    Fdiv(FdivError),
    Fmod(FmodError),
    Sqrt(SqrtError),
    Exp(ExpError),
    Log(LogError),
    Pow(PowError),
    Sin(SinError),
    Cos(CosError),
    Tan(TanError),
    Asin(AsinError),
    Acos(AcosError),
    Atan(AtanError),
    Atan2(Atan2Error),
    Jfeq(JfeqError),
    Jfne(JfneError),
    Jflt(JfltError),
    Jfle(JfleError),
    Jfgt(JfgtError),
    Jfge(JfgeError),
    Jisnan(JisnanError),
    Jisinf(JisinfError),
    Numtod(NumtodError),
    Dtonumz(DtonumzError),
    Dtonumn(DtonumnError),
    Ftod(FtodError),
    Dtof(DtofError),
    Dceil(DceilError),
    Dfloor(DfloorError),
    Dadd(DaddError),
    Dsub(DsubError),
    Dmul(DmulError),
    Ddiv(DdivError),
    Dmodr(DmodrError),
    Dmodq(DmodqError),
    Dsqrt(DsqrtError),
    Dexp(DexpError),
    Dlog(DlogError),
    Dpow(DpowError),
    Dsin(DsinError),
    Dcos(DcosError),
    Dtan(DtanError),
    Dasin(DasinError),
    Dacos(DacosError),
    Datan(DatanError),
    Datan2(Datan2Error),
    Jdeq(JdeqError),
    Jdne(JdneError),
    Jdlt(JdltError),
    Jdle(JdleError),
    Jdgt(JdgtError),
    Jdge(JdgeError),
    Jdisnan(JdisnanError),
    Jdisinf(JdisinfError),
}

/// A macro to generate a specific opcode error
/// with all its possible source errors and conversions.
macro_rules! opcode_error {
    ($name:literal $error:ident => $($variant:ident($source:ty)),+ $(,)?) => {
        #[derive(Debug)]
        #[non_exhaustive]
        pub(crate) enum $error {
            $(
                $variant($source),
            )*
        }

        impl std::fmt::Display for $error {
            fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
                write!(f, concat!("error executing opcode @", $name))
            }
        }

        impl std::error::Error for $error {
            fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
                match self {
                    $(
                        Self::$variant(err) => Some(err),
                    )+
                }
            }
        }

        $(
            impl From<$source> for $error {
                fn from(value: $source) -> Self {
                    Self::$variant(value)
                }
            }
        )+
    };
}

/// And now all the opcodes.

// Opcode 0x0.
/// Does nothing.
pub(crate) fn nop<G: Glauque>(_c: &mut Coryphelle<G>) -> Result<(), NopError> {
    Ok(())
}

#[derive(Debug)]
pub(crate) enum NopError {}

impl fmt::Display for NopError {
    fn fmt(&self, _f: &mut fmt::Formatter<'_>) -> fmt::Result {
        unreachable!()
    }
}

impl Error for NopError {}

// Opcode 0x10.
/// Adds L1 and L2, using standard 32-bit addition. Truncate the result to 32 bits if necessary.
pub(crate) fn add<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), AddError> {
    let [l1, l2, s1] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = l1.load(&c.main_memory, &mut c.stack)?;
    let l2 = l2.load(&c.main_memory, &mut c.stack)?;

    let result = l1.wrapping_add(l2);
    s1.store(&mut c.main_memory, &mut c.stack, result)?;

    Ok(())
}

opcode_error! {"add" AddError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    OperandStore(OperandStoreError),
}

// Opcode 0x11.
/// Computes (L1 - L2).
pub(crate) fn sub<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), SubError> {
    let [l1, l2, s1] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = l1.load(&c.main_memory, &mut c.stack)?;
    let l2 = l2.load(&c.main_memory, &mut c.stack)?;

    let result = l1.wrapping_sub(l2);
    s1.store(&mut c.main_memory, &mut c.stack, result)?;

    Ok(())
}

opcode_error! {"sub" SubError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    OperandStore(OperandStoreError),
}

// Opcode 0x12.
/// Computes (L1 * L2), and store the result in S1. Truncate the result to 32 bits if necessary.
pub(crate) fn mul<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), MulError> {
    let [l1, l2, s1] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = l1.load(&c.main_memory, &mut c.stack)?;
    let l2 = l2.load(&c.main_memory, &mut c.stack)?;

    let result = l1.wrapping_mul(l2);
    s1.store(&mut c.main_memory, &mut c.stack, result)?;

    Ok(())
}

opcode_error! {"mul" MulError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    OperandStore(OperandStoreError),
}

// Opcode 0x13.
/// Compute (L1 / L2), and store the result in S1. This is signed integer division.
pub(crate) fn div<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), DivError> {
    let [l1, l2, s1] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = l1.load(&c.main_memory, &mut c.stack)? as i32;
    let l2 = l2.load(&c.main_memory, &mut c.stack)? as i32;

    let result = l1.wrapping_div(l2) as u32;
    s1.store(&mut c.main_memory, &mut c.stack, result)?;

    Ok(())
}

opcode_error! {"div" DivError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    OperandStore(OperandStoreError),
}

// Opcode 0x14.
/// Compute (L1 % L2), and store the result in S1. This is the remainder from signed integer division.
pub(crate) fn mod_<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), ModError> {
    let [l1, l2, s1] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = l1.load(&c.main_memory, &mut c.stack)? as i32;
    let l2 = l2.load(&c.main_memory, &mut c.stack)? as i32;

    // The rules for the modulo are the same for Glulx and Rust.
    let result = l1.wrapping_rem(l2) as u32;
    s1.store(&mut c.main_memory, &mut c.stack, result)?;

    Ok(())
}

opcode_error! {"mod" ModError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    OperandStore(OperandStoreError),
}

// Opcode 0x15.
/// Computes the negative of L1.
pub(crate) fn neg<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), NegError> {
    let [l1, s1] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = l1.load(&c.main_memory, &mut c.stack)?;

    s1.store(&mut c.main_memory, &mut c.stack, l1.wrapping_neg() as u32)?;

    Ok(())
}

opcode_error! {"neg" NegError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    OperandStore(OperandStoreError),
}

// Opcode 0x18.
/// Compute the bitwise AND of L1 and L2.
pub(crate) fn bitand<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), BitandError> {
    let [l1, l2, s1] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = l1.load(&c.main_memory, &mut c.stack)?;
    let l2 = l2.load(&c.main_memory, &mut c.stack)?;

    let result = l1 & l2;
    s1.store(&mut c.main_memory, &mut c.stack, result)?;

    Ok(())
}

opcode_error! {"bitand" BitandError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    OperandStore(OperandStoreError),
}

// Opcode 0x19.
/// Compute the bitwise OR of L1 and L2.
pub(crate) fn bitor<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), BitorError> {
    let [l1, l2, s1] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = l1.load(&c.main_memory, &mut c.stack)?;
    let l2 = l2.load(&c.main_memory, &mut c.stack)?;

    let result = l1 | l2;
    s1.store(&mut c.main_memory, &mut c.stack, result)?;

    Ok(())
}

opcode_error! {"bitor" BitorError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    OperandStore(OperandStoreError),
}

// Opcode 0x1A.
/// Computes the bitwise XOR of L1 and L2.
pub(crate) fn bitxor<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), BitxorError> {
    let [l1, l2, s1] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = l1.load(&c.main_memory, &mut c.stack)?;
    let l2 = l2.load(&c.main_memory, &mut c.stack)?;

    let result = l1 ^ l2;
    s1.store(&mut c.main_memory, &mut c.stack, result)?;

    Ok(())
}

opcode_error! {"bitxor" BitxorError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    OperandStore(OperandStoreError),
}

// Opcode 0x1B.
/// Computes the bitwise negation of L1.
pub(crate) fn bitnot<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), BitnotError> {
    let [l1, s1] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = l1.load(&c.main_memory, &mut c.stack)?;

    s1.store(&mut c.main_memory, &mut c.stack, !l1)?;

    Ok(())
}

opcode_error! {"bitnot" BitnotError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    OperandStore(OperandStoreError),
}

// Opcode 0x1C.
/// Shifts the bits of L1 to the left by L2 places.
///
/// The bottom L2 bits are filled in with zeroes. If L2 is 32 or more, the
/// result is always zero.
pub(crate) fn shiftl<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), ShiftlError> {
    let [l1, l2, s1] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = l1.load(&c.main_memory, &mut c.stack)?;
    let l2 = l2.load(&c.main_memory, &mut c.stack)?;

    let result = l1.checked_shl(l2).unwrap_or(0);
    s1.store(&mut c.main_memory, &mut c.stack, result)?;

    Ok(())
}

opcode_error! {"shiftl" ShiftlError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    OperandStore(OperandStoreError),
}

// Opcode 0x1D.
/// Shifts the bits of L1 to the right by L2 places. The top L2 bits are
/// filled in with copies of the top bit of L1.
pub(crate) fn sshiftr<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), SshiftrError> {
    let [l1, l2, s1] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = l1.load(&c.main_memory, &mut c.stack)? as i32;
    let l2 = l2.load(&c.main_memory, &mut c.stack)?;

    let result = l1
        .checked_shr(l2)
        .unwrap_or(if l1 as u32 & 0x80000000 != 0 { -1 } else { 0 }) as u32;
    s1.store(&mut c.main_memory, &mut c.stack, result)?;

    Ok(())
}

opcode_error! {"sshiftr" SshiftrError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    OperandStore(OperandStoreError),
}

// Opcode 0x1E.
/// Shifts the bits of L1 to the right by L2 places.
///
/// The top L2 bits are filled in with zeroes. If L2 is 32 or more, the
/// result is always zero.
pub(crate) fn ushiftr<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), UshiftrError> {
    let [l1, l2, s1] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = l1.load(&c.main_memory, &mut c.stack)?;
    let l2 = l2.load(&c.main_memory, &mut c.stack)?;

    let result = l1.checked_shr(l2).unwrap_or(0);
    s1.store(&mut c.main_memory, &mut c.stack, result)?;

    Ok(())
}

opcode_error! {"ushiftr" UshiftrError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    OperandStore(OperandStoreError),
}

// Opcode 0x20.
/// Branch unconditionally to offset L1.
pub(crate) fn jump<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), JumpError> {
    let [l1] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = l1.load(&c.main_memory, &mut c.stack)?;

    // Special case for 0 and 1: return from function.
    if l1 == 0 || l1 == 1 {
        return_from_function(c, l1)?;
    } else {
        c.cpu.pc = c.cpu.pc.wrapping_add(l1).wrapping_sub(2);
    }

    Ok(())
}

opcode_error! {"jump" JumpError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    ReturnFromFunction(ReturnFromFunctionError),
}

// Opcode 0x22.
/// Branch to L2 if L1 is equal to zero.
pub(crate) fn jz<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), JzError> {
    let [l1, l2] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = l1.load(&c.main_memory, &mut c.stack)?;
    let l2 = l2.load(&c.main_memory, &mut c.stack)?;

    if l1 == 0 {
        // Special case for 0 and 1: return from function.
        if l2 == 0 || l2 == 1 {
            return_from_function(c, l2)?;
        } else {
            c.cpu.pc = c.cpu.pc.wrapping_add(l2).wrapping_sub(2);
        }
    }

    Ok(())
}

opcode_error! {"jz" JzError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    ReturnFromFunction(ReturnFromFunctionError),
}

// Opcode 0x23.
/// Branch to L2 if L1 is not equal to zero.
pub(crate) fn jnz<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), JnzError> {
    let [l1, l2] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = l1.load(&c.main_memory, &mut c.stack)?;
    let l2 = l2.load(&c.main_memory, &mut c.stack)?;

    if l1 != 0 {
        // Special case for 0 and 1: return from function.
        if l2 == 0 || l2 == 1 {
            return_from_function(c, l2)?;
        } else {
            c.cpu.pc = c.cpu.pc.wrapping_add(l2).wrapping_sub(2);
        }
    }

    Ok(())
}

opcode_error! {"jnz" JnzError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    ReturnFromFunction(ReturnFromFunctionError),
}

// Opcode 0x24.
/// Branch to L3 if L1 is equal to L2.
pub(crate) fn jeq<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), JeqError> {
    let [l1, l2, l3] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = l1.load(&c.main_memory, &mut c.stack)?;
    let l2 = l2.load(&c.main_memory, &mut c.stack)?;
    let l3 = l3.load(&c.main_memory, &mut c.stack)?;

    if l1 == l2 {
        // Special case for 0 and 1: return from function.
        if l3 == 0 || l3 == 1 {
            return_from_function(c, l3)?;
        } else {
            c.cpu.pc = c.cpu.pc.wrapping_add(l3).wrapping_sub(2);
        }
    }

    Ok(())
}

opcode_error! {"jeq" JeqError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    ReturnFromFunction(ReturnFromFunctionError),
}

// Opcode 0x25.
/// Branch to L3 if L1 is not equal to L2.
pub(crate) fn jne<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), JneError> {
    let [l1, l2, l3] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = l1.load(&c.main_memory, &mut c.stack)?;
    let l2 = l2.load(&c.main_memory, &mut c.stack)?;
    let l3 = l3.load(&c.main_memory, &mut c.stack)?;

    if l1 != l2 {
        // Special case for 0 and 1: return from function.
        if l3 == 0 || l3 == 1 {
            return_from_function(c, l3)?;
        } else {
            c.cpu.pc = c.cpu.pc.wrapping_add(l3).wrapping_sub(2);
        }
    }

    Ok(())
}

opcode_error! {"jne" JneError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    ReturnFromFunction(ReturnFromFunctionError),
}

// Opcode 0x26.
/// Branch to L3 if L1 is less than L2.
pub(crate) fn jlt<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), JltError> {
    let [l1, l2, l3] = c.cpu.read_operands(&c.main_memory)?;
    // This opcode compares its operands as signed integers.
    let l1 = l1.load(&c.main_memory, &mut c.stack)? as i32;
    let l2 = l2.load(&c.main_memory, &mut c.stack)? as i32;
    let l3 = l3.load(&c.main_memory, &mut c.stack)?;

    if l1 < l2 {
        // Special case for 0 and 1: return from function.
        if l3 == 0 || l3 == 1 {
            return_from_function(c, l3)?;
        } else {
            c.cpu.pc = c.cpu.pc.wrapping_add(l3).wrapping_sub(2);
        }
    }

    Ok(())
}

opcode_error! {"jlt" JltError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    ReturnFromFunction(ReturnFromFunctionError),
}

// Opcode 0x27.
/// Branch to L3 if L1 is greater than or equal to L2.
pub(crate) fn jge<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), JgeError> {
    let [l1, l2, l3] = c.cpu.read_operands(&c.main_memory)?;
    // This opcode compares its operands as signed integers.
    let l1 = l1.load(&c.main_memory, &mut c.stack)? as i32;
    let l2 = l2.load(&c.main_memory, &mut c.stack)? as i32;
    let l3 = l3.load(&c.main_memory, &mut c.stack)?;

    if l1 >= l2 {
        // Special case for 0 and 1: return from function.
        if l3 == 0 || l3 == 1 {
            return_from_function(c, l3)?;
        } else {
            c.cpu.pc = c.cpu.pc.wrapping_add(l3).wrapping_sub(2);
        }
    }

    Ok(())
}

opcode_error! {"jge" JgeError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    ReturnFromFunction(ReturnFromFunctionError),
}

// Opcode 0x28.
/// Branch to L3 if L1 is greater than L2.
pub(crate) fn jgt<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), JgtError> {
    let [l1, l2, l3] = c.cpu.read_operands(&c.main_memory)?;
    // This opcode compares its operands as signed integers.
    let l1 = l1.load(&c.main_memory, &mut c.stack)? as i32;
    let l2 = l2.load(&c.main_memory, &mut c.stack)? as i32;
    let l3 = l3.load(&c.main_memory, &mut c.stack)?;

    if l1 > l2 {
        // Special case for 0 and 1: return from function.
        if l3 == 0 || l3 == 1 {
            return_from_function(c, l3)?;
        } else {
            c.cpu.pc = c.cpu.pc.wrapping_add(l3).wrapping_sub(2);
        }
    }

    Ok(())
}

opcode_error! {"jgt" JgtError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    ReturnFromFunction(ReturnFromFunctionError),
}

// Opcode 0x29.
/// Branch to L3 if L1 is less than or equal L2.
pub(crate) fn jle<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), JleError> {
    let [l1, l2, l3] = c.cpu.read_operands(&c.main_memory)?;
    // This opcode compares its operands as signed integers.
    let l1 = l1.load(&c.main_memory, &mut c.stack)? as i32;
    let l2 = l2.load(&c.main_memory, &mut c.stack)? as i32;
    let l3 = l3.load(&c.main_memory, &mut c.stack)?;

    if l1 <= l2 {
        // Special case for 0 and 1: return from function.
        if l3 == 0 || l3 == 1 {
            return_from_function(c, l3)?;
        } else {
            c.cpu.pc = c.cpu.pc.wrapping_add(l3).wrapping_sub(2);
        }
    }

    Ok(())
}

opcode_error! {"jle" JleError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    ReturnFromFunction(ReturnFromFunctionError),
}

// Opcode 0x2A.
/// Branch to L3 if L1 is less than L2 (unsigned comparison).
pub(crate) fn jltu<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), JltuError> {
    let [l1, l2, l3] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = l1.load(&c.main_memory, &mut c.stack)?;
    let l2 = l2.load(&c.main_memory, &mut c.stack)?;
    let l3 = l3.load(&c.main_memory, &mut c.stack)?;

    if l1 < l2 {
        // Special case for 0 and 1: return from function.
        if l3 == 0 || l3 == 1 {
            return_from_function(c, l3)?;
        } else {
            c.cpu.pc = c.cpu.pc.wrapping_add(l3).wrapping_sub(2);
        }
    }

    Ok(())
}

opcode_error! {"jltu" JltuError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    ReturnFromFunction(ReturnFromFunctionError),
}

// Opcode 0x2B.
/// Branch to L3 if L1 is greater than or equal to L2 (unsigned comparison).
pub(crate) fn jgeu<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), JgeuError> {
    let [l1, l2, l3] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = l1.load(&c.main_memory, &mut c.stack)?;
    let l2 = l2.load(&c.main_memory, &mut c.stack)?;
    let l3 = l3.load(&c.main_memory, &mut c.stack)?;

    if l1 >= l2 {
        // Special case for 0 and 1: return from function.
        if l3 == 0 || l3 == 1 {
            return_from_function(c, l3)?;
        } else {
            c.cpu.pc = c.cpu.pc.wrapping_add(l3).wrapping_sub(2);
        }
    }

    Ok(())
}

opcode_error! {"jgeu" JgeuError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    ReturnFromFunction(ReturnFromFunctionError),
}

// Opcode 0x2C.
/// Branches to L3 if L1 is greater than L2 (unsigned comparison).
pub(crate) fn jgtu<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), JgtuError> {
    let [l1, l2, l3] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = l1.load(&c.main_memory, &mut c.stack)?;
    let l2 = l2.load(&c.main_memory, &mut c.stack)?;
    let l3 = l3.load(&c.main_memory, &mut c.stack)?;

    if l1 > l2 {
        // Special case for 0 and 1: return from function.
        if l3 == 0 || l3 == 1 {
            return_from_function(c, l3)?;
        } else {
            c.cpu.pc = c.cpu.pc.wrapping_add(l3).wrapping_sub(2);
        }
    }

    Ok(())
}

opcode_error! {"jgtu" JgtuError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    ReturnFromFunction(ReturnFromFunctionError),
}

// Opcode 0x2D.
/// Branch to L3 if L1 is less than or equal to L2 (unsigned comparison).
pub(crate) fn jleu<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), JleuError> {
    let [l1, l2, l3] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = l1.load(&c.main_memory, &mut c.stack)?;
    let l2 = l2.load(&c.main_memory, &mut c.stack)?;
    let l3 = l3.load(&c.main_memory, &mut c.stack)?;

    if l1 <= l2 {
        // Special case for 0 and 1: return from function.
        if l3 == 0 || l3 == 1 {
            return_from_function(c, l3)?;
        } else {
            c.cpu.pc = c.cpu.pc.wrapping_add(l3).wrapping_sub(2);
        }
    }

    Ok(())
}

opcode_error! {"jleu" JleuError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    ReturnFromFunction(ReturnFromFunctionError),
}

// Opcode 0x30.
/// Calls the function whose address is L1, passing in L2 arguments, and store the return result at S1.
pub(crate) fn call<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), CallError> {
    let [l1, l2, s1] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = l1.load(&c.main_memory, &mut c.stack)?;
    let l2 = l2.load(&c.main_memory, &mut c.stack)?;

    // Get the function's arguments from the stack.
    let mut args = Vec::with_capacity(l2.try_into().unwrap());
    for _ in 0..l2 {
        args.push(c.stack.pop_u32());
    }

    let (dest_type, dest_addr) = s1.to_dest_type_addr(&c.main_memory);

    call_function(c, l1, &args, dest_type, dest_addr)?;

    Ok(())
}

opcode_error! {"call" CallError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    CallFunction(CallFunctionError)
}

// Opcode 0x31.
/// Returns from the current function with the given return value.
pub(crate) fn return_<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), ReturnError> {
    let [l1] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = l1.load(&c.main_memory, &mut c.stack)?;
    return_from_function(c, l1)?;

    Ok(())
}

opcode_error! {"return" ReturnError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    ReturnFromFunction(ReturnFromFunctionError),
}

// Opcode 0x32.
/// Generates a "catch token", which can be used to jump back to this
/// execution point from a throw opcode.
///
/// The token is stored in S1, and then execution branches to offset L1. If
/// execution is proceeding from this point because of a throw, the thrown
/// value is stored instead, and the branch is ignored.
pub(crate) fn catch<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), CatchError> {
    let [s1, l1] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = l1.load(&c.main_memory, &mut c.stack)?;

    let (dest_type, dest_addr) = s1.to_dest_type_addr(&c.main_memory);
    c.stack.push_call_stub(&c.cpu, dest_type, dest_addr);
    let stack_ptr = c.stack.ptr();
    s1.store(&mut c.main_memory, &mut c.stack, stack_ptr)?;

    // Special case for 0 and 1: return from function.
    if l1 == 0 || l1 == 1 {
        return_from_function(c, l1)?;
    } else {
        c.cpu.pc = c.cpu.pc.wrapping_add(l1).wrapping_sub(2);
    }

    Ok(())
}

opcode_error! {"catch" CatchError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    OperandStore(OperandStoreError),
    ReturnFromFunction(ReturnFromFunctionError),
}

// Opcode 0x33.
/// Jumps back to a previously-executed catch opcode, and store the value L1.
///
/// L2 must be a valid catch token.
pub(crate) fn throw<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), ThrowError> {
    let [l1, l2] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = l1.load(&c.main_memory, &mut c.stack)?;
    let l2 = l2.load(&c.main_memory, &mut c.stack)?;

    if c.stack.ptr() < l2 {
        panic!("invalid catch token: value greater than the stack pointer");
    } else {
        c.stack.truncate(usize::try_from(l2).unwrap());
        let (dest_type, dest_addr) = c.stack.pop_call_stub(&mut c.cpu);
        store_at_dest(c, dest_type, dest_addr, l1)?;
    }

    Ok(())
}

opcode_error! {"throw" ThrowError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    StoreAtDest(StoreAtDestError),
}

// Opcode 0x34.
/// Call function whose address is L1, passing in L2 arguments, and pass
/// the return result out to whoever called the current function.
pub(crate) fn tailcall<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), TailcallError> {
    let [l1, l2] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = l1.load(&c.main_memory, &mut c.stack)?;
    let l2 = l2.load(&c.main_memory, &mut c.stack)?;

    // Get the function's arguments from the stack.
    let mut args = Vec::with_capacity(l2.try_into().unwrap());
    for _ in 0..l2 {
        args.push(c.stack.pop_u32());
    }

    // Throw away the current call frame.
    c.stack
        .truncate(usize::try_from(c.stack.frame_ptr).unwrap());

    // Call the function.
    call_function_without_stub(c, l1, &args)?;

    Ok(())
}

opcode_error! {"Tailcall" TailcallError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    CallFunction(CallFunctionError)
}

// Opcode 0x40.
/// Reads L1 and stores it at S1, without change.
pub(crate) fn copy<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), CopyError> {
    let [l1, s1] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = l1.load(&c.main_memory, &mut c.stack)?;
    s1.store(&mut c.main_memory, &mut c.stack, l1)?;

    Ok(())
}

opcode_error! {"copy" CopyError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    OperandStore(OperandStoreError),
}

// Opcode 0x41.
/// Reads an 16-bit value from L1 and store it at S1.
pub(crate) fn copys<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), CopysError> {
    let [l1, s1] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = l1.load_u16(&c.main_memory, &mut c.stack)?;
    s1.store_u16(&mut c.main_memory, &mut c.stack, l1)?;

    Ok(())
}

opcode_error! {"copys" CopysError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    MemoryRead(MemoryReadError),
    OperandStore(OperandStoreError),
}

// Opcode 0x42.
/// Reads an 8-bit value from L1 and store it at S1.
pub(crate) fn copyb<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), CopybError> {
    let [l1, s1] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = l1.load_u8(&c.main_memory, &mut c.stack)?;
    s1.store_u8(&mut c.main_memory, &mut c.stack, l1)?;

    Ok(())
}

opcode_error! {"copyb" CopybError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    OperandStore(OperandStoreError),
}

// Opcode 0x44.
/// Sign-extends a value, considered as an 16-bit value.
///
/// If the value's 8000 bit is set, the upper 16 bits are all set;
/// otherwise, the upper 16 bits are all cleared.
pub(crate) fn sexs<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), SexsError> {
    let [l1, s1] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = l1.load(&c.main_memory, &mut c.stack)? as i16 as u32;
    s1.store(&mut c.main_memory, &mut c.stack, l1)?;

    Ok(())
}

opcode_error! {"Sexs" SexsError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    OperandStore(OperandStoreError),
}

// Opcode 0x45.
/// Sign-extends a value, considered as an 8-bit value.
///
/// If the value's 80 bit is set, the upper 24 bits are all set; otherwise,
/// the upper 24 bits are all cleared.
pub(crate) fn sexb<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), SexbError> {
    let [l1, s1] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = l1.load(&c.main_memory, &mut c.stack)? as i8 as u32;
    s1.store(&mut c.main_memory, &mut c.stack, l1)?;

    Ok(())
}

opcode_error! {"sexb" SexbError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    OperandStore(OperandStoreError),
}

// Opcode 0x48.
/// Loads a 32-bit value from main memory address (L1 + 4 * L2), and stores it in S1.
pub(crate) fn aload<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), AloadError> {
    let [l1, l2, s1] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = l1.load(&c.main_memory, &mut c.stack)?;
    let l2 = l2.load(&c.main_memory, &mut c.stack)?;

    let value = c
        .main_memory
        .read_u32(l1.wrapping_add(l2.wrapping_mul(4)))?;
    s1.store(&mut c.main_memory, &mut c.stack, value)?;

    Ok(())
}

opcode_error! {"aload" AloadError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    MemoryRead(MemoryReadError),
    OperandStore(OperandStoreError),
}

// Opcode 0x49.
/// Loads a 16-bit value from main memory address (L1 + 2 * L2), and store it in S1.
pub(crate) fn aloads<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), AloadsError> {
    let [l1, l2, s1] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = l1.load(&c.main_memory, &mut c.stack)?;
    let l2 = l2.load(&c.main_memory, &mut c.stack)?;

    let value = c
        .main_memory
        .read_u16(l1.wrapping_add(l2.wrapping_mul(2)))? as u32;
    s1.store(&mut c.main_memory, &mut c.stack, value)?;

    Ok(())
}

opcode_error! {"aloads" AloadsError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    MemoryRead(MemoryReadError),
    OperandStore(OperandStoreError),
}

// Opcode 0x4A.
/// Loads an 8-bit value from main memory address (L1 + L2), and store it in S1.
pub(crate) fn aloadb<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), AloadbError> {
    let [l1, l2, s1] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = l1.load(&c.main_memory, &mut c.stack)?;
    let l2 = l2.load(&c.main_memory, &mut c.stack)?;

    let value = c.main_memory.read_u8(l1.wrapping_add(l2))? as u32;
    s1.store(&mut c.main_memory, &mut c.stack, value)?;

    Ok(())
}

opcode_error! {"aloadb" AloadbError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    MemoryRead(MemoryReadError),
    OperandStore(OperandStoreError),
}

// Opcode 0x4B.
/// Gets the bit (L2 % 8) of memory address (L1 + L2 / 8).
pub(crate) fn aloadbit<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), AloadbitError> {
    let [l1, l2, s1] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = l1.load(&c.main_memory, &mut c.stack)?;
    let l2 = l2.load(&c.main_memory, &mut c.stack)?;

    let result = c.main_memory.aloadbit(l1, l2)?;

    s1.store(&mut c.main_memory, &mut c.stack, result)?;

    Ok(())
}

opcode_error! {"aloadbit" AloadbitError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    MemoryRead(MemoryReadError),
    OperandStore(OperandStoreError),
}

// Opcode 0x4C.
/// Stores L3 into the 32-bit field at main memory address (L1 + 4 * L2).
pub(crate) fn astore<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), AstoreError> {
    let [l1, l2, l3] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = l1.load(&c.main_memory, &mut c.stack)?;
    let l2 = l2.load(&c.main_memory, &mut c.stack)?;
    let l3 = l3.load(&c.main_memory, &mut c.stack)?;
    let addr = l1.wrapping_add(l2.wrapping_mul(4));
    c.main_memory.write_u32(addr, l3)?;

    Ok(())
}

opcode_error! {"astore" AstoreError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    MemoryWrite(MemoryWriteError),
}

// Opcode 0x4D.
/// Stores L3 into the 16-bit field at main memory address (L1 + 2 * L2).
pub(crate) fn astores<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), AstoresError> {
    let [l1, l2, l3] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = l1.load(&c.main_memory, &mut c.stack)?;
    let l2 = l2.load(&c.main_memory, &mut c.stack)?;
    let l3 = l3.load(&c.main_memory, &mut c.stack)?;

    c.main_memory
        .write_u16(l1.wrapping_add(l2.wrapping_mul(2)), l3 as u16)?;

    Ok(())
}

opcode_error! {"astores" AstoresError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    MemoryWrite(MemoryWriteError),
}

// Opcode 0x4E.
/// Store L3 into the 8-bit field at main memory address (L1 + L2).
pub(crate) fn astoreb<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), AstorebError> {
    let [l1, l2, l3] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = l1.load(&c.main_memory, &mut c.stack)?;
    let l2 = l2.load(&c.main_memory, &mut c.stack)?;
    let l3 = l3.load(&c.main_memory, &mut c.stack)?;

    c.main_memory.write_u8(l1.wrapping_add(l2), l3 as u8)?;

    Ok(())
}

opcode_error! {"astoreb" AstorebError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    MemoryWrite(MemoryWriteError),
}

// Opcode 0x4F.
/// Sets or clears the bit (L2 % 8) of memory address (L1 + L2 / 8).
pub(crate) fn astorebit<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), AstorebitError> {
    let [l1, l2, l3] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = l1.load(&c.main_memory, &mut c.stack)?;
    let l2 = l2.load(&c.main_memory, &mut c.stack)?;
    let l3 = l3.load(&c.main_memory, &mut c.stack)?;

    let bit_pos = l2 % 8;

    let addr = if l2 as i32 >= 0 {
        l1.wrapping_add(l2 / 8)
    } else {
        l1.wrapping_sub((7_i32.wrapping_sub(l2 as i32) / 8) as u32)
    };

    let byte = c.main_memory.read_u8(addr)?;
    let byte = if l3 == 0 {
        byte & !(1 << bit_pos)
    } else {
        byte | 1 << bit_pos
    };

    c.main_memory.write_u8(addr, byte)?;

    Ok(())
}

opcode_error! {"astorebit" AstorebitError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    MemoryRead(MemoryReadError),
    MemoryWrite(MemoryWriteError),
}

// Opcode 0x50.
/// Stores a count of the number of values on the stack.
///
/// This counts only values above the current call frame.
pub(crate) fn stkcount<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), StkcountError> {
    let [s1] = c.cpu.read_operands(&c.main_memory)?;

    let result = c.stack.count();

    s1.store(&mut c.main_memory, &mut c.stack, result)?;

    Ok(())
}

opcode_error! {"stkcount" StkcountError =>
    ReadOperands(ReadOperandsError),
    OperandStore(OperandStoreError),
}

// Opcode 0x51.
/// Peeks at the L1'th value on the stack, without actually popping anything.
///
/// If L1 is zero, this is the top value; if one, it's the value below that;
/// etc. L1 must be less than the current stack-count.
///
/// (If L1 or S1 use the stack pop/push modes, the peek is counted after L1
/// is popped, but before the result is pushed.)
pub(crate) fn stkpeek<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), StkpeekError> {
    let [l1, s1] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = l1.load(&c.main_memory, &mut c.stack)?;

    let count = c.stack.count();
    if l1 >= count {
        return Err(StkpeekError::StackCountTooSmall(StackCountTooSmallError {
            count,
            needed: l1 + 1, // Index is 0-based.
        }));
    }

    let index = u32::try_from(c.stack.len()).unwrap() - 4 * l1 - 4;
    let result = c.stack.read_u32(index);

    s1.store(&mut c.main_memory, &mut c.stack, result)?;

    Ok(())
}

opcode_error! {"stkpeek" StkpeekError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    StackCountTooSmall(StackCountTooSmallError),
    OperandStore(OperandStoreError),
}

#[derive(Debug)]
#[non_exhaustive]
pub(crate) struct StackCountTooSmallError {
    /// The stack count.
    count: u32,

    /// The minimum stack count that was needed.
    needed: u32,
}

impl fmt::Display for StackCountTooSmallError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "the stack count is {}, but a stack count of at least {} was needed",
            self.count, self.needed
        )
    }
}

impl Error for StackCountTooSmallError {}

// Opcode 0x52.
/// Swap the top two values on the stack.
///
/// The current stack-count must be at least two.
pub(crate) fn stkswap<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), StkswapError> {
    let count = c.stack.count();
    if count < 2 {
        return Err(StkswapError::StackCountTooSmall(StackCountTooSmallError {
            count,
            needed: 2,
        }));
    }
    let stack_len = c.stack.len();
    c.stack.as_bytes_mut()[stack_len - 8..stack_len].rotate_left(4);

    Ok(())
}

opcode_error! {"stkswap" StkswapError =>
    StackCountTooSmall(StackCountTooSmallError)
}

// Opcode 0x53.
/// Rotates the top L1 values on the stack.
///
/// They are rotated up or down L2 places, with positive values meaning up
/// and negative meaning down. The current stack-count must be at least L1.
/// If either L1 or L2 is zero, nothing happens.
pub(crate) fn stkroll<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), StkrollError> {
    let [l1, l2] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = l1.load(&c.main_memory, &mut c.stack)?;
    let l1_usize = usize::try_from(l1).unwrap();
    let l2 = l2.load(&c.main_memory, &mut c.stack)? as i32;

    // No-op if L1 is zero.
    if l1 == 0 {
        return Ok(());
    }

    let count = c.stack.count();
    if l1 > count {
        return Err(StkrollError::StackCountTooSmall(StackCountTooSmallError {
            count,
            needed: l1,
        }));
    }

    let stack_len = c.stack.len();
    if l2 >= 0 {
        let l2 = usize::try_from(l2).unwrap();
        c.stack.as_bytes_mut()[stack_len - 4 * l1_usize..stack_len]
            .rotate_right((l2 * 4) % (l1_usize * 4));
    } else {
        let l2 = usize::try_from(-l2).unwrap();
        c.stack.as_bytes_mut()[stack_len - 4 * l1_usize..stack_len]
            .rotate_left((l2 * 4) % (l1_usize * 4));
    }

    Ok(())
}

opcode_error! {"stkroll" StkrollError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    StackCountTooSmall(StackCountTooSmallError)
}

// Opcode 0x54.
/// Peek at the top L1 values in the stack, and push duplicates onto the
/// stack in the same order.
///
/// If L1 is zero, nothing happens. L1 must not be greater than the current
/// stack-count. (If L1 uses the stack pop mode, the stkcopy is counted
/// after L1 is popped.)
pub(crate) fn stkcopy<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), StkcopyError> {
    let [l1] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = l1.load(&c.main_memory, &mut c.stack)?;
    let l1_usize = usize::try_from(l1).unwrap();

    let count = c.stack.count();
    if l1 > count {
        return Err(StkcopyError::StackCountTooSmall(StackCountTooSmallError {
            count,
            needed: l1,
        }));
    }

    // TODO: Use `extend_from_within` to avoid the allocation.
    let copied_values = c.stack.as_bytes()[c.stack.len() - 4 * l1_usize..].to_vec();
    c.stack.push_slice(&copied_values);

    Ok(())
}

opcode_error! {"stkcopy" StkcopyError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    StackCountTooSmall(StackCountTooSmallError)
}

// Opcode 0x70.
/// Sends L1 to the current stream. This sends a single character; the value L1 is truncated to eight bits.
pub(crate) fn streamchar<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), StreamcharError> {
    let [l1] = c.cpu.read_operands(&c.main_memory)?;
    // The value is truncated to 8 bits.
    let l1 = l1.load(&c.main_memory, &mut c.stack)? as u8;

    match c.cpu.io_system {
        IoSystem::Glk => c.glk.put_char(&mut c.main_memory, l1),
        IoSystem::Filter => {
            call_function(c, c.cpu.io_system_rock, &[l1 as u32], 0, 0)?;
        }
        IoSystem::Null => (),
    }

    Ok(())
}

opcode_error! {"streamchar" StreamcharError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    CallFunction(CallFunctionError)
}

// Opcode 0x71.
/// Sends L1 to the current stream, represented as a signed decimal number in ASCII.
pub(crate) fn streamnum<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), StreamnumError> {
    let [l1] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = l1.load(&c.main_memory, &mut c.stack)?;

    let stringified_number = (l1 as i32).to_string();

    // Using as_bytes is OK, since a number converted to as string is guaranteed to be ASCII.
    match c.cpu.io_system {
        IoSystem::Glk => c
            .glk
            .put_slice(&mut c.main_memory, stringified_number.as_bytes()),
        IoSystem::Filter => {
            c.stack.push_call_stub(&c.cpu, 0x11, 0);
            c.cpu.pc = l1;
            call_function(
                c,
                c.cpu.io_system_rock,
                &[stringified_number.as_bytes()[0] as u32],
                0x12,
                1,
            )?;
        }
        IoSystem::Null => (),
    }

    Ok(())
}

opcode_error! {"streamnum" StreamnumError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    CallFunction(CallFunctionError)
}

// Opcode 0x72.
/// Sends the string object at address L1 to the current stream.
pub(crate) fn streamstr<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), StreamstrError> {
    let [l1] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = l1.load(&c.main_memory, &mut c.stack)?;
    start_print_string(c, l1)?;

    Ok(())
}

opcode_error! {"streamstr" StreamstrError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    StartPrintString(StartPrintStringError),
}

// Opcode 0x73.
/// Sends L1 to the current stream. This sends a single (32-bit) character.
pub(crate) fn streamunichar<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), StreamunicharError> {
    let [l1] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = l1.load(&c.main_memory, &mut c.stack)?;

    match c.cpu.io_system {
        IoSystem::Glk => c.glk.put_char_uni(&mut c.main_memory, l1),
        IoSystem::Filter => {
            call_function(c, c.cpu.io_system_rock, &[l1], 0, 0)?;
        }
        IoSystem::Null => (),
    }

    Ok(())
}

opcode_error! {"streamunichar" StreamunicharError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    CallFunction(CallFunctionError)
}

// Opcode 0x100.
/// Tests the Gestalt selector number L1, with optional extra argument L2, and stores the result in S1. If the selector is not known, stores zero.
pub(crate) fn gestalt<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), GestaltError> {
    let [l1, l2, s1] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = l1.load(&c.main_memory, &mut c.stack)?;
    let l2 = l2.load(&c.main_memory, &mut c.stack)?;

    let result = match l1 {
        // GlulxVersion.
        0 => GLULX_VERSION.as_u32(),
        // TerpVersion.
        1 => {
            let major = env!("CARGO_PKG_VERSION_MAJOR").parse::<u16>().unwrap();
            let minor = env!("CARGO_PKG_VERSION_MINOR").parse::<u8>().unwrap();
            let patch = env!("CARGO_PKG_VERSION_PATCH").parse::<u8>().unwrap();
            ((major as u32) << 16) + ((minor as u32) << 8) + (patch as u32)
        }
        // ResizeMem
        2 => 1,
        // Undo
        3 => 1,
        // IOSystem
        4 => match l2 {
            0..=2 => 1, // We support null, filter and Glk.
            _ => 0,
        },
        // Unicode
        5 => 1,
        // MemCopy
        6 => 1,
        // MAlloc
        7 => 1,
        // MAllocHeap
        8 => c.main_memory.heap_start.map(NonZeroU32::get).unwrap_or(0),
        // Acceleration
        9 => 1,
        // AccelFunc
        10 => Acceleration::<G>::is_supported(l2) as u32,
        // Float
        11 => 1,
        // ExtUndo
        12 => 1,
        // Double
        13 => 1,
        _ => 0,
    };

    s1.store(&mut c.main_memory, &mut c.stack, result)?;

    Ok(())
}

opcode_error! {"gestalt" GestaltError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    OperandStore(OperandStoreError),
}

// Opcode 0x101.
pub(crate) fn debugtrap<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), DebugtrapError> {
    let [l1] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = l1.load(&c.main_memory, &mut c.stack)?;

    Err(DebugtrapError::ExecutedDebugTrap(ExecutedDebugTrapError(
        l1,
    )))
}

opcode_error! {"debugtrap" DebugtrapError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    ExecutedDebugTrap(ExecutedDebugTrapError)
}

#[derive(Debug)]
#[non_exhaustive]
pub(crate) struct ExecutedDebugTrapError(u32);

impl fmt::Display for ExecutedDebugTrapError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "debugtrap was executed with value {}", self.0)
    }
}

impl Error for ExecutedDebugTrapError {}

// Opcode 0x102.
/// Stores the current size of the memory map at S1.
pub(crate) fn getmemsize<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), GetmemsizeError> {
    let [s1] = c.cpu.read_operands(&c.main_memory)?;
    let mem_size = c.main_memory.len();
    s1.store(
        &mut c.main_memory,
        &mut c.stack,
        mem_size.try_into().unwrap(),
    )?;

    Ok(())
}

opcode_error! {"getmemsize" GetmemsizeError =>
    ReadOperands(ReadOperandsError),
    OperandStore(OperandStoreError),
}

// Opcode 0x103.
/// Sets the current size of the memory map.
///
/// The new value must be a multiple of 256, like all memory boundaries in
/// Glulx. It must be greater than or equal to ENDMEM (the initial
/// memory-size value which is stored in the header.)
pub(crate) fn setmemsize<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), SetmemsizeError> {
    let [l1, s1] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = l1.load(&c.main_memory, &mut c.stack)?;

    let result = if l1 % 256 != 0 || l1 < c.main_memory.endmem() || c.main_memory.heap_is_active() {
        1
    } else {
        c.main_memory.resize(l1.try_into().unwrap());
        0
    };

    s1.store(&mut c.main_memory, &mut c.stack, result)?;

    Ok(())
}

opcode_error! {"setmemsize" SetmemsizeError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    OperandStore(OperandStoreError),
}

// Opcode 0x104.
/// Branches unconditionally to address L1.
///
/// Unlike the other branch opcodes, this takes an absolute address, not an
/// offset. The special cases 0 and 1 (for returning) do not apply.
pub(crate) fn jumpabs<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), JumpabsError> {
    let [l1] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = l1.load(&c.main_memory, &mut c.stack)?;
    c.cpu.pc = l1;

    Ok(())
}

opcode_error! {"jumpabs" JumpabsError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
}

// Opcode 0x110.
/// Returns a random number in the range 0 to (L1 - 1); or, if L1 is
/// negative, the range (L1 + 1) to 0.
///
/// If L1 is zero, returns a random number in the full 32-bit integer
/// range.
pub(crate) fn random<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), RandomError> {
    let [l1, s1] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = l1.load(&c.main_memory, &mut c.stack)? as i32;

    let result = c.rng.gen(l1);

    s1.store(&mut c.main_memory, &mut c.stack, result as u32)?;

    Ok(())
}

opcode_error! {"random" RandomError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    OperandStore(OperandStoreError),
}

// Opcode 0x111.
/// Seeds the random-number generator with the value L1.
///
/// If L1 is zero, subsequent random numbers will be as genuinely
/// unpredictable as the terp can provide. If L1 is nonzero, subsequent
/// random numbers will follow a deterministic sequence, always the same for
/// a given nonzero seed.
pub(crate) fn setrandom<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), SetrandomError> {
    let [l1] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = l1.load(&c.main_memory, &mut c.stack)?;

    if l1 == 0 {
        c.rng = Rng::new();
    } else {
        c.rng = Rng::from_seed(l1);
    }

    Ok(())
}

opcode_error! {"setrandom" SetrandomError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
}

// Opcode 0x120.
/// Shuts down the interpreter and exits.
pub(crate) fn quit<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), QuitError> {
    c.quit();

    Ok(())
}

#[derive(Debug)]
pub(crate) enum QuitError {}

impl fmt::Display for QuitError {
    fn fmt(&self, _f: &mut fmt::Formatter<'_>) -> fmt::Result {
        unreachable!()
    }
}

impl Error for QuitError {}

// Opcode 0x121.
/// Perform sanity checks on the game file, using its length and checksum.
///
/// S1 is set to zero if everything looks good, 1 if there seems to be a problem.
pub(crate) fn verify<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), VerifyError> {
    let [s1] = c.cpu.read_operands(&c.main_memory)?;
    let is_verified = c.main_memory.is_verified();
    s1.store(&mut c.main_memory, &mut c.stack, !is_verified as u32)?;

    Ok(())
}

opcode_error! {"verify" VerifyError =>
    ReadOperands(ReadOperandsError),
    OperandStore(OperandStoreError),
}

// Opcode 0x122.
/// Restores the VM to its initial state (memory, stack, and registers).
///
/// Note that the current memory size is reset, as well as the contents of memory.
pub(crate) fn restart<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), RestartError> {
    // Load the initial snapshot.
    // We don't check the existence of the snapshot nor if the load was successful.
    // The initial snapshot is normally always valid.
    let snapshot = c.initial_snapshot.take().unwrap();
    c.load_quetzal(&snapshot);
    c.initial_snapshot = Some(snapshot);

    // Call the start function.
    // Since we already had to call it at the start of the story,
    // there should be no reason it can fail so we unwrap.
    call_function_without_stub(c, c.main_memory.start_function(), &[]).unwrap();

    Ok(())
}

#[derive(Debug)]
pub(crate) enum RestartError {}

impl fmt::Display for RestartError {
    fn fmt(&self, _f: &mut fmt::Formatter<'_>) -> fmt::Result {
        unreachable!()
    }
}

impl Error for RestartError {}

// Opcode 0x123.
/// Saves the VM state to the output stream L1.
///
/// S1 is set to 0 if the operation succeeded, 1 if it failed,
/// and -1 if the VM has just been restored
/// and is continuing from this instruction.
pub(crate) fn save<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), SaveError> {
    let [l1, s1] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = l1.load(&c.main_memory, &mut c.stack)?;

    match c.cpu.io_system {
        IoSystem::Null => return Err(SaveError::IoSystemIsNull(IoSystemIsNullError)),
        IoSystem::Filter => return Err(SaveError::IoSystemIsFilter(IoSystemIsFilterError)),
        IoSystem::Glk => {
            if let Some(stream) = G::opaque_id_from_u32(l1) {
                let save = c.quetzal_snapshot(s1);
                c.glk.put_slice_stream(&mut c.main_memory, stream, &save.0)
            } else {
                return Err(SaveError::InvalidGlkStream(InvalidGlkStreamError(l1)));
            }
        }
    }

    // If we get here then the operation succeeded.
    s1.store(&mut c.main_memory, &mut c.stack, 0)?;

    Ok(())
}

opcode_error! {"save" SaveError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    IoSystemIsNull(IoSystemIsNullError),
    IoSystemIsFilter(IoSystemIsFilterError),
    InvalidGlkStream(InvalidGlkStreamError),
    OperandStore(OperandStoreError),
}

#[derive(Debug)]
#[non_exhaustive]
pub(crate) struct IoSystemIsNullError;

impl fmt::Display for IoSystemIsNullError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "the current I/O system is \"null\"")
    }
}

impl Error for IoSystemIsNullError {}

#[derive(Debug)]
#[non_exhaustive]
pub(crate) struct IoSystemIsFilterError;

impl fmt::Display for IoSystemIsFilterError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "the current I/O system is \"filter\"")
    }
}

impl Error for IoSystemIsFilterError {}

#[derive(Debug)]
#[non_exhaustive]
pub(crate) struct InvalidGlkStreamError(u32);

impl fmt::Display for InvalidGlkStreamError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "the Glk stream {} is invalid", self.0)
    }
}

impl Error for InvalidGlkStreamError {}

// Opcode 0x124.
/// Restores the VM state from the input stream L1.
///
/// S1 is set to 1 if the operation failed.
/// If it succeeded, this instruction never returns a value.
pub(crate) fn restore<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), RestoreError> {
    let [l1, s1] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = l1.load(&c.main_memory, &mut c.stack)?;

    match c.cpu.io_system {
        IoSystem::Null => return Err(RestoreError::IoSystemIsNull(IoSystemIsNullError)),
        IoSystem::Filter => return Err(RestoreError::IoSystemIsFilter(IoSystemIsFilterError)),
        IoSystem::Glk => {
            if let Some(stream) = G::opaque_id_from_u32(l1) {
                // TODO: Specify a suitable capacity for the vec.
                let mut data = Vec::new();
                c.glk.get_to_end_stream(&c.main_memory, stream, &mut data);
                let snapshot = Quetzal::from_data(data).expect("TODO: handle invalid save file");
                if c.load_quetzal(&snapshot) {
                    return Ok(());
                }
            } else {
                return Err(RestoreError::InvalidGlkStream(InvalidGlkStreamError(l1)));
            }
        }
    }

    s1.store(&mut c.main_memory, &mut c.stack, 1)?;

    Ok(())
}

opcode_error! {"restore" RestoreError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    IoSystemIsNull(IoSystemIsNullError),
    IoSystemIsFilter(IoSystemIsFilterError),
    InvalidGlkStream(InvalidGlkStreamError),
    OperandStore(OperandStoreError),
}

// Opcode 0x126.
/// Restores the VM state from temporary storage.
///
/// S1 is set to 1 if the operation failed.
pub(crate) fn restoreundo<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), RestoreundoError> {
    let [s1] = c.cpu.read_operands(&c.main_memory)?;
    if let Some(snapshot) = c.undo_snapshots.pop() {
        if !c.load_quetzal(&snapshot) {
            s1.store(&mut c.main_memory, &mut c.stack, 1)?;
        }
    } else {
        s1.store(&mut c.main_memory, &mut c.stack, 1)?;
    }

    Ok(())
}

opcode_error! {"restoreundo" RestoreundoError =>
    ReadOperands(ReadOperandsError),
    OperandStore(OperandStoreError),
}

// Opcode 0x125.
/// Saves the VM state in a temporary location.
///
/// A location appropriate for rapid access will be chosen, so this may be
/// called once per turn. S1 is set to zero if the operation succeeded, 1
/// if it failed, and -1 if the VM state has just been restored.
pub(crate) fn saveundo<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), SaveundoError> {
    let [s1] = c.cpu.read_operands(&c.main_memory)?;
    // TODO: Limit the number of undo snapshots saved.
    let snapshot = c.quetzal_snapshot(s1);
    c.undo_snapshots.push(snapshot);

    s1.store(&mut c.main_memory, &mut c.stack, 0)?;

    Ok(())
}

opcode_error! {"saveundo" SaveundoError =>
    ReadOperands(ReadOperandsError),
    OperandStore(OperandStoreError),
}

// Opcode 0x127.
/// Protects a range of memory from restart, restore and restoreundo.
///
/// The protected range starts at address L1 and has a length of L2 bytes.
/// This memory is silently unaffected by the state-restoring operations.
/// (However, if the result-storage S1 is directed into the protected range,
/// that is not blocked.)
pub(crate) fn protect<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), ProtectError> {
    let [l1, l2] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = l1.load(&c.main_memory, &mut c.stack)?;
    let l2 = l2.load(&c.main_memory, &mut c.stack)?;

    c.main_memory.protected = crate::memory::Block { start: l1, len: l2 };

    Ok(())
}

opcode_error! {"protect" ProtectError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
}

// Opcode 0x128.
/// Test whether a VM state is available in temporary storage.
///
/// S1 is set to 0 if a state is available, 1 if not. If this returns 0,
/// then restoreundo is expected to succeed.
pub(crate) fn hasundo<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), HasundoError> {
    let [s1] = c.cpu.read_operands(&c.main_memory)?;
    s1.store(
        &mut c.main_memory,
        &mut c.stack,
        c.undo_snapshots.is_empty() as u32,
    )?;

    Ok(())
}

opcode_error! {"hasundo" HasundoError =>
    ReadOperands(ReadOperandsError),
    OperandStore(OperandStoreError),
}

// Opcode 0x129.
/// Discard a VM state (the most recently saved) from temporary storage.
///
/// If none is available, this does nothing.
pub(crate) fn discardundo<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), DiscardundoError> {
    c.undo_snapshots.pop();

    Ok(())
}

#[derive(Debug)]
pub(crate) enum DiscardundoError {}

impl fmt::Display for DiscardundoError {
    fn fmt(&self, _f: &mut fmt::Formatter<'_>) -> fmt::Result {
        unreachable!()
    }
}

impl Error for DiscardundoError {}

// Opcode 0x130.
// TODO: This will likely change a lot since the Glauque API is not finalised.
/// Calls the Glk API function whose identifier is L1, passing in L2 arguments. The return value is stored at S1.
/// (If the Glk function has no return value, zero is stored at S1.)
pub(crate) fn glk<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), GlkError> {
    let [l1, l2, s1] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = l1.load(&c.main_memory, &mut c.stack)?;
    let l2 = l2.load(&c.main_memory, &mut c.stack)?;

    // Get the Glk arguments from the stack.
    let mut args = Vec::with_capacity(l2.try_into().unwrap());
    for _ in 0..l2 {
        args.push(c.stack.pop_u32());
    }

    // Sometimes we have to handle special cases before dispatching,
    // like preprocessing the arguments.
    // I don't really like that
    // because it breaks a bit the dispatching abstraction,
    // but I suppose it's the best we can do.
    match l1 {
        // glk_exit
        0x0001 => {
            c.quit(); // Will call Glauque::exit.
            return Ok(());
        }
        // glk_put_string
        0x0082 => match c.main_memory.read_u8(args[0])? {
            // Skip the type byte for unencoded Latin-1 string.
            0xE0 => args[0] += 1,
            // Error on unencoded Unicode string.
            0xE2 => {
                return Err(GlkError::GlkPutString(
                    GlkPutStringError::PutStringWithUnicodeString,
                ))
            }
            // Error on compressed string.
            0xE1 => {
                return Err(GlkError::GlkPutString(
                    GlkPutStringError::PutStringWithCompressedString,
                ))
            }
            _ => {
                return Err(GlkError::GlkPutString(
                    GlkPutStringError::PutStringWithNonString,
                ))
            }
        },
        // glk_put_string_uni
        0x0129 => match c.main_memory.read_u8(args[0])? {
            // Skip the type bytes for unencoded Unicode string.
            0xE2 => args[0] += 4,
            // Error on unencoded Unicode string.
            0xE0 => {
                return Err(GlkError::GlkPutString(
                    GlkPutStringError::PutStringUniWithLatin1String,
                ))
            }
            // Error on compressed string.
            0xE1 => {
                return Err(GlkError::GlkPutString(
                    GlkPutStringError::PutStringUniWithCompressedString,
                ))
            }
            _ => {
                return Err(GlkError::GlkPutString(
                    GlkPutStringError::PutStringUniWithNonString,
                ))
            }
        },
        0xC0 | 0xC1 => {
            // If we are dispatching Glk functions `select` or `select_poll`,
            // we call them manually, stop right now and set the VM as awaiting an event.
            //
            // We're obliged to do that, since we split `select`/`select_poll` into two
            // to make it possible to pause the execution at this point.
            match l1 {
                0xC0 => c.glk.select(),
                0xC1 => c.glk.select_poll(),
                _ => unreachable!(),
            }
            c.status = StatusInternal::AwaitingEvent {
                event_addr: args[0],
                operand: s1,
                polling: l1 == 0xC1,
            };
            return Ok(());
        }
        // Do nothing for the rest.
        _ => (),
    }

    let dispatch_result = c
        .glk
        .dispatch(&mut c.main_memory, &mut c.stack, l1, &args)
        .expect("TODO: handle dispatch error");

    s1.store(&mut c.main_memory, &mut c.stack, dispatch_result)?;
    Ok(())
}

opcode_error! {"glk" GlkError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    MemoryRead(MemoryReadError),
    GlkPutString(GlkPutStringError),
    OperandStore(OperandStoreError),
}

#[derive(Debug)]
#[non_exhaustive]
pub(crate) enum GlkPutStringError {
    PutStringWithUnicodeString,
    PutStringWithCompressedString,
    PutStringWithNonString,
    PutStringUniWithLatin1String,
    PutStringUniWithCompressedString,
    PutStringUniWithNonString,
}

impl fmt::Display for GlkPutStringError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::PutStringWithUnicodeString => {
                write!(
                    f,
                    "tried to dispatch `glk_put_string` with a Unicode string"
                )
            }
            Self::PutStringWithCompressedString => {
                write!(
                    f,
                    "tried to dispatch `glk_put_string` with a compressed string"
                )
            }
            Self::PutStringWithNonString => {
                write!(f, "tried to dispatch `glk_put_string` with a non-string")
            }
            Self::PutStringUniWithLatin1String => {
                write!(
                    f,
                    "tried to dispatch `glk_put_string_uni` with a Latin-1 string"
                )
            }
            Self::PutStringUniWithCompressedString => {
                write!(
                    f,
                    "tried to dispatch `glk_put_string_uni` with a compressed string"
                )
            }
            Self::PutStringUniWithNonString => {
                write!(
                    f,
                    "tried to dispatch `glk_put_string_uni` with a non-string"
                )
            }
        }
    }
}

impl Error for GlkPutStringError {}

// Opcode 0x140.
/// Returns the address the terp is currently using for its string-decoding table.
///
/// If there is no table, set, this returns zero.
pub(crate) fn getstringtbl<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), GetstringtblError> {
    let [s1] = c.cpu.read_operands(&c.main_memory)?;
    let addr = c.main_memory.decoding_tree.addr;
    s1.store(&mut c.main_memory, &mut c.stack, addr)?;

    Ok(())
}

opcode_error! {"getstringtbl" GetstringtblError =>
    ReadOperands(ReadOperandsError),
    OperandStore(OperandStoreError),
}

// Opcode 0x141.
/// Changes the address the terp is using for its string-decoding table.
///
/// This may be zero, indicating that there is no table (in which case it is
/// illegal to print any compressed string). Otherwise, it must be the
/// address of a valid string-decoding table.
pub(crate) fn setstringtbl<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), SetstringtblError> {
    let [l1] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = l1.load(&c.main_memory, &mut c.stack)?;

    c.main_memory.decoding_tree.addr = l1;
    c.main_memory.cache_decoding_tree()?;

    Ok(())
}

opcode_error! {"setstringtbl" SetstringtblError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    CacheDecodingTree(CacheDecodingTreeError),
}

// Opcode 0x148.
/// Returns the current I/O system mode and rock.
///
/// Due to a long-standing bug in the reference interpreter, the two store
/// operands must be of the same general type: both main-memory/global
/// stores, both local variable stores, or both stack pushes.
pub(crate) fn getiosys<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), GetiosysError> {
    let [s1, s2] = c.cpu.read_operands(&c.main_memory)?;
    let io_system_number = match c.cpu.io_system {
        IoSystem::Null => 0,
        IoSystem::Filter => 1,
        IoSystem::Glk => 2,
    };

    s1.store(&mut c.main_memory, &mut c.stack, io_system_number)?;
    s2.store(&mut c.main_memory, &mut c.stack, c.cpu.io_system_rock)?;

    Ok(())
}

opcode_error! {"getiosys" GetiosysError =>
    ReadOperands(ReadOperandsError),
    OperandStore(OperandStoreError),
}

// Opcode 0x149.
/// Sets the I/O system mode and rock.
///
/// If the system L1 is not supported by the interpreter, it will default to the "null" system (0).
pub(crate) fn setiosys<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), SetiosysError> {
    let [l1, l2] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = l1.load(&c.main_memory, &mut c.stack)?;
    let l2 = l2.load(&c.main_memory, &mut c.stack)?;

    c.cpu.io_system = match l1 {
        0 => IoSystem::Null,
        1 => IoSystem::Filter,
        2 => IoSystem::Glk,
        _ => IoSystem::Null, // Defaults to null if unknown.
    };
    c.cpu.io_system_rock = l2;

    Ok(())
}

opcode_error! {"setiosys" SetiosysError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
}

// Opcode 0x150.
pub(crate) fn linearsearch<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), LinearsearchError> {
    let [l1, l2, l3, l4, l5, l6, l7, s1] = c.cpu.read_operands(&c.main_memory)?;
    let key = l1.load(&c.main_memory, &mut c.stack)?;
    let key_size = l2.load(&c.main_memory, &mut c.stack)?;
    let key_size_usize = usize::try_from(key_size).unwrap();
    let start = l3.load(&c.main_memory, &mut c.stack)?;
    let struct_size = l4.load(&c.main_memory, &mut c.stack)?;
    let struct_number = l5.load(&c.main_memory, &mut c.stack)?;
    let key_offset = l6.load(&c.main_memory, &mut c.stack)?;
    let options = l7.load(&c.main_memory, &mut c.stack)?;

    let key = if options & 0x1 == 0 {
        match &key_size_usize {
            1 | 2 | 4 => key.to_be_bytes()[4 - key_size_usize..4].to_vec(),
            _ => {
                return Err(LinearsearchError::InvalidSearchKeySize(
                    InvalidSearchKeySizeError(key_size),
                ))
            }
        }
    } else {
        c.main_memory.read_slice(key, key_size)?.to_vec()
    };

    let zero_key_terminates = options & 0x2 != 0;
    let mut found = None;
    for i in 0..struct_number {
        // TODO: If struct_number is 0xFF_FF_FF_FF (meaning no upper limit),
        // or a too large value, the tested key might go beyond the main
        // memory. Ideally, we ought to check that.
        let tested_key = c
            .main_memory
            .read_slice(start + i * struct_size + key_offset, key_size)?;
        // It works because slices are compared lexicographically.
        if tested_key == key {
            found = Some(i);
            break;
        }
        if zero_key_terminates && tested_key.iter().all(|byte| byte == &0) {
            break;
        }
    }

    let return_index = options & 0x4 != 0;
    let result = match found {
        Some(index) if return_index => index,
        Some(index) => start + struct_size * index,
        None if return_index => 0xFF_FF_FF_FF,
        None => 0,
    };

    s1.store(&mut c.main_memory, &mut c.stack, result)?;

    Ok(())
}

opcode_error! {"linearsearch" LinearsearchError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    MemoryRead(MemoryReadError),
    InvalidSearchKeySize(InvalidSearchKeySizeError),
    OperandStore(OperandStoreError),
}

#[derive(Debug)]
#[non_exhaustive]
pub(crate) struct InvalidSearchKeySizeError(pub(crate) u32);

impl fmt::Display for InvalidSearchKeySizeError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "invalid key size {}", self.0)
    }
}

impl Error for InvalidSearchKeySizeError {}

// Opcode 0x151.
pub(crate) fn binarysearch<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), BinarysearchError> {
    let [l1, l2, l3, l4, l5, l6, l7, s1] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = l1.load(&c.main_memory, &mut c.stack)?;
    let l2 = l2.load(&c.main_memory, &mut c.stack)?;
    let l3 = l3.load(&c.main_memory, &mut c.stack)?;
    let l4 = l4.load(&c.main_memory, &mut c.stack)?;
    let l5 = l5.load(&c.main_memory, &mut c.stack)?;
    let l6 = l6.load(&c.main_memory, &mut c.stack)?;
    let l7 = l7.load(&c.main_memory, &mut c.stack)?;

    let result = c.main_memory.binary_search([l1, l2, l3, l4, l5, l6, l7])?;

    s1.store(&mut c.main_memory, &mut c.stack, result)?;

    Ok(())
}

opcode_error! {"binarysearch" BinarysearchError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    MemoryRead(MemoryReadError),
    InvalidSearchKeySize(InvalidSearchKeySizeError),
    OperandStore(OperandStoreError),
}

// Opcode 0x152.
pub(crate) fn linkedsearch<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), LinkedsearchError> {
    let [l1, l2, l3, l4, l5, l6, s1] = c.cpu.read_operands(&c.main_memory)?;
    let key = l1.load(&c.main_memory, &mut c.stack)?;
    let key_size = l2.load(&c.main_memory, &mut c.stack)?;
    let key_size_usize = usize::try_from(key_size).unwrap();
    let mut start = l3.load(&c.main_memory, &mut c.stack)?;
    let key_offset = l4.load(&c.main_memory, &mut c.stack)?;
    let next_offset = l5.load(&c.main_memory, &mut c.stack)?;
    let options = l6.load(&c.main_memory, &mut c.stack)?;

    let key = if options & 0x1 == 0 {
        match &key_size_usize {
            1 | 2 | 4 => key.to_be_bytes()[4 - key_size_usize..4].to_vec(),
            _ => {
                return Err(LinkedsearchError::InvalidSearchKeySize(
                    InvalidSearchKeySizeError(key_size),
                ))
            }
        }
    } else {
        c.main_memory.read_slice(key, key_size)?.to_vec()
    };

    let zero_key_terminates = options & 0x2 != 0;
    while start != 0 {
        let tested_key = c.main_memory.read_slice(start + key_offset, key_size)?;
        // It works because slices are compared lexicographically.
        if tested_key == key {
            break;
        }
        if zero_key_terminates && tested_key.iter().all(|byte| byte == &0) {
            start = 0;
            break;
        }
        start = c.main_memory.read_u32(start + next_offset)?;
    }

    s1.store(&mut c.main_memory, &mut c.stack, start)?;

    Ok(())
}

opcode_error! {"linkedsearch" LinkedsearchError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    MemoryRead(MemoryReadError),
    InvalidSearchKeySize(InvalidSearchKeySizeError),
    OperandStore(OperandStoreError),
}

// Opcode 0x160.
/// Calls the function whose address is L1, passing zero argument, and store the return result at S1.
pub(crate) fn callf<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), CallfError> {
    let [l1, s1] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = l1.load(&c.main_memory, &mut c.stack)?;
    let (dest_type, dest_addr) = s1.to_dest_type_addr(&c.main_memory);

    call_function(c, l1, &[], dest_type, dest_addr)?;

    Ok(())
}

opcode_error! {"callf" CallfError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    CallFunction(CallFunctionError)
}

// Opcode 0x161.
/// Calls the function whose address is L1, passing L2 as argument, and store the return result at S1.
pub(crate) fn callfi<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), CallfiError> {
    let [l1, l2, s1] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = l1.load(&c.main_memory, &mut c.stack)?;
    let l2 = l2.load(&c.main_memory, &mut c.stack)?;
    let (dest_type, dest_addr) = s1.to_dest_type_addr(&c.main_memory);

    call_function(c, l1, &[l2], dest_type, dest_addr)?;

    Ok(())
}

opcode_error! {"callfi" CallfiError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    CallFunction(CallFunctionError)
}

// Opcode 0x162.
/// Calls the function whose address is L1, passing L2 and L3 as arguments, and store the return result at S1.
pub(crate) fn callfii<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), CallfiiError> {
    let [l1, l2, l3, s1] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = l1.load(&c.main_memory, &mut c.stack)?;
    let l2 = l2.load(&c.main_memory, &mut c.stack)?;
    let l3 = l3.load(&c.main_memory, &mut c.stack)?;
    let (dest_type, dest_addr) = s1.to_dest_type_addr(&c.main_memory);

    call_function(c, l1, &[l2, l3], dest_type, dest_addr)?;

    Ok(())
}

opcode_error! {"callfii" CallfiiError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    CallFunction(CallFunctionError)
}

// Opcode 0x163.
/// Calls the function whose address is L1, passing L2, L3 and L4 as arguments, and store the return result at S1.
pub(crate) fn callfiii<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), CallfiiiError> {
    let [l1, l2, l3, l4, s1] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = l1.load(&c.main_memory, &mut c.stack)?;
    let l2 = l2.load(&c.main_memory, &mut c.stack)?;
    let l3 = l3.load(&c.main_memory, &mut c.stack)?;
    let l4 = l4.load(&c.main_memory, &mut c.stack)?;
    let (dest_type, dest_addr) = s1.to_dest_type_addr(&c.main_memory);

    call_function(c, l1, &[l2, l3, l4], dest_type, dest_addr)?;

    Ok(())
}
opcode_error! {"callfiii" CallfiiiError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    CallFunction(CallFunctionError)
}

// Opcode 0x170.
/// Writes L1 zero bytes, starting at address L2.
pub(crate) fn mzero<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), MzeroError> {
    let [l1, l2] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = l1.load(&c.main_memory, &mut c.stack)?;
    let l2 = l2.load(&c.main_memory, &mut c.stack)?;

    for addr in l2..l2 + l1 {
        c.main_memory.write_u8(addr, 0)?;
    }

    Ok(())
}

opcode_error! {"mzero" MzeroError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    MemoryWrite(MemoryWriteError),
}

// Opcode 0x171.
/// Copies L1 bytes from address L2 to address L3.
///
/// It is safe to copy a block to an overlapping block.
pub(crate) fn mcopy<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), McopyError> {
    let [l1, l2, l3] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = l1.load(&c.main_memory, &mut c.stack)?;
    let l2 = l2.load(&c.main_memory, &mut c.stack)?;
    let l3 = l3.load(&c.main_memory, &mut c.stack)?;

    let slice = c.main_memory.read_slice(l2, l1)?.to_vec();
    c.main_memory.write_slice(l3, &slice)?;

    Ok(())
}

opcode_error! {"mcopy" McopyError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    MemoryRead(MemoryReadError),
    MemoryWrite(MemoryWriteError),
}

// Opcode 0x178.
/// Allocates a memory block of L1 bytes.
pub(crate) fn malloc<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), MallocError> {
    let [l1, s1] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = l1.load(&c.main_memory, &mut c.stack)?;

    let addr = c.main_memory.malloc(l1);

    s1.store(&mut c.main_memory, &mut c.stack, addr)?;

    Ok(())
}

opcode_error! {"malloc" MallocError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    OperandStore(OperandStoreError),
}

// Opcode 0x179.
/// Frees the memory block at address L1.
///
/// This must be the address of an extant block -- that is, a value
/// returned by malloc and not previously freed.
pub(crate) fn mfree<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), MfreeError> {
    let [l1] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = l1.load(&c.main_memory, &mut c.stack)?;

    c.main_memory.mfree(l1)
}

opcode_error! {"mfree" MfreeError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    NoBlockAtAddress(NoBlockAtAddressError),
}

/// The error type when trying to free memory with [`MainMemory::mfree`].
#[derive(Debug)]
#[non_exhaustive]
pub(crate) struct NoBlockAtAddressError {
    pub(crate) addr: u32,
}

impl fmt::Display for NoBlockAtAddressError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{:#X} is not the address of an extant block", self.addr)
    }
}

impl Error for NoBlockAtAddressError {}

// Opcode 0x180.
/// Request that the VM function with address L2 be replaced by the accelerated function whose number is L1.
///
/// If L1 is zero, the acceleration for address L2 is cancelled.
pub(crate) fn accelfunc<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), AccelfuncError> {
    let [l1, l2] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = l1.load(&c.main_memory, &mut c.stack)?;
    let l2 = l2.load(&c.main_memory, &mut c.stack)?;

    c.acceleration.register(l2, l1);

    Ok(())
}

opcode_error! {"accelfunc" AccelfuncError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
}

// Opcode 0x181.
/// Stores the value L2 in the parameter table at position L1.
pub(crate) fn accelparam<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), AccelparamError> {
    let [l1, l2] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = l1.load(&c.main_memory, &mut c.stack)?;
    let l2 = l2.load(&c.main_memory, &mut c.stack)?;

    c.acceleration.set_param(l1.try_into().unwrap(), l2);

    Ok(())
}

opcode_error! {"accelparam" AccelparamError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
}

// Opcode 0x190.
/// Converts an integer value to the closest equivalent float.
///
/// That is, if L1 is 1, then 0x3F800000 -- the float encoding of 1.0 --
/// will be stored in S1. Integer zero is converted to (positive) float
/// zero.
pub(crate) fn numtof<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), NumtofError> {
    let [l1, s1] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = l1.load(&c.main_memory, &mut c.stack)? as i32;

    let result = f32::to_bits(l1 as f32);
    s1.store(&mut c.main_memory, &mut c.stack, result)?;

    Ok(())
}

opcode_error! {"numtof" NumtofError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    OperandStore(OperandStoreError),
}

// Opcode 0x191.
/// Converts a float value to an integer, rounding towards zero.
///
/// If the value is outside the 32-bit integer range, or is NaN or
/// infinity, the result will be 0x7FFFFFFF (for positive values) or
/// 0x80000000 (for negative values).
pub(crate) fn ftonumz<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), FtonumzError> {
    let [l1, s1] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = f32::from_bits(l1.load(&c.main_memory, &mut c.stack)?);

    let result = if l1.is_nan() {
        if l1.is_sign_positive() {
            0x7F_FF_FF_FF
        } else {
            0x80_00_00_00
        }
    } else {
        l1 as i32 as u32 // Rust rounds toward 0 when casting.
    };

    s1.store(&mut c.main_memory, &mut c.stack, result)?;

    Ok(())
}

opcode_error! {"ftonumz" FtonumzError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    OperandStore(OperandStoreError),
}

// Opcode 0x192.
/// Converts a float value to an integer, rounding towards the nearest integer.
///
/// Again, overflows become 0x7FFFFFFF or 0x80000000.
pub(crate) fn ftonumn<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), FtonumnError> {
    let [l1, s1] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = f32::from_bits(l1.load(&c.main_memory, &mut c.stack)?);

    let result = if l1.is_nan() {
        if l1.is_sign_positive() {
            0x7F_FF_FF_FF
        } else {
            0x80_00_00_00
        }
    } else {
        l1.round() as i32 as u32
    };

    s1.store(&mut c.main_memory, &mut c.stack, result)?;

    Ok(())
}

opcode_error! {"ftonumn" FtonumnError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    OperandStore(OperandStoreError),
}

// Opcode 0x198.
/// Round L1 up (towards Inf) to the nearest integral value.
pub(crate) fn ceil<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), CeilError> {
    let [l1, s1] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = f32::from_bits(l1.load(&c.main_memory, &mut c.stack)?);

    let result = f32::to_bits(l1.ceil());
    s1.store(&mut c.main_memory, &mut c.stack, result)?;

    Ok(())
}

opcode_error! {"ceil" CeilError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    OperandStore(OperandStoreError),
}

// Opcode 0x199.
/// Round L1 down (towards −Inf) to the nearest integral value.
pub(crate) fn floor<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), FloorError> {
    let [l1, s1] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = f32::from_bits(l1.load(&c.main_memory, &mut c.stack)?);

    let result = f32::to_bits(l1.floor());
    s1.store(&mut c.main_memory, &mut c.stack, result)?;

    Ok(())
}

opcode_error! {"floor" FloorError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    OperandStore(OperandStoreError),
}

// Opcode 0x1A0.
/// Adds L1 and L2, using 32-bit floating point addition.
pub(crate) fn fadd<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), FaddError> {
    let [l1, l2, s1] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = f32::from_bits(l1.load(&c.main_memory, &mut c.stack)?);
    let l2 = f32::from_bits(l2.load(&c.main_memory, &mut c.stack)?);

    let result = f32::to_bits(l1 + l2);
    s1.store(&mut c.main_memory, &mut c.stack, result)?;

    Ok(())
}

opcode_error! {"fadd" FaddError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    OperandStore(OperandStoreError),
}

// Opcode 0x1A1.
/// Subtracts L2 from L1, using 32-bit floating point subtraction.
pub(crate) fn fsub<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), FsubError> {
    let [l1, l2, s1] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = f32::from_bits(l1.load(&c.main_memory, &mut c.stack)?);
    let l2 = f32::from_bits(l2.load(&c.main_memory, &mut c.stack)?);

    let result = f32::to_bits(l1 - l2);
    s1.store(&mut c.main_memory, &mut c.stack, result)?;

    Ok(())
}

opcode_error! {"fsub" FsubError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    OperandStore(OperandStoreError),
}

// Opcode 0x1A2.
/// Multiplies L1 and L2, using 32-bit floating point multiplication.
pub(crate) fn fmul<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), FmulError> {
    let [l1, l2, s1] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = f32::from_bits(l1.load(&c.main_memory, &mut c.stack)?);
    let l2 = f32::from_bits(l2.load(&c.main_memory, &mut c.stack)?);

    let result = f32::to_bits(l1 * l2);
    s1.store(&mut c.main_memory, &mut c.stack, result)?;

    Ok(())
}

opcode_error! {"fmul" FmulError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    OperandStore(OperandStoreError),
}

// Opcode 0x1A3.
/// Computes (L1 / L2), using 32-bit floating point division.
pub(crate) fn fdiv<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), FdivError> {
    let [l1, l2, s1] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = f32::from_bits(l1.load(&c.main_memory, &mut c.stack)?);
    let l2 = f32::from_bits(l2.load(&c.main_memory, &mut c.stack)?);

    let result = f32::to_bits(l1 / l2);
    s1.store(&mut c.main_memory, &mut c.stack, result)?;

    Ok(())
}

opcode_error! {"fdiv" FdivError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    OperandStore(OperandStoreError),
}

// Opcode 0x1A4.
/// Performs a floating-point modulo operation.
///
/// S1 is the remainder (or modulus); S2 is the quotient.
pub(crate) fn fmod<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), FmodError> {
    let [l1, l2, s1, s2] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = f32::from_bits(l1.load(&c.main_memory, &mut c.stack)?);
    let l2 = f32::from_bits(l2.load(&c.main_memory, &mut c.stack)?);

    // For some reason we have to take care of a special case.
    let (modulus, quotient) = if l1.is_infinite() || l2 == 0.0 {
        (f32::NAN, f32::NAN)
    } else {
        ((l1 % l2), (l1 / l2).trunc())
    };

    s1.store(&mut c.main_memory, &mut c.stack, f32::to_bits(modulus))?;
    s2.store(&mut c.main_memory, &mut c.stack, f32::to_bits(quotient))?;

    Ok(())
}

opcode_error! {"fmod" FmodError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    OperandStore(OperandStoreError),
}

// Opcode 0x1A8.
/// Computes the square root of L1.
pub(crate) fn sqrt<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), SqrtError> {
    let [l1, s1] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = f32::from_bits(l1.load(&c.main_memory, &mut c.stack)?);

    let result = f32::to_bits(l1.sqrt());
    s1.store(&mut c.main_memory, &mut c.stack, result)?;

    Ok(())
}

opcode_error! {"sqrt" SqrtError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    OperandStore(OperandStoreError),
}

// Opcode 0x1A9.
/// Computes e^L1.
pub(crate) fn exp<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), ExpError> {
    let [l1, s1] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = f32::from_bits(l1.load(&c.main_memory, &mut c.stack)?);

    let result = f32::to_bits(l1.exp());
    s1.store(&mut c.main_memory, &mut c.stack, result)?;

    Ok(())
}

opcode_error! {"exp" ExpError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    OperandStore(OperandStoreError),
}

// Opcode 0x1AA.
/// Computes the log of L1 (base e).
pub(crate) fn log<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), LogError> {
    let [l1, s1] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = f32::from_bits(l1.load(&c.main_memory, &mut c.stack)?);

    let result = f32::to_bits(l1.ln());
    s1.store(&mut c.main_memory, &mut c.stack, result)?;

    Ok(())
}

opcode_error! {"log" LogError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    OperandStore(OperandStoreError),
}

// Opcode 0x1AB.
/// Compute L1 raised to the L2 power.
pub(crate) fn pow<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), PowError> {
    let [l1, l2, s1] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = f32::from_bits(l1.load(&c.main_memory, &mut c.stack)?);
    let l2 = f32::from_bits(l2.load(&c.main_memory, &mut c.stack)?);

    // For some reason, pow(1, NaN) doesn't return 1.0 for some values of
    // NaN. It's OK with f32::NAN, but not with the NaN used in Glulxercise.
    let result = if (l1 - 1.0).abs() <= f32::EPSILON || l2 == 0.0 {
        1.0
    } else {
        l1.powf(l2)
    };
    s1.store(&mut c.main_memory, &mut c.stack, f32::to_bits(result))?;

    Ok(())
}

opcode_error! {"pow" PowError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    OperandStore(OperandStoreError),
}

// Opcode 0x1B0.
/// Computes the sine of L1.
pub(crate) fn sin<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), SinError> {
    let [l1, s1] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = f32::from_bits(l1.load(&c.main_memory, &mut c.stack)?);

    let result = f32::to_bits(l1.sin());
    s1.store(&mut c.main_memory, &mut c.stack, result)?;

    Ok(())
}

opcode_error! {"sin" SinError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    OperandStore(OperandStoreError),
}

// Opcode 0x1B1.
/// Computes the cosine of L1.
pub(crate) fn cos<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), CosError> {
    let [l1, s1] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = f32::from_bits(l1.load(&c.main_memory, &mut c.stack)?);

    let result = f32::to_bits(l1.cos());
    s1.store(&mut c.main_memory, &mut c.stack, result)?;

    Ok(())
}

opcode_error! {"cos" CosError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    OperandStore(OperandStoreError),
}

// Opcode 0x1B2.
/// Computes the tangent of L1.
pub(crate) fn tan<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), TanError> {
    let [l1, s1] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = f32::from_bits(l1.load(&c.main_memory, &mut c.stack)?);

    let result = f32::to_bits(l1.tan());
    s1.store(&mut c.main_memory, &mut c.stack, result)?;

    Ok(())
}

opcode_error! {"tan" TanError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    OperandStore(OperandStoreError),
}

// Opcode 0x1B3.
/// Computes the arcsine of L1.
pub(crate) fn asin<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), AsinError> {
    let [l1, s1] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = f32::from_bits(l1.load(&c.main_memory, &mut c.stack)?);

    let result = f32::to_bits(l1.asin());
    s1.store(&mut c.main_memory, &mut c.stack, result)?;

    Ok(())
}

opcode_error! {"asin" AsinError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    OperandStore(OperandStoreError),
}

// Opcode 0x1B4.
/// Computes the arccosine of L1.
pub(crate) fn acos<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), AcosError> {
    let [l1, s1] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = f32::from_bits(l1.load(&c.main_memory, &mut c.stack)?);

    let result = f32::to_bits(l1.acos());
    s1.store(&mut c.main_memory, &mut c.stack, result)?;

    Ok(())
}

opcode_error! {"acos" AcosError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    OperandStore(OperandStoreError),
}

// Opcode 0x1B5.
/// Computes the arctangent of L1.
pub(crate) fn atan<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), AtanError> {
    let [l1, s1] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = f32::from_bits(l1.load(&c.main_memory, &mut c.stack)?);

    let result = f32::to_bits(l1.atan());
    s1.store(&mut c.main_memory, &mut c.stack, result)?;

    Ok(())
}

opcode_error! {"atan" AtanError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    OperandStore(OperandStoreError),
}

// Opcode 0x1B6.
/// Computes the arctangent of L1 / L2.
pub(crate) fn atan2<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), Atan2Error> {
    let [l1, l2, s1] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = f32::from_bits(l1.load(&c.main_memory, &mut c.stack)?);
    let l2 = f32::from_bits(l2.load(&c.main_memory, &mut c.stack)?);

    let result = f32::to_bits(l1.atan2(l2));
    s1.store(&mut c.main_memory, &mut c.stack, result)?;

    Ok(())
}

opcode_error! {"atan2" Atan2Error =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    OperandStore(OperandStoreError),
}

// Opcode 0x1C0.
/// Branches to L4 if the difference between L1 and L2 is less than or
/// equal to (plus or minus) L3.
///
/// The sign of L3 is ignored.
pub(crate) fn jfeq<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), JfeqError> {
    let [l1, l2, l3, l4] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = f32::from_bits(l1.load(&c.main_memory, &mut c.stack)?);
    let l2 = f32::from_bits(l2.load(&c.main_memory, &mut c.stack)?);
    let l3 = f32::from_bits(l3.load(&c.main_memory, &mut c.stack)?);
    let l4 = l4.load(&c.main_memory, &mut c.stack)?;

    let are_equal = if l1.is_nan() || l2.is_nan() || l3.is_nan() {
        false
    } else if l1.is_infinite() && l2.is_infinite() {
        l1.signum() == l2.signum()
    } else if l3.is_infinite() {
        true
    } else {
        (l1 - l2).abs() <= l3.abs()
    };

    if are_equal {
        // Special case for 0 and 1: return from function.
        if l4 == 0 || l4 == 1 {
            return_from_function(c, l4)?;
        } else {
            c.cpu.pc = c.cpu.pc.wrapping_add(l4).wrapping_sub(2);
        }
    }

    Ok(())
}

opcode_error! {"jfeq" JfeqError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    ReturnFromFunction(ReturnFromFunctionError),
}

// Opcode 0x1C1.
/// Branches to L4 if the difference between L1 and L2 is greater than (plus or minus) L3.
///
/// The sign of L3 is ignored.
pub(crate) fn jfne<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), JfneError> {
    let [l1, l2, l3, l4] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = f32::from_bits(l1.load(&c.main_memory, &mut c.stack)?);
    let l2 = f32::from_bits(l2.load(&c.main_memory, &mut c.stack)?);
    let l3 = f32::from_bits(l3.load(&c.main_memory, &mut c.stack)?);
    let l4 = l4.load(&c.main_memory, &mut c.stack)?;

    let are_equal = if l1.is_nan() || l2.is_nan() || l3.is_nan() {
        false
    } else if l1.is_infinite() && l2.is_infinite() {
        l1.signum() == l2.signum()
    } else if l3.is_infinite() {
        true
    } else {
        (l1 - l2).abs() <= l3.abs()
    };

    if !are_equal {
        // Special case for 0 and 1: return from function.
        if l4 == 0 || l4 == 1 {
            return_from_function(c, l4)?;
        } else {
            c.cpu.pc = c.cpu.pc.wrapping_add(l4).wrapping_sub(2);
        }
    }

    Ok(())
}

opcode_error! {"jfne" JfneError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    ReturnFromFunction(ReturnFromFunctionError),
}

// Opcode 0x1C2.
/// Branches to L3 if L1 is less than L2 (using floating point comparison).
pub(crate) fn jflt<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), JfltError> {
    let [l1, l2, l3] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = f32::from_bits(l1.load(&c.main_memory, &mut c.stack)?);
    let l2 = f32::from_bits(l2.load(&c.main_memory, &mut c.stack)?);
    let l3 = l3.load(&c.main_memory, &mut c.stack)?;

    if l1 < l2 {
        // Special case for 0 and 1: return from function.
        if l3 == 0 || l3 == 1 {
            return_from_function(c, l3)?;
        } else {
            c.cpu.pc = c.cpu.pc.wrapping_add(l3).wrapping_sub(2);
        }
    }

    Ok(())
}

opcode_error! {"jflt" JfltError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    ReturnFromFunction(ReturnFromFunctionError),
}

// Opcode 0x1C3.
/// Branches to L3 if L1 is less than or equal to L2 (using floating point comparison).
pub(crate) fn jfle<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), JfleError> {
    let [l1, l2, l3] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = f32::from_bits(l1.load(&c.main_memory, &mut c.stack)?);
    let l2 = f32::from_bits(l2.load(&c.main_memory, &mut c.stack)?);
    let l3 = l3.load(&c.main_memory, &mut c.stack)?;

    if l1 <= l2 {
        // Special case for 0 and 1: return from function.
        if l3 == 0 || l3 == 1 {
            return_from_function(c, l3)?;
        } else {
            c.cpu.pc = c.cpu.pc.wrapping_add(l3).wrapping_sub(2);
        }
    }

    Ok(())
}

opcode_error! {"jfle" JfleError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    ReturnFromFunction(ReturnFromFunctionError),
}

// Opcode 0x1C4.
/// Branches to L3 if L1 is greater than L2 (using floating point comparison).
pub(crate) fn jfgt<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), JfgtError> {
    let [l1, l2, l3] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = f32::from_bits(l1.load(&c.main_memory, &mut c.stack)?);
    let l2 = f32::from_bits(l2.load(&c.main_memory, &mut c.stack)?);
    let l3 = l3.load(&c.main_memory, &mut c.stack)?;

    if l1 > l2 {
        // Special case for 0 and 1: return from function.
        if l3 == 0 || l3 == 1 {
            return_from_function(c, l3)?;
        } else {
            c.cpu.pc = c.cpu.pc.wrapping_add(l3).wrapping_sub(2);
        }
    }

    Ok(())
}

opcode_error! {"jfgt" JfgtError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    ReturnFromFunction(ReturnFromFunctionError),
}

// Opcode 0x1C5.
/// Branches to L3 if L1 is greater than or equal to L2 (using floating point comparison).
pub(crate) fn jfge<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), JfgeError> {
    let [l1, l2, l3] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = f32::from_bits(l1.load(&c.main_memory, &mut c.stack)?);
    let l2 = f32::from_bits(l2.load(&c.main_memory, &mut c.stack)?);
    let l3 = l3.load(&c.main_memory, &mut c.stack)?;

    if l1 >= l2 {
        // Special case for 0 and 1: return from function.
        if l3 == 0 || l3 == 1 {
            return_from_function(c, l3)?;
        } else {
            c.cpu.pc = c.cpu.pc.wrapping_add(l3).wrapping_sub(2);
        }
    }

    Ok(())
}

opcode_error! {"jfge" JfgeError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    ReturnFromFunction(ReturnFromFunctionError),
}

// Opcode 0x1C8.
/// Branches to L2 if L1 is a NaN value.
pub(crate) fn jisnan<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), JisnanError> {
    let [l1, l2] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = f32::from_bits(l1.load(&c.main_memory, &mut c.stack)?);
    let l2 = l2.load(&c.main_memory, &mut c.stack)?;

    if l1.is_nan() {
        // Special case for 0 and 1: return from function.
        if l2 == 0 || l2 == 1 {
            return_from_function(c, l2)?;
        } else {
            c.cpu.pc = c.cpu.pc.wrapping_add(l2).wrapping_sub(2);
        }
    }

    Ok(())
}

opcode_error! {"jisnan" JisnanError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    ReturnFromFunction(ReturnFromFunctionError),
}

// Opcode 0x1C9.
/// Branches to L2 if L1 is an infinity.
pub(crate) fn jisinf<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), JisinfError> {
    let [l1, l2] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = f32::from_bits(l1.load(&c.main_memory, &mut c.stack)?);
    let l2 = l2.load(&c.main_memory, &mut c.stack)?;

    if l1.is_infinite() {
        // Special case for 0 and 1: return from function.
        if l2 == 0 || l2 == 1 {
            return_from_function(c, l2)?;
        } else {
            c.cpu.pc = c.cpu.pc.wrapping_add(l2).wrapping_sub(2);
        }
    }

    Ok(())
}

opcode_error! {"jisinf" JisinfError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    ReturnFromFunction(ReturnFromFunctionError),
}

// Opcode 0x200.
/// Converts an integer value to the closest equivalent 64-bit float.
pub(crate) fn numtod<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), NumtodError> {
    let [l1, s1, s2] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = l1.load(&c.main_memory, &mut c.stack)?;
    Operand::store_double(&mut c.main_memory, &mut c.stack, s1, s2, l1 as i32 as f64)?;

    Ok(())
}

opcode_error! {"numtod" NumtodError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    OperandStore(OperandStoreError),
}

// Opcode 0x201.
/// Converts a float value to an integer, rounding towards zero.
///
/// If the value is outside the 32-bit integer range, or is NaN or
/// infinity, the result will be 0x7FFFFFFF (for positive values) or
/// 0x80000000 (for negative values).
pub(crate) fn dtonumz<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), DtonumzError> {
    let [l1, l2, s1] = c.cpu.read_operands(&c.main_memory)?;
    let l1l2 = Operand::load_double(&c.main_memory, &mut c.stack, l1, l2)?;
    let result = if l1l2.is_nan() {
        if l1l2.is_sign_positive() {
            0x7F_FF_FF_FF
        } else {
            0x80_00_00_00
        }
    } else {
        l1l2.trunc() as i32 as u32
    };

    s1.store(&mut c.main_memory, &mut c.stack, result)?;

    Ok(())
}

opcode_error! {"dtonumz" DtonumzError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    OperandStore(OperandStoreError),
}

// Opcode 0x202.
/// Converts a float value to an integer, rounding towards the nearest integer.
///
/// Again, overflows become 0x7FFFFFFF or 0x80000000.
pub(crate) fn dtonumn<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), DtonumnError> {
    let [l1, l2, s1] = c.cpu.read_operands(&c.main_memory)?;
    let l1l2 = Operand::load_double(&c.main_memory, &mut c.stack, l1, l2)?;
    let result = if l1l2.is_nan() {
        if l1l2.is_sign_positive() {
            0x7F_FF_FF_FF
        } else {
            0x80_00_00_00
        }
    } else {
        l1l2.round() as i32 as u32
    };

    s1.store(&mut c.main_memory, &mut c.stack, result)?;

    Ok(())
}

opcode_error! {"dtonumn" DtonumnError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    OperandStore(OperandStoreError),
}

// Opcode 0x203.
/// Converts a float value L1 to a double value, stored as S2:S1.
pub(crate) fn ftod<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), FtodError> {
    let [l1, s1, s2] = c.cpu.read_operands(&c.main_memory)?;
    let l1 = l1.load(&c.main_memory, &mut c.stack)?;
    let result = f32::from_bits(l1) as f64;
    Operand::store_double(&mut c.main_memory, &mut c.stack, s1, s2, result)?;

    Ok(())
}

opcode_error! {"ftod" FtodError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    OperandStore(OperandStoreError),
}

// Opcode 0x204.
/// Converts a double value L1:L2 to a float value, stored as S1.
pub(crate) fn dtof<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), DtofError> {
    let [l1, l2, s1] = c.cpu.read_operands(&c.main_memory)?;
    let l1l2 = Operand::load_double(&c.main_memory, &mut c.stack, l1, l2)?;
    s1.store(&mut c.main_memory, &mut c.stack, f32::to_bits(l1l2 as f32))?;

    Ok(())
}

opcode_error! {"dtof" DtofError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    OperandStore(OperandStoreError),
}

// Opcode 0x208.
/// Round L1:L2 up (towards Inf) to the nearest integral value.
pub(crate) fn dceil<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), DceilError> {
    let [l1, l2, s1, s2] = c.cpu.read_operands(&c.main_memory)?;
    let l1l2 = Operand::load_double(&c.main_memory, &mut c.stack, l1, l2)?;
    Operand::store_double(&mut c.main_memory, &mut c.stack, s1, s2, l1l2.ceil())?;

    Ok(())
}

opcode_error! {"dceil" DceilError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    OperandStore(OperandStoreError),
}

// Opcode 0x209.
/// Round L1:L2 down (towards −Inf) to the nearest integral value.
pub(crate) fn dfloor<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), DfloorError> {
    let [l1, l2, s1, s2] = c.cpu.read_operands(&c.main_memory)?;
    let l1l2 = Operand::load_double(&c.main_memory, &mut c.stack, l1, l2)?;
    Operand::store_double(&mut c.main_memory, &mut c.stack, s1, s2, l1l2.floor())?;

    Ok(())
}

opcode_error! {"dfloor" DfloorError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    OperandStore(OperandStoreError),
}

// Opcode 0x210.
/// Adds L1:L2 and L3:L4, using 64-bit floating point addition.
pub(crate) fn dadd<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), DaddError> {
    let [l1, l2, l3, l4, s1, s2] = c.cpu.read_operands(&c.main_memory)?;
    let l1l2 = Operand::load_double(&c.main_memory, &mut c.stack, l1, l2)?;
    let l3l4 = Operand::load_double(&c.main_memory, &mut c.stack, l3, l4)?;
    Operand::store_double(&mut c.main_memory, &mut c.stack, s1, s2, l1l2 + l3l4)?;

    Ok(())
}

opcode_error! {"dadd" DaddError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    OperandStore(OperandStoreError),
}

// Opcode 0x211.
/// Subtracts L3:L4 from L1:L2, using 64-bit floating point subtraction.
pub(crate) fn dsub<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), DsubError> {
    let [l1, l2, l3, l4, s1, s2] = c.cpu.read_operands(&c.main_memory)?;
    let l1l2 = Operand::load_double(&c.main_memory, &mut c.stack, l1, l2)?;
    let l3l4 = Operand::load_double(&c.main_memory, &mut c.stack, l3, l4)?;
    Operand::store_double(&mut c.main_memory, &mut c.stack, s1, s2, l1l2 - l3l4)?;

    Ok(())
}

opcode_error! {"dsub" DsubError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    OperandStore(OperandStoreError),
}

// Opcode 0x212.
/// Multiplies L1:L2 and L3:L4, using 64-bit floating point multiplication.
pub(crate) fn dmul<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), DmulError> {
    let [l1, l2, l3, l4, s1, s2] = c.cpu.read_operands(&c.main_memory)?;
    let l1l2 = Operand::load_double(&c.main_memory, &mut c.stack, l1, l2)?;
    let l3l4 = Operand::load_double(&c.main_memory, &mut c.stack, l3, l4)?;
    Operand::store_double(&mut c.main_memory, &mut c.stack, s1, s2, l1l2 * l3l4)?;

    Ok(())
}

opcode_error! {"dmul" DmulError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    OperandStore(OperandStoreError),
}

// Opcode 0x213.
/// Computes (L1:L2 / L3:L4), using 64-bit floating point division.
pub(crate) fn ddiv<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), DdivError> {
    let [l1, l2, l3, l4, s1, s2] = c.cpu.read_operands(&c.main_memory)?;
    let l1l2 = Operand::load_double(&c.main_memory, &mut c.stack, l1, l2)?;
    let l3l4 = Operand::load_double(&c.main_memory, &mut c.stack, l3, l4)?;
    Operand::store_double(&mut c.main_memory, &mut c.stack, s1, s2, l1l2 / l3l4)?;

    Ok(())
}

opcode_error! {"ddiv" DdivError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    OperandStore(OperandStoreError),
}

// Opcode 0x214.
/// Gives the remainder (or modulus) of L1:L2 and L3:L4.
pub(crate) fn dmodr<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), DmodrError> {
    let [l1, l2, l3, l4, s1, s2] = c.cpu.read_operands(&c.main_memory)?;
    let l1l2 = Operand::load_double(&c.main_memory, &mut c.stack, l1, l2)?;
    let l3l4 = Operand::load_double(&c.main_memory, &mut c.stack, l3, l4)?;

    // For some reason we have to take care of a special case.
    let modulus = if l1l2.is_infinite() || l3l4 == 0.0 {
        f64::NAN
    } else {
        l1l2 % l3l4
    };

    Operand::store_double(&mut c.main_memory, &mut c.stack, s1, s2, modulus)?;

    Ok(())
}

opcode_error! {"dmodr" DmodrError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    OperandStore(OperandStoreError),
}

// Opcode 0x215.
/// Gives the quotient of L1:L2 and L3:L4.
pub(crate) fn dmodq<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), DmodqError> {
    let [l1, l2, l3, l4, s1, s2] = c.cpu.read_operands(&c.main_memory)?;
    let l1l2 = Operand::load_double(&c.main_memory, &mut c.stack, l1, l2)?;
    let l3l4 = Operand::load_double(&c.main_memory, &mut c.stack, l3, l4)?;
    // For some reason we have to take care of a special case.

    let quotient = if l1l2.is_infinite() || l3l4 == 0.0 {
        f64::NAN
    } else {
        (l1l2 / l3l4).trunc()
    };

    Operand::store_double(&mut c.main_memory, &mut c.stack, s1, s2, quotient)?;

    Ok(())
}

opcode_error! {"dmodq" DmodqError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    OperandStore(OperandStoreError),
}

// Opcode 0x218.
/// Computes the square root of L1:L2.
pub(crate) fn dsqrt<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), DsqrtError> {
    let [l1, l2, s1, s2] = c.cpu.read_operands(&c.main_memory)?;
    let l1l2 = Operand::load_double(&c.main_memory, &mut c.stack, l1, l2)?;
    Operand::store_double(&mut c.main_memory, &mut c.stack, s1, s2, l1l2.sqrt())?;

    Ok(())
}

opcode_error! {"dsqrt" DsqrtError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    OperandStore(OperandStoreError),
}

// Opcode 0x219.
/// Computes e^L1:L2.
pub(crate) fn dexp<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), DexpError> {
    let [l1, l2, s1, s2] = c.cpu.read_operands(&c.main_memory)?;
    let l1l2 = Operand::load_double(&c.main_memory, &mut c.stack, l1, l2)?;
    Operand::store_double(&mut c.main_memory, &mut c.stack, s1, s2, l1l2.exp())?;

    Ok(())
}

opcode_error! {"dexp" DexpError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    OperandStore(OperandStoreError),
}

// Opcode 0x21A.
/// Computes the log of L1:L2 (base e).
pub(crate) fn dlog<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), DlogError> {
    let [l1, l2, s1, s2] = c.cpu.read_operands(&c.main_memory)?;
    let l1l2 = Operand::load_double(&c.main_memory, &mut c.stack, l1, l2)?;
    Operand::store_double(&mut c.main_memory, &mut c.stack, s1, s2, l1l2.ln())?;

    Ok(())
}

opcode_error! {"dlog" DlogError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    OperandStore(OperandStoreError),
}

// Opcode 0x21B.
/// Compute L1:L2 raised to the L3:L4 power.
pub(crate) fn dpow<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), DpowError> {
    let [l1, l2, l3, l4, s1, s2] = c.cpu.read_operands(&c.main_memory)?;
    let l1l2 = Operand::load_double(&c.main_memory, &mut c.stack, l1, l2)?;
    let l3l4 = Operand::load_double(&c.main_memory, &mut c.stack, l3, l4)?;
    Operand::store_double(&mut c.main_memory, &mut c.stack, s1, s2, l1l2.powf(l3l4))?;

    Ok(())
}

opcode_error! {"dpow" DpowError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    OperandStore(OperandStoreError),
}

// Opcode 0x220.
/// Computes the sine of L1:L2.
pub(crate) fn dsin<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), DsinError> {
    let [l1, l2, s1, s2] = c.cpu.read_operands(&c.main_memory)?;
    let l1l2 = Operand::load_double(&c.main_memory, &mut c.stack, l1, l2)?;
    Operand::store_double(&mut c.main_memory, &mut c.stack, s1, s2, l1l2.sin())?;

    Ok(())
}

opcode_error! {"dsin" DsinError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    OperandStore(OperandStoreError),
}

// Opcode 0x221.
/// Computes the cosine of L1:L2.
pub(crate) fn dcos<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), DcosError> {
    let [l1, l2, s1, s2] = c.cpu.read_operands(&c.main_memory)?;
    let l1l2 = Operand::load_double(&c.main_memory, &mut c.stack, l1, l2)?;
    Operand::store_double(&mut c.main_memory, &mut c.stack, s1, s2, l1l2.cos())?;

    Ok(())
}

opcode_error! {"dcos" DcosError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    OperandStore(OperandStoreError),
}

// Opcode 0x222.
/// Computes the tangent of L1:L2.
pub(crate) fn dtan<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), DtanError> {
    let [l1, l2, s1, s2] = c.cpu.read_operands(&c.main_memory)?;
    let l1l2 = Operand::load_double(&c.main_memory, &mut c.stack, l1, l2)?;
    Operand::store_double(&mut c.main_memory, &mut c.stack, s1, s2, l1l2.tan())?;

    Ok(())
}

opcode_error! {"dtan" DtanError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    OperandStore(OperandStoreError),
}

// Opcode 0x223.
/// Computes the arcsine of L1:L2.
pub(crate) fn dasin<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), DasinError> {
    let [l1, l2, s1, s2] = c.cpu.read_operands(&c.main_memory)?;
    let l1l2 = Operand::load_double(&c.main_memory, &mut c.stack, l1, l2)?;
    Operand::store_double(&mut c.main_memory, &mut c.stack, s1, s2, l1l2.asin())?;

    Ok(())
}

opcode_error! {"dasin" DasinError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    OperandStore(OperandStoreError),
}

// Opcode 0x224.
/// Computes the arccosine of L1:L2.
pub(crate) fn dacos<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), DacosError> {
    let [l1, l2, s1, s2] = c.cpu.read_operands(&c.main_memory)?;
    let l1l2 = Operand::load_double(&c.main_memory, &mut c.stack, l1, l2)?;
    Operand::store_double(&mut c.main_memory, &mut c.stack, s1, s2, l1l2.acos())?;

    Ok(())
}

opcode_error! {"dacos" DacosError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    OperandStore(OperandStoreError),
}

// Opcode 0x225.
/// Computes the arctangent of L1:L2.
pub(crate) fn datan<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), DatanError> {
    let [l1, l2, s1, s2] = c.cpu.read_operands(&c.main_memory)?;
    let l1l2 = Operand::load_double(&c.main_memory, &mut c.stack, l1, l2)?;
    Operand::store_double(&mut c.main_memory, &mut c.stack, s1, s2, l1l2.atan())?;

    Ok(())
}

opcode_error! {"datan" DatanError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    OperandStore(OperandStoreError),
}

// Opcode 0x226.
/// Computes the arctangent of L1:L2 / L3:L4.
pub(crate) fn datan2<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), Datan2Error> {
    let [l1, l2, l3, l4, s1, s2] = c.cpu.read_operands(&c.main_memory)?;
    let l1l2 = Operand::load_double(&c.main_memory, &mut c.stack, l1, l2)?;
    let l3l4 = Operand::load_double(&c.main_memory, &mut c.stack, l3, l4)?;
    Operand::store_double(&mut c.main_memory, &mut c.stack, s1, s2, l1l2.atan2(l3l4))?;

    Ok(())
}

opcode_error! {"datan2" Datan2Error =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    OperandStore(OperandStoreError),
}

// Opcode 0x230.
/// Branches to L7 if the difference between L1:L2 and L3:L4 is less than or
/// equal to (plus or minus) L5:L6.
///
/// The sign of L5:L6 is ignored.
pub(crate) fn jdeq<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), JdeqError> {
    let [l1, l2, l3, l4, l5, l6, l7] = c.cpu.read_operands(&c.main_memory)?;
    let l1l2 = Operand::load_double(&c.main_memory, &mut c.stack, l1, l2)?;
    let l3l4 = Operand::load_double(&c.main_memory, &mut c.stack, l3, l4)?;
    let l5l6 = Operand::load_double(&c.main_memory, &mut c.stack, l5, l6)?;
    let l7 = l7.load(&c.main_memory, &mut c.stack)?;

    let are_equal = if l1l2.is_nan() || l3l4.is_nan() || l5l6.is_nan() {
        false
    } else if l1l2.is_infinite() && l3l4.is_infinite() {
        l1l2.signum() == l3l4.signum()
    } else if l5l6.is_infinite() {
        true
    } else {
        (l1l2 - l3l4).abs() <= l5l6.abs()
    };

    if are_equal {
        // Special case for 0 and 1: return from function.
        if l7 == 0 || l7 == 1 {
            return_from_function(c, l7)?;
        } else {
            c.cpu.pc = c.cpu.pc.wrapping_add(l7).wrapping_sub(2);
        }
    }

    Ok(())
}

opcode_error! {"jdeq" JdeqError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    ReturnFromFunction(ReturnFromFunctionError),
}

// Opcode 0x231.
/// Branches to L7 if the difference between L1:L2 and L3:L4 is greater than (plus or minus) L5:L6.
///
/// The sign of L5:L6 is ignored.
pub(crate) fn jdne<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), JdneError> {
    let [l1, l2, l3, l4, l5, l6, l7] = c.cpu.read_operands(&c.main_memory)?;
    let l1l2 = Operand::load_double(&c.main_memory, &mut c.stack, l1, l2)?;
    let l3l4 = Operand::load_double(&c.main_memory, &mut c.stack, l3, l4)?;
    let l5l6 = Operand::load_double(&c.main_memory, &mut c.stack, l5, l6)?;
    let l7 = l7.load(&c.main_memory, &mut c.stack)?;

    let are_equal = if l1l2.is_nan() || l3l4.is_nan() || l5l6.is_nan() {
        false
    } else if l1l2.is_infinite() && l3l4.is_infinite() {
        l1l2.signum() == l3l4.signum()
    } else if l5l6.is_infinite() {
        true
    } else {
        (l1l2 - l3l4).abs() <= l5l6.abs()
    };

    if !are_equal {
        // Special case for 0 and 1: return from function.
        if l7 == 0 || l7 == 1 {
            return_from_function(c, l7)?;
        } else {
            c.cpu.pc = c.cpu.pc.wrapping_add(l7).wrapping_sub(2);
        }
    }

    Ok(())
}

opcode_error! {"jdne" JdneError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    ReturnFromFunction(ReturnFromFunctionError),
}

// Opcode 0x232.
/// Branches to L5 if L1:L2 is less than L3:L4 (using floating point comparison).
pub(crate) fn jdlt<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), JdltError> {
    let [l1, l2, l3, l4, l5] = c.cpu.read_operands(&c.main_memory)?;
    let l1l2 = Operand::load_double(&c.main_memory, &mut c.stack, l1, l2)?;
    let l3l4 = Operand::load_double(&c.main_memory, &mut c.stack, l3, l4)?;
    let l5 = l5.load(&c.main_memory, &mut c.stack)?;

    if l1l2 < l3l4 {
        // Special case for 0 and 1: return from function.
        if l5 == 0 || l5 == 1 {
            return_from_function(c, l5)?;
        } else {
            c.cpu.pc = c.cpu.pc.wrapping_add(l5).wrapping_sub(2);
        }
    }

    Ok(())
}

opcode_error! {"jdlt" JdltError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    ReturnFromFunction(ReturnFromFunctionError),
}

// Opcode 0x233.
/// Branches to L5 if L1:L2 is less than or equal to L3:L4 (using floating point comparison).
pub(crate) fn jdle<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), JdleError> {
    let [l1, l2, l3, l4, l5] = c.cpu.read_operands(&c.main_memory)?;
    let l1l2 = Operand::load_double(&c.main_memory, &mut c.stack, l1, l2)?;
    let l3l4 = Operand::load_double(&c.main_memory, &mut c.stack, l3, l4)?;
    let l5 = l5.load(&c.main_memory, &mut c.stack)?;

    if l1l2 <= l3l4 {
        // Special case for 0 and 1: return from function.
        if l5 == 0 || l5 == 1 {
            return_from_function(c, l5)?;
        } else {
            c.cpu.pc = c.cpu.pc.wrapping_add(l5).wrapping_sub(2);
        }
    }

    Ok(())
}

opcode_error! {"jdle" JdleError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    ReturnFromFunction(ReturnFromFunctionError),
}

// Opcode 0x234.
/// Branches to L5 if L1:L2 is greater than L3:L4 (using floating point comparison).
pub(crate) fn jdgt<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), JdgtError> {
    let [l1, l2, l3, l4, l5] = c.cpu.read_operands(&c.main_memory)?;
    let l1l2 = Operand::load_double(&c.main_memory, &mut c.stack, l1, l2)?;
    let l3l4 = Operand::load_double(&c.main_memory, &mut c.stack, l3, l4)?;
    let l5 = l5.load(&c.main_memory, &mut c.stack)?;

    if l1l2 > l3l4 {
        // Special case for 0 and 1: return from function.
        if l5 == 0 || l5 == 1 {
            return_from_function(c, l5)?;
        } else {
            c.cpu.pc = c.cpu.pc.wrapping_add(l5).wrapping_sub(2);
        }
    }

    Ok(())
}

opcode_error! {"jdgt" JdgtError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    ReturnFromFunction(ReturnFromFunctionError),
}

// Opcode 0x235.
/// Branches to L5 if L1:L2 is greater than or equal to L3:L4 (using floating point comparison).
pub(crate) fn jdge<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), JdgeError> {
    let [l1, l2, l3, l4, l5] = c.cpu.read_operands(&c.main_memory)?;
    let l1l2 = Operand::load_double(&c.main_memory, &mut c.stack, l1, l2)?;
    let l3l4 = Operand::load_double(&c.main_memory, &mut c.stack, l3, l4)?;
    let l5 = l5.load(&c.main_memory, &mut c.stack)?;

    if l1l2 >= l3l4 {
        // Special case for 0 and 1: return from function.
        if l5 == 0 || l5 == 1 {
            return_from_function(c, l5)?;
        } else {
            c.cpu.pc = c.cpu.pc.wrapping_add(l5).wrapping_sub(2);
        }
    }

    Ok(())
}

opcode_error! {"jdge" JdgeError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    ReturnFromFunction(ReturnFromFunctionError),
}

// Opcode 0x238.
/// Branches to L3 if L1:L2 is a NaN value.
pub(crate) fn jdisnan<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), JdisnanError> {
    let [l1, l2, l3] = c.cpu.read_operands(&c.main_memory)?;
    let l1l2 = Operand::load_double(&c.main_memory, &mut c.stack, l1, l2)?;
    let l3 = l3.load(&c.main_memory, &mut c.stack)?;

    if l1l2.is_nan() {
        // Special case for 0 and 1: return from function.
        if l3 == 0 || l3 == 1 {
            return_from_function(c, l3)?;
        } else {
            c.cpu.pc = c.cpu.pc.wrapping_add(l3).wrapping_sub(2);
        }
    }

    Ok(())
}

opcode_error! {"jdisnan" JdisnanError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    ReturnFromFunction(ReturnFromFunctionError),
}

// Opcode 0x239.
/// Branches to L3 if L1:L2 is an infinity.
pub(crate) fn jdisinf<G: Glauque>(c: &mut Coryphelle<G>) -> Result<(), JdisinfError> {
    let [l1, l2, l3] = c.cpu.read_operands(&c.main_memory)?;
    let l1l2 = Operand::load_double(&c.main_memory, &mut c.stack, l1, l2)?;
    let l3 = l3.load(&c.main_memory, &mut c.stack)?;

    if l1l2.is_infinite() {
        // Special case for 0 and 1: return from function.
        if l3 == 0 || l3 == 1 {
            return_from_function(c, l3)?;
        } else {
            c.cpu.pc = c.cpu.pc.wrapping_add(l3).wrapping_sub(2);
        }
    }

    Ok(())
}

opcode_error! {"jdisinf" JdisinfError =>
    ReadOperands(ReadOperandsError),
    OperandLoad(OperandLoadError),
    ReturnFromFunction(ReturnFromFunctionError),
}
