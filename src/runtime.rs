//! This module contains all the functions that make the machine run
//! (calling functions, outputting strings...)

use std::{error::Error, fmt};

use glauque::Glauque;

use crate::accel::AcceleratedCallError;
use crate::cpu::{store_at_dest, IoSystem, StoreAtDestError};
use crate::decoding::{self, DecodedString, StringPart};
use crate::memory::{CacheDecodingTreeError, MemoryReadError, PushCallFrameError};
use crate::op::OpcodeError;
use crate::{Coryphelle, StepError};

/// Calls the function at the given address without pushing a call stub.
///
/// This is used at the start of the story and by the `tailcall` opcode.
pub(crate) fn call_function_without_stub<G: Glauque>(
    c: &mut Coryphelle<G>,
    addr: u32,
    args: &[u32],
) -> Result<(), CallFunctionError> {
    c.stack
        .push_call_frame(&mut c.cpu, &c.main_memory, addr, args)
        .map_err(|err| CallFunctionError {
            addr,
            kind: err.into(),
        })?;

    // If the function is accelerated, we execute it right now
    // and return its result.
    // We can't directly store the result
    // without skipping pushing the call frame like in `call_function`
    // because we don't have access to the destination type and address here.

    if let Some(result) =
        c.acceleration
            .call(&mut c.main_memory, &c.cpu.io_system, &mut c.glk, addr, args)
    {
        let result = result.map_err(|err| CallFunctionError {
            addr,
            kind: err.into(),
        })?;
        return_from_function(c, result).map_err(|err| CallFunctionError {
            addr,
            kind: err.into(),
        })?;
    }

    Ok(())
}

/// Calls the function at the given address in the main memory.
///
/// The slice `args` contains the arguments given to the function.
///
/// `dest_type` determines how the return value will be stored:
///
/// * `0`: Don't store it; `dest_addr` should be `0` in that case.
/// * `1`: Store it in the main memory; `dest_addr` will indicate where.
/// * `2`: Store in a local variable. `dest_addr` will indicate where in the call frame.
/// * `3`: Push it on the stack; `dest_addr` should be `0` in that case.
pub(crate) fn call_function<G: Glauque>(
    c: &mut Coryphelle<G>,
    addr: u32,
    args: &[u32],
    dest_type: u32,
    dest_addr: u32,
) -> Result<(), CallFunctionError> {
    // If the function is accelerated, we execute it right now
    // and store its result before returning early.
    if let Some(result) =
        c.acceleration
            .call(&mut c.main_memory, &c.cpu.io_system, &mut c.glk, addr, args)
    {
        return store_at_dest(
            c,
            dest_type,
            dest_addr,
            result.map_err(|err| CallFunctionError {
                addr,
                kind: err.into(),
            })?,
        )
        .map_err(|err| CallFunctionError {
            addr,
            kind: CallFunctionErrorKind::StoreAtDest(err),
        });
    }

    c.stack.push_call_stub(&c.cpu, dest_type, dest_addr);
    c.stack
        .push_call_frame(&mut c.cpu, &c.main_memory, addr, args)
        .map_err(|err| CallFunctionError {
            addr,
            kind: err.into(),
        })?;

    Ok(())
}

/// The error type returned by `call_function` and `call_function_whithout_stub`.
#[derive(Debug)]
#[non_exhaustive]
pub(crate) struct CallFunctionError {
    /// The address of the called function.
    addr: u32,

    /// The kind of error encountered.
    kind: CallFunctionErrorKind,
}

impl fmt::Display for CallFunctionError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "failed to call function at address {}", self.addr)
    }
}

impl Error for CallFunctionError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        match &self.kind {
            CallFunctionErrorKind::PushCallFrame(err) => Some(err),
            CallFunctionErrorKind::AcceleratedCall(err) => Some(err),
            CallFunctionErrorKind::StoreAtDest(err) => Some(err),
            CallFunctionErrorKind::ReturnFromFunction(err) => Some(err),
        }
    }
}

/// An enum describing the kind of [`CallFunctionError`].
#[derive(Debug)]
enum CallFunctionErrorKind {
    PushCallFrame(PushCallFrameError),
    AcceleratedCall(AcceleratedCallError),
    StoreAtDest(StoreAtDestError),
    ReturnFromFunction(ReturnFromFunctionError),
}

impl From<PushCallFrameError> for CallFunctionErrorKind {
    fn from(value: PushCallFrameError) -> Self {
        Self::PushCallFrame(value)
    }
}

impl From<AcceleratedCallError> for CallFunctionErrorKind {
    fn from(value: AcceleratedCallError) -> Self {
        Self::AcceleratedCall(value)
    }
}

impl From<ReturnFromFunctionError> for CallFunctionErrorKind {
    fn from(value: ReturnFromFunctionError) -> Self {
        Self::ReturnFromFunction(value)
    }
}

/// Returns from the current function with the given value.
pub(crate) fn return_from_function<G: Glauque>(
    c: &mut Coryphelle<G>,
    value: u32,
) -> Result<(), ReturnFromFunctionError> {
    // Throw away the current call frame.
    c.stack
        .truncate(usize::try_from(c.stack.frame_ptr).unwrap());

    // Check if we returned from the topmost function, in which case the story ends.
    if c.stack.ptr() == 0 {
        c.quit();
        return Ok(());
    }

    // Retrieve the values from the call stub.
    let (dest_type, dest_addr) = c.stack.pop_call_stub(&mut c.cpu);

    store_at_dest(c, dest_type, dest_addr, value)
        .map_err(|err| ReturnFromFunctionError { kind: err.into() })
}

#[derive(Debug)]
#[non_exhaustive]
pub(crate) struct ReturnFromFunctionError {
    kind: ReturnFromFunctionErrorKind,
}

impl fmt::Display for ReturnFromFunctionError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "failed to return from function")
    }
}

impl Error for ReturnFromFunctionError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        match &self.kind {
            ReturnFromFunctionErrorKind::StoreAtDest(err) => Some(err),
        }
    }
}

#[derive(Debug)]
enum ReturnFromFunctionErrorKind {
    StoreAtDest(StoreAtDestError),
}

impl From<StoreAtDestError> for ReturnFromFunctionErrorKind {
    fn from(value: StoreAtDestError) -> Self {
        Self::StoreAtDest(value)
    }
}

/// Starts printing the string at the given address.
pub(crate) fn start_print_string<G: Glauque>(
    c: &mut Coryphelle<G>,
    addr: u32,
) -> Result<(), StartPrintStringError> {
    // We print unencoded strings directly, without pushing call stubs.
    // We don't push stubs for compressed strings either. One will be pushed in `decode_string` if necessary.
    let string_type = c
        .main_memory
        .read_u8(addr)
        .map_err(|err| StartPrintStringError {
            addr,
            kind: StartPrintStringErrorKind::MemoryRead(err),
        })?;
    match string_type {
        // Byte-strings (i.e. Latin-1 with Glk).
        0xE0 => {
            print_unencoded_byte_string(c, addr + 1, false).map_err(|err| StartPrintStringError {
                addr,
                kind: err.into(),
            })
        }
        // Word-strings (i.e. Unicode with Glk).
        0xE2 => {
            print_unencoded_word_string(c, addr + 4, false).map_err(|err| StartPrintStringError {
                addr,
                kind: err.into(),
            })
        }
        // Compressed strings.
        0xE1 => decode_string(c, addr + 1, None).map_err(|err| StartPrintStringError {
            addr,
            kind: StartPrintStringErrorKind::DecodeString(err),
        }),
        ty => Err(StartPrintStringError {
            addr,
            kind: StartPrintStringErrorKind::InvalidStringType(InvalidStringTypeError(ty)),
        }),
    }
}

/// The error type returned when starting to print a string failed.
#[derive(Debug)]
#[non_exhaustive]
pub(crate) struct StartPrintStringError {
    addr: u32,
    kind: StartPrintStringErrorKind,
}

impl fmt::Display for StartPrintStringError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "couldn't start printing string at addr {}", self.addr)
    }
}

impl Error for StartPrintStringError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        match &self.kind {
            StartPrintStringErrorKind::MemoryRead(err) => Some(err),
            StartPrintStringErrorKind::PrintUnencodedString(err) => Some(err),
            StartPrintStringErrorKind::DecodeString(err) => Some(err),
            StartPrintStringErrorKind::InvalidStringType(err) => Some(err),
        }
    }
}

/// An enum describing the kinds of [`StartPrintStringError`].
#[derive(Debug)]
enum StartPrintStringErrorKind {
    MemoryRead(MemoryReadError),
    PrintUnencodedString(PrintUnencodedStringError),
    DecodeString(DecodeStringError),
    InvalidStringType(InvalidStringTypeError),
}

impl From<PrintUnencodedStringError> for StartPrintStringErrorKind {
    fn from(value: PrintUnencodedStringError) -> Self {
        Self::PrintUnencodedString(value)
    }
}

#[derive(Debug)]
struct InvalidStringTypeError(u8);

impl fmt::Display for InvalidStringTypeError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "The type {:#X} is not a valid string type", self.0)
    }
}

impl Error for InvalidStringTypeError {}

/// Prints an unencoded, null-terminated byte string (i.e. Latin-1 with Glk).
///
/// The address is the one of the first character to print.
///
/// If `resuming` is true, then we are continuing to print the string
/// (during decoding or filtering) and not starting.
pub(crate) fn print_unencoded_byte_string<G: Glauque>(
    c: &mut Coryphelle<G>,
    addr: u32,
    resuming: bool,
) -> Result<(), PrintUnencodedStringError> {
    match c.cpu.io_system {
        IoSystem::Glk => {
            // TODO: We ought to check if the addresses are in retained memory.
            //
            // TODO: Having a `put_iter` instead of a `put_slice`
            // would avoid having to count the length.
            let addr_usize = usize::try_from(addr).unwrap();
            let len = c.main_memory.as_bytes()[addr_usize..]
                .iter()
                .take_while(|&&byte| byte != 0)
                .count();
            // TODO: We use `to_vec` because of the borrow checker,
            // but we should find a way not to allocate a new Vector.
            let bytes = c.main_memory.as_bytes()[addr_usize..addr_usize + len].to_vec();
            c.glk.put_slice(&mut c.main_memory, &bytes);
        }
        IoSystem::Filter => {
            let ch = c
                .main_memory
                .read_u8(addr)
                .map_err(|err| PrintUnencodedStringError { kind: err.into() })?
                as u32;
            if ch != 0 {
                // If we are not resuming, then we need to push the call stub we skipped earlier.
                if !resuming {
                    c.stack.push_call_stub(&c.cpu, 0x11, 0);
                }
                c.cpu.pc = addr + 1;
                call_function(c, c.cpu.io_system_rock, &[ch], 0x13, 0)
                    .map_err(|err| PrintUnencodedStringError { kind: err.into() })?;
                return Ok(());
            }
        }
        IoSystem::Null => (),
    }

    // We finished printing the string. If we are resuming, we need to pop
    // the 0x10 or 0x11 call stub.
    if resuming {
        let (dest_type, dest_addr) = c.stack.pop_call_stub(&mut c.cpu);
        match dest_type {
            0x10 => decode_string(c, c.cpu.pc, Some(dest_addr))
                .map_err(|err| PrintUnencodedStringError { kind: err.into() })?,
            0x11 => (), // String finished, we continue as normal.
            n => panic!("invalid DestType when finishing decoding string: {:X}", n),
        }
    }

    Ok(())
}

/// Prints an unencoded, null-terminated word string (i.e. Unicode with Glk).
///
/// The address is the one of the first character to print.
///
/// If `resuming` is true, then we are continuing to print the string
/// (during decoding or filtering) and not starting.
pub(crate) fn print_unencoded_word_string<G: Glauque>(
    c: &mut Coryphelle<G>,
    addr: u32,
    resuming: bool,
) -> Result<(), PrintUnencodedStringError> {
    match c.cpu.io_system {
        IoSystem::Glk => {
            // TODO: We ought to check if the addresses are in retained memory.
            let addr_usize = usize::try_from(addr).unwrap();
            // TODO: Having a `put_iter` instead of a `put_slice`
            // could avoid having to allocate a Vec.
            let chars = c.main_memory.as_bytes()[addr_usize..]
                .chunks(4)
                .take_while(|&bytes| bytes != [0, 0, 0, 0])
                .map(|bytes| u32::from_be_bytes(bytes.try_into().unwrap()))
                .collect::<Vec<_>>();
            c.glk.put_slice_uni(&mut c.main_memory, &chars);
        }
        IoSystem::Filter => {
            let ch = c
                .main_memory
                .read_u32(addr)
                .map_err(|err| PrintUnencodedStringError { kind: err.into() })?;
            if ch != 0 {
                // If we are not resuming, then we need to push the call stub we skipped earlier.
                if !resuming {
                    c.stack.push_call_stub(&c.cpu, 0x11, 0);
                }
                c.cpu.pc = addr + 4;
                call_function(c, c.cpu.io_system_rock, &[ch], 0x14, 0)
                    .map_err(|err| PrintUnencodedStringError { kind: err.into() })?;
                return Ok(());
            }
        }
        IoSystem::Null => (),
    }

    // We finished printing the string. If we are resuming, we need to pop
    // the 0x10 or 0x11 call stub.
    if resuming {
        let (dest_type, dest_addr) = c.stack.pop_call_stub(&mut c.cpu);
        match dest_type {
            0x10 => decode_string(c, c.cpu.pc, Some(dest_addr))
                .map_err(|err| PrintUnencodedStringError { kind: err.into() })?,
            0x11 => (), // String finished, we continue as normal.
            n => panic!("invalid DestType when finishing decoding string: {:X}", n),
        }
    }

    Ok(())
}

/// The error type returned when printing an unencoded string.
#[derive(Debug)]
#[non_exhaustive]
pub(crate) struct PrintUnencodedStringError {
    kind: PrintUnencodedStringErrorKind,
}

impl fmt::Display for PrintUnencodedStringError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "failed to print string")
    }
}

impl Error for PrintUnencodedStringError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        match &self.kind {
            PrintUnencodedStringErrorKind::MemoryRead(err) => Some(err),
            PrintUnencodedStringErrorKind::CallFunction(err) => Some(err),
            PrintUnencodedStringErrorKind::DecodeString(err) => Some(err),
        }
    }
}

/// An enum describing the kinds of [`PrintUnencodedStringError`]
#[derive(Debug)]
enum PrintUnencodedStringErrorKind {
    MemoryRead(MemoryReadError),
    CallFunction(CallFunctionError),
    DecodeString(DecodeStringError),
}

impl From<MemoryReadError> for PrintUnencodedStringErrorKind {
    fn from(value: MemoryReadError) -> Self {
        Self::MemoryRead(value)
    }
}

impl From<CallFunctionError> for PrintUnencodedStringErrorKind {
    fn from(value: CallFunctionError) -> Self {
        Self::CallFunction(value)
    }
}

impl From<DecodeStringError> for PrintUnencodedStringErrorKind {
    fn from(value: DecodeStringError) -> Self {
        Self::DecodeString(value)
    }
}

/// Decodes the string at the given address using the current decoding table.
///
/// A `bit_pos` of `None` means we are starting (and not resuming) the string-decoding.
pub(crate) fn decode_string<G: Glauque>(
    c: &mut Coryphelle<G>,
    addr: u32,
    bit_pos: Option<u32>,
) -> Result<(), DecodeStringError> {
    let mut decoded_string = DecodedString::new();
    let mut current_byte_addr = addr;
    let mut current_byte = c
        .main_memory
        .read_u8(current_byte_addr)
        .map_err(|err| DecodeStringError { kind: err.into() })?;

    let (mut bit_pos, resuming) = match bit_pos {
        Some(x) => (x, true),
        None => (0, false),
    };

    // No decoding table is set.
    if c.main_memory.decoding_tree.addr == 0 {
        panic!("tried to decode a string without a decoding table");
    }

    let mut current_node = match c.main_memory.decoding_tree.root.as_ref() {
        Some(root) => root,
        None => {
            // There is no cached tree, so build it.
            c.main_memory
                .cache_decoding_tree()
                .map_err(|err| DecodeStringError {
                    kind: DecodeStringErrorKind::CachDecodingTree(err),
                })?;
            &c.main_memory.decoding_tree.root.as_ref().unwrap()
        }
    };

    loop {
        match current_node {
            decoding::Node::Branch(dest_0, dest_1) => {
                let bit = current_byte & 0b1 << bit_pos;
                bit_pos = if bit_pos == 7 {
                    // Go the the next byte.
                    current_byte_addr += 1;
                    current_byte = c
                        .main_memory
                        .read_u8(current_byte_addr)
                        .map_err(|err| DecodeStringError { kind: err.into() })?;
                    0
                } else {
                    bit_pos + 1
                };
                current_node = if bit == 0 { dest_0 } else { dest_1 };
            }
            decoding::Node::Terminator => {
                // Output the decoded string.
                for part in decoded_string.parts() {
                    match part {
                        StringPart::Bytes(bytes) => c.glk.put_slice(&mut c.main_memory, bytes),
                        StringPart::Words(words) => c.glk.put_slice_uni(&mut c.main_memory, words),
                    }
                }
                // If we are resuming, that means there was an indirect call during the
                // string decoding and we didn't skip pushing the call stub, so we have to
                // pop it.
                if resuming {
                    let (dest_type, dest_addr) = c.stack.pop_call_stub(&mut c.cpu);
                    match dest_type {
                        0x10 => decode_string(c, c.cpu.pc, Some(dest_addr))?,
                        0x11 => (), // Topmost string finished, we continue as normal.
                        n => panic!("invalid DestType when finishing decoding string: {:X}", n),
                    }
                }
                return Ok(());
            }
            decoding::Node::Byte(byte) => {
                match c.cpu.io_system {
                    IoSystem::Glk => decoded_string.push_byte(*byte),
                    IoSystem::Filter => {
                        let ch = *byte as u32;
                        // If we are not resuming, then we need to push the call stub we skipped earlier.
                        if !resuming {
                            c.stack.push_call_stub(&c.cpu, 0x11, 0);
                        }
                        c.cpu.pc = current_byte_addr;
                        call_function(c, c.cpu.io_system_rock, &[ch], 0x10, bit_pos)
                            .map_err(|err| DecodeStringError { kind: err.into() })?;
                        return Ok(());
                    }
                    IoSystem::Null => (),
                }
                current_node = &c.main_memory.decoding_tree.root.as_ref().unwrap();
            }
            decoding::Node::ByteString { start, end } => {
                match c.cpu.io_system {
                    IoSystem::Glk => {
                        for i in *start..*end {
                            let byte = c
                                .main_memory
                                .read_u8(i)
                                .map_err(|err| DecodeStringError { kind: err.into() })?;
                            decoded_string.push_byte(byte);
                        }
                    }
                    IoSystem::Filter => {
                        let start = *start;
                        // If we are not resuming, then we need to push the call stub we skipped earlier.
                        if !resuming {
                            c.stack.push_call_stub(&c.cpu, 0x11, 0);
                        }
                        c.cpu.pc = current_byte_addr;
                        c.stack.push_call_stub(&c.cpu, 0x10, bit_pos);
                        print_unencoded_byte_string(c, start, true)
                            .map_err(|err| DecodeStringError { kind: err.into() })?;
                        return Ok(());
                    }
                    IoSystem::Null => (),
                }
                current_node = &c.main_memory.decoding_tree.root.as_ref().unwrap();
            }
            decoding::Node::Word(word) => {
                match c.cpu.io_system {
                    IoSystem::Glk => decoded_string.push_word(*word),
                    IoSystem::Filter => {
                        let ch = *word;
                        // If we are not resuming, then we need to push the call stub we skipped earlier.
                        if !resuming {
                            c.stack.push_call_stub(&mut c.cpu, 0x11, 0);
                        }
                        c.cpu.pc = current_byte_addr;
                        call_function(c, c.cpu.io_system_rock, &[ch], 0x10, bit_pos)
                            .map_err(|err| DecodeStringError { kind: err.into() })?;
                        return Ok(());
                    }
                    IoSystem::Null => (),
                }
                current_node = &c.main_memory.decoding_tree.root.as_ref().unwrap();
            }
            decoding::Node::WordString { start, end } => {
                match c.cpu.io_system {
                    IoSystem::Glk => {
                        for i in (*start..*end).step_by(4) {
                            let word = c
                                .main_memory
                                .read_u32(i)
                                .map_err(|err| DecodeStringError { kind: err.into() })?;
                            decoded_string.push_word(word);
                        }
                    }
                    IoSystem::Filter => {
                        let start = *start;
                        // If we are not resuming, then we need to push the call stub we skipped earlier.
                        if !resuming {
                            c.stack.push_call_stub(&c.cpu, 0x11, 0);
                        }
                        c.cpu.pc = current_byte_addr;
                        c.stack.push_call_stub(&c.cpu, 0x10, bit_pos);
                        print_unencoded_word_string(c, start, true)
                            .map_err(|err| DecodeStringError { kind: err.into() })?;
                        return Ok(());
                    }
                    IoSystem::Null => (),
                }
                current_node = &c.main_memory.decoding_tree.root.as_ref().unwrap();
            }
            decoding::Node::Indirection {
                addr,
                double,
                args_start,
                args_end,
            } => {
                // We copy the values of the node right now to avoid borrow checkers issues.
                let addr = *addr;
                let double = *double;
                let args_start = *args_start;
                let args_end = *args_end;

                // First we drain and print what has been decoded up to now.
                // (Drain because we'll go back at the root if printing unencoded strings.)
                for part in decoded_string.drain() {
                    match part {
                        StringPart::Bytes(bytes) => c.glk.put_slice(&mut c.main_memory, &bytes),
                        StringPart::Words(words) => c.glk.put_slice_uni(&mut c.main_memory, &words),
                    }
                }
                // Call the pointed function or print the pointed string.
                let call_addr = if double {
                    c.main_memory
                        .read_u32(addr)
                        .map_err(|err| DecodeStringError { kind: err.into() })?
                } else {
                    addr
                };
                let call_type = c
                    .main_memory
                    .read_u8(call_addr)
                    .map_err(|err| DecodeStringError { kind: err.into() })?;
                match call_type {
                    // Unencoded string: we just print them.
                    0xE0 => {
                        if c.cpu.io_system != IoSystem::Filter {
                            print_unencoded_byte_string(c, call_addr + 1, false)
                                .map_err(|err| DecodeStringError { kind: err.into() })?;
                        } else {
                            // If we are not resuming, then we need to push the call stub we skipped earlier.
                            if !resuming {
                                c.stack.push_call_stub(&c.cpu, 0x11, 0);
                            }
                            c.cpu.pc = current_byte_addr;
                            c.stack.push_call_stub(&c.cpu, 0x10, bit_pos);
                            print_unencoded_byte_string(c, call_addr + 1, true)
                                .map_err(|err| DecodeStringError { kind: err.into() })?;
                            return Ok(());
                        }
                    }
                    0xE2 => {
                        if c.cpu.io_system != IoSystem::Filter {
                            print_unencoded_word_string(c, call_addr + 4, false)
                                .map_err(|err| DecodeStringError { kind: err.into() })?;
                        } else {
                            // If we are not resuming, then we need to push the call stub we skipped earlier.
                            if !resuming {
                                c.stack.push_call_stub(&c.cpu, 0x11, 0);
                            }
                            c.cpu.pc = current_byte_addr;
                            c.stack.push_call_stub(&c.cpu, 0x10, bit_pos);
                            print_unencoded_word_string(c, call_addr + 4, true)
                                .map_err(|err| DecodeStringError { kind: err.into() })?;
                            return Ok(());
                        }
                    }
                    // Compressed string.
                    0xE1 => {
                        // If we are not resuming, then we need to push the call stub we skipped earlier.
                        if !resuming {
                            c.stack.push_call_stub(&c.cpu, 0x11, 0);
                        }
                        c.cpu.pc = current_byte_addr;
                        c.stack.push_call_stub(&c.cpu, 0x10, bit_pos);
                        decode_string(c, call_addr + 1, Some(0))?;
                        return Ok(());
                    }
                    // Function.
                    0xC0 | 0xC1 => {
                        // Retrieve the arguments.
                        let mut args = Vec::new();
                        if args_start != 0 {
                            args.reserve_exact(usize::try_from(args_end - args_start).unwrap() / 4);
                            for i in (args_start..args_end).step_by(4) {
                                let arg = c
                                    .main_memory
                                    .read_u32(i)
                                    .map_err(|err| DecodeStringError { kind: err.into() })?;
                                args.push(arg)
                            }
                        }
                        // If we are not resuming, then we need to push the call stub we skipped earlier.
                        if !resuming {
                            c.stack.push_call_stub(&c.cpu, 0x11, 0);
                        }
                        c.cpu.pc = current_byte_addr;
                        call_function(c, call_addr, &args, 0x10, bit_pos)
                            .map_err(|err| DecodeStringError { kind: err.into() })?;
                        return Ok(());
                    }
                    _ => panic!("calling a wrong address from within a string"),
                }
                current_node = &c.main_memory.decoding_tree.root.as_ref().unwrap();
            }
        }
    }
}

/// The error type returned when decoding a string.
#[derive(Debug)]
#[non_exhaustive]
pub(crate) struct DecodeStringError {
    kind: DecodeStringErrorKind,
}

impl fmt::Display for DecodeStringError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "failed to decode string")
    }
}

impl Error for DecodeStringError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        match &self.kind {
            DecodeStringErrorKind::MemoryRead(err) => Some(err),
            DecodeStringErrorKind::CachDecodingTree(err) => Some(err),
            DecodeStringErrorKind::PrintUnencodedString(err) => Some(err),
            DecodeStringErrorKind::CallFunction(err) => Some(err),
        }
    }
}

#[derive(Debug)]
enum DecodeStringErrorKind {
    MemoryRead(MemoryReadError),
    CachDecodingTree(CacheDecodingTreeError),
    // We need some indirection because both of the errors can be caused by the other.
    PrintUnencodedString(Box<PrintUnencodedStringError>),
    CallFunction(CallFunctionError),
}

impl From<MemoryReadError> for DecodeStringErrorKind {
    fn from(value: MemoryReadError) -> Self {
        Self::MemoryRead(value)
    }
}

impl From<PrintUnencodedStringError> for DecodeStringErrorKind {
    fn from(value: PrintUnencodedStringError) -> Self {
        Self::PrintUnencodedString(value.into())
    }
}

impl From<CallFunctionError> for DecodeStringErrorKind {
    fn from(value: CallFunctionError) -> Self {
        Self::CallFunction(value)
    }
}

/// An error indicating something went wrong during the execution of a story.
#[derive(Debug)]
#[non_exhaustive]
pub struct RuntimeError {
    /// The program counter at which the error was encountered.
    pub(crate) pc: u32,

    /// The kind of the error.
    pub(crate) kind: RuntimeErrorKind,
}

impl fmt::Display for RuntimeError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "runtime error at pc = {}", self.pc)
    }
}

impl Error for RuntimeError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        match &self.kind {
            RuntimeErrorKind::Step(err) => Some(err),
            RuntimeErrorKind::Opcode(err) => Some(err),
        }
    }
}

/// The kind of runtime error that can happen.
#[derive(Debug)]
pub(crate) enum RuntimeErrorKind {
    Step(StepError),
    Opcode(OpcodeError),
}

impl From<OpcodeError> for RuntimeErrorKind {
    fn from(value: OpcodeError) -> Self {
        Self::Opcode(value)
    }
}
