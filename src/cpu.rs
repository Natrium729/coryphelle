use std::{error::Error, fmt};

use glauque::Glauque;

use crate::memory::{MainMemory, MemoryReadError, MemoryWriteError, Stack};
use crate::runtime::{
    call_function, decode_string, print_unencoded_byte_string, print_unencoded_word_string,
    CallFunctionError, DecodeStringError, PrintUnencodedStringError,
};
use crate::Coryphelle;

#[derive(Debug)]
pub(crate) struct Cpu {
    pub(crate) pc: u32,
    pub(crate) io_system: IoSystem,
    pub(crate) io_system_rock: u32,
}

impl Cpu {
    pub(crate) fn new() -> Self {
        Self {
            pc: 0,
            io_system: IoSystem::Null,
            io_system_rock: 0,
        }
    }

    // Since this function is const-generic,
    // we'll end up with multiple copy of it in the executable.
    // It's a bit wasteful, but might lead to better optimisation
    // (since the compiler will know how many operands we are reading).
    //
    /// Gets the given number of opcode operands at the PC in the main memory.
    ///
    /// This function will leave the PC pointing at the next opcode.
    pub(crate) fn read_operands<const N: usize>(
        &mut self,
        mem: &MainMemory,
    ) -> Result<[Operand; N], ReadOperandsError> {
        let mut operands = std::array::from_fn(|_| Operand::ConstantZero);

        // The next PC will at least point just after the addressing modes.
        let mut next_pc = self.pc + (N as u32 + 1) / 2;

        for (i, nibble) in mem
            .read_slice(self.pc, (N as u32 + 1) / 2)
            .map_err(|err| ReadOperandsError { kind: err.into() })?
            .iter()
            .flat_map(|&byte| [byte & 0x0F, byte >> 4].into_iter())
            .enumerate()
            .take(N)
        {
            operands[i] = match nibble {
                0x0 => Operand::ConstantZero,
                0x1 => {
                    let val = mem
                        .read_u8(next_pc)
                        .map_err(|err| ReadOperandsError { kind: err.into() })?;
                    next_pc += 1;
                    Operand::Constant(val as i8 as u32)
                }
                0x2 => {
                    let val = mem
                        .read_u16(next_pc)
                        .map_err(|err| ReadOperandsError { kind: err.into() })?;
                    next_pc += 2;
                    Operand::Constant(val as i16 as u32)
                }
                0x3 => {
                    let val = mem
                        .read_u32(next_pc)
                        .map_err(|err| ReadOperandsError { kind: err.into() })?;
                    next_pc += 4;
                    Operand::Constant(val)
                }
                0x4 => {
                    return Err(ReadOperandsError {
                        kind: ReadOperandsErrorKind::UnusedAddressingMode { mode: 0x4 },
                    });
                }
                0x5 => {
                    let val = mem
                        .read_u8(next_pc)
                        .map_err(|err| ReadOperandsError { kind: err.into() })?;
                    next_pc += 1;
                    Operand::Address(val as u32)
                }
                0x6 => {
                    let val = mem
                        .read_u16(next_pc)
                        .map_err(|err| ReadOperandsError { kind: err.into() })?;
                    next_pc += 2;
                    Operand::Address(val as u32)
                }
                0x7 => {
                    let val = mem
                        .read_u32(next_pc)
                        .map_err(|err| ReadOperandsError { kind: err.into() })?;
                    next_pc += 4;
                    Operand::Address(val)
                }
                0x8 => Operand::Stack,
                0x9 => {
                    let val = mem
                        .read_u8(next_pc)
                        .map_err(|err| ReadOperandsError { kind: err.into() })?;
                    next_pc += 1;
                    Operand::CallFrameLocalAddress(val as u32)
                }
                0xA => {
                    let val = mem
                        .read_u16(next_pc)
                        .map_err(|err| ReadOperandsError { kind: err.into() })?;
                    next_pc += 2;
                    Operand::CallFrameLocalAddress(val as u32)
                }
                0xB => {
                    let val = mem
                        .read_u32(next_pc)
                        .map_err(|err| ReadOperandsError { kind: err.into() })?;
                    next_pc += 4;
                    Operand::Address(val)
                }
                0xC => {
                    return Err(ReadOperandsError {
                        kind: ReadOperandsErrorKind::UnusedAddressingMode { mode: 0xC },
                    });
                }
                0xD => {
                    let val = mem
                        .read_u8(next_pc)
                        .map_err(|err| ReadOperandsError { kind: err.into() })?;
                    next_pc += 1;
                    Operand::RamAddress(val as u32)
                }
                0xE => {
                    let val = mem
                        .read_u16(next_pc)
                        .map_err(|err| ReadOperandsError { kind: err.into() })?;
                    next_pc += 2;
                    Operand::RamAddress(val as u32)
                }
                0xF => {
                    let val = mem
                        .read_u32(next_pc)
                        .map_err(|err| ReadOperandsError { kind: err.into() })?;
                    next_pc += 4;
                    Operand::RamAddress(val)
                }
                _ => unreachable!("addressing mode is not a nibble"),
            }
        }

        self.pc = next_pc;
        Ok(operands)
    }
}

/// The error type returned when reading operands with [`read_operands`].
#[derive(Debug)]
#[non_exhaustive]
pub(crate) struct ReadOperandsError {
    /// The kind of error that was encountered.
    kind: ReadOperandsErrorKind,
}

impl fmt::Display for ReadOperandsError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self.kind {
            ReadOperandsErrorKind::UnusedAddressingMode { mode } => {
                write!(f, "unused operand addressing mode {mode:#X}")
            }
            ReadOperandsErrorKind::MemoryRead(_) => write!(f, "failed to read main memory"),
        }
    }
}

impl Error for ReadOperandsError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        match &self.kind {
            ReadOperandsErrorKind::UnusedAddressingMode { .. } => None,
            ReadOperandsErrorKind::MemoryRead(err) => Some(err),
        }
    }
}

/// An enum describing the kinds of [`ReadOperandsError`].
#[derive(Debug)]
enum ReadOperandsErrorKind {
    /// The addressing mode of the operand is not used by Glulx.
    ///
    /// The inner value is the addressing mode.
    #[non_exhaustive]
    UnusedAddressingMode { mode: u8 },

    /// Reading the main memory failed with the inner error.
    MemoryRead(MemoryReadError),
}

impl From<MemoryReadError> for ReadOperandsErrorKind {
    fn from(value: MemoryReadError) -> Self {
        Self::MemoryRead(value)
    }
}

/// Represents an operand for an opcode as well as how it is read/stored by the story.
#[derive(Debug, Clone, Copy)]
pub(crate) enum Operand {
    /// Constant 0, or when we don't care for the value.
    ConstantZero,

    /// The value of the operand is the number it contains.
    Constant(u32),

    /// The value of the operand is the `u32` at the given address in the main memory.
    ///
    /// When storing, a value will be written at that place instead.
    Address(u32),

    /// The value is popped of the stack.
    ///
    /// When storing, a value will be pushed instead.
    Stack,

    /// The value is in the locals of the call frame.
    CallFrameLocalAddress(u32),

    /// The value of the operand is the `u32` at the given address in the main memory (counted from RAMSTART).
    ///
    /// When storing, a value will be written at that place instead.
    RamAddress(u32),
}

impl Operand {
    /// Gets the actual u32 value specified by the given operand.
    pub(crate) fn load(self, mem: &MainMemory, stack: &mut Stack) -> Result<u32, OperandLoadError> {
        match self {
            Operand::ConstantZero => Ok(0),
            Operand::Constant(x) => Ok(x),
            Operand::Address(addr) => mem
                .read_u32(addr)
                .map_err(|err| OperandLoadError { kind: err.into() }),
            Operand::Stack => Ok(stack.pop_u32()),
            Operand::CallFrameLocalAddress(addr) => {
                Ok(stack.read_u32(stack.frame_locals_address() + addr))
            }
            Operand::RamAddress(addr) => mem
                .read_u32(mem.ramstart().wrapping_add(addr))
                .map_err(|err| OperandLoadError { kind: err.into() }),
        }
    }

    /// Gets the actual 16-bit value specified by the given operand.
    ///
    /// This is used by the copys opcode. Only two bytes are read from the
    /// location, except if it's the stack; in that case, a 32-bit value is
    /// popped then truncated to 16 bits.
    pub(crate) fn load_u16(
        self,
        mem: &MainMemory,
        stack: &mut Stack,
    ) -> Result<u16, OperandLoadError> {
        match self {
            Operand::ConstantZero => Ok(0),
            Operand::Constant(x) => Ok(x as u16),
            Operand::Address(addr) => mem
                .read_u16(addr)
                .map_err(|err| OperandLoadError { kind: err.into() }),
            Operand::Stack => Ok(stack.pop_u32() as u16),
            Operand::CallFrameLocalAddress(addr) => {
                Ok(stack.read_u16(stack.frame_locals_address() + addr))
            }
            Operand::RamAddress(addr) => mem
                .read_u16(mem.ramstart().wrapping_add(addr))
                .map_err(|err| OperandLoadError { kind: err.into() }),
        }
    }

    /// Gets the actual 8-bit value specified by the given operand.
    ///
    /// This is used by the copyb opcode. Only one byte is read from the
    /// location, except if it's the stack; in that case, a 32-bit value is
    /// popped then truncated to 8 bits.
    pub(crate) fn load_u8(
        self,
        mem: &MainMemory,
        stack: &mut Stack,
    ) -> Result<u8, OperandLoadError> {
        match self {
            Operand::ConstantZero => Ok(0),
            Operand::Constant(x) => Ok(x as u8),
            Operand::Address(addr) => mem
                .read_u8(addr)
                .map_err(|err| OperandLoadError { kind: err.into() }),
            Operand::Stack => Ok(stack.pop_u32() as u8),
            Operand::CallFrameLocalAddress(addr) => {
                Ok(stack.read_u8(stack.frame_locals_address() + addr))
            }
            Operand::RamAddress(addr) => mem
                .read_u8(mem.ramstart().wrapping_add(addr))
                .map_err(|err| OperandLoadError { kind: err.into() }),
        }
    }

    /// Stores the given u32 value at the place indicated by the operand.
    pub(crate) fn store(
        self,
        mem: &mut MainMemory,
        stack: &mut Stack,
        value: u32,
    ) -> Result<(), OperandStoreError> {
        match self {
            Operand::ConstantZero => Ok(()), // Throw away the value.
            Operand::Constant(_) => Ok(()),  // Ignore the case of constants.
            Operand::Address(addr) => mem
                .write_u32(addr, value)
                .map_err(|err| OperandStoreError { kind: err.into() }),
            Operand::Stack => {
                stack.push_u32(value);
                Ok(())
            }
            Operand::CallFrameLocalAddress(addr) => {
                stack.write_u32(stack.frame_locals_address() + addr, value);
                Ok(())
            }
            Operand::RamAddress(addr) => mem
                .write_u32(mem.ramstart().wrapping_add(addr), value)
                .map_err(|err| OperandStoreError { kind: err.into() }),
        }
    }

    /// Stores the given u16 value at the place indicated by the operand.
    ///
    /// This is used by the copys opcode. Only two bytes are written to the
    /// location, except if it's the stack; in that case, a 32-bit value in the
    /// 16-bits range is pushed.
    pub(crate) fn store_u16(
        self,
        mem: &mut MainMemory,
        stack: &mut Stack,
        value: u16,
    ) -> Result<(), OperandStoreError> {
        match self {
            Operand::ConstantZero => Ok(()), // Throw away the value.
            Operand::Constant(_) => Ok(()),  // Ignore the case of constants.
            Operand::Address(addr) => mem
                .write_u16(addr, value)
                .map_err(|err| OperandStoreError { kind: err.into() }),
            Operand::Stack => {
                stack.push_u32(value as u32);
                Ok(())
            }
            Operand::CallFrameLocalAddress(addr) => {
                stack.write_u16(stack.frame_locals_address() + addr, value);
                Ok(())
            }
            Operand::RamAddress(addr) => mem
                .write_u16(mem.ramstart().wrapping_add(addr), value)
                .map_err(|err| OperandStoreError { kind: err.into() }),
        }
    }

    /// Stores the given u8 value at the place indicated by the operand.
    ///
    /// This is used by the copyb opcode. Only one byte is written to the
    /// location, except if it's the stack; in that case, a 32-bit value in the
    /// range 0-255 is pushed.
    pub(crate) fn store_u8(
        self,
        mem: &mut MainMemory,
        stack: &mut Stack,
        value: u8,
    ) -> Result<(), OperandStoreError> {
        match self {
            Operand::ConstantZero => Ok(()), // Throw away the value.
            Operand::Constant(_) => Ok(()),  // Ignore the case of constants.
            Operand::Address(addr) => mem
                .write_u8(addr, value)
                .map_err(|err| OperandStoreError { kind: err.into() }),
            Operand::Stack => {
                stack.push_u32(value as u32);
                Ok(())
            }
            Operand::CallFrameLocalAddress(addr) => {
                stack.write_u8(stack.frame_locals_address() + addr, value);
                Ok(())
            }
            Operand::RamAddress(addr) => mem
                .write_u8(mem.ramstart().wrapping_add(addr), value)
                .map_err(|err| OperandStoreError { kind: err.into() }),
        }
    }

    // Gets the DestType and DestAddr numbers (used in call stubs) associated with the operand.
    pub(crate) fn to_dest_type_addr(self, mem: &MainMemory) -> (u32, u32) {
        match self {
            Operand::ConstantZero => (0, 0),
            Operand::Constant(_) => (0, 0), // Should never happen, we treat it as `ConstantZero`.
            Operand::Address(addr) => (1, addr),
            Operand::Stack => (3, 0),
            Operand::CallFrameLocalAddress(addr) => (2, addr),
            Operand::RamAddress(addr) => (1, mem.ramstart().wrapping_add(addr)),
        }
    }

    /// Reads a 64-bit float from two operands.
    pub(crate) fn load_double(
        mem: &MainMemory,
        stack: &mut Stack,
        high: Operand,
        low: Operand,
    ) -> Result<f64, OperandLoadError> {
        let high = high.load(mem, stack)?;
        let low = low.load(mem, stack)?;
        Ok(f64::from_bits(((high as u64) << 32) | low as u64))
    }

    /// Writes a 64-bit float to two operands.
    ///
    /// The `high` and `low` arguments are swapped compared to `read_double`.
    pub(crate) fn store_double(
        mem: &mut MainMemory,
        stack: &mut Stack,
        low_dest: Operand,
        high_dest: Operand,
        value: f64,
    ) -> Result<(), OperandStoreError> {
        let bits = f64::to_bits(value);
        let low = (bits & 0xFF_FF_FF_FF) as u32;
        let high = (bits >> 32) as u32;
        low_dest.store(mem, stack, low)?;
        high_dest.store(mem, stack, high)?;

        Ok(())
    }
}

/// The error type when loading an operand with [`Operand::load`] and the like.
#[derive(Debug)]
#[non_exhaustive]
pub(crate) struct OperandLoadError {
    /// The kind of error that was encountered.
    kind: OperandLoadErrorKind,
}

impl fmt::Display for OperandLoadError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "failed to load operand")
    }
}

impl Error for OperandLoadError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        match &self.kind {
            OperandLoadErrorKind::MemoryRead(err) => Some(err),
        }
    }
}

/// An enum describing the kinds of [`OperandLoadError`].
#[derive(Debug)]
enum OperandLoadErrorKind {
    MemoryRead(MemoryReadError),
}

impl From<MemoryReadError> for OperandLoadErrorKind {
    fn from(value: MemoryReadError) -> Self {
        Self::MemoryRead(value)
    }
}

/// The error type when loading an operand with [`Operand::load`] and the like.
#[derive(Debug)]
#[non_exhaustive]
pub(crate) struct OperandStoreError {
    /// The kind of error that was encountered.
    kind: OperandStoreErrorKind,
}

impl fmt::Display for OperandStoreError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "failed to store operand")
    }
}

impl Error for OperandStoreError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        match &self.kind {
            OperandStoreErrorKind::MemoryWrite(err) => Some(err),
        }
    }
}

/// An enum describing the kinds of [`OperandStoreError`].
#[derive(Debug)]
enum OperandStoreErrorKind {
    MemoryWrite(MemoryWriteError),
}

impl From<MemoryWriteError> for OperandStoreErrorKind {
    fn from(value: MemoryWriteError) -> Self {
        Self::MemoryWrite(value)
    }
}

// Not sure where to put it, so a free function it is.
// It's not in the `runtime` module because it's related to operands.
/// Stores the given value according to the call stub values DestAddr and DestType.
pub(crate) fn store_at_dest<G: Glauque>(
    c: &mut Coryphelle<G>,
    dest_type: u32,
    dest_addr: u32,
    value: u32,
) -> Result<(), StoreAtDestError> {
    match dest_type {
        0x00 => Ok(()), // Don't store the value.
        0x01 => c
            .main_memory
            .write_u32(dest_addr, value)
            .map_err(|err| StoreAtDestError {
                dest_type,
                dest_addr,
                kind: err.into(),
            }),
        0x02 => {
            c.stack
                .write_u32(c.stack.frame_locals_address() + dest_addr, value);
            Ok(())
        }
        0x03 => {
            c.stack.push_u32(value);
            Ok(())
        }
        0x10 => decode_string(c, c.cpu.pc, Some(dest_addr)).map_err(|err| StoreAtDestError {
            dest_type,
            dest_addr,
            kind: err.into(),
        }),
        0x11 => unreachable!(
            "finishing printing a string when returning from function should never happen"
        ),
        0x12 => {
            // Finished printing a digit with streamnum during filtering.
            // It's possible we aren't filtering anymore.
            match c.cpu.io_system {
                IoSystem::Filter => {
                    let dest_addr_usize = usize::try_from(dest_addr).unwrap();
                    let stringified_number = (c.cpu.pc as i32).to_string();
                    if dest_addr_usize < stringified_number.len() {
                        call_function(
                            c,
                            c.cpu.io_system_rock,
                            &[stringified_number.as_bytes()[dest_addr_usize] as u32],
                            0x12,
                            dest_addr + 1,
                        )
                        .map_err(|err| StoreAtDestError {
                            dest_type,
                            dest_addr,
                            kind: err.into(),
                        })?;
                        return Ok(());
                    }
                }
                IoSystem::Glk => {
                    // Print the rest of the number.
                    let dest_addr_usize = usize::try_from(dest_addr).unwrap();
                    let stringified_number = (c.cpu.pc as i32).to_string();
                    if dest_addr_usize < stringified_number.len() {
                        c.glk.put_slice(
                            &mut c.main_memory,
                            &stringified_number.as_bytes()[dest_addr_usize..],
                        )
                    }
                }
                IoSystem::Null => (),
            }
            // We finished printing the number, we now pop the initial call stub.
            let (second_dest_type, _) = c.stack.pop_call_stub(&mut c.cpu);
            if second_dest_type != 0x11 {
                // TODO: Check if it should actually be unreachable.
                todo!("wrong DestType when finishing filtering streamnum: {second_dest_type}");
            }
            Ok(())
        }
        0x13 => print_unencoded_byte_string(c, c.cpu.pc, true).map_err(|err| StoreAtDestError {
            dest_type,
            dest_addr,
            kind: err.into(),
        }),
        0x14 => print_unencoded_word_string(c, c.cpu.pc, true).map_err(|err| StoreAtDestError {
            dest_type,
            dest_addr,
            kind: err.into(),
        }),
        n => Err(StoreAtDestError {
            dest_type,
            dest_addr,
            kind: StoreAtDestErrorKind::InvalidDestType(InvalidDestTypeError(n)),
        }),
    }
}

#[derive(Debug)]
#[non_exhaustive]
pub(crate) struct StoreAtDestError {
    dest_type: u32,
    dest_addr: u32,
    kind: StoreAtDestErrorKind,
}

impl fmt::Display for StoreAtDestError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "failed to store at DestType {}, DestAddr {}",
            self.dest_type, self.dest_addr
        )
    }
}

impl Error for StoreAtDestError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        match &self.kind {
            StoreAtDestErrorKind::MemoryWrite(err) => Some(err),
            StoreAtDestErrorKind::CallFunction(err) => Some(err),
            StoreAtDestErrorKind::PrintUnencodedString(err) => Some(err),
            StoreAtDestErrorKind::DecodeString(err) => Some(err),
            StoreAtDestErrorKind::InvalidDestType(err) => Some(err),
        }
    }
}

#[derive(Debug)]
enum StoreAtDestErrorKind {
    MemoryWrite(MemoryWriteError),
    CallFunction(Box<CallFunctionError>),
    PrintUnencodedString(Box<PrintUnencodedStringError>),
    DecodeString(Box<DecodeStringError>),
    InvalidDestType(InvalidDestTypeError),
}

impl From<MemoryWriteError> for StoreAtDestErrorKind {
    fn from(value: MemoryWriteError) -> Self {
        Self::MemoryWrite(value)
    }
}

impl From<CallFunctionError> for StoreAtDestErrorKind {
    fn from(value: CallFunctionError) -> Self {
        Self::CallFunction(value.into())
    }
}

impl From<PrintUnencodedStringError> for StoreAtDestErrorKind {
    fn from(value: PrintUnencodedStringError) -> Self {
        Self::PrintUnencodedString(value.into())
    }
}

impl From<DecodeStringError> for StoreAtDestErrorKind {
    fn from(value: DecodeStringError) -> Self {
        Self::DecodeString(value.into())
    }
}

#[derive(Debug)]
#[non_exhaustive]
struct InvalidDestTypeError(u32);

impl fmt::Display for InvalidDestTypeError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "invalid DestType {}", self.0)
    }
}

impl Error for InvalidDestTypeError {}

/// The possible IO systems of a Glulx file.
#[derive(PartialEq, Eq, Debug)]
pub(crate) enum IoSystem {
    Null = 0,
    Filter = 1,
    Glk = 2,
}
