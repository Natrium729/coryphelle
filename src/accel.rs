use std::{collections::HashMap, error::Error, fmt, marker::PhantomData};

use glauque::Glauque;

use crate::{
    cpu::IoSystem,
    memory::{MainMemory, MemoryReadError},
    op::{AloadbitError, BinarysearchError},
};

type AcceleratedFn<G> = fn(
    &Acceleration<G>,
    &mut MainMemory,
    &IoSystem,
    &mut G,
    &[u32],
) -> Result<i32, AcceleratedCallError>;

#[derive(Debug)]
pub(crate) struct Acceleration<G> {
    /// A map from Glulx's addresses (i.e. `u32`s)
    /// to an accelerated function number.
    registered: HashMap<u32, u32>,

    params: [i32; 9],

    phantom: PhantomData<G>,
}

/// Gets a value from a slice of 32-bit numbers
/// as if it were an Inform 6 function argument.
///
/// This means that it will default to 0 is `i` is out of bounds,
/// and that it will be converted to an `i32`.
fn get_arg(args: &[u32], i: usize) -> i32 {
    *args.get(i).unwrap_or(&0) as i32
}

/// Gets a 32-bit value from a place in the memory,
/// as if using Inform 6's `-->` operator.
fn from_word_array(mem: &MainMemory, addr: i32, i: i32) -> Result<i32, MemoryReadError> {
    mem.read_u32(addr as u32 + i as u32 * 4)
        .map(|val| val as i32)
}

/// Gets a 8-bit value from a place in the memory,
/// as if using Inform 6's `->` operator.
fn from_byte_array(mem: &MainMemory, addr: i32, i: i32) -> Result<i32, MemoryReadError> {
    mem.read_u8(addr as u32 + i as u32).map(|val| val as i32)
}

impl<G> Acceleration<G> {
    pub(crate) fn new() -> Self {
        Self {
            registered: HashMap::new(),
            params: [0; 9],
            phantom: PhantomData,
        }
    }
}

// TODO: We could use an enum to represent the function
// instead of using bare numbers.
// It would push the checks at the boundaries,
// And no checks would be needed in the hashmap.

impl<G: Glauque> Acceleration<G> {
    fn get_fn(n: u32) -> Option<AcceleratedFn<G>> {
        match n {
            1 => Some(Self::fn1_z_region),
            2 => Some(Self::fn2_cp_tab),
            3 => Some(Self::fn3_ra_pr),
            4 => Some(Self::fn4_rl_pr),
            5 => Some(Self::fn5_oc_cl),
            6 => Some(Self::fn6_rv_pr),
            7 => Some(Self::fn7_op_pr),
            8 => Some(Self::fn8_cp_tab),
            9 => Some(Self::fn9_ra_pr),
            10 => Some(Self::fn10_rl_pr),
            11 => Some(Self::fn11_oc_cl),
            12 => Some(Self::fn12_rv_pr),
            13 => Some(Self::fn13_op_pr),
            _ => None,
        }
    }

    pub(crate) fn is_supported(n: u32) -> bool {
        Self::get_fn(n).is_some()
    }

    /// Marks the Glulx address `addr` as corresponding to
    /// the accelerated function `n`.
    ///
    /// Will only register the function if it is supported.
    ///
    /// If `n` is 0, removes the function.
    pub(crate) fn register(&mut self, addr: u32, n: u32) {
        if n == 0 {
            self.registered.remove(&addr);
        } else if Acceleration::<G>::is_supported(n) {
            self.registered.insert(addr, n);
        }
    }

    pub(crate) fn set_param(&mut self, pos: usize, value: u32) {
        if pos >= self.params.len() {
            return;
        }

        // Since params are considered as Inform 6 values,
        // they are signed numbers.
        self.params[pos] = value as i32;
    }

    /// Calls the accelerated function corresponding to the given address.
    ///
    /// Returns `None` if the address is not registered.
    pub(crate) fn call(
        &self,
        mem: &mut MainMemory,
        iosys: &IoSystem,
        io: &mut G,
        addr: u32,
        args: &[u32],
    ) -> Option<Result<u32, AcceleratedCallError>> {
        let n = *self.registered.get(&addr)?;
        // We unwrap because we'll never call this with unsupported functions
        // (because `register` doesn't register the function in that case).
        let returned = Self::get_fn(n).unwrap()(self, mem, iosys, io, args);
        Some(returned.map(|val| val as u32))
    }

    fn print(&self, io: &mut G, mem: &mut MainMemory, iosys: &IoSystem, msg: &str) {
        debug_assert!(msg.is_ascii());
        if matches!(iosys, IoSystem::Glk) {
            io.put_slice(mem, msg.as_bytes());
        }
    }

    // And now all the accelerated functions.

    fn obj_in_class(&self, mem: &MainMemory, obj: i32) -> Result<bool, MemoryReadError> {
        let left = from_word_array(mem, obj + 13 + self.params[7], 0)?;
        Ok(left == self.params[2])
    }

    fn fn1_z_region(
        &self,
        mem: &mut MainMemory,
        _iosys: &IoSystem,
        _io: &mut G,
        args: &[u32],
    ) -> Result<i32, AcceleratedCallError> {
        let addr = get_arg(args, 0);

        if addr < 36 {
            return Ok(0);
        }

        let end_mem: u32 = mem.len().try_into().unwrap();
        if addr as u32 >= end_mem {
            return Ok(0);
        }

        let tb = from_byte_array(mem, addr, 0).map_err(|err| AcceleratedCallError {
            num: 1,
            kind: err.into(),
        })?;

        if tb >= 0xE0 {
            return Ok(3);
        }
        if tb >= 0xC0 {
            return Ok(2);
        }
        if tb >= 0x70
            && tb <= 0x7F
            && addr
                >= from_word_array(mem, 0, 2).map_err(|err| AcceleratedCallError {
                    num: 1,
                    kind: err.into(),
                })?
        {
            return Ok(1);
        }

        Ok(0)
    }

    fn fn2_cp_tab(
        &self,
        mem: &mut MainMemory,
        iosys: &IoSystem,
        io: &mut G,
        args: &[u32],
    ) -> Result<i32, AcceleratedCallError> {
        let obj = get_arg(args, 0);
        let id = get_arg(args, 1);

        if self
            .fn1_z_region(mem, iosys, io, &[obj as u32])
            .map_err(|err| AcceleratedCallError {
                num: 2,
                kind: err.into(),
            })?
            != 1
        {
            self.print(
                io,
                mem,
                iosys,
                "[** Programming error: tried to find the \".\" of (something) **]\n",
            );
            return Ok(0);
        }

        let mut otab = from_word_array(mem, obj, 4).map_err(|err| AcceleratedCallError {
            num: 2,
            kind: err.into(),
        })?;

        if otab == 0 {
            return Ok(0);
        }

        let max = from_word_array(mem, otab, 0).map_err(|err| AcceleratedCallError {
            num: 2,
            kind: err.into(),
        })?;
        otab += 4;

        mem.binary_search([id as u32, 2, otab as u32, 10, max as u32, 0, 0])
            .map_err(|err| AcceleratedCallError {
                num: 2,
                kind: err.into(),
            })
            .map(|val| val as i32)
    }

    fn fn3_ra_pr(
        &self,
        mem: &mut MainMemory,
        iosys: &IoSystem,
        io: &mut G,
        args: &[u32],
    ) -> Result<i32, AcceleratedCallError> {
        let mut obj = get_arg(args, 0);
        let mut id = get_arg(args, 1);

        let mut cla = 0;

        if (id as u32 & 0xFFFF0000) != 0 {
            cla = from_word_array(mem, self.params[0], (id as u32 & 0xFFFF) as i32).map_err(
                |err| AcceleratedCallError {
                    num: 3,
                    kind: err.into(),
                },
            )?;
            if self
                .fn5_oc_cl(mem, iosys, io, &[obj as u32, cla as u32])
                .map_err(|err| AcceleratedCallError {
                    num: 3,
                    kind: err.into(),
                })?
                == 0
            {
                return Ok(0);
            }
            id = (id as u32 >> 16) as i32;
            obj = cla;
        }

        let prop = self
            .fn2_cp_tab(mem, iosys, io, &[obj as u32, id as u32])
            .map_err(|err| AcceleratedCallError {
                num: 3,
                kind: err.into(),
            })?;
        if prop == 0 {
            return Ok(0);
        }

        if self
            .obj_in_class(mem, obj)
            .map_err(|err| AcceleratedCallError {
                num: 3,
                kind: err.into(),
            })?
            && cla == 0
        {
            if id < self.params[1] || id >= self.params[1] + 8 {
                return Ok(0);
            }
        }

        if from_word_array(mem, self.params[6], 0).map_err(|err| AcceleratedCallError {
            num: 3,
            kind: err.into(),
        })? != obj
        {
            let ix = mem
                .aloadbit(prop as u32, 72)
                .map_err(|err| AcceleratedCallError {
                    num: 10,
                    kind: err.into(),
                })? as i32;
            if ix != 0 {
                return Ok(0);
            }
        }

        from_word_array(mem, prop, 1).map_err(|err| AcceleratedCallError {
            num: 3,
            kind: err.into(),
        })
    }

    fn fn4_rl_pr(
        &self,
        mem: &mut MainMemory,
        iosys: &IoSystem,
        io: &mut G,
        args: &[u32],
    ) -> Result<i32, AcceleratedCallError> {
        let mut obj = get_arg(args, 0);
        let mut id = get_arg(args, 1);

        let mut cla = 0;

        if (id as u32 & 0xFFFF0000) != 0 {
            cla = from_word_array(mem, self.params[0], id & 0xFFFF).map_err(|err| {
                AcceleratedCallError {
                    num: 4,
                    kind: err.into(),
                }
            })?;
            if self
                .fn5_oc_cl(mem, iosys, io, &[obj as u32, cla as u32])
                .map_err(|err| AcceleratedCallError {
                    num: 4,
                    kind: err.into(),
                })?
                == 0
            {
                return Ok(0);
            }
            id = (id as u32 >> 16) as i32;
            obj = cla;
        }

        let prop = self
            .fn2_cp_tab(mem, iosys, io, &[obj as u32, id as u32])
            .map_err(|err| AcceleratedCallError {
                num: 4,
                kind: err.into(),
            })?;

        if prop == 0 {
            return Ok(0);
        }

        if self
            .obj_in_class(mem, obj)
            .map_err(|err| AcceleratedCallError {
                num: 4,
                kind: err.into(),
            })?
            && cla == 0
        {
            if id < self.params[1] || id >= self.params[1] + 8 {
                return Ok(0);
            }
        }

        let mut ix;

        if from_word_array(mem, self.params[6], 0).map_err(|err| AcceleratedCallError {
            num: 4,
            kind: err.into(),
        })? != obj
        {
            ix = mem
                .aloadbit(prop as u32, 72)
                .map_err(|err| AcceleratedCallError {
                    num: 10,
                    kind: err.into(),
                })? as i32;
            if ix != 0 {
                return Ok(0);
            }
        }

        ix = mem
            .read_u16((prop as u32).wrapping_add(2))
            .map_err(|err| AcceleratedCallError {
                num: 10,
                kind: err.into(),
            })? as i32;

        Ok(4 * ix)
    }

    fn fn5_oc_cl(
        &self,
        mem: &mut MainMemory,
        iosys: &IoSystem,
        io: &mut G,
        args: &[u32],
    ) -> Result<i32, AcceleratedCallError> {
        let obj = get_arg(args, 0);
        let cla = get_arg(args, 1);

        let zr = self
            .fn1_z_region(mem, iosys, io, &[obj as u32])
            .map_err(|err| AcceleratedCallError {
                num: 5,
                kind: err.into(),
            })?;

        if zr == 3 {
            return Ok((cla == self.params[5]) as i32);
        }

        if zr == 2 {
            return Ok((cla == self.params[4]) as i32);
        }

        if zr != 1 {
            return Ok(0);
        }

        if cla == self.params[2] {
            return Ok((self
                .obj_in_class(mem, obj)
                .map_err(|err| AcceleratedCallError {
                    num: 5,
                    kind: err.into(),
                })?
                || obj == self.params[2]
                || obj == self.params[5]
                || obj == self.params[4]
                || obj == self.params[3]) as i32);
        }

        if cla == self.params[3] {
            if self
                .obj_in_class(mem, obj)
                .map_err(|err| AcceleratedCallError {
                    num: 5,
                    kind: err.into(),
                })?
                || obj == self.params[2]
                || obj == self.params[5]
                || obj == self.params[4]
                || obj == self.params[3]
            {
                return Ok(0);
            };
            return Ok(1);
        }

        if cla == self.params[5] || cla == self.params[4] {
            return Ok(0);
        }

        if !self
            .obj_in_class(mem, cla)
            .map_err(|err| AcceleratedCallError {
                num: 5,
                kind: err.into(),
            })?
        {
            self.print(
                io,
                mem,
                iosys,
                "[** Programming error: tried to apply 'ofclass' with non-class **]\n",
            );
            return Ok(0);
        }

        let inlist = self
            .fn3_ra_pr(mem, iosys, io, &[obj as u32, 2])
            .map_err(|err| AcceleratedCallError {
                num: 5,
                kind: err.into(),
            })?;

        if inlist == 0 {
            return Ok(0);
        }

        let inlistlen = self
            .fn4_rl_pr(mem, iosys, io, &[obj as u32, 2])
            .map_err(|err| AcceleratedCallError {
                num: 5,
                kind: err.into(),
            })?
            / 4;

        for jx in 0..inlistlen {
            if from_word_array(mem, inlist, jx).map_err(|err| AcceleratedCallError {
                num: 5,
                kind: err.into(),
            })? == cla
            {
                return Ok(1);
            }
        }

        Ok(0)
    }

    fn fn6_rv_pr(
        &self,
        mem: &mut MainMemory,
        iosys: &IoSystem,
        io: &mut G,
        args: &[u32],
    ) -> Result<i32, AcceleratedCallError> {
        let obj = get_arg(args, 0);
        let id = get_arg(args, 1);

        let addr = self
            .fn3_ra_pr(mem, iosys, io, &[obj as u32, id as u32])
            .map_err(|err| AcceleratedCallError {
                num: 6,
                kind: err.into(),
            })?;

        if addr == 0 {
            if id > 0 && id < self.params[1] {
                return from_word_array(mem, self.params[8], id).map_err(|err| {
                    AcceleratedCallError {
                        num: 6,
                        kind: err.into(),
                    }
                });
            }
            self.print(
                io,
                mem,
                iosys,
                "[** Programming error: tried to read (something) **]\n",
            );
            return Ok(0);
        }

        from_word_array(mem, addr, 0).map_err(|err| AcceleratedCallError {
            num: 6,
            kind: err.into(),
        })
    }

    fn fn7_op_pr(
        &self,
        mem: &mut MainMemory,
        iosys: &IoSystem,
        io: &mut G,
        args: &[u32],
    ) -> Result<i32, AcceleratedCallError> {
        let obj = get_arg(args, 0);
        let id = get_arg(args, 1);

        let zr = self
            .fn1_z_region(mem, iosys, io, &[obj as u32])
            .map_err(|err| AcceleratedCallError {
                num: 7,
                kind: err.into(),
            })?;

        if zr == 3 {
            return Ok((id == self.params[1] + 6 || id == self.params[1] + 7) as i32);
        }

        if zr == 2 {
            return Ok((id == self.params[1] + 5) as i32);
        }

        if zr != 1 {
            return Ok(0);
        }

        if id >= self.params[1] && id < self.params[1] + 8 {
            if self
                .obj_in_class(mem, obj)
                .map_err(|err| AcceleratedCallError {
                    num: 7,
                    kind: err.into(),
                })?
            {
                return Ok(1);
            }
        }

        Ok((self
            .fn3_ra_pr(mem, iosys, io, &[obj as u32, id as u32])
            .map_err(|err| AcceleratedCallError {
                num: 7,
                kind: err.into(),
            })?
            != 0) as i32)
    }

    fn fn8_cp_tab(
        &self,
        mem: &mut MainMemory,
        iosys: &IoSystem,
        io: &mut G,
        args: &[u32],
    ) -> Result<i32, AcceleratedCallError> {
        let obj = get_arg(args, 0);
        let id = get_arg(args, 1);

        if self
            .fn1_z_region(mem, iosys, io, &[obj as u32])
            .map_err(|err| AcceleratedCallError {
                num: 8,
                kind: err.into(),
            })?
            != 1
        {
            self.print(
                io,
                mem,
                iosys,
                "[** Programming error: tried to find the \".\" of (something) **]\n",
            );
            return Ok(0);
        }

        let mut otab = from_word_array(&mem, obj, 3 + self.params[7] / 4).map_err(|err| {
            AcceleratedCallError {
                num: 8,
                kind: err.into(),
            }
        })?;

        if otab == 0 {
            return Ok(0);
        }

        let max = from_word_array(mem, otab, 0).map_err(|err| AcceleratedCallError {
            num: 8,
            kind: err.into(),
        })?;
        otab += 4;

        Ok(mem
            .binary_search([id as u32, 2, otab as u32, 10, max as u32, 0, 0])
            .map_err(|err| AcceleratedCallError {
                num: 8,
                kind: err.into(),
            })? as i32)
    }

    fn fn9_ra_pr(
        &self,
        mem: &mut MainMemory,
        iosys: &IoSystem,
        io: &mut G,
        args: &[u32],
    ) -> Result<i32, AcceleratedCallError> {
        let mut obj = get_arg(args, 0);
        let mut id = get_arg(args, 1);

        let mut cla = 0;

        if (id as u32 & 0xFFFF0000) != 0 {
            cla = from_word_array(mem, self.params[0], (id as u32 & 0xFFFF) as i32).map_err(
                |err| AcceleratedCallError {
                    num: 9,
                    kind: err.into(),
                },
            )?;
            if self
                .fn11_oc_cl(mem, iosys, io, &[obj as u32, cla as u32])
                .map_err(|err| AcceleratedCallError {
                    num: 9,
                    kind: err.into(),
                })?
                == 0
            {
                return Ok(0);
            }
            id = ((id as u32) >> 16) as i32;
            obj = cla;
        }

        let prop = self
            .fn8_cp_tab(mem, iosys, io, &[obj as u32, id as u32])
            .map_err(|err| AcceleratedCallError {
                num: 9,
                kind: err.into(),
            })?;

        if prop == 0 {
            return Ok(0);
        }

        if self
            .obj_in_class(mem, obj)
            .map_err(|err| AcceleratedCallError {
                num: 9,
                kind: err.into(),
            })?
            && cla == 0
        {
            if id < self.params[1] || id >= self.params[1] + 8 {
                return Ok(0);
            }
        }

        if from_word_array(mem, self.params[6], 0).map_err(|err| AcceleratedCallError {
            num: 9,
            kind: err.into(),
        })? != obj
        {
            let ix = mem
                .aloadbit(prop as u32, 72)
                .map_err(|err| AcceleratedCallError {
                    num: 10,
                    kind: err.into(),
                })? as i32;
            if ix != 0 {
                return Ok(0);
            }
        }

        from_word_array(mem, prop, 1).map_err(|err| AcceleratedCallError {
            num: 9,
            kind: err.into(),
        })
    }

    fn fn10_rl_pr(
        &self,
        mem: &mut MainMemory,
        iosys: &IoSystem,
        io: &mut G,
        args: &[u32],
    ) -> Result<i32, AcceleratedCallError> {
        let mut obj = get_arg(args, 0);
        let mut id = get_arg(args, 1);

        let mut cla = 0;

        if (id as u32 & 0xFFFF0000) != 0 {
            cla = from_word_array(mem, self.params[0], (id as u32 & 0xFFFF) as i32).map_err(
                |err| AcceleratedCallError {
                    num: 10,
                    kind: err.into(),
                },
            )?;
            if self
                .fn11_oc_cl(mem, iosys, io, &[obj as u32, cla as u32])
                .map_err(|err| AcceleratedCallError {
                    num: 10,
                    kind: err.into(),
                })?
                == 0
            {
                return Ok(0);
            }
            id = (id as u32 >> 16) as i32;
            obj = cla;
        }

        let prop = self
            .fn8_cp_tab(mem, iosys, io, &[obj as u32, id as u32])
            .map_err(|err| AcceleratedCallError {
                num: 10,
                kind: err.into(),
            })?;

        if prop == 0 {
            return Ok(0);
        }

        if self
            .obj_in_class(mem, obj)
            .map_err(|err| AcceleratedCallError {
                num: 10,
                kind: err.into(),
            })?
            && cla == 0
        {
            if id < self.params[1] || id >= self.params[1] + 8 {
                return Ok(0);
            }
        }

        let mut ix;

        if from_word_array(mem, self.params[6], 0).map_err(|err| AcceleratedCallError {
            num: 10,
            kind: err.into(),
        })? != obj
        {
            ix = mem
                .aloadbit(prop as u32, 72)
                .map_err(|err| AcceleratedCallError {
                    num: 10,
                    kind: err.into(),
                })? as i32;
            if ix != 0 {
                return Ok(0);
            }
        }

        ix = mem
            .read_u16((prop as u32).wrapping_add(2))
            .map_err(|err| AcceleratedCallError {
                num: 10,
                kind: err.into(),
            })? as i32;

        Ok(4 * ix)
    }

    fn fn11_oc_cl(
        &self,
        mem: &mut MainMemory,
        iosys: &IoSystem,
        io: &mut G,
        args: &[u32],
    ) -> Result<i32, AcceleratedCallError> {
        let obj = get_arg(args, 0);
        let cla = get_arg(args, 1);

        let zr = self
            .fn1_z_region(mem, iosys, io, &[obj as u32])
            .map_err(|err| AcceleratedCallError {
                num: 11,
                kind: err.into(),
            })?;

        if zr == 3 {
            return Ok((cla == self.params[5]) as i32);
        }

        if zr == 2 {
            return Ok((cla == self.params[4]) as i32);
        }

        if zr != 1 {
            return Ok(0);
        }

        if cla == self.params[2] {
            return Ok((self
                .obj_in_class(mem, obj)
                .map_err(|err| AcceleratedCallError {
                    num: 11,
                    kind: err.into(),
                })?
                || obj == self.params[2]
                || obj == self.params[5]
                || obj == self.params[4]
                || obj == self.params[3]) as i32);
        }

        if cla == self.params[3] {
            return Ok(!(self
                .obj_in_class(mem, obj)
                .map_err(|err| AcceleratedCallError {
                    num: 11,
                    kind: err.into(),
                })?
                || obj == self.params[2]
                || obj == self.params[5]
                || obj == self.params[4]
                || obj == self.params[3]) as i32);
        }

        if cla == self.params[5] || cla == self.params[4] {
            return Ok(0);
        }

        if !self
            .obj_in_class(mem, cla)
            .map_err(|err| AcceleratedCallError {
                num: 11,
                kind: err.into(),
            })?
        {
            self.print(
                io,
                mem,
                iosys,
                "[** Programming error: tried to apply 'ofclass' with non-class **]\n",
            );
            return Ok(0);
        }

        let inlist = self
            .fn9_ra_pr(mem, iosys, io, &[obj as u32, 2])
            .map_err(|err| AcceleratedCallError {
                num: 11,
                kind: err.into(),
            })?;

        if inlist == 0 {
            return Ok(0);
        }

        let inlistlen = self
            .fn10_rl_pr(mem, iosys, io, &[obj as u32, 2])
            .map_err(|err| AcceleratedCallError {
                num: 11,
                kind: err.into(),
            })?
            / 4;

        for jx in 0..inlistlen {
            if from_word_array(&mem, inlist, jx).map_err(|err| AcceleratedCallError {
                num: 11,
                kind: err.into(),
            })? == cla
            {
                return Ok(1);
            }
        }

        Ok(0)
    }

    fn fn12_rv_pr(
        &self,
        mem: &mut MainMemory,
        iosys: &IoSystem,
        io: &mut G,
        args: &[u32],
    ) -> Result<i32, AcceleratedCallError> {
        let obj = get_arg(args, 0);
        let id = get_arg(args, 1);

        let addr = self
            .fn9_ra_pr(mem, iosys, io, &[obj as u32, id as u32])
            .map_err(|err| AcceleratedCallError {
                num: 12,
                kind: err.into(),
            })?;

        if addr == 0 {
            if id > 0 && id < self.params[1] {
                return from_word_array(mem, self.params[8], id).map_err(|err| {
                    AcceleratedCallError {
                        num: 12,
                        kind: err.into(),
                    }
                });
            }

            self.print(
                io,
                mem,
                iosys,
                "[** Programming error: tried to read (something) **]\n",
            );
            return Ok(0);
        }

        from_word_array(mem, addr, 0).map_err(|err| AcceleratedCallError {
            num: 12,
            kind: err.into(),
        })
    }

    fn fn13_op_pr(
        &self,
        mem: &mut MainMemory,
        iosys: &IoSystem,
        io: &mut G,
        args: &[u32],
    ) -> Result<i32, AcceleratedCallError> {
        let obj = get_arg(args, 0);
        let id = get_arg(args, 1);

        let zr = self
            .fn1_z_region(mem, iosys, io, &[obj as u32])
            .map_err(|err| AcceleratedCallError {
                num: 13,
                kind: err.into(),
            })?;

        if zr == 3 {
            return Ok((id == self.params[1] + 6 || id == self.params[1] + 7) as i32);
        }

        if zr == 2 {
            return Ok((id == self.params[1] + 5) as i32);
        }

        if zr != 1 {
            return Ok(0);
        }

        if id >= self.params[1] && id < self.params[1] + 8 {
            if self
                .obj_in_class(mem, obj)
                .map_err(|err| AcceleratedCallError {
                    num: 13,
                    kind: err.into(),
                })?
            {
                return Ok(1);
            }
        }

        if self
            .fn9_ra_pr(mem, iosys, io, &[obj as u32, id as u32])
            .map_err(|err| AcceleratedCallError {
                num: 13,
                kind: err.into(),
            })?
            != 0
        {
            return Ok(1);
        }

        Ok(0)
    }
}

#[derive(Debug)]
#[non_exhaustive]
pub(crate) struct AcceleratedCallError {
    num: u32,
    kind: AcceleratedCallErrorKind,
}

impl fmt::Display for AcceleratedCallError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let fn_name = match self.num {
            1 => "Z__Region",
            2 => "CP__Tab",
            3 => "RA__Pr",
            4 => "RL__Pr",
            5 => "OC__CL",
            6 => "RV__Pr",
            7 => "OP__Pr",
            8 => "CP__Tab/Inform 6.33",
            9 => "RA__Pr/Inform 6.33",
            10 => "RL__Pr/Inform 6.33",
            11 => "OC__Cl/Inform 6.33",
            12 => "RV__Pr/Inform 6.33",
            13 => "OP__Pr/Inform 6.33",
            n => unreachable!("the accelerated function number {n} should never be able to be registered and called")
        };

        write!(
            f,
            "error during execution of accelerated function number {} `{}`",
            self.num, fn_name
        )
    }
}

impl Error for AcceleratedCallError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        match &self.kind {
            AcceleratedCallErrorKind::AccereratedCall(err) => Some(err),
            AcceleratedCallErrorKind::MemoryRead(err) => Some(err),
            AcceleratedCallErrorKind::Aloadbit(err) => Some(err),
            AcceleratedCallErrorKind::Binarysearch(err) => Some(err),
        }
    }
}

#[derive(Debug)]
#[non_exhaustive]
enum AcceleratedCallErrorKind {
    AccereratedCall(Box<AcceleratedCallError>),
    MemoryRead(MemoryReadError),
    Aloadbit(AloadbitError),
    Binarysearch(BinarysearchError),
}

impl From<AcceleratedCallError> for AcceleratedCallErrorKind {
    fn from(value: AcceleratedCallError) -> Self {
        Self::AccereratedCall(Box::new(value))
    }
}

impl From<MemoryReadError> for AcceleratedCallErrorKind {
    fn from(value: MemoryReadError) -> Self {
        Self::MemoryRead(value)
    }
}

impl From<BinarysearchError> for AcceleratedCallErrorKind {
    fn from(value: BinarysearchError) -> Self {
        Self::Binarysearch(value)
    }
}

impl From<AloadbitError> for AcceleratedCallErrorKind {
    fn from(value: AloadbitError) -> Self {
        Self::Aloadbit(value)
    }
}
