use std::cmp::Ordering;
use std::collections::{BTreeMap, HashSet};
use std::error::Error;
use std::fmt;
use std::num::NonZeroU32;

use crate::{
    cpu::Cpu,
    decoding::{self, NewNodeError},
    op::{
        AloadbitError, BinarysearchError, InvalidSearchKeySizeError, MfreeError,
        NoBlockAtAddressError,
    },
    GlulxVersion,
};

/// Determines if a value at the given index and of the given size is in the range `start..end`.
///
/// Always returns false with a size of 0.
/// It might seem strange,
/// but it's because writing a value of size 0 in this range wouldn't change it.
pub(crate) fn is_addr_in_range(addr: u32, size: u32, start: u32, end: u32) -> bool {
    size > 0 && addr >= start - (size - 1) && addr < end
}

#[derive(Debug, PartialEq, Eq, Hash)]
pub(crate) struct Block {
    pub(crate) start: u32,
    pub(crate) len: u32,
}

#[derive(Debug)]
pub(crate) struct MainMemory {
    bytes: Vec<u8>,
    verified: bool,

    /// The memory block protected from restart, restore and restoreundo.
    ///
    /// If both values of the block are 0, it means no memory is protected.
    pub(crate) protected: Block,

    /// The address of the beginning of the heap in the main memory.
    ///
    /// `None` indicates the heap is not currently active.
    pub(crate) heap_start: Option<NonZeroU32>,

    /// The currently allocated heap blocks,
    /// the keys being their address and the values their length.
    pub(crate) heap_blocks: BTreeMap<u32, u32>,

    // It's easier to store the decoding tree in the main memory,
    // which might need to invalidate it on writes.
    //
    /// The tree used to decode strings.
    pub(crate) decoding_tree: decoding::Tree,

    retained: HashSet<Block>,
}

impl MainMemory {
    pub(crate) fn new(memory: Vec<u8>) -> Result<Self, NewMainMemoryError> {
        // Check if the size is enough.
        // (The ROM must at least be 256 bytes long.)
        if memory.len() < 256 {
            return Err(NewMainMemoryError::TooSmall(memory.len()));
        }

        let mut memory = MainMemory {
            bytes: memory,
            verified: false,
            protected: Block { start: 0, len: 0 },
            heap_start: None,
            heap_blocks: BTreeMap::new(),
            decoding_tree: decoding::Tree {
                addr: 0,
                len: 0,
                root: None,
            },
            retained: HashSet::new(),
        };

        // Check the magic number.
        let magic_number = memory.magic_number();
        if magic_number != b"Glul" {
            return Err(NewMainMemoryError::InvalidMagicNumber([
                magic_number[0],
                magic_number[1],
                magic_number[2],
                magic_number[3],
            ]));
        }

        // Verify the memory.
        if memory.compute_checksum() == memory.checksum() {
            memory.verified = true;
        }

        // Resize the memory so that it has a length of ENDMEM.
        memory.bytes.resize(memory.endmem().try_into().unwrap(), 0);

        // Cache the decoding tree.
        memory.decoding_tree.addr = memory.decoding_table();
        memory.cache_decoding_tree().expect("TODO handle error");

        Ok(memory)
    }

    pub(crate) fn len(&self) -> usize {
        self.bytes.len()
    }

    pub(crate) fn as_bytes(&self) -> &[u8] {
        &self.bytes
    }

    /// Retrieves the magic number of the memory as a slice of `u8` of length 4.
    ///
    /// Since it's a Glulx story, the magic number is always `b"Glul"`.
    pub(crate) fn magic_number(&self) -> &[u8] {
        &self.bytes[..4]
    }

    /// Retrieves the version of Glulx that the memory uses as the tuple `(major: u16, minor: u8, subminor: u8)`.
    pub(crate) fn glulx_version(&self) -> GlulxVersion {
        GlulxVersion::new(
            u16::from_be_bytes(self.bytes[4..6].try_into().unwrap()),
            self.bytes[6],
            self.bytes[7],
        )
    }

    /// Retrieves the index of the beginning of the RAM in this memory.
    pub(crate) fn ramstart(&self) -> u32 {
        u32::from_be_bytes(self.bytes[8..12].try_into().unwrap())
    }

    /// Retrieves the index of the end of the initial memory in this memory.
    pub(crate) fn _extstart(&self) -> u32 {
        u32::from_be_bytes(self.bytes[12..16].try_into().unwrap())
    }

    /// Retrieves the maximum length of this memory (i.e. the end of the story memory map).
    pub(crate) fn endmem(&self) -> u32 {
        u32::from_be_bytes(self.bytes[16..20].try_into().unwrap())
    }

    /// Retrieves the size of the stack needed by this memory.
    pub(crate) fn stack_size(&self) -> u32 {
        u32::from_be_bytes(self.bytes[20..24].try_into().unwrap())
    }

    /// Retrieves the index of the function that starts the story in this memory.
    pub(crate) fn start_function(&self) -> u32 {
        u32::from_be_bytes(self.bytes[24..28].try_into().unwrap())
    }

    /// Retrieves the index of the initial table used to decode compressed strings in this memory.
    pub(crate) fn decoding_table(&self) -> u32 {
        u32::from_be_bytes(self.bytes[28..32].try_into().unwrap())
    }

    /// Retrieves the expected value of the checksum of this memory.
    pub(crate) fn checksum(&self) -> u32 {
        u32::from_be_bytes(self.bytes[32..36].try_into().unwrap())
    }

    /// Retrieves the descriptive value of this memory as a slice of `u8` of length 4.
    ///
    /// For stories compiled with Inform, this will be equal to `b"Info"`.
    pub(crate) fn _descriptive(&self) -> &[u8] {
        &self.bytes[36..40]
    }

    /// Returns true if the checksum of this memory equals the value stored in its header.
    pub(crate) fn is_verified(&self) -> bool {
        self.verified
    }

    /// Computes the checksum of the current state of this memory.
    fn compute_checksum(&self) -> u32 {
        let mut sum: u32 = 0;
        for i in 0..self.bytes.len() / 4 {
            sum = sum.wrapping_add(u32::from_be_bytes(
                self.bytes[i * 4..i * 4 + 4].try_into().unwrap(),
            ));
        }

        // We remove the checksum since it has been added from the story header.
        sum.wrapping_sub(self.checksum())
    }

    pub(crate) fn resize(&mut self, new_len: usize) {
        self.bytes.resize(new_len, 0)
    }

    pub(crate) fn truncate(&mut self, len: usize) {
        self.bytes.truncate(len)
    }

    pub(crate) fn heap_is_active(&self) -> bool {
        self.heap_start.is_some()
    }

    /// Allocates a memory block of the given length and returns its address.
    pub(crate) fn malloc(&mut self, len: u32) -> u32 {
        // TODO
        // We could surely do better than just searching some free space by looking between each consecutive block
        // (for example by keeping a free list).
        // Although we can't assume any particular structure for the heap,
        // because we could inherit it from another interpreter when restoring a game.

        let mem_end = u32::try_from(self.len()).unwrap();

        let addr = match self.heap_start {
            Some(start) => {
                // The heap is already active.
                // We'll try to find a place between extant blocks.
                let mut alloc_addr;
                let start = start.get();
                // We add a fictive zero-sized block at the start of the heap
                // so we can check the free space at the start.
                let mut iter = std::iter::once((&start, &0))
                    .chain(self.heap_blocks.iter())
                    .peekable();
                loop {
                    // We can unwrap because if the heap is active,
                    // we have at least one block,
                    // and we break out of the loop when the peeked block is None.
                    let (addr1, len1) = iter.next().unwrap();
                    alloc_addr = addr1 + len1;

                    let Some((addr2, _)) = iter.peek() else {
                        // We're at the end of the heap,
                        // we'll allocate the new block right after the current one
                        // (which is the last).
                        break;
                    };

                    // We check if the space between the current block and the next is enough.
                    if **addr2 - alloc_addr >= len {
                        break;
                    }

                    // If we reach this point,
                    // the space wasn't enough so we loop back to check the next consecutive pair.
                }

                alloc_addr
            }
            None => {
                // The heap is inactive.
                // We set the heap start and add the block at the end of the memory.
                debug_assert!(self.heap_blocks.is_empty());

                // The memory end is guaranteed to be non zero.
                self.heap_start = Some(mem_end.try_into().unwrap());
                mem_end
            }
        };

        self.heap_blocks.insert(addr, len);

        // Extend the memory if needed.
        if addr + len > mem_end {
            // At that point, we know `heap_start` is not `None`.
            let heap_start = self.heap_start.unwrap().get();
            // Find the smallest multiple of 256 greater or equal to the space needed for the new block.
            let mut heap_size = addr + len + 256 - 1;
            heap_size -= heap_size % 256;
            // And add 256 so we have some room if the initial space needed was close or equal to a multiple of 256.
            heap_size += 256;
            self.resize((heap_start + heap_size).try_into().unwrap());
        }

        addr
    }

    /// Frees the memory block of the given address.
    pub(crate) fn mfree(&mut self, addr: u32) -> Result<(), MfreeError> {
        if self.heap_blocks.remove(&addr).is_none() {
            return Err(MfreeError::NoBlockAtAddress(NoBlockAtAddressError { addr }));
        };

        if self.heap_blocks.is_empty() {
            self.truncate(self.heap_start.unwrap().get().try_into().unwrap());
            self.heap_start = None;
        }

        Ok(())
    }

    pub(crate) fn cache_decoding_tree(&mut self) -> Result<(), CacheDecodingTreeError> {
        self.decoding_tree.len = self
            .read_u32(self.decoding_tree.addr)
            .map_err(|err| CacheDecodingTreeError { kind: err.into() })?;

        self.decoding_tree.root = Some(
            decoding::Node::new(
                &self,
                self.read_u32(self.decoding_tree.addr + 8)
                    .map_err(|err| CacheDecodingTreeError { kind: err.into() })?,
            )
            .map_err(|err| CacheDecodingTreeError {
                kind: CacheDecodingTreeErrorKind::NewNodeError(err),
            })?,
        );

        Ok(())
    }

    // TODO: Not sure it's needed. It's the same as `write_slice` but without the checks.
    pub(crate) fn splice(&mut self, start: usize, end: usize, replace_with: &[u8]) {
        self.bytes.splice(start..end, replace_with.iter().copied());
    }

    /// Reads the byte at the given address in this memory.
    pub(crate) fn read_u8(&self, addr: u32) -> Result<u8, MemoryReadError> {
        if self.is_u8_in_retained_memory(addr) {
            return Err(MemoryReadError {
                addr,
                size: 1,
                kind: MemoryReadErrorKind::InRetainedMemory,
            });
        }

        let addr_usize = usize::try_from(addr).unwrap();

        self.bytes.get(addr_usize).copied().ok_or(MemoryReadError {
            addr,
            size: 1,
            kind: MemoryReadErrorKind::OutOfBound,
        })
    }

    /// Writes the given `u8` at the given address in this memory, overwriting the value already there.
    pub(crate) fn write_u8(&mut self, addr: u32, value: u8) -> Result<(), MemoryWriteError> {
        if self.is_u8_in_retained_memory(addr) {
            return Err(MemoryWriteError {
                addr,
                size: 1,
                kind: MemoryWriteErrorKind::InRetainedMemory,
            });
        }
        if addr < self.ramstart() {
            return Err(MemoryWriteError {
                addr,
                size: 1,
                kind: MemoryWriteErrorKind::InRom,
            });
        }
        // Invalidate the cached decoding tree if we are writing in it.
        let tree = &self.decoding_tree;
        if tree.root.is_some() && is_addr_in_range(addr, 1, tree.addr, tree.addr + tree.len) {
            self.decoding_tree.root = None;
        }
        let addr_usize = usize::try_from(addr).unwrap();

        if addr_usize >= self.bytes.len() {
            return Err(MemoryWriteError {
                addr,
                size: 1,
                kind: MemoryWriteErrorKind::OutOfBound,
            });
        }
        self.bytes[addr_usize] = value;

        Ok(())
    }

    /// Reads two bytes from the given address in this memory and returns it as an `u16`.
    pub(crate) fn read_u16(&self, addr: u32) -> Result<u16, MemoryReadError> {
        if self.is_u16_in_retained_memory(addr) {
            return Err(MemoryReadError {
                addr,
                size: 2,
                kind: MemoryReadErrorKind::InRetainedMemory,
            });
        }

        let addr_usize = usize::try_from(addr).unwrap();

        self.bytes
            .get(addr_usize..addr_usize + 2)
            .map(|slice| u16::from_be_bytes(slice.try_into().unwrap()))
            .ok_or(MemoryReadError {
                addr,
                size: 2,
                kind: MemoryReadErrorKind::OutOfBound,
            })
    }

    /// Writes two bytes at the given address in this memory, overwriting the values already there.
    pub(crate) fn write_u16(&mut self, addr: u32, value: u16) -> Result<(), MemoryWriteError> {
        if self.is_u16_in_retained_memory(addr) {
            return Err(MemoryWriteError {
                addr,
                size: 2,
                kind: MemoryWriteErrorKind::InRetainedMemory,
            });
        }
        if addr < self.ramstart() {
            return Err(MemoryWriteError {
                addr,
                size: 2,
                kind: MemoryWriteErrorKind::InRom,
            });
        }
        // Invalidate the cached decoding tree if we are writing in it.
        let tree = &self.decoding_tree;
        if tree.root.is_some() && is_addr_in_range(addr, 2, tree.addr, tree.addr + tree.len) {
            self.decoding_tree.root = None;
        }
        let addr_usize = usize::try_from(addr).unwrap();

        if addr_usize + 1 >= self.bytes.len() {
            return Err(MemoryWriteError {
                addr,
                size: 2,
                kind: MemoryWriteErrorKind::OutOfBound,
            });
        }

        for (i, byte) in value.to_be_bytes().into_iter().enumerate() {
            self.bytes[addr_usize + i] = byte;
        }

        Ok(())
    }

    /// Reads four bytes from the given address in this memory and returns it as an `u32`.
    pub(crate) fn read_u32(&self, addr: u32) -> Result<u32, MemoryReadError> {
        if self.is_u32_in_retained_memory(addr) {
            return Err(MemoryReadError {
                addr,
                size: 4,
                kind: MemoryReadErrorKind::InRetainedMemory,
            });
        }

        let addr_usize = usize::try_from(addr).unwrap();

        self.bytes
            .get(addr_usize..addr_usize + 4)
            .map(|slice| u32::from_be_bytes(slice.try_into().unwrap()))
            .ok_or(MemoryReadError {
                addr,
                size: 4,
                kind: MemoryReadErrorKind::OutOfBound,
            })
    }

    /// Writes four bytes at the given address in this memory, overwriting the values already there.
    pub(crate) fn write_u32(&mut self, addr: u32, value: u32) -> Result<(), MemoryWriteError> {
        if self.is_u32_in_retained_memory(addr) {
            return Err(MemoryWriteError {
                addr,
                size: 4,
                kind: MemoryWriteErrorKind::InRetainedMemory,
            });
        }
        if addr < self.ramstart() {
            return Err(MemoryWriteError {
                addr,
                size: 4,
                kind: MemoryWriteErrorKind::InRom,
            });
        }
        // Invalidate the cached decoding tree if we are writing in it.
        let tree = &self.decoding_tree;
        if tree.root.is_some() && is_addr_in_range(addr, 4, tree.addr, tree.addr + tree.len) {
            self.decoding_tree.root = None;
        }

        let addr_usize = usize::try_from(addr).unwrap();

        if addr_usize + 3 >= self.bytes.len() {
            return Err(MemoryWriteError {
                addr,
                size: 1,
                kind: MemoryWriteErrorKind::OutOfBound,
            });
        }

        for (i, byte) in value.to_be_bytes().into_iter().enumerate() {
            self.bytes[addr_usize + i] = byte;
        }

        Ok(())
    }

    /// Reads the slice of `u8` at the given address and with the given length in this memory.
    pub(crate) fn read_slice(&self, addr: u32, length: u32) -> Result<&[u8], MemoryReadError> {
        if self.is_slice_in_retained_memory(addr, length) {
            return Err(MemoryReadError {
                addr,
                size: length,
                kind: MemoryReadErrorKind::InRetainedMemory,
            });
        }
        let addr = usize::try_from(addr).unwrap();
        let length = usize::try_from(length).unwrap();
        Ok(&self.bytes[addr..addr + length])
    }

    // TODO: Find a better name?
    pub(crate) fn write_slice_inner(
        &mut self,
        addr: u32,
        values: &[u8],
    ) -> Result<(), MemoryWriteError> {
        if addr < self.ramstart() {
            return Err(MemoryWriteError {
                addr,
                size: values.len().try_into().unwrap(),
                kind: MemoryWriteErrorKind::InRom,
            });
        }
        // Invalidate the cached decoding tree if we are writing in it.
        let tree = &self.decoding_tree;
        if tree.root.is_some()
            && is_addr_in_range(
                addr,
                values.len().try_into().unwrap(),
                tree.addr,
                tree.addr + tree.len,
            )
        {
            self.decoding_tree.root = None;
        }

        let addr_usize = usize::try_from(addr).unwrap();

        if addr_usize + values.len() - 1 >= self.bytes.len() {
            return Err(MemoryWriteError {
                addr,
                size: values.len().try_into().unwrap(),
                kind: MemoryWriteErrorKind::OutOfBound,
            });
        }

        self.bytes.splice(
            addr_usize..addr_usize + values.len(),
            values.iter().cloned(),
        );

        Ok(())
    }

    /// Writes the given `u8`s in this memory, beginning from the given
    /// address, overwriting the values already there.
    pub(crate) fn write_slice(&mut self, addr: u32, values: &[u8]) -> Result<(), MemoryWriteError> {
        if self.is_slice_in_retained_memory(addr, values.len().try_into().unwrap()) {
            return Err(MemoryWriteError {
                addr,
                size: values.len().try_into().unwrap(),
                kind: MemoryWriteErrorKind::InRetainedMemory,
            });
        }
        self.write_slice_inner(addr, values)
    }

    /// Determine if the `u8` at the given address in the main memory is in a section retained by Glauque.
    pub(crate) fn is_u8_in_retained_memory(&self, addr: u32) -> bool {
        for block in &self.retained {
            if is_addr_in_range(addr, 1, block.start, block.start + block.len) {
                return true;
            }
        }
        false
    }

    /// Determine if the `u16` starting at the given address in the main memory is in a section retained by Glauque.
    pub(crate) fn is_u16_in_retained_memory(&self, addr: u32) -> bool {
        for block in &self.retained {
            if is_addr_in_range(addr, 2, block.start, block.start + block.len) {
                return true;
            }
        }
        false
    }

    /// Determine if the `u32` starting at the given address in the main memory is in a section retained by Glauque.
    pub(crate) fn is_u32_in_retained_memory(&self, addr: u32) -> bool {
        for block in &self.retained {
            if is_addr_in_range(addr, 4, block.start, block.start + block.len) {
                return true;
            }
        }
        false
    }

    /// Determine if the slice at the given address and with the given length
    /// in the main memory is in a section retained by Glauque.
    pub(crate) fn is_slice_in_retained_memory(&self, addr: u32, length: u32) -> bool {
        for block in &self.retained {
            if is_addr_in_range(addr, length, block.start, block.start + block.len) {
                return true;
            }
        }
        false
    }

    pub(crate) fn aloadbit(&self, l1: u32, l2: u32) -> Result<u32, AloadbitError> {
        let bit_pos = l2 % 8;

        // TODO: It should be possible to make it branchless, right?
        let addr = if l2 as i32 >= 0 {
            l1.wrapping_add(l2 / 8)
        } else {
            l1.wrapping_sub((7_i32.wrapping_sub(l2 as i32) / 8) as u32)
        };

        let byte = self.read_u8(addr)?;

        return Ok((byte & 1 << bit_pos != 0) as u32);
    }

    pub(crate) fn binary_search(&self, args: [u32; 7]) -> Result<u32, BinarysearchError> {
        let [key, key_size, start, struct_size, num_structs, key_offset, options] = args;

        let key_size_usize = usize::try_from(key_size).unwrap();
        let key = if options & 0x1 == 0 {
            match &key_size_usize {
                1 | 2 | 4 => key.to_be_bytes()[4 - key_size_usize..4].to_vec(),
                _ => {
                    return Err(BinarysearchError::InvalidSearchKeySize(
                        InvalidSearchKeySizeError(key_size),
                    ))
                }
            }
        } else {
            self.read_slice(key, key_size)?.to_vec()
        };

        // We could use Rust's binary_search (on the slice type),
        // but since the keys can go beyond a struct's bound and into the next struct
        // (or even overlap),
        // we would first need to iterate over the keys to collect them,
        // which would defeat the purpose of a binary search.

        // For the bounds:
        // - We use signed integer to handle the case when the upper one reach -1.
        // - We widen to 64 bits so that it can still contain the whole u32 range.
        // (I believe the widening is not strictly necessary since they are indices
        // and not addresses, and I doubt stories would go so far, but well.)
        let mut lower_bound = 0_i64;
        let mut upper_bound = i64::from(num_structs) - 1;
        let mut found = None;
        while lower_bound <= upper_bound {
            // Won't overflow because the original values come from an u32,
            // and we're working with i64s.
            let i = (lower_bound + upper_bound) / 2;
            // `i` should always be in the range of a u32, so the cast is OK.
            let tested_key =
                self.read_slice(start + i as u32 * struct_size + key_offset, key_size)?;
            // It works because slices are compared lexicographically.
            match tested_key.cmp(&key) {
                Ordering::Less => lower_bound = i + 1,
                Ordering::Greater => upper_bound = i - 1,
                Ordering::Equal => {
                    // `i` should always be in the range of a u32, so the cast is OK.
                    found = Some(i as u32);
                    break;
                }
            }
        }

        let return_index = options & 0x4 != 0;

        match found {
            Some(index) if return_index => Ok(index),
            Some(index) => Ok(start + index * struct_size),
            None if return_index => Ok(0xFFFFFFFF),
            None => Ok(0),
        }
    }
}

#[derive(Debug)]
pub(crate) enum NewMainMemoryError {
    InvalidMagicNumber([u8; 4]),
    TooSmall(usize),
}

impl Error for NewMainMemoryError {}

impl fmt::Display for NewMainMemoryError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        use NewMainMemoryError::*;

        match self {
            // TODO: Remove String allocation?
            InvalidMagicNumber(magic) => write!(
                f, "invalid magic number: {:?} (ASCII for \"{}\") instead of [71, 108, 117, 108] (ASCII for \"Glul\")",
                magic, String::from_utf8_lossy(magic)
            ),
            TooSmall(len) => write!(f, "image too small: {len} while minimum is 256"),
        }
    }
}

/// The error type when reading values in the main memory.
///
/// It is returned by `MainMemory::read_*` functions.
#[derive(Debug)]
#[non_exhaustive]
pub(crate) struct MemoryReadError {
    /// Ihe address that was being read.
    addr: u32,

    /// The number of bytes being read.
    size: u32,

    /// The kind of error that was encountered.
    kind: MemoryReadErrorKind,
}

impl fmt::Display for MemoryReadError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let size = self.size;
        let addr = self.addr;
        write!(f, "tried to read {size} byte(s) from address {addr:#X}")?;
        match self.kind {
            MemoryReadErrorKind::InRetainedMemory => write!(f, ", which is in retained memory"),
            MemoryReadErrorKind::OutOfBound => write!(f, ", which is out of bounds",),
        }
    }
}

impl Error for MemoryReadError {}

/// An enum describing the kind of [`MemoryReadError`].
#[derive(Debug)]
enum MemoryReadErrorKind {
    /// The address is out of bound.
    OutOfBound,

    /// The address is in memory retained by Glauque.
    InRetainedMemory,
}

/// The error type when writing values in the main memory.
///
/// It is returned by `MainMemory::write_*` functions.
#[derive(Debug)]
#[non_exhaustive]
pub(crate) struct MemoryWriteError {
    /// Ihe address that was being read.
    addr: u32,

    /// The number of bytes being read.
    size: u32,

    /// The kind of error that was encountered.
    kind: MemoryWriteErrorKind,
}

impl fmt::Display for MemoryWriteError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let size = self.size;
        let addr = self.addr;
        write!(f, "tried to write {size} byte(s) to address {addr:#X}")?;
        match self.kind {
            MemoryWriteErrorKind::InRetainedMemory => write!(f, ", which is in retained memory"),
            MemoryWriteErrorKind::InRom => write!(f, ", which is in ROM",),
            MemoryWriteErrorKind::OutOfBound => write!(f, ", which is out of bounds",),
        }
    }
}

impl Error for MemoryWriteError {}

/// An enum describing the kind of [`MemoryReadError`].
#[derive(Debug)]
enum MemoryWriteErrorKind {
    /// The address is out of bound.
    OutOfBound,

    /// The address is in ROM
    InRom,

    /// The address is in memory retained by Glauque.
    InRetainedMemory,
}

/// The error type when caching the decoding tree with [`MainMemory::cache_decoding_tree`].
#[derive(Debug)]
#[non_exhaustive]
pub(crate) struct CacheDecodingTreeError {
    /// The kind of error that was encountered.
    kind: CacheDecodingTreeErrorKind,
}

impl fmt::Display for CacheDecodingTreeError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "couldn't cache decoding tree")
    }
}

impl Error for CacheDecodingTreeError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        match &self.kind {
            CacheDecodingTreeErrorKind::MemoryRead(err) => Some(err),
            CacheDecodingTreeErrorKind::NewNodeError(err) => Some(err),
        }
    }
}

/// An enum describing the kind of [`CacheDecodingTreeError`].
#[derive(Debug)]
enum CacheDecodingTreeErrorKind {
    /// Reading the main memory failed with the inner error.
    MemoryRead(MemoryReadError),

    NewNodeError(NewNodeError),
}

impl From<MemoryReadError> for CacheDecodingTreeErrorKind {
    fn from(value: MemoryReadError) -> Self {
        Self::MemoryRead(value)
    }
}

impl glauque::Memory for MainMemory {
    fn read_u8s_to_end(&self, addr: u32) -> &[u8] {
        let addr: usize = addr.try_into().unwrap();
        &self.bytes.get(addr..).expect("TODO handle error")
    }

    fn write_u8s(&mut self, addr: u32, values: impl IntoIterator<Item = u8>) {
        // TODO: error if writing in ROM.
        // TODO: error if writing out of bounds.
        // TODO: reuse `write_u8` above?
        let start: usize = addr.try_into().unwrap();
        values
            .into_iter()
            .enumerate()
            .for_each(|(i, byte)| self.bytes[start + i] = byte);
    }

    fn read_u32(&self, addr: u32) -> u32 {
        let addr = usize::try_from(addr).unwrap();
        u32::from_be_bytes(
            self.bytes
                .get(addr..addr + 4)
                .expect("TODO handle error")
                .try_into()
                .unwrap(),
        )
    }

    fn read_u32s(&self, addr: u32, len: u32) -> Vec<u32> {
        let addr = usize::try_from(addr).unwrap();
        let len = usize::try_from(len).unwrap();
        self.bytes
            .get(addr..addr + len * 4)
            .expect("TODO handle error")
            .chunks_exact(4)
            .map(|bytes| u32::from_be_bytes(bytes.try_into().unwrap()))
            .collect()
    }

    fn write_u32(&mut self, addr: u32, value: u32) {
        // TODO: error if writing in ROM.
        // TODO: error if writing out of bounds.
        let addr = usize::try_from(addr).unwrap();
        for (i, byte) in value.to_be_bytes().into_iter().enumerate() {
            self.bytes[addr + i] = byte;
        }
    }

    fn write_u32s(&mut self, addr: u32, values: impl IntoIterator<Item = u32>) {
        // TODO: error if writing in ROM.
        // TODO: error if writing out of bounds.
        // TODO : reuse `write_u32` above?
        let start: usize = addr.try_into().unwrap();
        values
            .into_iter()
            .flat_map(|word| word.to_be_bytes())
            .enumerate()
            .for_each(|(i, byte)| self.bytes[start + i] = byte);
    }

    fn retain_memory(&mut self, start: u32, len: u32) {
        self.retained.insert(Block { start, len });
    }

    fn release_memory(&mut self, start: u32, len: u32) {
        self.retained.remove(&Block { start, len });
    }
}

#[derive(Debug)]
pub(crate) struct Stack {
    bytes: Vec<u8>,
    pub(crate) frame_ptr: u32,
}

impl Stack {
    pub(crate) fn with_capacity(capacity: usize) -> Self {
        Stack {
            bytes: Vec::with_capacity(capacity),
            frame_ptr: 0,
        }
    }

    pub(crate) fn as_bytes(&self) -> &[u8] {
        &self.bytes
    }

    pub(crate) fn as_bytes_mut(&mut self) -> &mut [u8] {
        &mut self.bytes
    }

    pub(crate) fn len(&self) -> usize {
        self.bytes.len()
    }

    /// Retrieves the current stack pointer (i.e. its length).
    pub(crate) fn ptr(&self) -> u32 {
        self.bytes.len().try_into().unwrap()
    }

    /// Retrieves the address of the current call frame's locals.
    pub(crate) fn frame_locals_address(&self) -> u32 {
        // FramePtr + LocalsPos
        self.frame_ptr + self.read_u32(self.frame_ptr + 4)
    }

    /// Gets the number of 32-bit values above the current call frame.
    pub(crate) fn count(&self) -> u32 {
        // Stack pointer minus the position of the end of the current call frame.
        let byte_number = self.ptr() - self.frame_ptr - self.read_u32(self.frame_ptr);

        // Divide by 4 because we count the number of 32-bit values.
        byte_number / 4
    }

    pub(crate) fn clear(&mut self) {
        self.bytes.clear();
    }

    pub(crate) fn resize(&mut self, new_len: usize) {
        self.bytes.resize(new_len, 0);
    }

    pub(crate) fn truncate(&mut self, len: usize) {
        self.bytes.truncate(len);
    }

    /// Reads the byte at the given address in this stack.
    pub(crate) fn read_u8(&self, addr: u32) -> u8 {
        let addr = usize::try_from(addr).unwrap();
        self.bytes[addr]
    }

    /// Reads two bytes from the given address in this stack and returns it as a `u16`.
    pub(crate) fn read_u16(&self, addr: u32) -> u16 {
        let addr = usize::try_from(addr).unwrap();
        u16::from_be_bytes(self.bytes[addr..addr + 2].try_into().unwrap())
    }

    /// Reads four bytes from the given address in the stack and returns it as an `u32`.
    pub(crate) fn read_u32(&self, addr: u32) -> u32 {
        let addr = usize::try_from(addr).unwrap();
        u32::from_be_bytes(self.bytes[addr..addr + 4].try_into().unwrap())
    }

    // TODO: Pushing doesn't check the capacity. So if the story keeps pushing, we'll consume all the host's memory instead of returning a stack overflow error. Maybe we should check use a boxed slice, or use `push_within_capacity` when it's stabilised.

    /// Pushes the given byte on this stack.
    pub(crate) fn push_u8(&mut self, x: u8) {
        self.bytes.push(x);
    }

    /// Pushes the given `u32` on this stack.
    pub(crate) fn push_u32(&mut self, x: u32) {
        self.bytes.extend_from_slice(&x.to_be_bytes())
    }

    /// Pushes each `u8` from the given slice on this stack.
    pub(crate) fn push_slice(&mut self, slice: &[u8]) {
        self.bytes.extend_from_slice(slice);
    }

    /// Pops 4 bytes off this stack and return them as a `u32`.
    pub(crate) fn pop_u32(&mut self) -> u32 {
        // Since the values are pushed on the stack as big end first, we retrieve it as little endian.
        u32::from_le_bytes([
            self.bytes.pop().unwrap(),
            self.bytes.pop().unwrap(),
            self.bytes.pop().unwrap(),
            self.bytes.pop().unwrap(),
        ])
    }

    /// Writes an `u8` at the given position on this stack, overwriting the value already present.
    pub(crate) fn write_u8(&mut self, pos: u32, value: u8) {
        let pos = usize::try_from(pos).unwrap();
        self.bytes[pos] = value;
    }

    /// Writes a `u16` at the given position on this stack, overwriting the two bytes already present.
    pub(crate) fn write_u16(&mut self, pos: u32, value: u16) {
        let pos = usize::try_from(pos).unwrap();
        for (i, byte) in value.to_be_bytes().into_iter().enumerate() {
            self.bytes[pos + i] = byte;
        }
    }

    /// Writes an `u32` at the given position on the stack, overwriting the four bytes already present.
    pub(crate) fn write_u32(&mut self, pos: u32, value: u32) {
        let pos = usize::try_from(pos).unwrap();
        for (i, byte) in value.to_be_bytes().into_iter().enumerate() {
            self.bytes[pos + i] = byte;
        }
    }

    /// Pushes a call stub on the stack with the given DestType and DestAddr.
    pub(crate) fn push_call_stub(&mut self, cpu: &Cpu, dest_type: u32, dest_addr: u32) {
        self.push_u32(dest_type);
        self.push_u32(dest_addr);
        self.push_u32(cpu.pc);
        self.push_u32(self.frame_ptr);
    }

    /// Pops a call stub off the stack and return its DestType and DestAddr.
    pub(crate) fn pop_call_stub(&mut self, cpu: &mut Cpu) -> (u32, u32) {
        self.frame_ptr = self.pop_u32();
        cpu.pc = self.pop_u32();
        let dest_addr = self.pop_u32();
        let dest_type = self.pop_u32();
        (dest_type, dest_addr)
    }

    /// Builds the call frame for the function at the given address in the main memory.
    ///
    /// The PC will be set to the first instruction of the function.
    ///
    /// The slice `args` contains the arguments given to the function.
    pub(crate) fn push_call_frame(
        &mut self,
        cpu: &mut Cpu,
        mem: &MainMemory,
        addr: u32,
        args: &[u32],
    ) -> Result<(), PushCallFrameError> {
        // 1. Set the frame pointer to the stack pointer.

        self.frame_ptr = self.ptr();

        // 2. Build and push the call frame.

        let fn_type = mem
            .read_u8(addr)
            .map_err(|err| PushCallFrameError { kind: err.into() })?;
        if !matches!(fn_type, 0xC0 | 0xC1) {
            return Err(PushCallFrameError {
                kind: PushCallFrameErrorKind::InvalidFunctionType { ty: fn_type },
            });
        }

        // We push zeros for FrameLen and LocalsPos. They'll be changed when we'll know their value.
        self.resize(self.len() + 8);

        // We construct the locals while pushing the formats of the locals.
        let mut args_cursor = 0;
        let mut locals = Vec::new();
        let mut args_byte_number = 0;
        for i in (1..).step_by(2) {
            let local_type = mem
                .read_u8(addr + i)
                .map_err(|err| PushCallFrameError { kind: err.into() })?;
            let local_count = mem
                .read_u8(addr + i + 1)
                .map_err(|err| PushCallFrameError { kind: err.into() })?;

            // Push the format of the locals.
            self.push_u8(local_type);
            self.push_u8(local_count);

            // A pair of zeros indicates the end of the format of the locals.
            if local_type == 0 && local_count == 0 {
                break;
            }

            // We will now store the locals and arguments to be pushed later.
            let local_type = local_type as usize;
            let local_count = local_count as usize;

            // Add the required padding so that the locals we are storing are aligned.
            locals.resize(
                locals.len() + local_type - (locals.len() + local_type - 1) % local_type - 1,
                0x00,
            );

            // If the functions is local-argument, store them now.
            if fn_type == 0xC1 && args_cursor < args.len() {
                for &arg in args[args_cursor..args.len().min(local_count)].iter() {
                    if local_type == 1 {
                        locals.push(arg as u8);
                        args_byte_number += 1;
                    } else if local_type == 2 {
                        locals.extend((arg as u16).to_be_bytes().iter());
                        args_byte_number += 2;
                    } else if local_type == 4 {
                        locals.extend(arg.to_be_bytes().iter());
                        args_byte_number += 4;
                    }
                }
                args_cursor += local_count;
            }

            // Add the rest of the locals initialised to zero.
            locals.resize(
                locals.len() + local_type * local_count - args_byte_number,
                0,
            );
        }

        // We set PC to the next opcode to execute, which is after the format of the locals in the main memory,
        // i.e function address + 1 (to skip the function type) + length of format of locals.
        // (At this point, the stack pointer is at the end of the format of the locals in the call frame, without the padding.
        // We also remove 8 to account for FrameLen and LocalsPos.
        cpu.pc = addr + self.ptr() - self.frame_ptr - 7;

        // We push the padding for the formats of the locals, if required.
        if self.ptr() % 4 != 0 {
            self.push_u8(0x00);
            self.push_u8(0x00);
        }

        // The stack pointer is now at LocalPos, so we set the value of LocalPos left at zero in the call frame earlier.
        self.write_u32(self.frame_ptr + 4, self.ptr() - self.frame_ptr);

        // We now push the locals we stored earlier.
        for x in locals.drain(..) {
            self.push_u8(x)
        }

        // We push the padding for the locals, if required.
        self.resize(self.len() + 3 - (usize::try_from(self.ptr()).unwrap() + 3) % 4);

        // The stack pointer is now at the end of the call frame, so we set the value of FrameLen left at zero earlier.
        self.write_u32(self.frame_ptr, self.ptr() - self.frame_ptr);

        // 3. Push the arguments of the function if its type is C0.

        if fn_type == 0xC0 {
            for &arg in args.iter().rev() {
                self.push_u32(arg);
            }
            self.push_u32(args.len().try_into().unwrap());
        }

        Ok(())
    }
}

/// The error type returned by [`Stack::push_call_frame`].
#[derive(Debug)]
#[non_exhaustive]
pub(crate) struct PushCallFrameError {
    /// The kind of error that was encountered.
    kind: PushCallFrameErrorKind,
}

impl fmt::Display for PushCallFrameError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "failed to push call frame: ")?;
        match &self.kind {
            PushCallFrameErrorKind::InvalidFunctionType { ty } => {
                write!(f, "invalid function type {ty:#X}")
            }
            PushCallFrameErrorKind::MemoryRead(_) => {
                write!(f, "couldn't read main memory")
            }
        }
    }
}

impl Error for PushCallFrameError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        match &self.kind {
            PushCallFrameErrorKind::InvalidFunctionType { .. } => None,
            PushCallFrameErrorKind::MemoryRead(err) => Some(err),
        }
    }
}

/// An enum describing the kind of [`PushCallFrameError`].
#[derive(Debug)]
enum PushCallFrameErrorKind {
    /// The function was of an invalid type
    InvalidFunctionType { ty: u8 },

    /// Reading the main memory failed with the inner error.
    MemoryRead(MemoryReadError),
}

impl From<MemoryReadError> for PushCallFrameErrorKind {
    fn from(value: MemoryReadError) -> Self {
        Self::MemoryRead(value)
    }
}

impl glauque::Stack for Stack {
    fn push_u32(&mut self, value: u32) {
        self.push_u32(value);
    }

    fn push_u32s(&mut self, values: impl IntoIterator<Item = u32>) {
        for value in values.into_iter() {
            self.push_u32(value);
        }
    }
}
