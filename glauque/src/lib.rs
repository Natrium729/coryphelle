use std::{cmp::Ordering, fmt, num::NonZeroU32};

pub mod dispatch;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct GlkVersion {
    /// The major number of this version (e.g. the 0 in 0.7.5).
    pub major: u16,

    /// The minor number of this version (e.g. the 7 in 0.7.5).
    pub minor: u8,

    /// The subminor number of the version (e.g. the 5 in 0.7.5).
    pub subminor: u8, // "Subminor" is the term used in the spec.
}

impl GlkVersion {
    pub fn as_u32(&self) -> u32 {
        (u32::from(self.major) << 16) + (u32::from(self.minor) << 8) + u32::from(self.subminor)
    }
}

/// The Glk version implemented by Glauque.
const GLK_VERSION: GlkVersion = GlkVersion {
    major: 0,
    minor: 7,
    subminor: 5,
};

// TODO: have a different type for each kind of opaque object?
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct OpaqueId(NonZeroU32);

impl OpaqueId {
    pub(crate) fn new(inner: u32) -> Option<Self> {
        NonZeroU32::new(inner).map(Self)
    }

    fn as_u32(&self) -> u32 {
        self.0.get()
    }
}

/// A struct that from which `OpaqueId`s can be created.
///
/// All IDs from a same struct are guaranteed to be unique,
/// but IDs generated from different factory can be equal.
pub struct OpaqueIdFactory {
    next_raw_id: Option<NonZeroU32>,
}

impl OpaqueIdFactory {
    /// Creates a new `OpaqueIdFactory`
    ///
    /// # Examples
    ///
    /// ```
    /// use glauque::OpaqueIdFactory;
    ///
    /// let mut id_factory = OpaqueIdFactory::new();
    /// ```
    pub fn new() -> Self {
        Self {
            next_raw_id: Some(1.try_into().unwrap()),
        }
    }

    /// Gets a new `OpaqueId` from this factory.
    ///
    /// All IDs generated from the same factory are guaranteed to be unique.
    ///
    /// Returns `None` if all opaque IDs havs been exhausted for this factory.
    ///
    /// # Examples
    ///
    /// ```
    /// use glauque::OpaqueIdFactory;
    ///
    /// let mut id_factory = OpaqueIdFactory::new();
    /// let id1 = id_factory.get();
    /// let id2 = id_factory.get();
    /// assert_ne!(id1, id2);
    /// ```
    pub fn get(&mut self) -> Option<OpaqueId> {
        let next_raw_id = self.next_raw_id?;
        let id = OpaqueId(next_raw_id);
        self.next_raw_id = next_raw_id.checked_add(1);
        Some(id)
    }
}

fn opaque_option_to_u32(opaque: Option<OpaqueId>) -> u32 {
    opaque.map(|x| x.as_u32()).unwrap_or(0)
}

#[derive(Debug)]
#[non_exhaustive]
#[repr(u32)]
pub enum Gestalt {
    Version = 0,
    CharInput = 1,
    LineInput = 2,
    CharOutput = 3,
    MouseInput = 4,
    Timer = 5,
    Graphics = 6,
    DrawImage = 7,
    Sound = 8,
    SoundVolume = 9,
    SoundNotify = 10,
    Hyperlinks = 11,
    HyperlinkInput = 12,
    SoundMusic = 13,
    GraphicsTransparency = 14,
    Unicode = 15,
    UnicodeNorm = 16,
    LineInputEcho = 17,
    LineTerminators = 18,
    LineTerminatorKey = 19,
    DateTime = 20,
    Sound2 = 21,
    ResourceStream = 22,
    GraphicsCharInput = 23,
}

impl From<Gestalt> for u32 {
    fn from(value: Gestalt) -> Self {
        value as u32
    }
}

impl TryFrom<u32> for Gestalt {
    type Error = u32;

    fn try_from(value: u32) -> Result<Self, Self::Error> {
        let result = match value {
            0 => Self::Version,
            1 => Self::CharInput,
            2 => Self::LineInput,
            3 => Self::CharOutput,
            4 => Self::MouseInput,
            5 => Self::Timer,
            6 => Self::Graphics,
            7 => Self::DrawImage,
            8 => Self::Sound,
            9 => Self::SoundVolume,
            10 => Self::SoundNotify,
            11 => Self::Hyperlinks,
            12 => Self::HyperlinkInput,
            13 => Self::SoundMusic,
            14 => Self::GraphicsTransparency,
            15 => Self::Unicode,
            16 => Self::UnicodeNorm,
            17 => Self::LineInputEcho,
            18 => Self::LineTerminators,
            19 => Self::LineTerminatorKey,
            20 => Self::DateTime,
            21 => Self::Sound2,
            22 => Self::ResourceStream,
            23 => Self::GraphicsCharInput,
            n => return Err(n),
        };

        Ok(result)
    }
}

pub enum GestaltCharOutput {
    CannotPrint = 0,
    ApproxPrint = 1,
    ExactPrint = 2,
}

impl From<GestaltCharOutput> for u32 {
    fn from(value: GestaltCharOutput) -> Self {
        value as u32
    }
}

#[derive(Debug, Clone, Copy)]
pub enum Key {
    Code(u32),
    Unknown,
    Left,
    Right,
    Up,
    Down,
    Return,
    Delete,
    Escape,
    Tab,
    PageUp,
    PageDown,
    Home,
    End,
    Func1,
    Func2,
    Func3,
    Func4,
    Func5,
    Func6,
    Func7,
    Func8,
    Func9,
    Func10,
    Func11,
    Func12,
}

impl From<char> for Key {
    fn from(value: char) -> Self {
        Self::Code(value as u32)
    }
}

impl From<u32> for Key {
    fn from(value: u32) -> Self {
        match value {
            0xFFFFFFFF => Key::Unknown,
            0xFFFFFFFE => Key::Left,
            0xFFFFFFFD => Key::Right,
            0xFFFFFFFC => Key::Up,
            0xFFFFFFFB => Key::Down,
            0xFFFFFFFA => Key::Return,
            0xFFFFFFF9 => Key::Delete,
            0xFFFFFFF8 => Key::Escape,
            0xFFFFFFF7 => Key::Tab,
            0xFFFFFFF6 => Key::PageUp,
            0xFFFFFFF5 => Key::PageDown,
            0xFFFFFFF4 => Key::Home,
            0xFFFFFFF3 => Key::End,
            0xFFFFFFEF => Key::Func1,
            0xFFFFFFEE => Key::Func2,
            0xFFFFFFED => Key::Func3,
            0xFFFFFFEC => Key::Func4,
            0xFFFFFFEB => Key::Func5,
            0xFFFFFFEA => Key::Func6,
            0xFFFFFFE9 => Key::Func7,
            0xFFFFFFE8 => Key::Func8,
            0xFFFFFFE7 => Key::Func9,
            0xFFFFFFE6 => Key::Func10,
            0xFFFFFFE5 => Key::Func11,
            0xFFFFFFE4 => Key::Func12,
            // TODO: That would accept invalid Unicode code points
            // Make sure that's OK.
            code => Key::Code(code),
        }
    }
}

impl From<Key> for u32 {
    fn from(value: Key) -> Self {
        match value {
            Key::Code(code) => code,
            Key::Unknown => 0xFFFFFFFF,
            Key::Left => 0xFFFFFFFE,
            Key::Right => 0xFFFFFFFD,
            Key::Up => 0xFFFFFFFC,
            Key::Down => 0xFFFFFFFB,
            Key::Return => 0xFFFFFFFA,
            Key::Delete => 0xFFFFFFF9,
            Key::Escape => 0xFFFFFFF8,
            Key::Tab => 0xFFFFFFF7,
            Key::PageUp => 0xFFFFFFF6,
            Key::PageDown => 0xFFFFFFF5,
            Key::Home => 0xFFFFFFF4,
            Key::End => 0xFFFFFFF3,
            Key::Func1 => 0xFFFFFFEF,
            Key::Func2 => 0xFFFFFFEE,
            Key::Func3 => 0xFFFFFFED,
            Key::Func4 => 0xFFFFFFEC,
            Key::Func5 => 0xFFFFFFEB,
            Key::Func6 => 0xFFFFFFEA,
            Key::Func7 => 0xFFFFFFE9,
            Key::Func8 => 0xFFFFFFE8,
            Key::Func9 => 0xFFFFFFE7,
            Key::Func10 => 0xFFFFFFE6,
            Key::Func11 => 0xFFFFFFE5,
            Key::Func12 => 0xFFFFFFE4,
        }
    }
}

#[derive(Debug)]
pub enum EventType {
    None,
    Timer,
    CharInput(Key),
    LineInput {
        command: Vec<char>,
        terminator: Option<Key>,
    },
    MouseInput,
    Arrange,
    Redraw,
    SoundNotify,
    Hyperlink,
    VolumeNotify,
}

impl EventType {
    fn as_id(&self) -> u32 {
        match self {
            EventType::None => 0,
            EventType::Timer => 1,
            EventType::CharInput(_) => 2,
            EventType::LineInput { .. } => 3,
            EventType::MouseInput => 4,
            EventType::Arrange => 5,
            EventType::Redraw => 6,
            EventType::SoundNotify => 7,
            EventType::Hyperlink => 8,
            EventType::VolumeNotify => 9,
        }
    }
}

pub struct Event {
    pub ty: EventType,
    pub win: Option<OpaqueId>,
}

impl Event {
    pub fn as_raw_ints(&self) -> [u32; 4] {
        let (val1, val2) = match &self.ty {
            EventType::None => (0, 0), // unused, unused
            EventType::Timer => todo!(),
            EventType::CharInput(key) => ((*key).into(), 0), // key code, unused
            EventType::LineInput {
                command,
                terminator,
            } => {
                // Ideally, we should check if the conversion to a `usize` for the length
                // won't return an error, but the Glauque checked that already
                // when creating the event and truncating the command
                // to the `max_len` argument of `request_line_input`.
                //
                // TODO: Check that the above is actually true.
                //
                // In any case, it would only fail on architectures smaller than 32 bits
                // and if the length in unusually big.
                (
                    command.len().try_into().unwrap(),
                    terminator.map(|k| k.into()).unwrap_or(0),
                )
            }
            EventType::MouseInput => todo!(),
            EventType::Arrange => todo!(),
            EventType::Redraw => todo!(),
            EventType::SoundNotify => todo!(),
            EventType::Hyperlink => todo!(),
            EventType::VolumeNotify => todo!(),
        };

        [self.ty.as_id(), opaque_option_to_u32(self.win), val1, val2]
    }
}

#[repr(u32)]
pub enum WindowPosition {
    Left = 0x0,
    Right = 0x1,
    Above = 0x2,
    Below = 0x3,
}

impl TryFrom<u32> for WindowPosition {
    type Error = WindowPositionTryFromU32Error;

    fn try_from(value: u32) -> Result<Self, Self::Error> {
        let pos = match value {
            0x0 => Self::Left,
            0x1 => Self::Right,
            0x2 => Self::Above,
            0x3 => Self::Below,
            val => return Err(WindowPositionTryFromU32Error(val)),
        };

        Ok(pos)
    }
}

#[derive(Debug)]
#[non_exhaustive]
pub struct WindowPositionTryFromU32Error(u32);

impl fmt::Display for WindowPositionTryFromU32Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "0x{:X} doesn't correspond to a window position", self.0)
    }
}

impl std::error::Error for WindowPositionTryFromU32Error {}

#[repr(u32)]
pub enum WindowSplitMethod {
    Fixed = 0x10,
    Proportional = 0x20,
}

impl TryFrom<u32> for WindowSplitMethod {
    type Error = WindowSplitMethodTryFromU32Error;

    fn try_from(value: u32) -> Result<Self, Self::Error> {
        let split_method = match value {
            0x10 => WindowSplitMethod::Fixed,
            0x20 => WindowSplitMethod::Proportional,
            val => return Err(WindowSplitMethodTryFromU32Error(val)),
        };
        Ok(split_method)
    }
}

#[derive(Debug)]
#[non_exhaustive]
pub struct WindowSplitMethodTryFromU32Error(u32);

impl fmt::Display for WindowSplitMethodTryFromU32Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "0x{:X} doesn't correspond to a window split method",
            self.0
        )
    }
}

impl std::error::Error for WindowSplitMethodTryFromU32Error {}

#[repr(u32)]
pub enum WindowBorder {
    Yes = 0x000,
    No = 0x100,
}

impl TryFrom<u32> for WindowBorder {
    type Error = WindowBorderTryFromU32Error;

    fn try_from(value: u32) -> Result<Self, Self::Error> {
        let border = match value {
            0x000 => WindowBorder::Yes,
            0x100 => WindowBorder::No,
            val => return Err(WindowBorderTryFromU32Error(val)),
        };

        Ok(border)
    }
}

#[derive(Debug)]
#[non_exhaustive]
pub struct WindowBorderTryFromU32Error(u32);

impl fmt::Display for WindowBorderTryFromU32Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "0x{:X} doesn't correspond to a window border", self.0)
    }
}

impl std::error::Error for WindowBorderTryFromU32Error {}

pub struct WindowMethod {
    position: WindowPosition,
    split_method: WindowSplitMethod,
    border: WindowBorder,
}

impl TryFrom<u32> for WindowMethod {
    type Error = WindowMethodTryFromU32Error;

    fn try_from(value: u32) -> Result<Self, Self::Error> {
        let position = (value & 0xF)
            .try_into()
            .map_err(
                |e: WindowPositionTryFromU32Error| WindowMethodTryFromU32Error {
                    value: value & 0xF,
                    kind: e.into(),
                },
            )?;

        let split_method =
            (value & 0xF0)
                .try_into()
                .map_err(
                    |e: WindowSplitMethodTryFromU32Error| WindowMethodTryFromU32Error {
                        value: value & 0xF0,
                        kind: e.into(),
                    },
                )?;

        let border = (value & 0xF00)
            .try_into()
            .map_err(
                |e: WindowBorderTryFromU32Error| WindowMethodTryFromU32Error {
                    value: value & 0xF00,
                    kind: e.into(),
                },
            )?;

        Ok(WindowMethod {
            position,
            split_method,
            border,
        })
    }
}

#[derive(Debug)]
#[non_exhaustive]
pub struct WindowMethodTryFromU32Error {
    value: u32,
    kind: WindowMethodTryFromU32ErrorKind,
}

impl fmt::Display for WindowMethodTryFromU32Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "0x{:X} doesn't correspond to a window method",
            self.value
        )
    }
}

impl std::error::Error for WindowMethodTryFromU32Error {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        match &self.kind {
            WindowMethodTryFromU32ErrorKind::WindowBorder(e) => Some(e),
            WindowMethodTryFromU32ErrorKind::WindowPosition(e) => Some(e),
            WindowMethodTryFromU32ErrorKind::WindowSplitMethod(e) => Some(e),
        }
    }
}

// TODO: See if we should put this enum in a `window` module
// to remove the "Window" prefix.
#[allow(clippy::enum_variant_names)]
#[derive(Debug)]
enum WindowMethodTryFromU32ErrorKind {
    #[non_exhaustive]
    WindowPosition(WindowPositionTryFromU32Error),
    #[non_exhaustive]
    WindowSplitMethod(WindowSplitMethodTryFromU32Error),
    #[non_exhaustive]
    WindowBorder(WindowBorderTryFromU32Error),
}

impl From<WindowPositionTryFromU32Error> for WindowMethodTryFromU32ErrorKind {
    fn from(value: WindowPositionTryFromU32Error) -> Self {
        Self::WindowPosition(value)
    }
}

impl From<WindowSplitMethodTryFromU32Error> for WindowMethodTryFromU32ErrorKind {
    fn from(value: WindowSplitMethodTryFromU32Error) -> Self {
        Self::WindowSplitMethod(value)
    }
}

impl From<WindowBorderTryFromU32Error> for WindowMethodTryFromU32ErrorKind {
    fn from(value: WindowBorderTryFromU32Error) -> Self {
        Self::WindowBorder(value)
    }
}

impl From<WindowMethod> for u32 {
    fn from(value: WindowMethod) -> Self {
        let pos_byte = match value.position {
            WindowPosition::Left => 0x0,
            WindowPosition::Right => 0x1,
            WindowPosition::Above => 0x2,
            WindowPosition::Below => 0x3,
        };

        let split_byte = match value.split_method {
            WindowSplitMethod::Fixed => 0x10,
            WindowSplitMethod::Proportional => 0x20,
        };

        let border_byte = match value.border {
            WindowBorder::Yes => 0x000,
            WindowBorder::No => 0x100,
        };

        pos_byte | split_byte | border_byte
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[repr(u32)]
pub enum WindowType {
    AllTypes = 0,
    Pair = 1,
    Blank = 2,
    TextBuffer = 3,
    TextGrid = 4,
    Graphics = 5,
}

impl TryFrom<u32> for WindowType {
    type Error = WindowTypeTryFromU32Error;

    fn try_from(value: u32) -> Result<Self, Self::Error> {
        match value {
            0 => Ok(WindowType::AllTypes),
            1 => Ok(WindowType::Pair),
            2 => Ok(WindowType::Blank),
            3 => Ok(WindowType::TextBuffer),
            4 => Ok(WindowType::TextGrid),
            5 => Ok(WindowType::Graphics),
            val => Err(WindowTypeTryFromU32Error(val)),
        }
    }
}

#[derive(Debug)]
#[non_exhaustive]
pub struct WindowTypeTryFromU32Error(u32);

impl fmt::Display for WindowTypeTryFromU32Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "0x{:X} doesn't correspond to a file mode", self.0)
    }
}

impl std::error::Error for WindowTypeTryFromU32Error {}

impl From<WindowType> for u32 {
    fn from(value: WindowType) -> Self {
        value as u32
    }
}

pub enum Style {
    Normal = 0,
    Emphasized = 1,
    Preformatted = 2,
    Header = 3,
    Subheader = 4,
    Alert = 5,
    Note = 6,
    BlockQuote = 7,
    Input = 8,
    User1 = 9,
    User2 = 10,
}

impl From<u32> for Style {
    fn from(value: u32) -> Self {
        match value {
            0 => Style::Normal,
            1 => Style::Emphasized,
            2 => Style::Preformatted,
            3 => Style::Header,
            4 => Style::Subheader,
            5 => Style::Alert,
            6 => Style::Note,
            7 => Style::BlockQuote,
            8 => Style::Input,
            9 => Style::User1,
            10 => Style::User2,
            _ => Style::Normal, // Default to normal.
        }
    }
}

pub enum Stylehint {
    Indentation(i32),
    ParaIndentation(i32),
    Justification(TextAlignement),
    Size(i32),
    Weight(FontWeight),
    Oblique(bool),
    Proportional(bool),
    TextColor(Color),
    BackColor(Color),
    ReverseColor(bool),
}

impl TryFrom<(u32, u32)> for Stylehint {
    type Error = (); // TODO: Choose more appropriate type.

    fn try_from((hint, val): (u32, u32)) -> Result<Self, Self::Error> {
        match hint {
            0 => Ok(Stylehint::Indentation(val as i32)),
            1 => Ok(Stylehint::ParaIndentation(val as i32)),
            2 => Ok(Stylehint::Justification(val.try_into()?)),
            3 => Ok(Stylehint::Size(val as i32)),
            4 => Ok(Stylehint::Weight(match val.cmp(&0) {
                Ordering::Less => FontWeight::LightWeight,
                Ordering::Equal => FontWeight::Normal,
                Ordering::Greater => FontWeight::HeavyWeight,
            })),
            5 => Ok(Stylehint::Oblique(val != 0)),
            6 => Ok(Stylehint::Proportional(val != 0)),
            7 => Ok(Stylehint::TextColor(val.into())),
            8 => Ok(Stylehint::BackColor(val.into())),
            9 => Ok(Stylehint::ReverseColor(val != 0)),
            _ => Err(()),
        }
    }
}

#[derive(Debug, Clone, Copy)]
pub enum TextAlignement {
    LeftFlush = 0,
    LeftRight = 1,
    Centered = 2,
    RightFlush = 3,
}

impl TryFrom<u32> for TextAlignement {
    type Error = (); // TODO: Choose more appropriate type.

    fn try_from(value: u32) -> Result<Self, Self::Error> {
        match value {
            0 => Ok(TextAlignement::LeftFlush),
            1 => Ok(TextAlignement::LeftRight),
            2 => Ok(TextAlignement::Centered),
            3 => Ok(TextAlignement::RightFlush),
            _ => Err(()),
        }
    }
}

#[derive(Debug, Clone, Copy)]
pub enum FontWeight {
    LightWeight = -1,
    Normal = 0,
    HeavyWeight = 1,
}

#[derive(Debug, Clone, Copy)]
pub struct Color {
    pub r: u8,
    pub g: u8,
    pub b: u8,
}

impl From<u32> for Color {
    fn from(value: u32) -> Self {
        // We simply ignore the most significant byte.
        Color {
            r: (value >> 16) as u8,
            g: (value >> 8) as u8,
            b: value as u8,
        }
    }
}

#[repr(u32)]
pub enum FileMode {
    Write = 0x01,
    Read = 0x02,
    ReadWrite = 0x03,
    WriteAppend = 0x05,
}

impl TryFrom<u32> for FileMode {
    type Error = FileModeTryFromU32Error;

    fn try_from(value: u32) -> Result<Self, Self::Error> {
        match value {
            0x01 => Ok(FileMode::Write),
            0x02 => Ok(FileMode::Read),
            0x03 => Ok(FileMode::ReadWrite),
            0x05 => Ok(FileMode::WriteAppend),
            val => Err(FileModeTryFromU32Error(val)),
        }
    }
}

#[derive(Debug)]
#[non_exhaustive]
pub struct FileModeTryFromU32Error(u32);

impl fmt::Display for FileModeTryFromU32Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "0x{:X} doesn't correspond to a file mode", self.0)
    }
}

impl std::error::Error for FileModeTryFromU32Error {}

impl From<FileMode> for u32 {
    fn from(value: FileMode) -> Self {
        value as u32
    }
}

#[derive(Debug)]
pub struct StreamResult {
    pub read_count: u32,
    pub write_count: u32,
}

#[derive(Debug)]
pub struct FileUsage {
    pub ty: FileUsageType,
    pub mode: FileUsageMode,
}

impl TryFrom<u32> for FileUsage {
    type Error = FileUsageTryFromU32Error;

    fn try_from(value: u32) -> Result<Self, Self::Error> {
        let ty = (value & 0x0F)
            .try_into()
            .map_err(|e: FileUsageTypeTryFromU32Error| FileUsageTryFromU32Error {
                value,
                kind: e.into(),
            })?;

        let mode = (value & 0xF00)
            .try_into()
            .map_err(|e: FileUsageModeTryFromU32Error| FileUsageTryFromU32Error {
                value,
                kind: e.into(),
            })?;

        Ok(Self { ty, mode })
    }
}

#[derive(Debug)]
#[non_exhaustive]
pub struct FileUsageTryFromU32Error {
    value: u32,
    kind: FileUsageTryFromU32ErrorKind,
}

impl fmt::Display for FileUsageTryFromU32Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{:#X} doesn't correspond to a file usage", self.value)
    }
}

impl std::error::Error for FileUsageTryFromU32Error {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        match &self.kind {
            FileUsageTryFromU32ErrorKind::Type(e) => Some(e),
            FileUsageTryFromU32ErrorKind::Mode(e) => Some(e),
        }
    }
}

#[derive(Debug)]
pub enum FileUsageTryFromU32ErrorKind {
    #[non_exhaustive]
    Type(FileUsageTypeTryFromU32Error),
    #[non_exhaustive]
    Mode(FileUsageModeTryFromU32Error),
}

impl From<FileUsageTypeTryFromU32Error> for FileUsageTryFromU32ErrorKind {
    fn from(value: FileUsageTypeTryFromU32Error) -> Self {
        Self::Type(value)
    }
}

impl From<FileUsageModeTryFromU32Error> for FileUsageTryFromU32ErrorKind {
    fn from(value: FileUsageModeTryFromU32Error) -> Self {
        Self::Mode(value)
    }
}

impl From<FileUsage> for u32 {
    fn from(value: FileUsage) -> Self {
        value.ty as u32 | value.mode as u32
    }
}

#[derive(Debug, Clone, Copy)]
pub enum FileUsageType {
    Data = 0x00,
    SavedGame = 0x01,
    Transcript = 0x02,
    InputRecord = 0x03,
}

impl TryFrom<u32> for FileUsageType {
    type Error = FileUsageTypeTryFromU32Error;

    fn try_from(value: u32) -> Result<Self, Self::Error> {
        match value {
            0x00 => Ok(Self::Data),
            0x01 => Ok(Self::SavedGame),
            0x02 => Ok(Self::Transcript),
            0x03 => Ok(Self::InputRecord),
            val => Err(FileUsageTypeTryFromU32Error(val)),
        }
    }
}

#[derive(Debug)]
#[non_exhaustive]
pub struct FileUsageTypeTryFromU32Error(u32);

impl fmt::Display for FileUsageTypeTryFromU32Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{:#X} doesn't correspond to a file usage type", self.0)
    }
}

impl std::error::Error for FileUsageTypeTryFromU32Error {}

impl From<FileUsageType> for u32 {
    fn from(value: FileUsageType) -> Self {
        value as u32
    }
}

#[derive(Debug, Clone, Copy)]
pub enum FileUsageMode {
    Text = 0x100,
    Binary = 0x000,
}

impl TryFrom<u32> for FileUsageMode {
    type Error = FileUsageModeTryFromU32Error;

    fn try_from(value: u32) -> Result<Self, Self::Error> {
        match value {
            0x100 => Ok(Self::Text),
            0x000 => Ok(Self::Binary),
            val => Err(FileUsageModeTryFromU32Error(val)),
        }
    }
}

#[derive(Debug)]
#[non_exhaustive]
pub struct FileUsageModeTryFromU32Error(u32);

impl fmt::Display for FileUsageModeTryFromU32Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{:#X} doesn't correspond to a file usage mode", self.0)
    }
}

impl std::error::Error for FileUsageModeTryFromU32Error {}

impl From<FileUsageMode> for u32 {
    fn from(value: FileUsageMode) -> Self {
        value as u32
    }
}

/// This enum is used to indicate how the read/write mark position of a stream should be counted when set.
#[derive(Debug, PartialEq, Eq)]
pub enum SeekMode {
    /// The position is the set to a number of characters after the beginning of the file.
    Start = 0,

    /// The position is the set to a number of characters after the current position
    /// (moving backwards if the number is negative).
    Current = 1,

    /// The position is the set to a number of characters after the end of the file.
    /// (this number should always be zero or negative,
    /// so that this will move backwards to a position within the file.)
    End = 2,
}

impl TryFrom<u32> for SeekMode {
    type Error = SeekModeTryFromU32Error;

    fn try_from(value: u32) -> Result<Self, Self::Error> {
        match value {
            0 => Ok(Self::Start),
            1 => Ok(Self::Current),
            2 => Ok(Self::End),
            n => Err(SeekModeTryFromU32Error(n)),
        }
    }
}

#[derive(Debug)]
pub struct SeekModeTryFromU32Error(u32);

impl fmt::Display for SeekModeTryFromU32Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{:#X} doesn't correspond to a seek mode", self.0)
    }
}

pub struct Timeval {
    pub high_sec: i32,
    pub low_sec: u32,
    pub microsec: i32,
}

pub struct Date {
    pub year: i32,
    pub month: i32,
    pub day: i32,
    pub weekday: i32,
    pub hour: i32,
    pub minute: i32,
    pub second: i32,
    pub microsec: i32,
}

pub trait Memory {
    fn read_u8s(&self, addr: u32, len: u32) -> &[u8] {
        let len: usize = len.try_into().unwrap();
        &self.read_u8s_to_end(addr)[..len]
    }

    fn read_u8s_to_end(&self, addr: u32) -> &[u8];

    fn write_u8(&mut self, addr: u32, value: u8) {
        self.write_u8s(addr, [value]);
    }

    fn write_u8s(&mut self, addr: u32, values: impl IntoIterator<Item = u8>);

    fn read_u32(&self, addr: u32) -> u32;

    // We're obliged to return a Vec here
    // because `Self` is usually represented by a byte array
    // so we can't directly borrow a slice of u32 from it.
    //
    // TODO: I don't like that because it breaks the abstraction:
    // we assume the way the memory is represented.
    // Find a better way of doing.
    fn read_u32s(&self, addr: u32, len: u32) -> Vec<u32>;

    // TODO: Add default implementation that simply uses `write_u32s`.
    fn write_u32(&mut self, addr: u32, value: u32);

    fn write_u32s(&mut self, addr: u32, values: impl IntoIterator<Item = u32>);

    // TODO: Right now the implementers has to not forget to use it
    // in the Glk functions that retain/release memory
    //
    // We should find a way to call it here
    // and not put the responsability on the implementer.
    //
    // TestGlauque don’t call it yet at the time of writing.

    fn retain_memory(&mut self, start: u32, len: u32);

    fn release_memory(&mut self, start: u32, len: u32);
}

pub trait Stack {
    fn push_u32(&mut self, value: u32);
    fn push_u32s(&mut self, values: impl IntoIterator<Item = u32>);
}

// TODO: move all function that have a default implementation
// that doesn't need to be overwritten in a separate (maybe private) trait?
pub trait Glauque {
    type Output;

    // TODO: I don't really like this way of retrieving the output.
    // Maybe make `select` (and `select_poll`) return the output?
    /// Gets what Glauque has output up to the last call to `Glauque::{select, select_poll}`.
    ///
    /// Should only be called just after having called `select` or `select_poll`.
    ///
    /// Calling this function several time between a call to `select`/`select_poll` and `accept` might not give the same result.
    /// Implementations are allowed to clear the accumulated output when calling this function.
    fn output(&mut self) -> Self::Output;

    /// Displays a warning message.
    ///
    /// Should be used only for non-fatal issues.
    fn warn(&mut self, msg: String);

    /// Displays an error message.
    fn error(&mut self, msg: String);

    // 0x0001
    /// Makes whatever cleanup or other actions necessary when the Glauque app ends.
    fn exit(&mut self);

    // 0x0002
    // I don't think that one is needed,
    // since Glauque doesn't take full control of the process.
    // fn set_interrupt_handler

    // 0x0003
    /// Makes some quick maintenance task or gives time to other processes.
    ///
    /// The default implementation does nothing,
    /// which is almost always all that is required.
    fn tick(&mut self) {
        // There's usually nothing to do in tick.
    }

    // 0x0004
    /// Queries some capability or info on this Glauque implementation.
    ///
    /// The capability is specified by `sel` and sometimes requires a value `val`.
    /// Set the latter to 0 otherwise.
    ///
    /// # Notes to Implementors
    ///
    /// You should never implement it.
    /// The default implementation simply delegates to the `gestalt_*` functions.
    /// Those are the ones you should provide.
    ///
    /// This function might later be moved to a separate trait with a blanket implementation.
    fn gestalt(&self, sel: Gestalt, val: u32) -> u32 {
        self.gestalt_ext(sel, val, &mut [])
    }

    // 0x0005
    /// Queries some capability or info on this Glauque implementation.
    ///
    /// The capability is specified by `sel` and sometimes requires a value `val`.
    /// Set the latter to 0 otherwise.
    /// Some selectors will read or write values from the slice.
    /// If these values are not needed, an empty slice can be used.
    ///
    /// # Notes to Implementors
    ///
    /// You should never implement it.
    /// The default implementation simply delegates to the `gestalt_*` functions.
    /// Those are the ones you should provide.
    ///
    /// This function might later be moved to a separate trait with a blanket implementation.
    fn gestalt_ext(&self, sel: Gestalt, val: u32, slice: &mut [u32]) -> u32 {
        use Gestalt as G;
        match sel {
            G::Version => self.gestalt_version().as_u32(),
            G::CharInput => self.gestalt_char_input(val.into()) as u32,
            G::LineInput => self.gestalt_line_input(val.into()) as u32,
            G::CharOutput => {
                let (result, len) = self.gestalt_char_output(val);
                if slice.len() >= 1 {
                    slice[0] = len;
                }
                result.into()
            }
            G::MouseInput => {
                if let Ok(window_type) = val.try_into() {
                    self.gestalt_mouse_input(window_type) as u32
                } else {
                    0
                }
            }
            G::Timer => self.gestalt_timer() as u32,
            G::Graphics => self.gestalt_graphics() as u32,
            G::DrawImage => {
                if let Ok(window_type) = val.try_into() {
                    self.gestalt_draw_image(window_type) as u32
                } else {
                    0
                }
            }
            G::Sound => self.gestalt_sound() as u32,
            G::SoundVolume => self.gestalt_sound_volume() as u32,
            G::SoundNotify => self.gestalt_sound_notify() as u32,
            G::Hyperlinks => self.gestalt_hyperlinks() as u32,
            G::HyperlinkInput => {
                if let Ok(window_type) = val.try_into() {
                    self.gestalt_hyperlink_input(window_type) as u32
                } else {
                    0
                }
            }
            G::SoundMusic => self.gestalt_sound_music() as u32,
            G::GraphicsTransparency => self.gestalt_graphics_transparency() as u32,
            G::Unicode => self.gestalt_unicode() as u32,
            G::UnicodeNorm => todo!("gestalt unicode_norm"),
            G::LineInputEcho => self.gestalt_line_input_echo() as u32,
            G::LineTerminators => self.gestalt_line_terminators() as u32,
            G::LineTerminatorKey => self.gestalt_line_terminator_key(val.into()) as u32,
            G::DateTime => todo!("gestalt date_time"),
            G::Sound2 => self.gestalt_sound2() as u32,
            G::ResourceStream => self.gestalt_resource_stream() as u32,
            G::GraphicsCharInput => self.gestalt_graphics_char_input() as u32,
        }
    }

    /// Returns the Glk version currently implemented by Glauque.
    ///
    /// # Notes to Implementors
    ///
    /// You should never override the default implementation.
    ///
    /// This function might later be moved to a separate trait with a blanket implementation.
    fn gestalt_version(&self) -> GlkVersion {
        GLK_VERSION
    }

    /// Returns true if the given key can be entered as a character input.
    fn gestalt_char_input(&self, ch: Key) -> bool;

    /// Returns true if the given key can be typed in a line input.
    fn gestalt_line_input(&self, ch: Key) -> bool {
        match ch {
            // Non-printable Latin-1 characters
            Key::Code(0..=31 | 127..=159) => false,
            // Other characterss
            // TODO: that covers all Unicode, check if that's correct.
            Key::Code(_) => true,
            // All the special characters are untypable.
            Key::Unknown => false,
            Key::Left => false,
            Key::Right => false,
            Key::Up => false,
            Key::Down => false,
            Key::Return => false,
            Key::Delete => false,
            Key::Escape => false,
            Key::Tab => false,
            Key::PageUp => false,
            Key::PageDown => false,
            Key::Home => false,
            Key::End => false,
            Key::Func1 => false,
            Key::Func2 => false,
            Key::Func3 => false,
            Key::Func4 => false,
            Key::Func5 => false,
            Key::Func6 => false,
            Key::Func7 => false,
            Key::Func8 => false,
            Key::Func9 => false,
            Key::Func10 => false,
            Key::Func11 => false,
            Key::Func12 => false,
        }
    }

    /// Determines if a character can be printed accurately.
    ///
    /// The first element of the returned tuple indicates how the character will be printed.
    ///
    /// The second element is the length of what would actually be printed.
    fn gestalt_char_output(&self, ch: u32) -> (GestaltCharOutput, u32);

    /// Returns true if mouse input is supported in the given window type.
    fn gestalt_mouse_input(&self, window_type: WindowType) -> bool;

    /// Returns true if timer events are supported.
    fn gestalt_timer(&self) -> bool;

    /// Returns true if the graphics functions are available.
    ///
    /// They include:
    ///
    /// - `image_draw`
    /// - `image_draw_scaled`
    /// - `image_get_info`
    /// - `window_erase_rect`
    /// - `window_fill_rect`
    /// - `window_set_background_color`
    /// - `glk_window_flow_break`
    ///
    /// It also includes the capability to create graphics windows.
    ///
    /// If the function returns false. These functions may have no effect, or they may cause a run-time error. Trying to create a graphics window will give `None`.
    fn gestalt_graphics(&self) -> bool;

    /// Returns true if images can be drawn in windows of the given type.
    ///
    /// If this function returns false, `image_draw` and `image_draw_scaled` will always return false.
    fn gestalt_draw_image(&self, window_type: WindowType) -> bool;

    /// Returns true if the Glk pre-0.7.3 sound functions are available.
    ///
    /// They include:
    ///
    /// - `schannel_create`
    /// - `schannel_destroy`
    /// - `schannel_iterate`
    /// - `schannel_get_rock`
    /// - `schannel_play`
    /// - `schannel_play_ext`
    /// - `schannel_stop`
    /// - `schannel_set_volume`
    /// - `sound_load_hint`
    ///
    /// If this function returns false, the functions above should not be called.
    /// They may have no effect, or they may cause a run-time error.
    ///
    /// This function is guaranteed to return true if `gestalt_sound2` does.
    fn gestalt_sound(&self) -> bool;

    /// Returns true if `schannel_set_volume` works.
    ///
    /// If this function returns false, `schannel_set_volume` has no effect.
    fn gestalt_sound_volume(&self) -> bool;

    /// Returns true if sound notification events are supported.
    ///
    /// This function is guaranteed to return true if `gestalt_sound2` does.
    fn gestalt_sound_notify(&self) -> bool;

    /// Returns true if the hyperlinks functions are available.
    ///
    /// They include:
    ///
    /// - `set_hyperlink`
    /// - `set_hyperlink_stream`
    /// - `request_hyperlink_event`
    /// - `cancel_hyperlink_event`
    ///
    /// If this function returns false, the functions above should not be called.
    /// They may have no effect, or they may cause a run-time error.
    fn gestalt_hyperlinks(&self) -> bool;

    /// Return true if windows of the given type support hyperlinks.
    ///
    /// If this function returns false,
    /// it is still legal to call `set_hyperlink` and `request_hyperlink_event`,
    /// but they will have no effect, and you will never get hyperlink events.
    fn gestalt_hyperlink_input(&self, window_type: WindowType) -> bool;

    /// Returns true if music sound resources are supported.
    ///
    /// If false is returned, only sampled sounds can be played.
    fn gestalt_sound_music(&self) -> bool;

    /// Returns true if images with an alpha channel can be drawn with the appropriate transparency.
    ///
    /// If false is returned, the alpha channel is ignored;
    /// Fully transparent areas will be drawn in an implementation-defined color.
    fn gestalt_graphics_transparency(&self) -> bool;

    // TODO: Maybe remove the default implementation.
    /// Returns true if the core Unicode functions are available.
    ///
    /// If false is returned, they should not be called.
    /// They may print nothing, print gibberish, or cause a run-time error.
    fn gestalt_unicode(&self) -> bool {
        true // Unicode is supported by default.
    }

    /// Returns true if `set_echo_line_event is supported.
    fn gestalt_line_input_echo(&self) -> bool;

    /// Returns true if `set_terminators_line_event` is supported.
    fn gestalt_line_terminators(&self) -> bool;

    /// Returns true if `ch` can be passed to `set_terminators_line_event`.
    ///
    /// If false is returned, that keycode will be ignored as a line terminator.
    /// Printable characters and `Key::Return` will always return false.
    fn gestalt_line_terminator_key(&self, ch: Key) -> bool;

    /// Returns true if the sound functions are available.
    ///
    /// If false is returned, these functions should not be called.
    /// They may have no effect, or they may cause a run-time error.
    fn gestalt_sound2(&self) -> bool;

    /// Returns true if `stream_open_resource` and `stream_open_resource_uni` are available.
    fn gestalt_resource_stream(&self) -> bool;

    /// Returns true if graphics windows can accept character input requests.
    fn gestalt_graphics_char_input(&self) -> bool;

    // 0x0020
    /// Returns the opaque ID and rock of the window after the one represented by the given opaque ID.
    ///
    /// If `None` is given, the opaque ID and rock of the first window is returned.
    ///
    /// If `None` is returned, the given opaque ID represented the last window.
    fn window_iterate(&self, opaque: Option<OpaqueId>) -> Option<(OpaqueId, u32)>;

    // 0x0021
    /// Returns the rock value of the window with the given opaque ID.
    fn window_get_rock(&self, win: OpaqueId) -> u32;

    // 0x0022
    /// Returns the ID of the root window.
    ///
    /// If there are no windows, `Nore` is returned.
    fn window_get_root(&self) -> Option<OpaqueId>;

    // 0x0023
    /// Open a new window and returns its ID.
    ///
    /// Returns `None` if the window could not be created.
    ///
    /// If there are no windows, the first three arguments are meaningless.
    /// The `split` argument must be `None`, and method and size are ignored.
    ///
    /// If any windows exist, new windows must be created by splitting existing ones.
    /// The `split` argument is the window you want to split; this must not be `None`.
    fn window_open(
        &mut self,
        split: Option<OpaqueId>,
        method: WindowMethod,
        size: u32,
        ty: WindowType,
        rock: u32,
    ) -> Option<OpaqueId>;

    // 0x0024
    /// Closes the window with the given ID.
    ///
    /// The returned value contains the character count of the window stream.
    fn window_close(&mut self, win: OpaqueId) -> StreamResult;

    // 0x0025
    /// Returns the actual width and height of the window, in its measurement system.
    fn window_get_size(&self, win: OpaqueId) -> (u32, u32);

    // 0x0026
    /// Changes the size of an existing split.
    fn window_set_arrangement(
        &mut self,
        win: OpaqueId,
        method: WindowMethod,
        size: u32,
        key: Option<OpaqueId>,
    );

    // 0x0027
    /// Returns the constraint of the pair window with the given ID.
    ///
    /// The result is a `(WindowMethod, u32, Option<OpaqueId>)`,
    /// where the `u32` the window size and the opaque ID the ID of the key window.
    fn window_get_arrangement(&self, win: OpaqueId) -> (WindowMethod, u32, Option<OpaqueId>);

    // 0x0028
    /// Returns the type of the window withe the given ID.
    fn window_get_type(&self, win: OpaqueId) -> WindowType;

    // 0x0029
    /// Returns the ID of the window which is the parent of the window.
    ///
    /// If the given window is the root window, this function returns `None`,
    /// since the root window has no parent.
    ///
    /// The parent of every window is a pair window.
    fn window_get_parent(&self, win: OpaqueId) -> Option<OpaqueId>;

    // 0x002A
    /// Erases the given window.
    ///
    /// The meaning of this depends on the window type.
    fn window_clear(&mut self, win: OpaqueId);

    // 0x002B
    /// Moves the cursor of the given grid window.
    fn window_move_cursor(&mut self, win: OpaqueId, x: u32, y: u32);

    // 0x002C
    /// Returns the ID of the stream which is associated with the given window.
    fn window_get_stream(&self, win: OpaqueId) -> OpaqueId;

    // 0x002D
    /// Sets the echo stream of the window with the given ID to the stream with the other ID.
    /// The stream has to be a valid output stream.
    ///
    /// To make a window stop echoing, this function can be called with a `stream` of `None`.
    fn window_set_echo_stream(&mut self, win: OpaqueId, stream: Option<OpaqueId>);

    // 0x002E
    /// Returns the ID of the current echo stream of the window with the given ID.
    ///
    /// If the window is not echoing, `None` is returned.
    fn window_get_echo_stream(&self, win: OpaqueId) -> Option<OpaqueId>;

    // 0x002F
    /// Sets the current stream to the stream of the window with the given ID.
    fn set_window(&mut self, win: Option<OpaqueId>) {
        let stream = win.map(|win| self.window_get_stream(win));
        self.stream_set_current(stream);
    }

    // 0x0030
    /// Returns the other child of the parent of the window with the given ID.
    ///
    /// If the window is the root window, `None` is returned.
    fn window_get_sibling(&self, win: OpaqueId) -> Option<OpaqueId>;

    //0x0040
    /// Returns the opaque ID and rock of the stream after the one represented by the given opaque ID.
    ///
    /// If `None` is given, the opaque ID and rock of the first stream is returned.
    ///
    /// If `None` is returned, the given opaque ID represented the last stream.
    fn stream_iterate(&self, opaque: Option<OpaqueId>) -> Option<(OpaqueId, u32)>;

    // 0x0041
    /// Returns the rock value of the stream with the given ID.
    ///
    /// Window streams always have a rock value of 0.
    fn stream_get_rock(&self, stream: OpaqueId) -> u32;

    // 0x0042
    /// Opens a stream which reads from or writes to a disk file and returns its ID.
    fn stream_open_file(
        &mut self,
        fileref: OpaqueId,
        mode: FileMode,
        rock: u32,
    ) -> Option<OpaqueId>;

    // 0x0043
    // TODO: This should have a `Memory` argument so that we can retain it.
    /// Opens a stream which reads from or writes into a space in memory and returns its ID.
    ///
    /// # Notes to Implementors
    ///
    /// This function must call `retain_memory` on the `Memory`.
    fn stream_open_memory(
        &mut self,
        mem: &impl Memory,
        start: u32,
        len: u32,
        mode: FileMode,
        rock: u32,
    ) -> OpaqueId;

    // 0x0044
    /// Closes the stream with the given ID.
    ///
    ///
    /// If the closed stream is the current output stream,
    /// the current output stream is set to NULL.
    ///
    /// window streams cannot be closed; use `window_close` instead.
    ///
    /// # Notes to Implementors
    ///
    /// This function must call `release_memory` on the `Memory`.
    fn stream_close(&mut self, mem: &mut impl Memory, stream: OpaqueId) -> StreamResult;

    // 0x0045
    /// Sets the position of the read/write mark of the stream with the given ID.
    fn stream_set_position(&mut self, stream: OpaqueId, pos: i32, seek_mode: SeekMode);

    // 0x0046
    /// Returns the position of the read/write mark of the stream with the given ID.
    fn stream_get_position(&self, stream: OpaqueId) -> u32;

    // 0x0047
    // TODO: Choose a better error type.
    /// Sets the current stream to the one with the given ID.
    ///
    /// It must be an output stream.
    ///
    /// If `None` is given, then the current stream is not set to anything.
    fn stream_set_current(&mut self, stream: Option<OpaqueId>);

    // 0x0048
    /// Returns the ID of the current stream, or `None` if there is none.
    fn stream_get_current(&self) -> Option<OpaqueId>;

    // 0x0049
    /// Opens the given data resource for reading and returns its ID.
    ///
    /// Returns `None` if the resource chunk with the given number does not exist.
    fn stream_open_resource(&mut self, file_num: u32, rock: u32) -> Option<OpaqueId>;

    // 0x0060
    /// Creates a reference to a temporary file and returns its ID if the operation succeeded.
    fn fileref_create_temp(&mut self, usage: FileUsage, rock: u32) -> Option<OpaqueId>;

    // 0x0061
    /// Creates a reference to a file with a specific name and returns its ID if the operation succeeded.
    fn fileref_create_by_name(
        &mut self,
        usage: FileUsage,
        name: &[u8],
        rock: u32,
    ) -> Option<OpaqueId>;

    // 0x0062
    /// Creates a reference to a file by asking the player to locate it and returns its ID if the operation succeeded.
    fn fileref_create_by_prompt(
        &mut self,
        usage: FileUsage,
        mode: FileMode,
        rock: u32,
    ) -> Option<OpaqueId>;

    // 0x0063
    /// Destroys the fileref with the given ID.
    ///
    /// This does not affect the disk file;
    /// it just reclaims the resources allocated by the `fileref_create_*` functions.
    ///
    /// It is legal to destroy a fileref after opening a file with it
    /// (while the file is still open).
    fn fileref_destroy(&mut self, fileref: OpaqueId);

    // 0x0064
    /// Returns the opaque ID and rock of the fileref after the one represented by the given opaque ID.
    ///
    /// If `None` is given, the opaque ID and rock of the first fileref is returned.
    ///
    /// If `None` is returned, the given opaque ID represented the last fileref.
    fn fileref_iterate(&self, opaque: Option<OpaqueId>) -> Option<(OpaqueId, u32)>;

    // 0x0065
    /// Returns the rock value of the fileref with the given ID.
    fn fileref_get_rock(&self, fileref: OpaqueId) -> u32;

    // 0x0066
    /// Deletes the file referred to by the fileref with the given ID.
    ///
    /// It does not destroy the fileref itself.
    ///
    /// This function should only be called with a fileref that refers to an existing file.
    fn fileref_delete_file(&self, fileref: OpaqueId);

    // 0x0067
    /// Returns true if the fileref with the given ID refers to an existing file, false otherwise.
    fn fileref_does_file_exist(&self, fileref: OpaqueId) -> bool;

    // 0x0068
    /// Copies an existing file reference, but changes the usage.
    ///
    /// The original fileref is not modified.
    fn fileref_create_from_fileref(
        &mut self,
        usage: FileUsage,
        fileref: OpaqueId,
        rock: u32,
    ) -> OpaqueId;

    // 0x0080
    /// Prints one character to the current stream.
    ///
    /// The character is assumed to be a Latin-1 code point.
    fn put_char(&mut self, mem: &mut impl Memory, ch: u8) {
        let stream = self
            .stream_get_current()
            .expect("TODO: handle no current stream");
        self.put_char_stream(mem, stream, ch);
    }

    // 0x0081
    /// Prints one character to the stream with the given Opaque ID.
    ///
    /// The character is assumed to be a Latin-1 code point.
    fn put_char_stream(&mut self, mem: &mut impl Memory, stream: OpaqueId, ch: u8) {
        self.put_slice_stream(mem, stream, &[ch]);
    }

    // 0x0082, 0x0084
    /// Prints a slice of characters to the current stream.
    ///
    /// The characters are assumed to be Latin-1 code points.
    fn put_slice(&mut self, mem: &mut impl Memory, bytes: &[u8]) {
        let stream = self
            .stream_get_current()
            .expect("TODO: handle no current stream");
        self.put_slice_stream(mem, stream, bytes);
    }

    // 0x0080, 0x0084
    /// Prints the characters from the given iterator to the current stream.
    ///
    /// The characters are assumed to be Latin-1 code points.
    fn put_chars(&mut self, mem: &mut impl Memory, bytes: impl IntoIterator<Item = u8>) {
        let stream = self
            .stream_get_current()
            .expect("TODO: handle no current stream");
        self.put_chars_stream(mem, stream, bytes);
    }

    // 0x0083, 0x0085
    /// Prints a slice of characters to the stream with the given ID.
    ///
    /// The characters are assumed to be Latin-1 code points.
    fn put_slice_stream(&mut self, mem: &mut impl Memory, stream: OpaqueId, bytes: &[u8]);

    // 0x0083, 0x0085
    /// Prints the characters from the given iterator to the stream with the given ID.
    ///
    /// The characters are assumed to be Latin-1 code points.
    fn put_chars_stream(
        &mut self,
        mem: &mut impl Memory,
        stream: OpaqueId,
        bytes: impl IntoIterator<Item = u8>,
    );

    // 0x0086
    /// Changes the style of the current output stream
    fn set_style(&mut self, style: Style) {
        let stream = self
            .stream_get_current()
            .expect("TODO handle no current stream");
        self.set_style_stream(stream, style);
    }

    // 0x0087
    /// Changes the style of the stream with the given ID.
    fn set_style_stream(&mut self, stream: OpaqueId, style: Style);

    // 0x0090
    /// Reads one character from the stream with the given ID.
    fn get_char_stream(&mut self, mem: &impl Memory, stream: OpaqueId) -> u8;

    // 0x0091
    // TODO:
    /// Reads characters from the stream with the given ID, until either `slice.len()` - 1 characters have been read or a newline has been read,
    /// and returns the number of characters actually read.
    ///
    /// A terminal null character (`'\0'`) is placed on the end.
    fn get_line_stream(&mut self, mem: &impl Memory, stream: OpaqueId, slice: &mut [u8]) -> u32;

    // 0x0092
    /// Reads `slice.len()` characters from the stream with the given ID,
    /// unless the end of stream is reached first,
    /// and returns the number of characters actually read.
    fn get_slice_stream(&mut self, mem: &impl Memory, stream: OpaqueId, slice: &mut [u8]) -> u32;

    // This function is not in the spec, but is useful
    // especially for Glulx's `restore` opcode.
    //
    // TODO: Make it work like `get_slice_stream` and take a slice,
    // and return the number of characters actually read?
    /// Reads characters from the stream with the given ID until the end of stream is reached.
    fn get_to_end_stream(
        &mut self,
        mem: &impl Memory,
        stream: OpaqueId,
        buffer: &mut impl Extend<u8>,
    );

    // 0x00A0
    /// Converts the given Latin-1 character to lower case.
    fn char_to_lower(ch: u8) -> u8 {
        match ch {
            0x41..=0x5A | 0xC0..=0xD6 | 0xD8..=0xDE => ch + 0x20,
            ch => ch,
        }
    }

    // 0x00A1
    fn char_to_upper(ch: u8) -> u8 {
        match ch {
            0x41..=0x5A | 0xC0..=0xD6 | 0xD8..=0xDE => ch - 0x20,
            ch => ch,
        }
    }

    // 0x00B0
    /// Sets hints about the appearance of the given style for the given type of window.
    fn stylehint_set(&mut self, win_type: WindowType, style: Style, hint: Stylehint);

    // 0x00B1
    /// Clears hints about the appearance of the given style for the given type of window.
    fn stylehint_clear(&mut self, win_type: WindowType, style: Style, hint: Stylehint);

    // 0x00B2
    /// Returns true if the two given styles are visually distinguishable in the window with the given ID.
    fn style_distinguish(&self, win: OpaqueId, style1: Style, style2: Style) -> bool;

    // 0x00B3
    // TODO: We could have a different function for each style hint
    // so that they return an appropriate type instead of lumping everything in a u32.
    // But right now only this function will do.
    /// Tests an attribute of one style in the window with the given ID.
    ///
    /// If the attribute cannot be determined, `None` is returned.
    ///
    /// The meaning of the value depends on the hint which was tested;
    /// see the Glk specification for morne detail.
    fn style_measure(&self, win: OpaqueId, style: Style, hint: Stylehint) -> Option<u32>;

    // The original `select` and `select_poll` have been split into two parts:
    //
    // - The new `select`/`select_poll` are for indicating
    // that we are now awaiting for events,
    // they could do things like flushing the output.
    // - The new `accept` is for handling events,
    // and writing its results into memory.

    // 0x00C0
    /// Tells this Glauque implementation that we are ready to accept an input.
    ///
    /// It is here that, for example, output is flushed to the screen.
    fn select(&mut self);

    // 0x00C1
    // As for `select`, `select_poll` is split into two parts,
    // the second being the same `accept` above.
    //
    // TODO: In our split select/accept model, maybe polling is not necessary,
    // we could just use `select` directly?
    /// Checks if some event is pending.
    fn select_poll(&mut self);

    /// Handle the given event and writes its result to memory.
    ///
    /// This should be called at some point after every `select` or `select_poll`.
    fn accept(&mut self, mem: &mut impl Memory, event: Event) -> Event;

    // 0x00D0
    // Since `addr` is an address in the `Memory` and not a pointer,
    // we will accept NULL (i.e. 0).
    //
    // In any case, for a Glulx VM,
    // the address 0 is in the ROM so writing to it will cause an error.
    //
    // TODO: Use `usize` for the address? It will avoid needless conversions
    // and allow to play small stories on 16-bit systems (maybe).
    /// Requests input of a line of Latin-1 characters in the window with the given ID.
    ///
    /// If `init_len` is non-zero,
    /// the input will be prefilled with the contents of the given memory at the given address.
    ///
    /// When the input is received, it will be written in the memory at the given address.
    fn request_line_event(
        &mut self,
        mem: &impl Memory,
        win: OpaqueId,
        addr: u32,
        max_len: u32,
        init_len: u32,
    );

    // 0x00D1
    /// Cancels a pending request for line input in the window with the given ID.
    ///
    /// The returned event will be as if the player had hit enter,
    /// and the input composed so far will be stored according
    /// to the previous call to `request_line_event` or `request_line_event_uni`.
    ///
    /// For convenience, it is legal to call this function
    /// even if there is no line input request on the window.
    /// The event type will of None in this case.
    fn cancel_line_event(&mut self, win: OpaqueId) -> Event;

    // 0x00D2
    /// Requests input of a Latin-1 character or special key in the window with the given ID.
    fn request_char_event(&mut self, win: OpaqueId);

    // 0x00D3
    /// Cancels a pending request for character input in the window with the given ID.
    ///
    /// For convenience, it is legal to call this function
    /// even if there is no character input request on that window.
    /// Nothing happens in that case.
    fn cancel_char_event(&mut self, win: OpaqueId);

    // 0x00D4
    /// Requests a mouse input in the window with the given ID.
    ///
    /// Only text grid windows and graphics windows can have mouse input.
    fn request_mouse_event(&mut self, win: OpaqueId);

    // 0x00D5
    /// Cancels a pending request for mouse input in the window with the given ID.
    ///
    /// For convenience, it is legal to call this function
    /// even if there is no mouse input request on that window.
    /// Nothing happens in that case.
    fn cancel_mouse_event(&mut self, win: OpaqueId);

    // 0x00D6
    /// Request that an event be sent at fixed intervals.
    ///
    /// Calling it with 0 will cancel timer events.
    fn request_timer_events(&mut self, millis: u32);

    // 0x00E0
    /// Returns the width and height of the image resource with the given identifier.
    ///
    /// If there is no such image, `None` is returned.
    fn image_get_info(&self, image: u32) -> Option<(u32, u32)>;

    // 0x00E1
    /// Draws the image resource with the given identifier in the window with the given ID.
    ///
    /// The position of the image is given by `val1` and `val2`,
    /// but their meaning varies depending on what kind of window you are drawing in.
    ///
    /// False is returned if the drawing operation did not succeed.
    fn image_draw(&mut self, image: u32, val1: i32, val2: i32) -> bool;

    // 0x00E2
    /// This function is similar to `image_draw`, but it scales the image to the given width and height.
    fn image_draw_scaled(
        &mut self,
        image: u32,
        val1: i32,
        val2: i32,
        witdth: u32,
        height: u32,
    ) -> bool;

    // 0x00E8
    /// If the current point in the buffer window with the given ID
    /// is indented around a margin-aligned image,
    /// this acts makes the following text start a new line below the image.
    ///
    /// In windows other than text buffers, this function has no effect.
    fn window_flow_break(&mut self, win: OpaqueId);

    // 0x00E9
    /// Fills a rectangle with the background color of the window with the given ID.
    ///
    /// It is legitimate for part of the rectangle to fall outside the window.
    ///
    /// If `width` or `height` is zero, nothing is drawn.
    fn window_erase_rect(&mut self, left: i32, top: i32, width: u32, height: u32);

    // 0x00EA
    /// Fills a rectangle with the given color of the window with the given ID.
    ///
    /// It is legitimate for part of the rectangle to fall outside the window.
    ///
    /// If `width` or `height` is zero, nothing is drawn.
    fn window_fill_rect(
        &mut self,
        win: OpaqueId,
        color: Color,
        left: i32,
        top: i32,
        width: u32,
        height: u32,
    );

    // 0x00EB
    /// Sets the background color of the window with the given ID.
    ///
    /// It does not change what is currently displayed;
    /// it only affects subsequent clears and resizes.
    ///
    /// The initial background color of each window is white.
    fn window_set_background_color(&mut self, win: OpaqueId, color: Color);

    // 0x00F0
    /// Returns the opaque ID and rock of the sound channel after the one represented by the given opaque ID.
    ///
    /// If `None` is given, the opaque ID and rock of the first channel is returned.
    ///
    /// If `None` is returned, the given opaque ID represented the last channel.
    fn schannel_iterate(&self, opaque: Option<OpaqueId>) -> Option<(OpaqueId, u32)>;

    // 0x00F1
    /// Returns the rock value of the channel with the given ID.
    fn schannel_get_rock(&self, chan: OpaqueId) -> u32;

    // 0x00F2
    /// Creates a sound channel with volume at maximum and returns its ID.
    ///
    /// Returns `None` if sounds are not supported.
    fn schannel_create(&mut self, rock: u32) -> Option<OpaqueId> {
        self.schannel_create_ext(rock, 0x10000)
    }

    // 0x00F3
    /// Destroys the channel with the given ID.
    ///
    /// If the channel is playing a sound,
    /// the sound stops immediately (with no notification event).
    fn schannel_destroy(&mut self, chan: OpaqueId);

    // 0x00F4
    /// Creates a sound channel with the given volume and returns its ID.
    ///
    /// Returns `None` if sounds are not supported.
    fn schannel_create_ext(&mut self, rock: u32, volume: u32) -> Option<OpaqueId>;

    // 0x00F7
    // TODO: Instead of slices, we could use two iterators, or even one iterator of tuples.
    /// Works the same as `schannel_play_ext`, except that more than one sound can be specified.
    ///
    /// The channel IDs and sound resource numbers are given as two slice,
    /// which must be the same length.
    ///
    /// The `notify` argument applies to all the sounds;
    /// the repeats value for all the sounds is 1.
    ///
    /// All the sounds will begin at exactly the same time.
    ///
    /// The return value is the number of sounds that began playing correctly.
    ///
    /// If the notify argument is nonzero,
    /// you will get a separate sound notification event as each sound finishes.
    /// They will all have the same notify value.
    fn schannel_play_multi(&mut self, chans: &[OpaqueId], sounds: &[u32], notify: u32) -> u32;

    // 0x00F8
    /// Begins playing the given sound on the channel with the given ID.
    ///
    /// If the channel was already playing a sound (even the same one),
    /// the old sound is stopped (with no notification event).
    ///
    /// False is returned if there was any problem playing the sound.
    fn schannel_play(&mut self, chan: OpaqueId, sound: u32) -> bool {
        self.schannel_play_ext(chan, sound, 1, 0)
    }

    // 0x00F9
    /// works the same as `schannel_play`, but with additional options.
    ///
    /// Calling `schannel_play(chan, sound)` is exactly equivalent to `schannel_play_ext(chan, sound, 1, 0)`.
    ///
    /// The `repeats` value is the number of times the sound should be repeated.
    /// A value of 0xFFFFFFFF means that the sound should repeat forever.
    /// A value of 0 means that the sound will not be played at all; nothing happens.
    /// (Although a previous sound on the channel will be stopped,
    /// and the function will return true.)
    ///
    /// The `notify` value should be non-zero in order to request a sound notification event.
    /// In that case, when the sound is completed,
    /// there will be an event with type `SoundNotify` event.
    ///
    /// If sound notification is requested and the repeat value is greater than one,
    /// the event will only happen after the last repetition.
    /// If the repeat value is 0 or 0xFFFFFFFF, the notification event will neven happen.
    /// Similarly, if the sound is stopped or interrupted,
    /// or if the channel is destroyed while the sound is playing,
    /// there will be no notification event.
    fn schannel_play_ext(&mut self, chan: OpaqueId, sound: u32, repeats: u32, notify: u32) -> bool;

    // 0x00FA
    /// Stops any sound playing in the channel with the given ID.
    ///
    /// No notification event is generated, even if one was requested.
    ///
    /// If no sound is playing, this has no effect.
    fn schannel_stop(&mut self, chan: OpaqueId);

    // 0x00FB
    /// Sets the volume in the channel with the given ID.
    ///
    /// A `volume` of 0 is silence, 0x10000 is full volume.
    /// The volume can be overdrive by setting a value greater than 0x10000,
    /// but it's not recommended.
    fn schannel_set_volume(&mut self, chan: OpaqueId, volume: u32) {
        self.schannel_set_volume_ext(chan, volume, 0, 0);
    }

    // 0x00FC
    /// Gives a hint about whether the given sound should be loaded or not.
    ///
    /// If `flag` is true,
    /// the Glauque implementation may preload the sound or do other initialization,
    /// so that `schannel_play` will be faster.
    ///
    /// If `flag` is false,
    /// the implementation may release memory or other resources associated with the sound.
    fn sound_load_hint(&self, sound: u32, flag: bool);

    // 0x00FD
    /// Sets the volume in the channel with the given ID like `schannel_set_volume` but with additional options.
    ///
    /// A `volume` of 0 is silence, 0x10000 is full volume.
    /// The volume can be overdrive by setting a value greater than 0x10000,
    /// but it's not recommended.
    ///
    /// If `duration` is zero, the change is immediate.
    /// Otherwise, the change begins immediately,
    /// and occurs smoothly over the next `duration` milliseconds.
    ///
    /// The notify value should be non-zero in order to request a volume notification event.
    /// In that case, when the volume change is completed,
    /// there will be a `VolumeNotify` event.
    fn schannel_set_volume_ext(&mut self, chan: OpaqueId, volume: u32, duration: u32, notify: u32);

    // 0x00FE
    /// Pause any sound playing in the channel with the given ID.
    ///
    /// This does not generate any notification events.
    /// If the channel is already paused, this does nothing.
    ///
    /// New sounds started in a paused channel are paused immediately.
    ///
    /// A volume change in progress is not paused, and may proceed to completion,
    /// generating a notification if appropriate.
    fn schannel_pause(&mut self, chan: OpaqueId);

    // 0x00FF
    /// Unpause the channel with the given ID.
    ///
    /// Any paused sounds begin playing where they left off.
    /// If the channel is not already paused, this does nothing.
    fn schannel_unpause(&mut self, chan: OpaqueId);

    // 0x0100
    /// Sets the current link value in the current output stream.
    ///
    /// See `set_hyperlink_stream` for more detail.
    fn set_hyperlink(&mut self, link_val: u32) {
        let stream = self
            .stream_get_current()
            .expect("TODO handle no current stream");
        self.set_hyperlink_stream(stream, link_val);
    }

    // 0x0101
    /// Sets the current link value in the output stream with the given ID.
    ///
    /// A link value is any non-zero integer; zero indicates no link.
    /// Subsequent text output is considered to make up the body of the link,
    /// which continues until the link value is changed (or set to zero).
    ///
    /// Setting the link value of a stream to the value it already has has no effect.
    ///
    /// If the library supports images,
    /// they take on the current link value as they are output, just as text does.
    /// (This includes margin-aligned images, which can lead to some peculiar situations,
    /// since a right-margin image may not appear directly adjacent to the text it was output with.)
    fn set_hyperlink_stream(&mut self, stream: OpaqueId, link_val: u32);

    // 0x0102
    /// Requests a hyperlink input in the window with the given ID.
    fn request_hyperlink_event(&mut self, win: OpaqueId);

    // 0x0103
    /// Cancels a pending request for hyperlink input in the window with the given ID.
    ///
    /// For convenience, it is legal to call this function
    /// even if there is no hyperlink input request on that window.
    /// Nothing happens in that case.
    fn cancel_hyperlink_event(&mut self, win: OpaqueId);

    // 0x0120
    // We return a string because if we returned a slice if u32s,
    // we'd lose the fact that the bytes are valid UTF-8.
    /// Returns the string corresponding to the lowercase of the given slice taken as Unicode code points.
    fn slice_to_lower_case_uni(slice: &[u32]) -> String {
        // We could flatmap the chars to their lowercase independantly and collect,
        // it would avoid an allocation, but lowercasing a string can depend on context.
        let string = slice
            .into_iter()
            .map(|&ch| char::from_u32(ch).unwrap_or(char::REPLACEMENT_CHARACTER))
            .collect::<String>();
        string.to_lowercase()
    }

    // 0x0121
    /// Returns the string corresponding to the uppercase of the given slice taken as Unicode code points.
    fn slice_to_upper_case_uni(slice: &[u32]) -> String {
        // Unlike `slice_to_lower_case_uni`,
        // uppercasing a string don't depend on context (I think),
        // so we flatmap the chars to their uppercase independantly and collect.
        slice
            .into_iter()
            .flat_map(|&ch| {
                char::from_u32(ch)
                    .unwrap_or(char::REPLACEMENT_CHARACTER)
                    .to_uppercase()
            })
            .collect()
    }

    // 0x0122
    // Rust doesn't have a function for that,
    // and it's not trivial to implement, so no default implementation here.
    /// Returns the string corresponding to the titlecase of the given slice taken as Unicode code points.
    fn slice_to_title_case_uni(slice: &[u32]) -> String;

    // 0x0123
    /// Returns the canonical decomposition ("Normalization Form D") of the given slice taken as Unicode code points.
    fn slice_canon_decompose_uni(slice: &[u32]) -> String;

    // 0x0124
    /// Returns the canonical decomposition and recomposition ("Normalization Form C") of the given slice taken as Unicode code points.
    fn slice_canon_normalize_uni(slice: &[u32]) -> String;

    // 0x0128
    /// Prints one character to the current stream.
    ///
    /// The character is assumed to be a Unicode code point.
    fn put_char_uni(&mut self, mem: &mut impl Memory, ch: u32) {
        let stream = self
            .stream_get_current()
            .expect("TODO: handle no current stream");
        self.put_char_stream_uni(mem, stream, ch);
    }

    // 0x0129, 0x012A
    /// Prints a slice of characters to the current stream.
    ///
    /// The characters are assumed to be Unicode code points.
    fn put_slice_uni(&mut self, mem: &mut impl Memory, chars: &[u32]) {
        let stream = self
            .stream_get_current()
            .expect("TODO: handle no current stream");
        self.put_slice_stream_uni(mem, stream, chars);
    }

    // 0x012B
    /// Prints one character to the stream with the given Opaque ID.
    ///
    /// The character is assumed to be a Unicode code point.
    fn put_char_stream_uni(&mut self, mem: &mut impl Memory, stream: OpaqueId, ch: u32) {
        self.put_slice_stream_uni(mem, stream, &[ch]);
    }

    // 0x0129, 0x012A
    /// Prints the characters from the given iterator to the current stream.
    ///
    /// The characters are assumed to be Unicode code points.
    fn put_chars_uni(&mut self, mem: &mut impl Memory, chars: impl IntoIterator<Item = u32>) {
        let stream = self
            .stream_get_current()
            .expect("TODO: handle no current stream");
        self.put_chars_stream_uni(mem, stream, chars);
    }

    // 0x012C, 0x012D
    /// Prints a slice of characters to the stream with the given ID.
    ///
    /// The characters are assumed to be Unicode code points.
    fn put_slice_stream_uni(&mut self, mem: &mut impl Memory, stream: OpaqueId, chars: &[u32]);

    // 0x012C, 0x012D
    /// Prints the characters from the given iterator to the stream with the given ID.
    ///
    /// The characters are assumed to be Unicode code points.
    fn put_chars_stream_uni(
        &mut self,
        mem: &mut impl Memory,
        stream: OpaqueId,
        chars: impl IntoIterator<Item = u32>,
    );

    // 0x0130
    /// Reads one Unicode character from the stream with the given ID.
    fn get_char_stream_uni(&mut self, mem: &impl Memory, stream: OpaqueId) -> u32;

    // 0x0131
    /// Reads `slice.len()` Unicode characters from the stream with the given ID,
    /// unless the end of stream is reached first,
    /// and returns the number of characters actually read.
    fn get_slice_stream_uni(
        &mut self,
        mem: &impl Memory,
        stream: OpaqueId,
        slice: &mut [u32],
    ) -> u32;

    // 0x0132
    /// Reads Unicodeo characters from the stream with the given ID,
    /// until either `slice.len()` - 1 characters have been read or a newline has been read,
    /// and returns the number of characters actually read.
    ///
    /// A terminal null character (`'\0'`) is placed on the end.
    fn get_line_stream_uni(
        &mut self,
        mem: &impl Memory,
        stream: OpaqueId,
        slice: &mut [u32],
    ) -> u32;

    // 0x0138
    /// Opens a Unicode stream which reads from or writes to a disk file and returns its ID.
    fn stream_open_file_uni(
        &mut self,
        fileref: OpaqueId,
        mode: FileMode,
        rock: u32,
    ) -> Option<OpaqueId>;

    // 0x0139
    // TODO: This should have a `Memory` argument so that we can retain it.
    /// Opens a stream which reads from or writes into a space in memory and returns its ID.
    ///
    /// The Memory is taken as a collection of 32-bit values.
    ///
    /// # Notes to Implementors
    ///
    /// This function must call `retain_memory` on the `Memory`.
    fn stream_open_memory_uni(
        &mut self,
        mem: &impl Memory,
        start: u32,
        len: u32,
        mode: FileMode,
        rock: u32,
    ) -> OpaqueId;

    // 0x013A
    /// Opens the given data resource for reading as a Unicode stream and returns its ID.
    ///
    /// Returns `None` if the resource chunk with the given number does not exist.
    fn stream_open_resource_uni(&mut self, file_num: u32, rock: u32) -> Option<OpaqueId>;

    // 0x0140
    /// Requests input of a Latin-1 character or special key in the window with the given ID.
    fn request_char_event_uni(&mut self, win: OpaqueId);

    // 0x0141
    /// Requests input of a line of Unicode characters in the window with the given ID.
    ///
    /// This works the same as `request_line_event, except u32s will be written in memory instead of u8s.
    ///
    /// # Notes to Implementors
    ///
    /// If possible, fully composed Unicode characters should be written in the memory,
    /// rather than strings of base and composition characters.
    fn request_line_event_uni(
        &mut self,
        mem: &impl Memory,
        win: OpaqueId,
        addr: u32,
        max_len: u32,
        init_len: u32,
    );

    // 0x0150
    /// Enable or disable line input echoing in the buffer window with the given ID.
    ///
    /// If `val` is false, all subsequent line input requests in the window
    /// will leave the buffer unchanged after the input is completed or cancelled;
    /// the player's input will not be printed.
    ///
    /// If `val` is true,
    /// subsequent input requests will have the normal printing behavior.
    fn set_echo_line_event(&mut self, win: OpaqueId, val: bool);

    // 0x0151
    /// Makes it possible for the player to submit line events with special keys in the window with the given ID.
    ///
    /// The slice must only contain special keycodes.
    /// It should not contain regular printable characters, nor `Return`
    /// (which represents the default enter key and will always be recognized).
    ///
    /// An empty slice can be useo to return to the default behavior.
    fn set_terminators_line_event(&mut self, win: OpaqueId, keycodes: &[Key]);

    // 0x0160
    // TODO: Make it return a `(i64, i32)` instead of a timeval
    // (That will be handle on the dispatch layer.)
    /// Gets the Unix time (number of seconds since 1970).
    fn current_time() -> Timeval;

    // 0x0161
    /// Gets the Unix time (number of seconds since 1970) divided by the given factor as a u32.
    ///
    /// For example, if `factor` is 60, the result will be the number of minutes since 1970.
    ///
    /// If `factor` is 1, you will get the Unix time directly, but the value will be truncated starting some time in 2038.
    fn current_simple_time(factor: u32) -> i32;

    // 0x0168
    /// Converts the given timestamp (as returned by `current_time`) to a broken-out structure in universal time (UTC).
    fn time_to_date_utc(time: Timeval) -> Date;

    // 0x0169
    /// Converts the given timestamp (as returned by `current_time`) to a broken-out structure in local time.
    fn time_to_date_local(time: Timeval) -> Date;

    // 0x016A
    /// Converts the given timestamp (as returned by `current_simple_time`) to a broken-out structure in universal time (UTC).
    fn simple_time_to_date_utc(time: i32, factor: u32) -> Date;

    // 0x016B
    /// Converts the given timestamp (as returned by `current_simple_time`) to a broken-out structure in local time.
    fn simple_time_to_date_local(time: i32, factor: u32) -> Date;

    // 0x016C
    /// Converts the broken-out structure interpreted as universal time to a timestamp.
    ///
    /// The weekday value is ignored.
    /// The other values need not be in their normal ranges; they will be normalized.
    ///
    /// If the time cannot be represented for whatever reasons, `None` may be returned.
    fn date_to_time_utc(date: Date, factor: u32) -> Option<Timeval>;

    // 0x016D
    /// Converts the broken-out structure interpreted as universal time to a timestamp.
    ///
    /// The weekday value is ignored.
    /// The other values need not be in their normal ranges; they will be normalized.
    ///
    /// If the time cannot be represented for whatever reasons, `None` may be returned.
    ///
    /// This function may not be smart about Daylight Saving Time conversions.
    fn date_to_time_local(date: Date, factor: u32) -> Option<Timeval>;

    // 0x016E
    /// Converts the broken-out structure interpreted as universal time to a timestamp divided by `factor`.
    ///
    /// The weekday value is ignored.
    /// The other values need not be in their normal ranges; they will be normalized.
    ///
    /// If the time cannot be represented for whatever reason, `None` may be returned.
    fn date_to_simple_time_utc(date: Date, factor: u32) -> Option<i32>;

    // 0x016F
    /// Converts the broken-out structure interpreted as local time to a timestamp divided by `factor`.
    ///
    /// The weekday value is ignored.
    /// The other values need not be in their normal ranges; they will be normalized.
    ///
    /// If the time cannot be represented for whatever reason, `None` may be returned.
    fn date_to_simple_time_local(date: Date, factor: u32) -> Option<i32>;
}

pub use dispatch::Dispatch;
