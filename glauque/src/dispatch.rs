use std::fmt;

use crate::{
    opaque_option_to_u32, FileMode, Gestalt, Glauque, Memory, OpaqueId, Stack, StreamResult,
    Timeval, WindowBorder, WindowMethod, WindowPosition, WindowSplitMethod, WindowType,
};

// First, some traits used for conversions between `u32`s and Glk types.
// They will be used by the `Dispatch` trait to get the correct arguments.

trait SimpleType: Sized {
    /// The name of the Glk type.
    ///
    /// Used in error reporting.
    const NAME: &'static str;

    fn from_u32(value: u32) -> Result<Self, TypeFromU32Error>;

    fn into_u32(self) -> u32;

    /// Reads the `u32` at the given address in the `Memory`
    /// and convert it into `Self`.
    fn at_addr(mem: &mut impl Memory, addr: u32) -> Result<Self, TypeFromU32Error> {
        let value = mem.read_u32(addr);
        Self::from_u32(value)
    }

    /// Converts `self` into an `u32`
    /// then writes it in the `Memory` at the given address.
    fn write_to_addr(self, mem: &mut impl Memory, stack: &mut impl Stack, addr: u32) {
        if addr == 0xFF_FF_FF_FF {
            stack.push_u32(self.into_u32());
        } else if addr != 0 {
            mem.write_u32(addr, self.into_u32());
        }
    }
}

// The following trait could have been a free function
// since we only implement it for `u32`,
// but calling a method is nicer.

trait IntoSimpleType<T: SimpleType> {
    fn into_simple_type(self) -> Result<T, TypeFromU32Error>;
}

impl<T: SimpleType> IntoSimpleType<T> for u32 {
    fn into_simple_type(self) -> Result<T, TypeFromU32Error> {
        SimpleType::from_u32(self)
    }
}

trait CompoundType {
    /// The name of the Glk type.
    ///
    /// Used in error reporting.
    const NAME: &'static str;

    // I would have preferred to have a `const SIZE`,
    // and the functions returning a `[u32; Self::SIZE]`,
    // but Rust doesn't like that.
    //
    // So we'll use an associated type
    // that will always be an array.
    type ARRAY: IntoIterator<Item = u32>;

    fn into_u32_array(self) -> Self::ARRAY;

    /// Converts `self` into an array of `u32`s
    /// then write them in the `Memory` at the given address.
    fn write_to_addr(self, mem: &mut impl Memory, stack: &mut impl Stack, addr: u32)
    where
        Self: Sized,
    {
        if addr == 0xFF_FF_FF_FF {
            stack.push_u32s(self.into_u32_array());
        } else if addr != 0 {
            mem.write_u32s(addr, self.into_u32_array());
        }
    }
}

#[derive(Debug)]
#[non_exhaustive]
struct TypeFromU32Error {
    value: Vec<u32>,
    target: &'static str,
    kind: TypeFromU32ErrorKind,
}

impl fmt::Display for TypeFromU32Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_str("impossible to convert ")?;
        if self.value.len() == 1 {
            write!(f, "{}", self.value[0])?;
        } else {
            write!(f, "{:?}", self.value)?;
        }
        write!(f, "into {}", self.target)
    }
}

impl std::error::Error for TypeFromU32Error {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        Some(&self.kind)
    }
}

#[derive(Debug)]
enum TypeFromU32ErrorKind {
    #[non_exhaustive]
    IllegalNull,
    UnknownGestalt(u32),
    FileMode(<FileMode as TryFrom<u32>>::Error),
    WindowMethod(<WindowMethod as TryFrom<u32>>::Error),
    WindowType(<WindowType as TryFrom<u32>>::Error),
}

impl fmt::Display for TypeFromU32ErrorKind {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::IllegalNull => f.write_str("illegal NULL opaque object ID"),
            Self::UnknownGestalt(sel) => write!(f, "unknown gestalt selector {sel}"),
            Self::FileMode(_) => f.write_str("invalid file mode"),
            Self::WindowMethod(_) => f.write_str("invalid window method"),
            Self::WindowType(_) => f.write_str("invalid window type"),
        }
    }
}

impl std::error::Error for TypeFromU32ErrorKind {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        match self {
            Self::IllegalNull => None,
            Self::UnknownGestalt(_) => None,
            Self::FileMode(e) => Some(e),
            Self::WindowMethod(e) => Some(e),
            Self::WindowType(e) => Some(e),
        }
    }
}

// Now, the implementations for all the types appearing in Glauque functions.

impl SimpleType for u32 {
    const NAME: &'static str = "glui32";

    fn from_u32(value: u32) -> Result<Self, TypeFromU32Error> {
        Ok(value)
    }

    fn into_u32(self) -> u32 {
        self
    }
}

impl SimpleType for bool {
    const NAME: &'static str = "bool";

    fn from_u32(value: u32) -> Result<Self, TypeFromU32Error> {
        Ok(value != 0)
    }

    fn into_u32(self) -> u32 {
        self as u32
    }
}

impl SimpleType for Option<OpaqueId> {
    const NAME: &'static str = "opaque object ID";

    fn from_u32(value: u32) -> Result<Self, TypeFromU32Error> {
        Ok(OpaqueId::new(value))
    }

    fn into_u32(self) -> u32 {
        opaque_option_to_u32(self)
    }
}

impl SimpleType for OpaqueId {
    const NAME: &'static str = "non-NULL opaque object ID";

    fn from_u32(value: u32) -> Result<Self, TypeFromU32Error> {
        OpaqueId::new(value).ok_or(TypeFromU32Error {
            value: vec![value],
            target: Self::NAME,
            kind: TypeFromU32ErrorKind::IllegalNull,
        })
    }

    fn into_u32(self) -> u32 {
        self.as_u32()
    }
}

impl SimpleType for Gestalt {
    const NAME: &'static str = "gestalt selector";

    fn from_u32(value: u32) -> Result<Self, TypeFromU32Error> {
        value.try_into().map_err(|e| TypeFromU32Error {
            value: vec![value],
            target: Self::NAME,
            kind: TypeFromU32ErrorKind::UnknownGestalt(e),
        })
    }

    fn into_u32(self) -> u32 {
        self.into()
    }
}

impl SimpleType for WindowMethod {
    const NAME: &'static str = "window method";

    fn from_u32(value: u32) -> Result<Self, TypeFromU32Error> {
        value.try_into().map_err(|e| TypeFromU32Error {
            value: vec![value],
            target: Self::NAME,
            kind: TypeFromU32ErrorKind::WindowMethod(e),
        })
    }

    fn into_u32(self) -> u32 {
        self.into()
    }
}

impl SimpleType for WindowType {
    const NAME: &'static str = "window type";

    fn from_u32(value: u32) -> Result<Self, TypeFromU32Error> {
        value.try_into().map_err(|e| TypeFromU32Error {
            value: vec![value],
            target: Self::NAME,
            kind: TypeFromU32ErrorKind::WindowType(e),
        })
    }

    fn into_u32(self) -> u32 {
        self.into()
    }
}

impl SimpleType for FileMode {
    const NAME: &'static str = "file mode";

    fn from_u32(value: u32) -> Result<Self, TypeFromU32Error> {
        value.try_into().map_err(|e| TypeFromU32Error {
            value: vec![value],
            target: Self::NAME,
            kind: TypeFromU32ErrorKind::FileMode(e),
        })
    }

    fn into_u32(self) -> u32 {
        self.into()
    }
}

impl CompoundType for StreamResult {
    const NAME: &'static str = "stream_result";

    type ARRAY = [u32; 2];

    fn into_u32_array(self) -> Self::ARRAY {
        [self.read_count, self.write_count]
    }
}

impl CompoundType for Timeval {
    const NAME: &'static str = "timeval";

    type ARRAY = [u32; 3];

    fn into_u32_array(self) -> Self::ARRAY {
        [self.high_sec as u32, self.low_sec, self.microsec as u32]
    }
}

// And now the dispatch mechanism.

pub trait Dispatch: Glauque {
    fn opaque_id_from_u32(val: u32) -> Option<OpaqueId> {
        OpaqueId::new(val)
    }

    /// Calls the Glauque functions corresponding to the given `id`.
    ///
    /// The `args` are converted to the needed type according to the dispatching rules.
    ///
    /// # Panics
    ///
    /// Since the functions `select` and `select_poll` (id 0xC0 and 0xC1) from Glk are split into two
    /// to allow pausing a VM when it needs to await an event,
    /// dispatching them is not useful nor allowed and will panic.
    fn dispatch(
        &mut self,
        mem: &mut impl Memory,
        stack: &mut impl Stack,
        id: u32,
        args: &[u32],
    ) -> Result<u32, Error> {
        /// A helper function to check if we got got enough args when dispatching.
        fn check_args_len(args: &[u32], n: usize) -> Result<(), ErrorKind> {
            // We won't be too strict and accept more arguments than required.
            // The arguments in excess will simply be ignored.
            if args.len() < n {
                return Err(ErrorKind::NotEnoughArgs {
                    asked: n,
                    got: args.len(),
                });
            }
            Ok(())
        }

        /// A helper function taking the return value of `Glauque::{class}_iterate`
        /// and handling its dispatching.
        fn iterated_object_outcome(
            mem: &mut impl Memory,
            stack: &mut impl Stack,
            x: Option<(OpaqueId, u32)>,
            rock_addr: u32,
        ) -> u32 {
            match x {
                None => 0,
                Some((id, rock)) => {
                    rock.write_to_addr(mem, stack, rock_addr);
                    id.as_u32()
                }
            }
        }

        // TODO: This function won't be necessary once `try` blocks are available.
        //
        /// A helper function doing the actual dispatch.
        ///
        /// It's only purpose is to make it possible to use the `?` operator
        /// without having to map the errors each time.
        fn inner<D: Dispatch + ?Sized>(
            s: &mut D,
            mem: &mut impl Memory,
            stack: &mut impl Stack,
            id: u32,
            args: &[u32],
        ) -> Result<u32, ErrorKind> {
            let outcome = match id {
                0x0001 => {
                    check_args_len(args, 0)?;
                    s.exit();
                    0
                }
                0x0002 => {
                    todo!("dispatching Glk set_interrupt_handler (0x{id:X}) with args {args:?}");
                }
                0x0003 => {
                    check_args_len(args, 0)?;
                    s.tick();
                    0
                }
                0x0004 => {
                    check_args_len(args, 2)?;
                    let Ok(sel) = args[0].into_simple_type() else {
                        // Ignore unknown gestalts.
                        return Ok(0);
                    };
                    let val = args[1];
                    s.gestalt(sel, val)
                }
                0x0005 => {
                    check_args_len(args, 4)?;
                    let sel = args[0].into_simple_type()?;
                    let val = args[1].into_simple_type()?;
                    let addr = args[2];
                    let mut slice = if addr == 0 {
                        Vec::new()
                    } else {
                        mem.read_u32s(addr, args[3])
                    };
                    let result = s.gestalt_ext(sel, val, &mut slice);
                    // TODO: Have some kind of direct view to the memory
                    // so that we don't need to write back
                    // (`gestalt_ext` would write to it directly).
                    mem.write_u32s(addr, slice);
                    result
                }
                0x0020 => {
                    check_args_len(args, 2)?;
                    let win = OpaqueId::new(args[0]);
                    let next = s.window_iterate(win);

                    iterated_object_outcome(mem, stack, next, args[1])
                }
                0x0021 => {
                    check_args_len(args, 1)?;
                    let win = args[0].into_simple_type()?;
                    s.window_get_rock(win)
                }
                0x0022 => {
                    todo!("dispatching Glk window_get_root (0x{id:X}) with args {args:?}");
                }
                0x0023 => {
                    check_args_len(args, 5)?;
                    let split = OpaqueId::new(args[0]);
                    let method = if split.is_none() {
                        // We are trying to open the root window,
                        // so the method doesn't matter
                        WindowMethod {
                            split_method: WindowSplitMethod::Proportional,
                            position: WindowPosition::Below,
                            border: WindowBorder::Yes,
                        }
                    } else {
                        args[1].into_simple_type()?
                    };
                    let size = args[2];
                    let win_type = args[3].into_simple_type()?;
                    let rock = args[4];
                    let id = s.window_open(split, method, size, win_type, rock);
                    opaque_option_to_u32(id)
                }
                0x0024 => {
                    todo!("dispatching Glk window_close (0x{id:X}) with args {args:?}");
                }
                0x0025 => {
                    check_args_len(args, 3)?;
                    let id = args[0].into_simple_type()?;
                    let (width, height) = s.window_get_size(id);
                    width.write_to_addr(mem, stack, args[1]);
                    height.write_to_addr(mem, stack, args[2]);
                    0
                }
                0x0026 => {
                    check_args_len(args, 4)?;
                    let win = args[0].into_simple_type()?;
                    let method = args[1].into_simple_type()?;
                    let size = args[2];
                    let key = args[3].into_simple_type()?;
                    s.window_set_arrangement(win, method, size, key);
                    0
                }
                0x0027 => {
                    todo!("dispatching Glk window_get_arrangement (0x{id:X}) with args {args:?}");
                }
                0x0028 => {
                    todo!("dispatching Glk window_get_type (0x{id:X}) with args {args:?}");
                }
                0x0029 => {
                    check_args_len(args, 1)?;
                    let win = args[0].into_simple_type()?;
                    let parent = s.window_get_parent(win);
                    opaque_option_to_u32(parent)
                }
                0x002A => {
                    check_args_len(args, 1)?;
                    let win = args[0].into_simple_type()?;
                    s.window_clear(win);
                    0
                }
                0x002B => {
                    check_args_len(args, 3)?;
                    let id = args[0].into_simple_type()?;
                    s.window_move_cursor(id, args[1], args[2]);
                    0
                }
                0x002C => {
                    todo!("dispatching Glk window_get_stream (0x{id:X}) with args {args:?}");
                }
                0x002D => {
                    todo!("dispatching Glk window_set_echo_stream (0x{id:X}) with args {args:?}");
                }
                0x002E => {
                    todo!("dispatching Glk window_get_echo_stream (0x{id:X}) with args {args:?}");
                }
                0x002F => {
                    check_args_len(args, 1)?;
                    let win = OpaqueId::new(args[0]);
                    s.set_window(win);
                    0
                }
                0x0030 => {
                    todo!("dispatching Glk window_get_sibling (0x{id:X}) with args {args:?}");
                }
                0x0040 => {
                    check_args_len(args, 2)?;
                    let next = s.stream_iterate(args[0].into_simple_type()?);
                    iterated_object_outcome(mem, stack, next, args[1])
                }
                0x0041 => {
                    todo!("dispatching Glk stream_get_rock (0x{id:X}) with args {args:?}");
                }
                0x0042 => {
                    check_args_len(args, 3)?;
                    let fileref = args[0].into_simple_type()?;
                    let mode = args[1].into_simple_type()?;
                    let rock = args[2];
                    let stream = s.stream_open_file(fileref, mode, rock);
                    opaque_option_to_u32(stream)
                }
                0x0043 => {
                    check_args_len(args, 4)?;
                    let start = args[0];
                    let len = args[1];
                    let mode = args[2].into_simple_type()?;
                    let rock = args[3];
                    s.stream_open_memory(mem, start, len, mode, rock).as_u32()
                }
                0x0044 => {
                    check_args_len(args, 2)?;
                    let id = args[0].into_simple_type()?;
                    let result = s.stream_close(mem, id);
                    result.write_to_addr(mem, stack, args[1]);
                    0
                }
                0x0045 => {
                    todo!("dispatching Glk stream_set_position (0x{id:X}) with args {args:?}");
                }
                0x0046 => {
                    todo!("dispatching Glk stream_get_position (0x{id:X}) with args {args:?}");
                }
                0x0047 => {
                    check_args_len(args, 1)?;
                    let stream = OpaqueId::new(args[0]);
                    s.stream_set_current(stream);
                    0
                }
                0x0048 => {
                    check_args_len(args, 0)?;
                    opaque_option_to_u32(s.stream_get_current())
                }
                0x0049 => {
                    todo!("dispatching Glk stream_open_resource (0x{id:X}) with args {args:?}");
                }
                0x0060 => {
                    check_args_len(args, 2)?;
                    let usage = args[0].try_into().expect("TODO: handle invalid file usage");
                    let file_ref = s.fileref_create_temp(usage, args[1]);
                    opaque_option_to_u32(file_ref)
                }
                0x0061 => {
                    todo!("dispatching Glk fileref_create_by_name (0x{id:X}) with args {args:?}");
                }
                0x0062 => {
                    todo!("dispatching Glk fileref_create_by_prompt (0x{id:X}) with args {args:?}");
                }
                0x0063 => {
                    check_args_len(args, 1)?;
                    let fileref = args[0]
                        .into_simple_type()
                        .expect("TODO: handle NULL fileref");
                    s.fileref_destroy(fileref);
                    0
                }
                0x0064 => {
                    check_args_len(args, 2)?;
                    let next = s.fileref_iterate(OpaqueId::new(args[0]));
                    iterated_object_outcome(mem, stack, next, args[1])
                }
                0x0065 => {
                    todo!("dispatching Glk fileref_get_rock (0x{id:X}) with args {args:?}");
                }
                0x0066 => {
                    check_args_len(args, 1)?;
                    let fileref = args[0]
                        .into_simple_type()
                        .expect("TODO: handle NULL fileref");
                    s.fileref_delete_file(fileref);
                    0
                }
                0x0067 => {
                    check_args_len(args, 1)?;
                    let fileref = args[0]
                        .into_simple_type()
                        .expect("TODO: handle NULL fileref");
                    s.fileref_does_file_exist(fileref) as u32
                }
                0x0068 => {
                    todo!(
                        "dispatching Glk fileref_create_from_fileref (0x{id:X}) with args {args:?}"
                    );
                }
                0x0080 => {
                    check_args_len(args, 1)?;
                    // TODO: we simply truncate the argument.
                    // We might want to print a warning,
                    // or print a character code.
                    s.put_char(mem, args[0] as u8);
                    0
                }
                0x0081 => {
                    todo!("dispatching Glk put_char_stream (0x{id:X}) with args {args:?}");
                }
                0x0082 => {
                    check_args_len(args, 1)?;
                    let addr = args[0];
                    // TODO: Try not to allocate a Vec
                    // while still satisfying the borrow checker.
                    // If not possible, then simply use `put_slice`
                    // (and possibly remove put_chars.)
                    let bytes = mem
                        .read_u8s_to_end(addr)
                        .iter()
                        .copied()
                        .take_while(|&byte| byte != 0)
                        .collect::<Vec<_>>();
                    s.put_chars(mem, bytes);
                    0
                }
                0x0083 => {
                    todo!("dispatching Glk put_string_stream (0x{id:X}) with args {args:?}");
                }
                0x0084 => {
                    check_args_len(args, 2)?;
                    // TODO: Try not to allocate a Vec
                    // while still satisfying the borrow checker.
                    let addr = args[0];
                    let len = args[1];
                    let buffer = &mem.read_u8s(addr, len).to_vec();
                    println!();
                    s.put_slice(mem, buffer);
                    0
                }
                0x0085 => {
                    todo!("dispatching Glk put_buffer_stream (0x{id:X}) with args {args:?}");
                }
                0x0086 => {
                    check_args_len(args, 1)?;
                    s.set_style(args[0].into());
                    0
                }
                0x0087 => {
                    todo!("dispatching Glk set_style_stream (0x{id:X}) with args {args:?}");
                }
                0x0090 => {
                    todo!("dispatching Glk get_char_stream (0x{id:X}) with args {args:?}");
                }
                0x0091 => {
                    todo!("dispatching Glk get_line_stream (0x{id:X}) with args {args:?}");
                }
                0x0092 => {
                    todo!("dispatching Glk get_buffer_stream (0x{id:X}) with args {args:?}");
                }
                0x00A0 => {
                    check_args_len(args, 1)?;
                    D::char_to_lower(args[0] as u8) as u32
                }
                0x00A1 => {
                    todo!("dispatching Glk char_to_upper (0x{id:X}) with args {args:?}");
                }
                0x00B0 => {
                    check_args_len(args, 4)?;
                    let win_type = args[0].into_simple_type()?;
                    let style = args[1].into();
                    if let Ok(hint) = (args[2], args[3]).try_into() {
                        // Ignore unknown stylehints.
                        s.stylehint_set(win_type, style, hint);
                    }
                    0
                }
                0x00B1 => {
                    todo!("dispatching Glk stylehint_clear (0x{id:X}) with args {args:?}");
                }
                0x00B2 => {
                    todo!("dispatching Glk style_distinguish (0x{id:X}) with args {args:?}");
                }
                0x00B3 => {
                    todo!("dispatching Glk style_measure (0x{id:X}) with args {args:?}");
                }
                0x00C0 => {
                    // Since we split the original Glk select into two,
                    // we won't support dispatching `select`.
                    panic!("dispatching `select` is not supported.")
                }
                0x00C1 => {
                    // Since we split the original Glk select_poll into two,
                    // we won't support dispatching `select_poll`.
                    panic!("dispatching `select_poll` is not supported.")
                }
                0x00D0 => {
                    check_args_len(args, 4)?;
                    let win = args[0].into_simple_type()?;
                    s.request_line_event(mem, win, args[1], args[2], args[3]);
                    0
                }
                0x00D1 => {
                    todo!("dispatching Glk cancel_line_event (0x{id:X}) with args {args:?}");
                }
                0x00D2 => {
                    check_args_len(args, 1)?;
                    let win = args[0].into_simple_type()?;
                    s.request_char_event(win);
                    0
                }
                0x00D3 => {
                    todo!("dispatching Glk cancel_char_event (0x{id:X}) with args {args:?}");
                }
                0x00D4 => {
                    todo!("dispatching Glk request_mouse_event (0x{id:X}) with args {args:?}");
                }
                0x00D5 => {
                    todo!("dispatching Glk cancel_mouse_event (0x{id:X}) with args {args:?}");
                }
                0x00D6 => {
                    todo!("dispatching Glk request_timer_events (0x{id:X}) with args {args:?}");
                }
                0x00E0 => {
                    todo!("dispatching Glk image_get_info (0x{id:X}) with args {args:?}");
                }
                0x00E1 => {
                    todo!("dispatching Glk image_draw (0x{id:X}) with args {args:?}");
                }
                0x00E2 => {
                    todo!("dispatching Glk image_draw_scaled (0x{id:X}) with args {args:?}");
                }
                0x00E8 => {
                    todo!("dispatching Glk window_flow_break (0x{id:X}) with args {args:?}");
                }
                0x00E9 => {
                    todo!("dispatching Glk window_erase_rect (0x{id:X}) with args {args:?}");
                }
                0x00EA => {
                    todo!("dispatching Glk window_fill_rect (0x{id:X}) with args {args:?}");
                }
                0x00EB => {
                    todo!(
                        "dispatching Glk window_set_background_color (0x{id:X}) with args {args:?}"
                    );
                }
                0x00F0 => {
                    check_args_len(args, 2)?;
                    let next = s.stream_iterate(OpaqueId::new(args[0]));
                    iterated_object_outcome(mem, stack, next, args[1])
                }
                0x00F1 => {
                    todo!("dispatching Glk schannel_get_rock (0x{id:X}) with args {args:?}");
                }
                0x00F2 => {
                    check_args_len(args, 1)?;
                    opaque_option_to_u32(s.schannel_create(args[0]))
                }
                0x00F3 => {
                    todo!("dispatching Glk schannel_destroy (0x{id:X}) with args {args:?}");
                }
                0x00F4 => {
                    todo!("dispatching Glk schannel_create_ext (0x{id:X}) with args {args:?}");
                }
                0x00F7 => {
                    todo!("dispatching Glk schannel_play_multi (0x{id:X}) with args {args:?}");
                }
                0x00F8 => {
                    todo!("dispatching Glk schannel_play (0x{id:X}) with args {args:?}");
                }
                0x00F9 => {
                    todo!("dispatching Glk schannel_play_ext (0x{id:X}) with args {args:?}");
                }
                0x00FA => {
                    todo!("dispatching Glk schannel_stop (0x{id:X}) with args {args:?}");
                }
                0x00FB => {
                    todo!("dispatching Glk schannel_set_volume (0x{id:X}) with args {args:?}");
                }
                0x00FC => {
                    todo!("dispatching Glk sound_load_hint (0x{id:X}) with args {args:?}");
                }
                0x00FD => {
                    todo!("dispatching Glk schannel_set_volume_ext (0x{id:X}) with args {args:?}");
                }
                0x00FE => {
                    todo!("dispatching Glk schannel_pause (0x{id:X}) with args {args:?}");
                }
                0x00FF => {
                    todo!("dispatching Glk schannel_unpause (0x{id:X}) with args {args:?}");
                }
                0x0100 => {
                    todo!("dispatching Glk set_hyperlink (0x{id:X}) with args {args:?}");
                }
                0x0101 => {
                    todo!("dispatching Glk set_hyperlink_stream (0x{id:X}) with args {args:?}");
                }
                0x0102 => {
                    todo!("dispatching Glk request_hyperlink_event (0x{id:X}) with args {args:?}");
                }
                0x0103 => {
                    todo!("dispatching Glk cancel_hyperlink_event (0x{id:X}) with args {args:?}");
                }
                0x0120 => {
                    check_args_len(args, 3)?;
                    let addr = args[0];
                    let max_len = args[1];
                    let initial_len = args[2];
                    if initial_len > max_len {
                        todo!(
                            "available length of buffer smaller than initial number of characters"
                        );
                    }

                    let slice = mem.read_u32s(addr, initial_len);
                    let lowercased = D::slice_to_lower_case_uni(&slice);

                    mem.write_u32s(
                        addr,
                        lowercased
                            .chars()
                            .take(max_len.try_into().unwrap())
                            .map(From::from),
                    );
                    lowercased.chars().count().try_into().unwrap()
                }
                0x0121 => {
                    todo!("dispatching Glk buffer_to_upper_case_uni (0x{id:X}) with args {args:?}");
                }
                0x0122 => {
                    todo!("dispatching Glk buffer_to_title_case_uni (0x{id:X}) with args {args:?}");
                }
                0x0123 => {
                    todo!(
                        "dispatching Glk buffer_canon_decompose_uni (0x{id:X}) with args {args:?}"
                    );
                }
                0x0124 => {
                    todo!(
                        "dispatching Glk buffer_canon_normalize_uni (0x{id:X}) with args {args:?}"
                    );
                }
                0x0128 => {
                    check_args_len(args, 1)?;
                    s.put_char_uni(mem, args[0]);
                    0
                }
                0x0129 => {
                    check_args_len(args, 1)?;
                    let addr = args[0];
                    // TODO: Try not to allocate a Vec
                    // while still satisfying the borrow checker.
                    // If not possible, then simply use `put_slice`
                    // (and possibly remove `put_chars`.)
                    //
                    // TODO: We assume the memory is in big-endian.
                    // But that's a Glulx specificity, not a Glk one.
                    let chars = mem
                        .read_u8s_to_end(addr)
                        .chunks(4)
                        .map(|bytes| u32::from_be_bytes(bytes.try_into().unwrap()))
                        .take_while(|&byte| byte != 0)
                        .collect::<Vec<_>>();
                    s.put_chars_uni(mem, chars);
                    0
                }
                0x012A => {
                    check_args_len(args, 2)?;
                    // TODO: Try not to allocate a Vec
                    // while still satisfying the borrow checker.
                    //
                    // TODO: We assume the memory is in big-endian.
                    // But that's a Glulx specificity, not a Glk one.
                    let addr = args[0];
                    let len: usize = args[1].try_into().unwrap();
                    let buffer = mem
                        .read_u8s_to_end(addr)
                        .chunks(4)
                        .take(len)
                        .map(|bytes| u32::from_be_bytes(bytes.try_into().unwrap()))
                        .collect::<Vec<_>>();
                    s.put_slice_uni(mem, &buffer);
                    0
                }
                0x012B => {
                    todo!("dispatching Glk put_char_stream_uni (0x{id:X}) with args {args:?}");
                }
                0x012C => {
                    todo!("dispatching Glk put_string_stream_uni (0x{id:X}) with args {args:?}");
                }
                0x012D => {
                    todo!("dispatching Glk put_buffer_stream_uni (0x{id:X}) with args {args:?}");
                }
                0x0130 => {
                    todo!("dispatching Glk get_char_stream_uni (0x{id:X}) with args {args:?}");
                }
                0x0131 => {
                    todo!("dispatching Glk get_buffer_stream_uni (0x{id:X}) with args {args:?}");
                }
                0x0132 => {
                    todo!("dispatching Glk get_line_stream_uni (0x{id:X}) with args {args:?}");
                }
                0x0138 => {
                    todo!("dispatching Glk stream_open_file_uni (0x{id:X}) with args {args:?}");
                }
                0x0139 => {
                    check_args_len(args, 4)?;
                    let start = args[0];
                    let len = args[1];
                    let mode = args[2].into_simple_type()?;
                    let rock = args[3];
                    s.stream_open_memory_uni(mem, start, len, mode, rock)
                        .as_u32()
                }
                0x013A => {
                    todo!("dispatching Glk stream_open_resource_uni (0x{id:X}) with args {args:?}");
                }
                0x0140 => {
                    todo!("dispatching Glk request_char_event_uni (0x{id:X}) with args {args:?}");
                }
                0x0141 => {
                    todo!("dispatching Glk request_line_event_uni (0x{id:X}) with args {args:?}");
                }
                0x0150 => {
                    check_args_len(args, 2)?;
                    let win = args[0].into_simple_type()?;
                    let val = args[1].into_simple_type()?;
                    s.set_echo_line_event(win, val);
                    0
                }
                0x0151 => {
                    todo!(
                        "dispatching Glk set_terminators_line_event (0x{id:X}) with args {args:?}"
                    );
                }
                0x0160 => {
                    check_args_len(args, 1)?;
                    D::current_time().write_to_addr(mem, stack, args[0]);
                    0
                }
                0x0161 => {
                    todo!("dispatching Glk current_simple_time (0x{id:X}) with args {args:?}");
                }
                0x0168 => {
                    todo!("dispatching Glk time_to_date_utc (0x{id:X}) with args {args:?}");
                }
                0x0169 => {
                    todo!("dispatching Glk time_to_date_local (0x{id:X}) with args {args:?}");
                }
                0x016A => {
                    todo!("dispatching Glk simple_time_to_date_utc (0x{id:X}) with args {args:?}");
                }
                0x016B => {
                    todo!(
                        "dispatching Glk simple_time_to_date_local (0x{id:X}) with args {args:?}"
                    );
                }
                0x016C => {
                    todo!("dispatching Glk date_to_time_utc (0x{id:X}) with args {args:?}");
                }
                0x016D => {
                    todo!("dispatching Glk date_to_time_local (0x{id:X}) with args {args:?}");
                }
                0x016E => {
                    todo!("dispatching Glk date_to_simple_time_utc (0x{id:X}) with args {args:?}");
                }
                0x016F => {
                    todo!(
                        "dispatching Glk date_to_simple_time_local (0x{id:X}) with args {args:?}"
                    );
                }
                n => todo!("handle invalid Glk call selector 0x{n:X}"),
            };

            Ok(outcome)
        }

        // Dispatch and convert the error kind to the actual error.
        inner(self, mem, stack, id, args).map_err(|kind| Error { selector: id, kind })
    }
}

impl<T: Glauque> Dispatch for T {}

#[derive(Debug)]
#[non_exhaustive]
pub struct Error {
    selector: u32,
    kind: ErrorKind,
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "error dispatching Glk call with selector {}",
            self.selector
        )
    }
}

impl std::error::Error for Error {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        Some(&self.kind)
    }
}

#[derive(Debug)]
enum ErrorKind {
    #[non_exhaustive]
    NotEnoughArgs {
        asked: usize,
        got: usize,
    },
    TypeFromU32(TypeFromU32Error),
}

impl fmt::Display for ErrorKind {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            ErrorKind::NotEnoughArgs { asked, got } => {
                write!(f, "expected {asked} arguments, got {got}")
            }
            ErrorKind::TypeFromU32(_) => f.write_str("can't convert argument from `u32`"),
        }
    }
}

impl std::error::Error for ErrorKind {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        match self {
            ErrorKind::TypeFromU32(e) => Some(e),
            ErrorKind::NotEnoughArgs { .. } => None,
        }
    }
}

impl From<TypeFromU32Error> for ErrorKind {
    fn from(value: TypeFromU32Error) -> Self {
        Self::TypeFromU32(value)
    }
}
