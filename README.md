# Coryphelle

Coryphelle is a [Glulx](https://eblong.com/zarf/glulx/) interpreter written in Rust.

It's not in a usable state right now and the API will change in the future. If you still want to try it, there's an example you can run with:

```
$ cargo run --example basic -- path/to/story.ulx
```

The supplied story will run in the terminal. Right now, the process will panic when encountering unimplemented features, so it mostly only works with Glulxercise — regular Inform 6 or 7 stories need features that aren't implemented yet. Glulxercise can be found on the Glulx main page linked above; its Inform 6 source file is also included in this repository (`tests/glulxercise` folder), and running `cargo test` will compile it if you have the Inform compiler in your `PATH`.

The example will also only work with bare Glulx files with extension `.ulx`, not with blorbed files with extension `.gblorb`.

The Glauque crate included in this repository defines a trait for implementing the [Glk API](https://eblong.com/zarf/glk/) used by Glulx (or at least something equivalent to it).
