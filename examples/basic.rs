//! This simple example shows the basics on how to run Coryphelle.
//!
//! To run:
//!
//! ```text
//! $ cargo run --example basic -- path/to/story.ulx
//! ```
//!
//! The story will be played with the command line.

use std::{fs, io::Write};

use coryphelle::Coryphelle;

use glauque::{Event, EventType};

use test_glauque::TestGlauque;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    // Get the path provided on the command line and open the story file.
    let image_path = std::env::args_os().nth(1).ok_or("No path provided.")?;
    let image = fs::read(image_path)?;

    // First get a unique string identifying this story file
    // with a crude "hash":
    // we take the first 128 bytes, interpret them as characters
    // and filter out those who would be illegal in paths.
    // It will keep unprintable characters,
    // but it's OK for our purpose.
    let story_id = image[..128]
        .into_iter()
        .map(|&byte| byte as char)
        .filter(|&ch| !"/\\<>:\"|?*".contains(ch))
        .collect();
    let glauque = TestGlauque::new(story_id);

    // Create a new story instance from the story image.
    let (mut story, result) = Coryphelle::builder(glauque, image).validate()?.start();
    // Handle the story's initial output.
    print!("{}", result?);
    std::io::stdout().flush().unwrap();

    // Ihe main loop.
    loop {
        let output;
        // Get and send the next input.
        match story.status() {
            coryphelle::Status::Ended => break,
            coryphelle::Status::Polling => {
                // We don't handle internally-spawned events right now.
                output = story.accept(Event {
                    ty: EventType::None,
                    win: None,
                })?;
            }
            coryphelle::Status::Awaiting => {
                let mut input = String::new();
                std::io::stdin().read_line(&mut input)?;
                // Strip trailing new line.
                let input = if let Some(stripped) = input.strip_suffix("\r\n") {
                    stripped
                } else if let Some(stripped) = input.strip_suffix('\n') {
                    stripped
                } else {
                    &input
                };
                // This method is from TestGlauque.
                // It won't be the same depending on the Glauque implementation.
                let event = story
                    .glk()
                    .event_from_str(input)
                    .ok_or("The story can't take events at this moment.")?;
                output = story.accept(event)?;
            }
        }
        print!("{output}");
        std::io::stdout().flush().unwrap();
    }

    println!("\n\n*** THE STORY HAS ENDED ***\n");

    Ok(())
}
