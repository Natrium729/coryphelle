use glauque::{
    Color, Date, Event, FileMode, FileUsage, GestaltCharOutput, Glauque, Key, Memory, OpaqueId,
    OpaqueIdFactory, SeekMode, StreamResult, Style, Stylehint, Timeval, WindowMethod, WindowType,
};

mod opaque;
mod stream;
mod stylehints;
mod update;
mod window;

use opaque::OpaqueCollection;
use stream::Stream;
use stylehints::WindowHints;
use update::Update;
use window::Window;

pub trait Delegator {
    fn gestalt_sound(&self) -> bool;

    fn gestalt_hyperlinks(&self) -> bool;

    fn gestalt_line_input_echo(&self) -> bool;

    fn set_echo_line_event(&mut self, win: OpaqueId, val: bool);
}

pub struct Deleglauque<D: Delegator> {
    delegator: D,

    updates: Vec<Update>,

    opaque_id_factory: OpaqueIdFactory,

    streams: OpaqueCollection<Stream>,
    windows: OpaqueCollection<Window>,
    filerefs: OpaqueCollection<()>,

    root_window: Option<OpaqueId>,
    current_stream: Option<OpaqueId>,

    stylehints: WindowHints,
}

impl<D: Delegator> Deleglauque<D> {
    pub fn new(delegator: D) -> Self {
        Self {
            delegator,

            // TODO: set a capacity.
            updates: Vec::new(),

            opaque_id_factory: OpaqueIdFactory::new(),

            streams: OpaqueCollection::new(),
            windows: OpaqueCollection::new(),
            filerefs: OpaqueCollection::new(),

            root_window: None,
            current_stream: None,

            stylehints: WindowHints::new(),
        }
    }

    fn get_opaque_id(&mut self) -> OpaqueId {
        self.opaque_id_factory.get().expect("opaque IDs exhausted")
    }
}

impl<D: Delegator> Glauque for Deleglauque<D> {
    type Output = Vec<Update>;

    fn output(&mut self) -> Self::Output {
        let cap = self.updates.capacity();
        std::mem::replace(&mut self.updates, Vec::with_capacity(cap))
    }

    fn warn(&mut self, _msg: String) {
        todo!("warn");
    }

    fn error(&mut self, _msg: String) {
        todo!("error");
    }

    // 0x0001
    fn exit(&mut self) {
        todo!("exit");
    }

    fn gestalt_char_input(&self, _ch: Key) -> bool {
        todo!("gestalt_char_input");
    }

    fn gestalt_char_output(&self, _ch: u32) -> (GestaltCharOutput, u32) {
        todo!("gestalt_char_output");
    }

    fn gestalt_mouse_input(&self, _window_type: WindowType) -> bool {
        todo!("gestalt_mouse_input");
    }

    fn gestalt_timer(&self) -> bool {
        todo!("gestalt_timer");
    }

    fn gestalt_graphics(&self) -> bool {
        todo!("gestalt_graphics");
    }

    fn gestalt_draw_image(&self, _window_type: WindowType) -> bool {
        todo!("gestalt_draw_image");
    }

    fn gestalt_sound(&self) -> bool {
        self.delegator.gestalt_sound()
    }

    fn gestalt_sound_volume(&self) -> bool {
        todo!("gestalt_sound_volume");
    }

    fn gestalt_sound_notify(&self) -> bool {
        todo!("gestalt_sound_notify");
    }

    fn gestalt_hyperlinks(&self) -> bool {
        self.delegator.gestalt_hyperlinks()
    }

    fn gestalt_hyperlink_input(&self, _window_type: WindowType) -> bool {
        todo!("gestalt_hyperlink_input");
    }

    fn gestalt_sound_music(&self) -> bool {
        todo!("gestalt_sound_music");
    }

    fn gestalt_graphics_transparency(&self) -> bool {
        todo!("gestalt_graphics_transparency");
    }

    fn gestalt_line_input_echo(&self) -> bool {
        self.delegator.gestalt_line_input_echo()
    }

    fn gestalt_line_terminators(&self) -> bool {
        todo!("gestalt_line_terminators");
    }

    fn gestalt_line_terminator_key(&self, _ch: Key) -> bool {
        todo!("gestalt_line_terminator_key");
    }

    fn gestalt_sound2(&self) -> bool {
        todo!("gestalt_sound2");
    }

    fn gestalt_resource_stream(&self) -> bool {
        todo!("gestalt_resource_stream");
    }

    fn gestalt_graphics_char_input(&self) -> bool {
        todo!("gestalt_graphics_char_input");
    }

    // 0x0020
    fn window_iterate(&self, id: Option<OpaqueId>) -> Option<(OpaqueId, u32)> {
        self.windows.iterate(id)
    }

    // 0x0021
    fn window_get_rock(&self, _win: OpaqueId) -> u32 {
        todo!("window_get_rock");
    }

    // 0x0022
    fn window_get_root(&self) -> Option<OpaqueId> {
        todo!("window_get_root");
    }

    // 0x0023
    fn window_open(
        &mut self,
        split: Option<OpaqueId>,
        _method: WindowMethod,
        _size: u32,
        ty: WindowType,
        rock: u32,
    ) -> Option<OpaqueId> {
        let win_id = self.get_opaque_id();
        let parent_id = match split {
            None => {
                if self.root_window.is_some() {
                    self.warn("can't open a new window without splitting an existing one".into());
                    return None;
                }
                self.root_window = Some(win_id);
                None
            }
            Some(sibling_id) => {
                // For the new parent, a pair window.
                let parent_id = self.get_opaque_id();

                let Some(sibling) = self.windows.get_mut(&sibling_id) else {
                    self.warn("no split window with the given ID".into());
                    return None;
                };
                // The parent of the split becomes the grandparent of the split and the new window
                // (because of the new pair window).
                let grandparent_id = sibling.parent;
                sibling.parent = Some(parent_id);

                // And now we create the parent.
                let parent_stream = Stream::new(stream::Window {
                    owner: parent_id,
                    ty: WindowType::Pair,
                });
                let parent_stream_id = self.get_opaque_id();
                self.streams.insert(parent_stream_id, parent_stream, 0);
                let parent = Window {
                    parent: grandparent_id,
                    stream: parent_stream_id,
                    stylehints: self.stylehints.get_stylehints(WindowType::Pair),
                };
                self.windows.insert(parent_id, parent, 0);

                Some(parent_id)
            }
        };

        let stream_id = self.get_opaque_id();
        let stream = Stream::new(stream::Window { owner: win_id, ty });
        self.streams.insert(stream_id, stream, 0);

        let window = Window {
            parent: parent_id,
            stream: stream_id,
            stylehints: self.stylehints.get_stylehints(ty),
        };

        self.windows.insert(win_id, window, rock);

        self.updates.push(Update::WindowOpen {
            id: win_id,
            parent: parent_id,
            styles: update::Styles::from_array(&self.stylehints.get_stylehints(ty)),
        });

        Some(win_id)
    }

    // 0x0024
    fn window_close(&mut self, _win: OpaqueId) -> StreamResult {
        todo!("window_close")
    }

    // 0x0025
    fn window_get_size(&self, _win: OpaqueId) -> (u32, u32) {
        todo!("window_get_size");
    }

    // 0x0026
    fn window_set_arrangement(
        &mut self,
        _win: OpaqueId,
        _method: WindowMethod,
        _size: u32,
        _key: Option<OpaqueId>,
    ) {
        todo!("window_set_arrangement");
    }

    // 0x0027
    fn window_get_arrangement(&self, _win: OpaqueId) -> (WindowMethod, u32, Option<OpaqueId>) {
        todo!("window_get_arrangement")
    }

    // 0x0028
    fn window_get_type(&self, _win: OpaqueId) -> WindowType {
        todo!("window_get_type");
    }

    // 0x0029
    fn window_get_parent(&self, _win: OpaqueId) -> Option<OpaqueId> {
        todo!("window_get_parent");
    }

    // 0x002A
    fn window_clear(&mut self, _win: OpaqueId) {
        todo!("window_clear");
    }

    // 0x002B
    fn window_move_cursor(&mut self, _win: OpaqueId, _x: u32, _y: u32) {
        todo!("window_move_cursor");
    }

    // 0x002C
    fn window_get_stream(&self, win: OpaqueId) -> OpaqueId {
        let win = self
            .windows
            .get(&win)
            .expect("TODO: handle non-existent window");
        win.stream
    }

    // 0x002D
    fn window_set_echo_stream(&mut self, _win: OpaqueId, _stream: Option<OpaqueId>) {
        todo!("window_set_echo_stream");
    }

    // 0x002E
    fn window_get_echo_stream(&self, _win: OpaqueId) -> Option<OpaqueId> {
        todo!("window_get_echo_stream");
    }

    // 0x0030

    fn window_get_sibling(&self, _win: OpaqueId) -> Option<OpaqueId> {
        todo!("window_get_sibling");
    }

    // 0x0040
    fn stream_iterate(&self, id: Option<OpaqueId>) -> Option<(OpaqueId, u32)> {
        self.streams.iterate(id)
    }

    // 0x0041
    fn stream_get_rock(&self, _stream: OpaqueId) -> u32 {
        todo!("stream_get_rock");
    }

    // 0x0042
    fn stream_open_file(
        &mut self,
        _fileref: OpaqueId,
        _mode: FileMode,
        _rock: u32,
    ) -> Option<OpaqueId> {
        todo!("stream_open_file");
    }

    // 0x0043
    fn stream_open_memory(
        &mut self,
        _mem: &impl Memory,
        _start: u32,
        _len: u32,
        _mode: FileMode,
        _rock: u32,
    ) -> OpaqueId {
        todo!("stream_open_memory");
    }

    // 0x0044
    fn stream_close(&mut self, mem: &mut impl Memory, stream_id: OpaqueId) -> StreamResult {
        let (stream, rock) = self
            .streams
            .remove(&stream_id)
            .expect("TODO handle non-existent stream");
        if stream.is_window() {
            self.warn("Tried to close a window stream.".into());
            // We put back the stream into the collection.
            //  It's a bit wasteful but it avoids getting a reference to check the type then removing it,
            // which is the most common case (non-window stream).
            self.streams.insert(stream_id, stream, rock);
            return StreamResult {
                read_count: 0,
                write_count: 0,
            };
        }

        let Stream {
            read_count,
            write_count,
            ty,
        } = stream;

        if let stream::Type::Memory(mem_stream) = ty {
            if let stream::Charset::Unicode(buffer) = mem_stream.charset {
                mem.write_u32s(mem_stream.start, buffer.into_iter());
            }
            mem.release_memory(mem_stream.start, mem_stream.len);
        }

        if Some(stream_id) == self.current_stream {
            self.current_stream = None;
        }

        StreamResult {
            read_count,
            write_count,
        }
    }

    // 0x0045
    fn stream_set_position(&mut self, _stream: OpaqueId, _pos: i32, _seek_mode: SeekMode) {
        todo!("stream_set_position")
    }

    // 0x0046
    fn stream_get_position(&self, _stream: OpaqueId) -> u32 {
        todo!("stream_get_position")
    }

    // 0x0047
    fn stream_set_current(&mut self, stream: Option<OpaqueId>) {
        if let Some(id) = stream {
            if !self.streams.contains_key(&id) {
                todo!("can't set current stream to inexistent stream")
            }
        }

        self.current_stream = stream;
    }

    // 0x0048
    fn stream_get_current(&self) -> Option<crate::OpaqueId> {
        self.current_stream
    }

    // 0x0049
    fn stream_open_resource(&mut self, _file_num: u32, _rock: u32) -> Option<OpaqueId> {
        todo!("stream_open_resource");
    }

    // 0x0060
    fn fileref_create_temp(&mut self, _usage: glauque::FileUsage, _rock: u32) -> Option<OpaqueId> {
        todo!("fileref_create_temp");
    }

    // 0x0061
    fn fileref_create_by_name(
        &mut self,
        _usage: FileUsage,
        _name: &[u8],
        _rock: u32,
    ) -> Option<OpaqueId> {
        todo!("fileref_create_by_name")
    }

    // 0x0062
    fn fileref_create_by_prompt(
        &mut self,
        _usage: FileUsage,
        _mode: FileMode,
        _rock: u32,
    ) -> Option<OpaqueId> {
        todo!("fileref_create_by_prompt")
    }

    // 0x0063
    fn fileref_destroy(&mut self, _fileref: OpaqueId) {
        todo!("fileref_destroy");
    }

    // 0x0064
    fn fileref_iterate(&self, id: Option<OpaqueId>) -> Option<(OpaqueId, u32)> {
        self.filerefs.iterate(id)
    }

    // 0x0065
    fn fileref_get_rock(&self, _fileref: OpaqueId) -> u32 {
        todo!("fileref_get_rock");
    }

    // 0x0066
    fn fileref_delete_file(&self, _firelef: OpaqueId) {
        todo!("fileref_delete_file");
    }

    // 0x0067
    fn fileref_does_file_exist(&self, _fileref: OpaqueId) -> bool {
        todo!("fileref_does_file_exist");
    }

    // 0x0068
    fn fileref_create_from_fileref(
        &mut self,
        _usage: FileUsage,
        _fileref: OpaqueId,
        _rock: u32,
    ) -> OpaqueId {
        todo!("fileref_create_from_fileref")
    }

    // 0x0084, 0x0085
    fn put_slice_stream(&mut self, _mem: &mut impl Memory, stream: OpaqueId, bytes: &[u8]) {
        let stream = self
            .streams
            .get_mut(&stream)
            .expect("TODO putting slice in inexistent stream");
        stream.put_slice(&mut self.updates, bytes);
    }

    // 0x0084, 0x0085
    fn put_chars_stream(
        &mut self,
        _mem: &mut impl Memory,
        _stream: OpaqueId,
        _bytes: impl IntoIterator<Item = u8>,
    ) {
        todo!("put_chars_stream");
    }

    // 0x0087
    fn set_style_stream(&mut self, stream: OpaqueId, style: Style) {
        let stream = self
            .streams
            .get(&stream)
            .expect("TODO handle non-existent stream");
        // Only window streams support styles.
        if let Some(win_stream) = stream.to_window() {
            self.updates.push(Update::SetStyle {
                window: win_stream.owner,
                style,
            });
        }
    }

    // 0x0090
    fn get_char_stream(&mut self, _mem: &impl Memory, _stream: OpaqueId) -> u8 {
        todo!("get_char_stream")
    }

    // 0x0091
    // TODO:
    fn get_line_stream(&mut self, _mem: &impl Memory, _stream: OpaqueId, _slice: &mut [u8]) -> u32 {
        todo!("get_line_stream")
    }

    // 0x0092
    fn get_slice_stream(
        &mut self,
        _mem: &impl Memory,
        _stream: OpaqueId,
        _slice: &mut [u8],
    ) -> u32 {
        todo!("get_slice_stream")
    }

    // 0x0092
    fn get_to_end_stream(
        &mut self,
        _mem: &impl Memory,
        _stream: OpaqueId,
        _buffer: &mut impl Extend<u8>,
    ) {
        todo!("get_to_end_stream");
    }

    // 0x00B0
    fn stylehint_set(&mut self, ty: WindowType, style: Style, hint: Stylehint) {
        self.stylehints.set_stylehint(ty, style, hint);
    }

    // 0x00B1
    fn stylehint_clear(&mut self, _win_type: WindowType, _style: Style, _hint: Stylehint) {
        todo!("stylehint_clear");
    }

    // 0x00B2
    fn style_distinguish(&self, _win: OpaqueId, _style1: Style, _style2: Style) -> bool {
        todo!("style_distinguish");
    }

    // 0x00B3
    fn style_measure(&self, _win: OpaqueId, _style: Style, _hint: Stylehint) -> Option<u32> {
        todo!("style_measure")
    }

    // 0x00C0
    fn select(&mut self) {
        todo!("select");
    }

    // 0x00C1
    fn select_poll(&mut self) {
        todo!("select_poll");
    }

    fn accept(&mut self, _mem: &mut impl Memory, _event: Event) -> Event {
        todo!("accept");
    }

    // 0x00D0
    fn request_line_event(
        &mut self,
        _mem: &impl Memory,
        _win: OpaqueId,
        _addr: u32,
        _max_len: u32,
        _init_len: u32,
    ) {
        todo!("request_line_event");
    }

    // 0x00D1
    fn cancel_line_event(&mut self, _win: OpaqueId) -> Event {
        todo!("cancel_line_event")
    }

    // 0x00D2
    fn request_char_event(&mut self, _win: OpaqueId) {
        todo!("request_char_event");
    }

    // 0x00D3
    fn cancel_char_event(&mut self, _win: OpaqueId) {
        todo!("cancel_char_event")
    }

    // 0x00D4
    fn request_mouse_event(&mut self, _win: OpaqueId) {
        todo!("request_mouse_event");
    }

    // 0x00D5
    fn cancel_mouse_event(&mut self, _win: OpaqueId) {
        todo!("cancel_mouse_event");
    }

    // 0x00D6
    fn request_timer_events(&mut self, _millis: u32) {
        todo!("request_timer_events");
    }

    // 0x00E0
    fn image_get_info(&self, _image: u32) -> Option<(u32, u32)> {
        todo!("image_get_info");
    }

    // 0x00E1
    fn image_draw(&mut self, _image: u32, _val1: i32, _val2: i32) -> bool {
        todo!("image_draw");
    }

    // 0x00E2
    fn image_draw_scaled(
        &mut self,
        _image: u32,
        _val1: i32,
        _val2: i32,
        _witdth: u32,
        _height: u32,
    ) -> bool {
        todo!("image_draw_scaled");
    }

    // 0x00E8
    fn window_flow_break(&mut self, _win: OpaqueId) {
        todo!("window_flow_break");
    }

    // 0x00E9
    fn window_erase_rect(&mut self, _left: i32, _top: i32, _width: u32, _height: u32) {
        todo!("window_erase_rect");
    }

    // 0x00EA
    fn window_fill_rect(
        &mut self,
        _win: OpaqueId,
        _color: Color,
        _left: i32,
        _top: i32,
        _width: u32,
        _height: u32,
    ) {
        todo!("window_fill_rect");
    }

    // 0x00EB
    fn window_set_background_color(&mut self, _win: OpaqueId, _color: Color) {
        todo!("window_set_background_color");
    }

    // 0x00F0
    fn schannel_iterate(&self, _id: Option<OpaqueId>) -> Option<(OpaqueId, u32)> {
        todo!("schannel_iterate");
    }

    // 0x00F1
    /// Returns the rock value of the channel with the given ID.
    fn schannel_get_rock(&self, _chan: OpaqueId) -> u32 {
        todo!("schannel_get_rock");
    }

    // 0x00F4
    fn schannel_create_ext(&mut self, _rock: u32, _volume: u32) -> Option<OpaqueId> {
        todo!("schannel_create_ext");
    }

    // 0x00F3
    /// Destroys the channel with the given ID.
    ///
    /// If the channel is playing a sound,
    /// the sound stops immediately (with no notification event).
    fn schannel_destroy(&mut self, _chan: OpaqueId) {
        todo!("schannel_destroy");
    }

    // 0x00F7
    fn schannel_play_multi(&mut self, _chans: &[OpaqueId], _sounds: &[u32], _notify: u32) -> u32 {
        todo!("schannel_play_multi");
    }

    // 0x00F9
    fn schannel_play_ext(
        &mut self,
        _chan: OpaqueId,
        _sound: u32,
        _repeats: u32,
        _notify: u32,
    ) -> bool {
        todo!("schannel_play_ext");
    }

    // 0x00FA
    fn schannel_stop(&mut self, _chan: OpaqueId) {
        todo!("schannel_stop");
    }

    // 0x00FC
    fn sound_load_hint(&self, _sound: u32, _flag: bool) {
        todo!("sound_load_hint");
    }

    // 0x00FD
    fn schannel_set_volume_ext(
        &mut self,
        _chan: OpaqueId,
        _volume: u32,
        _duration: u32,
        _notify: u32,
    ) {
        todo!("schannel_set_volume_ext");
    }

    // 0x00FE
    fn schannel_pause(&mut self, _chan: OpaqueId) {
        todo!("schannel_pause");
    }

    // 0x00FF
    fn schannel_unpause(&mut self, _chan: OpaqueId) {
        todo!("schannel_unpause");
    }

    // 0x0101
    fn set_hyperlink_stream(&mut self, _stream: OpaqueId, _link_val: u32) {
        todo!("set_hyperlink_stream");
    }

    // 0x0102
    fn request_hyperlink_event(&mut self, _win: OpaqueId) {
        todo!("request_hyperlink_event");
    }

    // 0x0103
    fn cancel_hyperlink_event(&mut self, _win: OpaqueId) {
        todo!("cancel_hyperlink_event");
    }

    // 0x0122
    /// Returns the string corresponding to the titlecase of the given slice taken as Unicode code points.
    fn slice_to_title_case_uni(_slice: &[u32]) -> String {
        todo!("slice_to_title_case_uni");
    }

    // 0x0123
    fn slice_canon_decompose_uni(_slice: &[u32]) -> String {
        todo!("slice_canon_decompose_uni");
    }

    // 0x0124
    fn slice_canon_normalize_uni(_slice: &[u32]) -> String {
        todo!("slice_canon_normalize_uni");
    }

    // 0x012C, 0x012D
    fn put_slice_stream_uni(&mut self, _mem: &mut impl Memory, stream: OpaqueId, chars: &[u32]) {
        let stream = self
            .streams
            .get_mut(&stream)
            .expect("TODO putting uni slice in inexistent stream");
        stream.put_slice_uni(&mut self.updates, chars);
    }

    // 0x012C, 0x012D
    fn put_chars_stream_uni(
        &mut self,
        _mem: &mut impl Memory,
        _stream: OpaqueId,
        _chars: impl IntoIterator<Item = u32>,
    ) {
        todo!("put_chars_stream_uni");
    }

    // 0x0130
    fn get_char_stream_uni(&mut self, _mem: &impl Memory, _stream: OpaqueId) -> u32 {
        todo!("get_char_stream_uni");
    }

    // 0x0131
    fn get_slice_stream_uni(
        &mut self,
        _mem: &impl Memory,
        _stream: OpaqueId,
        _slice: &mut [u32],
    ) -> u32 {
        todo!("get_slice_stream_uni");
    }

    // 0x0132
    fn get_line_stream_uni(
        &mut self,
        _mem: &impl Memory,
        _stream: OpaqueId,
        _slice: &mut [u32],
    ) -> u32 {
        todo!("get_line_stream_uni");
    }

    // 0x0138
    fn stream_open_file_uni(
        &mut self,
        _fileref: OpaqueId,
        _mode: FileMode,
        _rock: u32,
    ) -> Option<OpaqueId> {
        todo!("stream_open_file_uni");
    }

    // 0x0139
    fn stream_open_memory_uni(
        &mut self,
        mem: &impl Memory,
        start: u32,
        len: u32,
        mode: FileMode,
        rock: u32,
    ) -> OpaqueId {
        if !matches!(mode, FileMode::Read | FileMode::Write | FileMode::ReadWrite) {
            todo!("handle invalid file mode in `streap_open_memory_uni");
        }

        let id = self.get_opaque_id();
        let buffer = mem.read_u32s(start, len);
        let stream = Stream::new(stream::Memory {
            start,
            len,
            charset: stream::Charset::Unicode(buffer),
            position: 0,
        });
        self.streams.insert(id, stream, rock);

        id
    }

    // 0x013A
    fn stream_open_resource_uni(&mut self, _file_num: u32, _rock: u32) -> Option<OpaqueId> {
        todo!("stream_open_resource_uni");
    }

    // 0x0140
    /// Requests input of a Latin-1 character or special key in the window with the given ID.
    fn request_char_event_uni(&mut self, _win: OpaqueId) {
        todo!("request_char_event_uni");
    }

    // 0x0141
    fn request_line_event_uni(
        &mut self,
        _mem: &impl Memory,
        _win: OpaqueId,
        _addr: u32,
        _max_len: u32,
        _init_len: u32,
    ) {
        todo!("request_line_event_uni")
    }

    // 0x0150
    fn set_echo_line_event(&mut self, win: OpaqueId, val: bool) {
        self.delegator.set_echo_line_event(win, val)
    }

    // 0x0151
    fn set_terminators_line_event(&mut self, _win: OpaqueId, _keycodes: &[Key]) {
        todo!("set_terminators_line_event");
    }

    fn current_time() -> Timeval {
        todo!("current_time");
    }

    // 0x0161
    fn current_simple_time(_factor: u32) -> i32 {
        todo!("current_simple_time");
    }

    // 0x0168
    fn time_to_date_utc(_time: Timeval) -> Date {
        todo!("time_to_date_utc");
    }

    // 0x0169
    fn time_to_date_local(_time: Timeval) -> Date {
        todo!("time_to_date_local");
    }

    // 0x016A
    fn simple_time_to_date_utc(_time: i32, _factor: u32) -> Date {
        todo!("simple_time_to_date_utc");
    }

    // 0x016B
    fn simple_time_to_date_local(_time: i32, _factor: u32) -> Date {
        todo!("simple_time_to_date_local");
    }

    // 0x016C
    fn date_to_time_utc(_date: Date, _factor: u32) -> Option<Timeval> {
        todo!("date_to_time_utc");
    }

    // 0x016D
    fn date_to_time_local(_date: Date, _factor: u32) -> Option<Timeval> {
        todo!("date_to_time_local");
    }

    // 0x016E
    fn date_to_simple_time_utc(_date: Date, _factor: u32) -> Option<i32> {
        todo!("date_to_simple_time_utc");
    }

    // 0x016F
    fn date_to_simple_time_local(_date: Date, _factor: u32) -> Option<i32> {
        todo!("date_to_simple_time_local");
    }
}
