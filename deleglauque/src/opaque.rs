use std::collections::HashMap;

use glauque::OpaqueId;

struct OpaqueCollectionEntry<T> {
    content: T,
    rock: u32,
    next: Option<OpaqueId>,
    // TODO: Keeping track of the previous is not strictly necessary
    // but it make it easier to remove entries.
    // We could not track the previous entry and walk the list instead.
    // Determine what's best.
    prev: Option<OpaqueId>,
}

pub(crate) struct OpaqueCollection<T> {
    map: HashMap<OpaqueId, OpaqueCollectionEntry<T>>,
    head: Option<OpaqueId>,
}

impl<T> OpaqueCollection<T> {
    // TODO: Add a `with_capacity` function
    // if we can determine capacities that suit average stories.

    pub(crate) fn new() -> Self {
        Self {
            map: HashMap::new(),
            head: None,
        }
    }

    pub(crate) fn contains_key(&self, id: &OpaqueId) -> bool {
        self.map.contains_key(id)
    }

    pub(crate) fn get(&self, id: &OpaqueId) -> Option<&T> {
        self.map.get(id).map(|entry| &entry.content)
    }

    pub(crate) fn get_mut(&mut self, id: &OpaqueId) -> Option<&mut T> {
        self.map.get_mut(id).map(|entry| &mut entry.content)
    }

    fn get_rock(&self, id: &OpaqueId) -> Option<u32> {
        self.map.get(&id).map(|entry| entry.rock)
    }

    pub(crate) fn insert(&mut self, id: OpaqueId, obj: T, rock: u32) {
        if let Some(head_id) = self.head {
            // The key `first_id` will be present in the collection
            // if we maintained it correctly, so we can unwrap.
            let head = self.map.get_mut(&head_id).unwrap();
            head.prev = Some(id);
        }

        let entry = OpaqueCollectionEntry {
            content: obj,
            rock,
            next: self.head,
            prev: None,
        };

        self.head = Some(id);
        self.map.insert(id, entry);
    }

    pub(crate) fn remove(&mut self, removed_id: &OpaqueId) -> Option<(T, u32)> {
        // Remove the element from the hashmap.
        let removed = self.map.remove(&removed_id)?;

        // Patch the linked list.
        let next_id = removed.next;
        let prev_id = removed.prev;
        // If there's a next/previous ID,
        // then the next/previous object must be in the collection,
        // since we normally maintain the linked list correctly.
        if let Some(next_id) = next_id {
            self.map.get_mut(&next_id).unwrap().prev = prev_id;
        }

        if let Some(prev_id) = prev_id {
            self.map.get_mut(&prev_id).unwrap().next = next_id;
        } else {
            self.head = next_id;
        }

        Some((removed.content, removed.rock))
    }

    pub(crate) fn iterate(&self, id: Option<OpaqueId>) -> Option<(OpaqueId, u32)> {
        let next_id = match id {
            None => self.head?,
            // TODO: We just return None if the collection doesn't contain the key.
            // It make sense because a non-existant object has nothing following.
            // But maybe make it an error or something?
            Some(id) => self.map.get(&id)?.next?,
        };
        // If we maintained the collection correctly,
        // `next_id` will be present so we can unwrap.
        let next_rock = self.map.get(&next_id).unwrap().rock;
        Some((next_id, next_rock))
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use glauque::OpaqueIdFactory;

    #[test]
    fn iterate() {
        let mut fac = OpaqueIdFactory::new();
        let id0 = fac.get().unwrap();
        let id1 = fac.get().unwrap();
        let id2 = fac.get().unwrap();

        let mut collection = OpaqueCollection::new();

        assert!(collection.iterate(None).is_none());

        collection.insert(id0, 0, 10);
        collection.insert(id1, 1, 11);
        collection.insert(id2, 2, 12);

        let mut vals = Vec::new();

        let mut current = None;
        while let Some((id, rock)) = collection.iterate(current) {
            vals.push((collection.get(&id).copied().unwrap(), rock));
            current = Some(id);
        }

        assert_eq!(vals.len(), 3);
        assert!(vals.contains(&(0, 10)));
        assert!(vals.contains(&(1, 11)));
        assert!(vals.contains(&(2, 12)));
    }
}
