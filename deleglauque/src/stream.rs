use glauque::{OpaqueId, WindowType};

use crate::update::Update;

pub(crate) struct Stream {
    pub(crate) read_count: u32,
    pub(crate) write_count: u32,
    pub(crate) ty: Type,
}

impl Stream {
    pub(crate) fn new(ty: impl Into<Type>) -> Self {
        Self {
            read_count: 0,
            write_count: 0,
            ty: ty.into(),
        }
    }

    pub(crate) fn to_window(&self) -> Option<&Window> {
        if let Type::Window(win) = &self.ty {
            Some(win)
        } else {
            None
        }
    }

    pub(crate) fn is_window(&self) -> bool {
        matches!(self.ty, Type::Window(_))
    }

    pub(crate) fn put_slice(&mut self, updates: &mut Vec<Update>, bytes: &[u8]) {
        self.write_count += u32::try_from(bytes.len()).expect("TODO");
        match &mut self.ty {
            Type::Window(window) => window.put_slice(updates, bytes),
            Type::Memory(memory) => memory.put_slice(bytes),
            Type::File(_file) => todo!("put chars file"),
        }
    }

    pub(crate) fn put_slice_uni(&mut self, updates: &mut Vec<Update>, chars: &[u32]) {
        self.write_count += u32::try_from(chars.len()).expect("TODO");
        match &mut self.ty {
            Type::Window(window) => window.put_slice_uni(updates, chars),
            Type::Memory(memory) => memory.put_slice_uni(chars),
            Type::File(_file) => todo!("put chars file"),
        }
    }
}

pub(crate) enum Type {
    Window(Window),
    Memory(Memory),
    File(File),
}

impl From<Window> for Type {
    fn from(value: Window) -> Self {
        Self::Window(value)
    }
}

impl From<Memory> for Type {
    fn from(value: Memory) -> Self {
        Self::Memory(value)
    }
}

impl From<File> for Type {
    fn from(value: File) -> Self {
        Self::File(value)
    }
}

pub(crate) struct Window {
    /// The ID of the window that owns this window stream.
    pub(crate) owner: OpaqueId,
    /// The type of the window that owns this window stream.
    pub(crate) ty: WindowType,
}

impl Window {
    fn put_slice(&self, updates: &mut Vec<Update>, bytes: &[u8]) {
        match self.ty {
            WindowType::AllTypes => unreachable!("A window can't be be `AllTypes`"),
            WindowType::Pair => {
                // Printing into a pair window does nothing.
            }
            WindowType::Blank => {
                // Printing into a blank window does nothing.
            }
            WindowType::TextBuffer => {
                let text = bytes.iter().map(|&byte| byte as char).collect();
                updates.push(Update::WindowBufferPrint {
                    window: self.owner,
                    text,
                })
            }
            WindowType::TextGrid => todo!("putting chars to text grid window"),
            WindowType::Graphics => {
                // Printing into a blank window does nothing.
            }
        }
    }

    fn put_slice_uni(&self, updates: &mut Vec<Update>, chars: &[u32]) {
        match self.ty {
            WindowType::AllTypes => unreachable!("A window can't be be `AllTypes`"),
            WindowType::Pair => {
                // Printing into a pair window does nothing.
            }
            WindowType::Blank => {
                // Printing into a blank window does nothing.
            }
            WindowType::TextBuffer => {
                let text = chars
                    .iter()
                    .map(|&ch| char::from_u32(ch).unwrap_or(char::REPLACEMENT_CHARACTER))
                    .collect();
                updates.push(Update::WindowBufferPrint {
                    window: self.owner,
                    text,
                })
            }
            WindowType::TextGrid => todo!("putting chars to text grid window"),
            WindowType::Graphics => {
                // Printing into a blank window does nothing.
            }
        }
    }
}

pub(crate) struct Memory {
    pub(crate) start: u32,
    pub(crate) len: u32,
    pub(crate) charset: Charset,
    pub(crate) position: u32,
}

impl Memory {
    fn put_slice(&mut self, bytes: &[u8]) {
        let len = usize::try_from(self.len).expect("can't convert u32 to usize");
        let pos = usize::try_from(self.position).expect("can't convert u32 to usize");
        match &mut self.charset {
            Charset::Latin1 => todo!(),
            Charset::Unicode(buffer) => {
                for (dest, &byte) in buffer[pos..].iter_mut().zip(bytes) {
                    *dest = byte as u32;
                }
                debug_assert_eq!(buffer.len(), len);
            }
        }
        self.position = (self.position
            + u32::try_from(bytes.len()).expect("can't convert usize to u32"))
        .min(self.len)
    }

    fn put_slice_uni(&mut self, chars: &[u32]) {
        let len = usize::try_from(self.len).expect("can't convert u32 to usize");
        let pos = usize::try_from(self.position).expect("can't convert u32 to usize");
        match &mut self.charset {
            Charset::Latin1 => todo!(),
            Charset::Unicode(buffer) => {
                for (dest, &char) in buffer[pos..].iter_mut().zip(chars) {
                    *dest = char;
                }
                debug_assert_eq!(buffer.len(), len);
            }
        }
        self.position = (self.position
            + u32::try_from(chars.len()).expect("can't convert usize to u32"))
        .min(self.len)
    }
}

pub(crate) enum Charset {
    Latin1,
    // Since a Glulx memory is a sequence of bytes
    // we'd have to count the write index four by four,
    // which breaks the abstraction (the memory could be represented in another way),
    // so instead we write all the u32s into a local vec
    // and write all the u32s at once when closing the stream
    // (so the memory will handle the the indexing).
    Unicode(Vec<u32>),
}

pub(crate) struct File {}
