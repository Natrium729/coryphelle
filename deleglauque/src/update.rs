use glauque::{OpaqueId, Style};

use crate::stylehints::Stylehints;

pub enum Update {
    WindowOpen {
        id: OpaqueId,
        parent: Option<OpaqueId>,
        styles: Styles,
    },
    WindowBufferPrint {
        window: OpaqueId,
        text: String,
    },
    SetStyle {
        window: OpaqueId,
        style: Style,
    },
}

pub struct Styles {
    pub normal: Stylehints,
    pub emphasized: Stylehints,
    pub preformatted: Stylehints,
    pub header: Stylehints,
    pub subheader: Stylehints,
    pub alert: Stylehints,
    pub note: Stylehints,
    pub block_quote: Stylehints,
    pub input: Stylehints,
    pub user1: Stylehints,
    pub user2: Stylehints,
}

impl Styles {
    pub(crate) fn from_array(arr: &[Stylehints; 11]) -> Self {
        Self {
            normal: arr[0].clone(),
            emphasized: arr[1].clone(),
            preformatted: arr[2].clone(),
            header: arr[3].clone(),
            subheader: arr[4].clone(),
            alert: arr[5].clone(),
            note: arr[6].clone(),
            block_quote: arr[7].clone(),
            input: arr[8].clone(),
            user1: arr[9].clone(),
            user2: arr[10].clone(),
        }
    }
}
