use glauque::{Color, FontWeight, Style, Stylehint, TextAlignement, WindowType};

#[derive(Debug, Clone)]
pub struct Stylehints {
    indentation: Option<i32>,
    para_indentation: Option<i32>,
    justification: Option<TextAlignement>,
    size: Option<i32>,
    weight: Option<FontWeight>,
    oblique: Option<bool>,
    proportional: Option<bool>,
    text_color: Option<Color>,
    back_color: Option<Color>,
    reverse_color: Option<bool>,
}

impl Stylehints {
    fn new() -> Self {
        Self {
            indentation: None,
            para_indentation: None,
            justification: None,
            size: None,
            weight: None,
            oblique: None,
            proportional: None,
            text_color: None,
            back_color: None,
            reverse_color: None,
        }
    }

    fn set_stylehint(&mut self, hint: Stylehint) {
        match hint {
            Stylehint::Indentation(indent) => self.indentation = Some(indent),
            Stylehint::ParaIndentation(par_indent) => self.para_indentation = Some(par_indent),
            Stylehint::Justification(alignement) => self.justification = Some(alignement),
            Stylehint::Size(size) => self.size = Some(size),
            Stylehint::Weight(weight) => self.weight = Some(weight),
            Stylehint::Oblique(oblique) => self.oblique = Some(oblique),
            Stylehint::Proportional(proportional) => self.proportional = Some(proportional),
            Stylehint::TextColor(color) => self.text_color = Some(color),
            Stylehint::BackColor(color) => self.back_color = Some(color),
            Stylehint::ReverseColor(reverse) => self.reverse_color = Some(reverse),
        }
    }
}

impl Default for Stylehints {
    fn default() -> Self {
        Self::new()
    }
}

pub(crate) struct WindowHints(
    // 11 styles for each of the 5 type of window.
    [[Stylehints; 11]; 5],
);

impl WindowHints {
    pub(crate) fn new() -> Self {
        Self(Default::default())
    }

    pub(crate) fn set_stylehint(&mut self, win_type: WindowType, style: Style, hint: Stylehint) {
        // Minus 1 because the first enum member is `AllTypes`,
        // which isn't a real window type.
        let i_win = usize::try_from(win_type as u32).expect("unable to convert u32 to usize") - 1;
        let i_style = usize::try_from(style as u32).expect("unable to convert u32 to usize");

        self.0[i_win][i_style].set_stylehint(hint);
    }

    pub(crate) fn get_stylehints(&self, win_type: WindowType) -> [Stylehints; 11] {
        let i_win = usize::try_from(win_type as u32).expect("unable to convert u32 to usize") - 1;
        self.0[i_win].clone()
    }
}
