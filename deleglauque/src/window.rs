use glauque::OpaqueId;

use crate::stylehints::Stylehints;

pub(crate) struct Window {
    pub(crate) parent: Option<OpaqueId>,
    pub(crate) stream: OpaqueId,
    pub(crate) stylehints: [Stylehints; 11],
}
